* Set up the serial port: serinit(speed)
serinit	MOV	R0,#-5		Point to first parm
	LCALL	?auto0		Point to it
	MOV	?R3,[R0]	Get LOW value
	INC	R0		Advance
	MOV	?R4,[R0]	Get HIGH value
* Modify this constant for correct operation at different CPU clocks
* Value is calculated as the CPU CRYSTAL (in Hz) divided by 384.
*	MOV	A,#28800	Get LOW conversion factor
*	MOV	B,#=28800	Get HIGH conversion factor
	MOV	A,#19200	Get LOW conversion factor
	MOV	B,#=19200	Get HIGH conversion factor

	LCALL	?div		Calculate timer value
	LCALL	?neg		Convert to count-up value
	MOV	TMOD,#%00100000	T1=8 bit auto-reload
	MOV	TH1,A		Timer 1 reload value
	MOV	TL1,A		Timer 1 initial value
	MOV	TCON,#%01001001	Run 1, Hold 0
	MOV	SCON,#%01010010	Mode 1, REN, TXRDY, RXEMPTY
	RET
* S/N: 6407

*
* Low level I/O functions to communicate with the console terminal
*
* Write a string to the console: putstr(char *string)
putstr	MOV	R0,#-5		Offset to parameter
	LCALL	?auto0		Set up address
	MOV	DPL,[R0]	Get low byte
	INC	R0		Step to next
	MOV	DPH,[R0]	Get high byte
?putstr	CLR	A		Zero OFFSET
	MOVC	A,[A+DPTR]	Get char
	JZ	?2		End of string, return
	LCALL	?putch		Output (with NEWLINE translation)
	INC	DPTR		Advance to next char
	SJMP	?putstr		And continue
* Write a character to the console: putch(char c)
putch	MOV	R0,#-5		Offset to parameter
	LCALL	?auto0		Set up address
	MOV	A,[R0]		Get char to write
?putch	JNB	SCON.1,*	Wait for the bit
	CLR	SCON.1		Indicte we are sending
	MOV	SBUF,A		Write out char
	CJNE	A,#$0A,?2	Not NEWLINE
	MOV	A,#$0D		Get CR
	SJMP	?putch		And go again
* Check for a character from the console: int chkch()
chkch	CLR	A		Assume zero
	JNB	SCON.0,?1	No data ready
* Get a character from the console: int getch()
getch	JNB	SCON.0,*	Wait for the bit
	CLR	SCON.0		Indicate we receved it
	MOV	A,SBUF		Read the data
	CJNE	A,#$0D,?1	Not CR, its OK
	MOV	A,#$0A		Convert to NEWLINE
?1	MOV	B,#0		Zero high byte
?2	RET
* S/N: 6407

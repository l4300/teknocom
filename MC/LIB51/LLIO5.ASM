*
* Low level "raw" I/O functions to communicate with the console terminal
*
* Write a character to the console: putchr(char c)
putchr	MOV	R0,#-5		Offset to parameter
	LCALL	?auto0		Set up address
	MOV	A,[R0]		Get char to write
	JNB	SCON.1,*	Wait for the bit
	CLR	SCON.1		Indicte we are sending
	MOV	SBUF,A		Write out char
	RET
* Check for a character from the console: int chkchr()
chkchr	MOV	A,#-1		Get -1
	MOV	B,A		Copy for 16 bit value
	JNB	SCON.0,?1	No data ready
* Get a character from the console: int getchr()
getchr	JNB	SCON.0,*	Wait for the bit
	CLR	SCON.0		Indicate we receved it
	MOV	A,SBUF		Read the data
	MOV	B,#0		Zero high byte
?1	RET
* S/N: 6407

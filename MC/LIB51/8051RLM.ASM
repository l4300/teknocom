*
* Middle file, define the stack (at the end of segment 2), and position the
* address counter at the beginning of RAM for the following data definitions.
*
?stk	EQU	*		Stack goes at top of user variables
	ORG	?RAM		External uninitialized data goes here!!!
* S/N: 6407

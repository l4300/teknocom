object Form: TMenuLoader
Menu = VBmainmenu
  Color = $00C0C0C0
  Caption = 'Form1'
  Height = 446
  Left = 9
  Top = 261
  Width = 582

 object SSDir1: TDirectoryListBox
 Height = 110
 Left = 304
 TabOrder = 4
 Top = 56
 Width = 257
 OnChange = SSDir1_Change
 end

 object SSDrive1: TDriveComboBox
 Height = 23
 Left = 304
 TabOrder = 3
 Top = 216
 Width = 257
 OnChange = SSDrive1_Change
 end

 object SSFile1: TFileListBox
 Height = 104
 Left = 0
 Mask = '*.asm'
 TabOrder = 2
 Top = 48
 Width = 257
 OnClick = SSFile1_Click
 end

 object LoadButton: TButton
 Caption = 'Load'
 Height = 24
 Left = 224
 TabOrder = 1
 Top = 280
 Width = 72
 OnClick = LoadButton_Click
 end

 object bar: TGroupBox
 Caption = 'bar'
 Height = 32
 Left = 0
 TabOrder = 0
 Top = 216
 Visible = false
 Width = 256
 end

 object Label2: TLabel
 Height = 25
 Left = 296
 Top = 184
 Width = 257
 end

 object Label1: TLabel
 Height = 25
 Left = 0
 Top = 24
 Width = 257
 end

 object status: TLabel
 Caption = 'Status'
 Height = 33
 Left = 0
 Top = 168
 Width = 257
 end
end

object Form: TTEMPLATE
Menu = VBmainmenu
  Color = $00C0C0C0
  Caption = 'Template'
  Height = 510
  Left = 100
  Top = 70
  Width = 597
  OnCreate = Form_Load

 object CMDialog1: TCMDialog
 Left = 0
 Top = 0
 end

   object Frame1: TPanel
   Color = $00808000
   Caption = '         Data or Q&&A        '
   Height = 49
   Left = 160
   TabOrder = 12
   Top = 88
   Width = 161

  object SSOption2: TRadioButton
  Caption = 'Data'
  Height = 25
  Left = 8
  TabOrder = 15
  Top = 16
  Width = 49
  OnClick = SSOption2_Click
  end

  object SSOption1: TRadioButton
  Caption = 'Q&&A'
  Height = 25
  Left = 96
  TabOrder = 16
  Top = 16
  Width = 49
  OnClick = SSOption1_Click
  end
 end

 object Grid: TGrid
 Color = $00FFFFFF
 Height = 281
 Left = 8
 TabOrder = 21
 Top = 152
 Width = 569
 OnClick = Grid_DblClick
 end

 object Grid1: TGrid
 Height = 177
 Left = 8
 TabOrder = 20
 Top = 152
 Visible = false
 Width = 569
 OnClick = Grid1_DblClick
 OnKeyPress = Grid1_KeyPress
 end

 object TemplateList: TListBox
 Color = $00C0C0C0
 Height = 87
 Left = 0
 TabOrder = 19
 Top = 0
 Width = 161
 end

 object Gauge: TGroupBox
 Caption = 'SSPanel'
 Height = 25
 Left = 8
 TabOrder = 9
 Top = 96
 Width = 137
 end

 object Command3: TButton
 Caption = 'St&ore Template'
 Height = 25
 Left = 168
 TabOrder = 8
 Top = 56
 Width = 97
 OnClick = Command3_Click
 end

 object Command2: TButton
 Caption = '&Load  Template'
 Height = 25
 Left = 168
 TabOrder = 7
 Top = 32
 Width = 97
 OnClick = Command2_Click
 end

 object Command1: TButton
 Caption = 'Scan &Templates'
 Height = 25
 Left = 168
 TabOrder = 6
 Top = 8
 Width = 97
 OnClick = Command1_Click
 end

 object TemplateName: TEdit
 Height = 19
 Left = 392
 TabOrder = 5
 Top = 64
 Width = 97
 OnChange = TemplateName_Change
 OnKeyPress = TemplateName_KeyPress
 OnExit = TemplateName_LostFocus
 end

 object FHighLimit: TEdit
 Height = 19
 Left = 504
 TabOrder = 4
 Top = 24
 Width = 65
 end

 object FLowLimit: TEdit
 Height = 19
 Left = 504
 TabOrder = 3
 Top = 64
 Width = 65
 end

 object FCamLimit: TEdit
 Height = 19
 Left = 392
 TabOrder = 2
 Top = 24
 Width = 97
 end

 object FColumns: TEdit
 Height = 19
 Left = 320
 TabOrder = 1
 Top = 64
 Width = 57
 OnChange = FColumns_Change
 end

 object FRows: TEdit
 Height = 19
 Left = 320
 TabOrder = 0
 Top = 24
 Width = 57
 OnChange = FRows_Change
 OnKeyPress = FRows_KeyPress
 end

 object Label9: TLabel
 Color = $00C0C000
 Caption = 'Template Name'
 Height = 17
 Left = 392
 Top = 48
 Width = 97
 end

 object Label8: TLabel
 Color = $00C0C000
 Caption = 'High Limit'
 Height = 17
 Left = 504
 Top = 8
 Width = 65
 end

 object Label5: TLabel
 Color = $00C0C000
 Caption = 'Low Limit'
 Height = 17
 Left = 504
 Top = 48
 Width = 65
 end

 object Label4: TLabel
 Color = $00C0C000
 Caption = 'CAM Count'
 Height = 17
 Left = 392
 Top = 8
 Width = 97
 end

 object Label2: TLabel
 Color = $00C0C000
 Caption = 'Columns'
 Height = 17
 Left = 320
 Top = 48
 Width = 57
 end

 object Label1: TLabel
 Color = $00C0C000
 Caption = 'Rows'
 Height = 17
 Left = 320
 Top = 8
 Width = 57
 end

   object VBmainmenu: TMainMenu
   object mnuFile: TMenuItem
   Caption = '&File'

  object mnuNew: TMenuItem
  Caption = '&New'
  OnClick = mnuNew_Click
  end

  object mnuLoadFileTemp: TMenuItem
  Caption = '&OpenTemplate '
  OnClick = mnuLoadFileTemp_Click
  end

  object mnuSaveTemplate: TMenuItem
  Caption = '&Save Template '
  OnClick = mnuSaveTemplate_Click
  end

  object mnusep: TMenuItem
  Caption = '-'
  end

  object mnuClose: TMenuItem
  Caption = '&Close'
  OnClick = mnuClose_Click
  end
 end

   object mnuEdit: TMenuItem
   Caption = '&Edit'

  object mnuCopy: TMenuItem
  Caption = '&Copy'
  OnClick = mnuCopy_Click
  end

  object mnuReset: TMenuItem
  Caption = '&Reset Column and Row Headers'
  OnClick = mnuReset_Click
  end

  object mnuResetQA: TMenuItem
  Caption = 'Reset &Questions and Answers'
  OnClick = mnuResetQA_Click
  end
 end

   object mnuWindow: TMenuItem
   Caption = '&Window'

  object mnuCascade: TMenuItem
  Caption = '&Cascade'
  OnClick = mnuCascade_Click
  end

  object mnuTile: TMenuItem
  Caption = '&Tile'
  OnClick = mnuTile_Click
  end
 end

   object mnuSettings: TMenuItem
   Caption = '&Settings'

  object mnuShowAvg: TMenuItem
  Caption = 'Show &Avg'
  OnClick = mnuShowAvg_Click
  end
 end
end
end

VERSION 2.00
Begin Form comm 
   Caption         =   "comm"
   ClientHeight    =   420
   ClientLeft      =   4200
   ClientTop       =   3930
   ClientWidth     =   2280
   Height          =   1005
   Left            =   4110
   LinkTopic       =   "Form1"
   ScaleHeight     =   420
   ScaleWidth      =   2280
   Top             =   3435
   Visible         =   0   'False
   Width           =   2460
   Begin PDQComm Comm 
      AutoSize        =   0   'False
      AutoZModem      =   0   'False
      BackColor       =   1
      Columns         =   80
      Echo            =   0   'False
      FastScroll      =   -1  'True
      ForeColor       =   15
      Height          =   840
      InBufferSize    =   8192
      Interval        =   55
      KeyTranslation  =   0  'None
      Left            =   1200
      Notification    =   0  'Manual
      OutBufferSize   =   8192
      Rows            =   25
      RTSEnable       =   -1  'True
      ScrollRows      =   0
      SmoothScroll    =   0   'False
      Top             =   0
      Width           =   840
      XferCarrierAbort=   0   'False
   End
   Begin Timer Timer1 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   600
      Top             =   0
   End
End
Option Explicit

Sub Timer1_Timer ()
    ticks = ticks + 1
End Sub


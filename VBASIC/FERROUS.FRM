VERSION 2.00
Begin Form Ferrous 
   BackColor       =   &H00C0C0C0&
   Caption         =   "Ferrous Calculations Test"
   ClientHeight    =   6465
   ClientLeft      =   1155
   ClientTop       =   1530
   ClientWidth     =   9465
   Height          =   6870
   Left            =   1095
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6465
   ScaleWidth      =   9465
   Top             =   1185
   Width           =   9585
   WindowState     =   2  'Maximized
   Begin TextBox fStableReading 
      BackColor       =   &H0080FFFF&
      Height          =   372
      Left            =   6120
      TabIndex        =   85
      Text            =   "15"
      Top             =   5160
      Width           =   972
   End
   Begin TextBox nfInfRead 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0080FFFF&
      ForeColor       =   &H00000000&
      Height          =   372
      Left            =   120
      TabIndex        =   81
      Text            =   " "
      Top             =   5160
      Width           =   972
   End
   Begin TextBox nfInfMax 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0080FFFF&
      ForeColor       =   &H00000000&
      Height          =   372
      Left            =   1320
      TabIndex        =   80
      Text            =   " "
      Top             =   5160
      Width           =   972
   End
   Begin TextBox probedeltafTemp 
      BackColor       =   &H00FFFF00&
      Height          =   360
      Left            =   8160
      TabIndex        =   78
      Text            =   " "
      Top             =   3000
      Width           =   1092
   End
   Begin TextBox ZeroOffset 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0080FFFF&
      ForeColor       =   &H00000000&
      Height          =   372
      Left            =   3720
      TabIndex        =   76
      Text            =   "500"
      Top             =   5160
      Width           =   972
   End
   Begin TextBox checkProbe 
      BackColor       =   &H00FFFF00&
      Height          =   360
      Left            =   8160
      TabIndex        =   75
      Text            =   " "
      Top             =   3960
      Width           =   1092
   End
   Begin TextBox nInfRead 
      Height          =   372
      Left            =   5400
      TabIndex        =   72
      Text            =   ".011"
      Top             =   3600
      Width           =   972
   End
   Begin TextBox nInfMax 
      Height          =   372
      Left            =   4080
      TabIndex        =   71
      Text            =   "1.2"
      Top             =   3600
      Width           =   972
   End
   Begin CommandButton Command4 
      Caption         =   "Load"
      Height          =   372
      Left            =   8280
      TabIndex        =   70
      Top             =   5400
      Width           =   972
   End
   Begin CommandButton Command3 
      Caption         =   "Store"
      Height          =   372
      Left            =   8280
      TabIndex        =   69
      Top             =   5880
      Width           =   972
   End
   Begin TextBox fxLambda 
      Height          =   360
      Left            =   8280
      TabIndex        =   63
      Top             =   1800
      Width           =   1212
   End
   Begin TextBox fXKappa 
      Height          =   360
      Left            =   5160
      TabIndex        =   62
      Top             =   2640
      Width           =   1212
   End
   Begin TextBox fCofBeta 
      Height          =   360
      Left            =   5160
      TabIndex        =   61
      Top             =   1800
      Width           =   1212
   End
   Begin TextBox fXBeta 
      Height          =   360
      Left            =   5160
      TabIndex        =   60
      Top             =   960
      Width           =   1212
   End
   Begin TextBox fX 
      Height          =   360
      Left            =   5160
      TabIndex        =   59
      Top             =   240
      Width           =   1212
   End
   Begin TextBox fActualAlpha 
      Height          =   360
      Left            =   8280
      TabIndex        =   55
      Top             =   960
      Width           =   1092
   End
   Begin TextBox fZeroAlpha 
      Height          =   360
      Left            =   8280
      TabIndex        =   54
      Top             =   240
      Width           =   1092
   End
   Begin TextBox fInternalAlpha 
      Height          =   360
      Left            =   6840
      TabIndex        =   53
      Top             =   960
      Width           =   1092
   End
   Begin TextBox deltafTemp 
      Height          =   360
      Left            =   6840
      TabIndex        =   51
      Text            =   " "
      Top             =   1800
      Width           =   1092
   End
   Begin TextBox deltaiTempRef 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0080FFFF&
      ForeColor       =   &H00000000&
      Height          =   372
      Left            =   6000
      TabIndex        =   46
      Text            =   " "
      Top             =   4440
      Width           =   1092
   End
   Begin TextBox deltaiActualRefInf 
      BackColor       =   &H00FFFFFF&
      FontBold        =   0   'False
      FontItalic      =   0   'False
      FontName        =   "MS Sans Serif"
      FontSize        =   8.25
      FontStrikethru  =   0   'False
      FontUnderline   =   0   'False
      ForeColor       =   &H00000000&
      Height          =   360
      Left            =   6840
      TabIndex        =   45
      Text            =   " "
      Top             =   240
      Width           =   1092
   End
   Begin TextBox deltaiActualRef0 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0080FFFF&
      ForeColor       =   &H00000000&
      Height          =   372
      Left            =   3120
      TabIndex        =   44
      Text            =   " "
      Top             =   4440
      Width           =   972
   End
   Begin TextBox deltafActualRef 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0080FFFF&
      ForeColor       =   &H00000000&
      Height          =   372
      Left            =   4560
      TabIndex        =   43
      Text            =   " "
      Top             =   4440
      Width           =   972
   End
   Begin TextBox iInternali 
      Height          =   360
      Left            =   3720
      TabIndex        =   38
      Text            =   " "
      Top             =   2640
      Width           =   1092
   End
   Begin TextBox iTempi 
      Height          =   360
      Left            =   3720
      TabIndex        =   37
      Text            =   " "
      Top             =   1800
      Width           =   1092
   End
   Begin TextBox iActuali 
      Height          =   360
      Left            =   3720
      TabIndex        =   36
      Text            =   " "
      Top             =   960
      Width           =   1092
   End
   Begin TextBox iZeroi 
      Height          =   360
      Left            =   3720
      TabIndex        =   35
      Top             =   240
      Width           =   1092
   End
   Begin CommandButton Command2 
      Caption         =   "Copy Zero"
      Height          =   372
      Left            =   8280
      TabIndex        =   34
      Top             =   4920
      Width           =   972
   End
   Begin CommandButton Command1 
      Caption         =   "Copy Inf"
      Height          =   372
      Left            =   8280
      TabIndex        =   33
      Top             =   4440
      Width           =   972
   End
   Begin TextBox ConsF 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0080FFFF&
      ForeColor       =   &H00000000&
      Height          =   372
      Left            =   6120
      TabIndex        =   26
      Text            =   "-1.066"
      Top             =   5880
      Width           =   972
   End
   Begin TextBox ConsE 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0080FFFF&
      ForeColor       =   &H00000000&
      Height          =   372
      Left            =   4920
      TabIndex        =   25
      Text            =   "6.169"
      Top             =   5880
      Width           =   972
   End
   Begin TextBox ConsD 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0080FFFF&
      ForeColor       =   &H00000000&
      Height          =   372
      Left            =   3720
      TabIndex        =   24
      Text            =   "2.190"
      Top             =   5880
      Width           =   972
   End
   Begin TextBox ConsC 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0080FFFF&
      ForeColor       =   &H00000000&
      Height          =   372
      Left            =   2520
      TabIndex        =   23
      Text            =   "2.507"
      Top             =   5880
      Width           =   972
   End
   Begin TextBox ConsB 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0080FFFF&
      ForeColor       =   &H00000000&
      Height          =   372
      Left            =   1320
      TabIndex        =   22
      Text            =   "-2.058"
      Top             =   5880
      Width           =   972
   End
   Begin TextBox ConsA 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0080FFFF&
      ForeColor       =   &H00000000&
      Height          =   372
      Left            =   120
      TabIndex        =   21
      Text            =   "0"
      Top             =   5880
      Width           =   972
   End
   Begin TextBox fMnRef 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0080FFFF&
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   2520
      TabIndex        =   19
      Text            =   "2.2"
      Top             =   5160
      Width           =   975
   End
   Begin TextBox fCofRef 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0080FFFF&
      ForeColor       =   &H00000000&
      Height          =   372
      Left            =   4920
      TabIndex        =   17
      Text            =   "3.9"
      Top             =   5160
      Width           =   972
   End
   Begin TextBox iInternalRef 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0080FFFF&
      ForeColor       =   &H00000000&
      Height          =   372
      Left            =   1560
      TabIndex        =   15
      Text            =   "1"
      Top             =   4440
      Width           =   972
   End
   Begin TextBox iTempRef 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0080FFFF&
      ForeColor       =   &H00000000&
      Height          =   372
      Left            =   120
      TabIndex        =   13
      Text            =   "1"
      Top             =   4440
      Width           =   972
   End
   Begin TextBox iActualRef0 
      Height          =   372
      Left            =   2760
      TabIndex        =   11
      Text            =   "1"
      Top             =   3600
      Width           =   972
   End
   Begin TextBox iActualRefInf 
      Height          =   372
      Left            =   1440
      TabIndex        =   9
      Text            =   "1"
      Top             =   3600
      Width           =   972
   End
   Begin TextBox iZeroRefInf 
      Height          =   372
      Left            =   120
      TabIndex        =   7
      Text            =   "1"
      Top             =   3600
      Width           =   972
   End
   Begin TextBox Result 
      BackColor       =   &H00008000&
      ForeColor       =   &H00FFFFFF&
      Height          =   372
      Left            =   2280
      TabIndex        =   5
      Top             =   480
      Width           =   1332
   End
   Begin CommandButton ReadFprobe0 
      Caption         =   "Read FProbe Actual"
      Height          =   375
      Left            =   1800
      TabIndex        =   4
      Top             =   2640
      Width           =   1815
   End
   Begin TextBox BurstCount 
      BackColor       =   &H00FFFFFF&
      Height          =   372
      Left            =   120
      TabIndex        =   2
      Text            =   "4"
      Top             =   480
      Width           =   1332
   End
   Begin CommandButton Read_FProbe 
      BackColor       =   &H00E0FFFF&
      Caption         =   "Read FProbe Inf"
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   2640
      Width           =   1575
   End
   Begin Grid Fgrid 
      Cols            =   4
      Height          =   1575
      Left            =   120
      Rows            =   5
      TabIndex        =   0
      Top             =   960
      Width           =   3495
   End
   Begin Label Label31 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "fXKappa"
      Height          =   255
      Left            =   5160
      TabIndex        =   67
      Top             =   2400
      Width           =   975
   End
   Begin Label Label30 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "fCofBeta"
      Height          =   255
      Left            =   5160
      TabIndex        =   66
      Top             =   1560
      Width           =   975
   End
   Begin Label Label27 
      BackColor       =   &H00C0C0C0&
      Caption         =   "fActualAlpha"
      Height          =   255
      Left            =   8160
      TabIndex        =   58
      Top             =   720
      Width           =   1335
   End
   Begin Label Label26 
      BackColor       =   &H00C0C0C0&
      Caption         =   "fZeroAlpha"
      Height          =   255
      Left            =   8280
      TabIndex        =   57
      Top             =   0
      Width           =   1575
   End
   Begin Label Label19 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "iInternali"
      Height          =   255
      Left            =   3720
      TabIndex        =   42
      Top             =   2400
      Width           =   975
   End
   Begin Label Label18 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "iTempi"
      Height          =   255
      Left            =   3720
      TabIndex        =   41
      Top             =   1560
      Width           =   975
   End
   Begin Label Label40 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "fStableRead"
      Height          =   255
      Left            =   6120
      TabIndex        =   86
      Top             =   4920
      Width           =   1095
   End
   Begin Label Label39 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "Probe deltaAct"
      Height          =   495
      Left            =   8160
      TabIndex        =   84
      Top             =   3480
      Width           =   1215
   End
   Begin Label Label7 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00C0C0C0&
      Caption         =   "iInternalRefInf"
      Height          =   195
      Index           =   1
      Left            =   1440
      TabIndex        =   16
      Top             =   4200
      Width           =   1215
   End
   Begin Label Label38 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "nfInfRead"
      Height          =   255
      Left            =   120
      TabIndex        =   83
      Top             =   4920
      Width           =   975
   End
   Begin Label Label37 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "nfInfMax"
      Height          =   255
      Left            =   1320
      TabIndex        =   82
      Top             =   4920
      Width           =   975
   End
   Begin Label Label36 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "Probe DeltafTemp"
      Height          =   495
      Left            =   8040
      TabIndex        =   79
      Top             =   2520
      Width           =   1335
   End
   Begin Label Label35 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "ZeroOffset"
      Height          =   255
      Left            =   3720
      TabIndex        =   77
      Top             =   4920
      Width           =   975
   End
   Begin Label Label34 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "nInfRead"
      Height          =   255
      Left            =   5400
      TabIndex        =   74
      Top             =   3360
      Width           =   975
   End
   Begin Label Label33 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "nInfMax"
      Height          =   255
      Left            =   4080
      TabIndex        =   73
      Top             =   3360
      Width           =   975
   End
   Begin Label Label32 
      BackColor       =   &H00C0C0C0&
      Caption         =   "fxLambda"
      Height          =   255
      Left            =   8400
      TabIndex        =   68
      Top             =   1560
      Width           =   975
   End
   Begin Label Label29 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "fXBeta"
      Height          =   255
      Left            =   5160
      TabIndex        =   65
      Top             =   720
      Width           =   975
   End
   Begin Label Label28 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "fX"
      Height          =   255
      Left            =   5160
      TabIndex        =   64
      Top             =   0
      Width           =   975
   End
   Begin Label Label25 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "fInternalAlpha"
      Height          =   255
      Left            =   6720
      TabIndex        =   56
      Top             =   720
      Width           =   1335
   End
   Begin Label Label24 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "deltafTemp"
      Height          =   255
      Left            =   6840
      TabIndex        =   52
      Top             =   1560
      Width           =   1095
   End
   Begin Label Label23 
      BackColor       =   &H00C0C0C0&
      Caption         =   "deltaiTempRef"
      Height          =   255
      Left            =   5880
      TabIndex        =   50
      Top             =   4200
      Width           =   1335
   End
   Begin Label Label22 
      BackColor       =   &H00C0C0C0&
      Caption         =   "deltaiActualRefInf"
      Height          =   255
      Left            =   6720
      TabIndex        =   49
      Top             =   0
      Width           =   1335
   End
   Begin Label Label21 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "deltActRef0"
      Height          =   255
      Left            =   3000
      TabIndex        =   48
      Top             =   4200
      Width           =   1095
   End
   Begin Label Label20 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "delActlRefInf"
      Height          =   255
      Left            =   4440
      TabIndex        =   47
      Top             =   4200
      Width           =   1095
   End
   Begin Label Label17 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "iActuali"
      Height          =   255
      Left            =   3720
      TabIndex        =   40
      Top             =   720
      Width           =   975
   End
   Begin Label Label11 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "iZeroi"
      Height          =   252
      Left            =   3720
      TabIndex        =   39
      Top             =   0
      Width           =   972
   End
   Begin Label Label16 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "Cons F"
      Height          =   255
      Left            =   6120
      TabIndex        =   28
      Top             =   5640
      Width           =   975
   End
   Begin Label Label15 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "Cons E"
      Height          =   255
      Left            =   4920
      TabIndex        =   32
      Top             =   5640
      Width           =   975
   End
   Begin Label Label14 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "Cons D"
      Height          =   255
      Left            =   3720
      TabIndex        =   31
      Top             =   5640
      Width           =   975
   End
   Begin Label Label13 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "Cons C"
      Height          =   255
      Left            =   2520
      TabIndex        =   30
      Top             =   5640
      Width           =   975
   End
   Begin Label Label12 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "Cons B"
      Height          =   255
      Left            =   1320
      TabIndex        =   29
      Top             =   5640
      Width           =   975
   End
   Begin Label Label10 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "Cons A"
      Height          =   255
      Left            =   120
      TabIndex        =   27
      Top             =   5640
      Width           =   975
   End
   Begin Label Label9 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "fMnRef"
      Height          =   255
      Left            =   2520
      TabIndex        =   20
      Top             =   4920
      Width           =   975
   End
   Begin Label Label8 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "fCofRef"
      Height          =   255
      Left            =   4920
      TabIndex        =   18
      Top             =   4920
      Width           =   975
   End
   Begin Label Label6 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "iTempRefInf"
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   4200
      Width           =   975
   End
   Begin Label Label5 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "iActualRef0"
      Height          =   255
      Left            =   2760
      TabIndex        =   12
      Top             =   3360
      Width           =   975
   End
   Begin Label Label3 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "iActualRefInf"
      Height          =   255
      Left            =   1440
      TabIndex        =   10
      Top             =   3360
      Width           =   975
   End
   Begin Label Label2 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "iZeroRefInf"
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   3360
      Width           =   975
   End
   Begin Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "Result"
      Height          =   375
      Left            =   2280
      TabIndex        =   6
      Top             =   120
      Width           =   1335
   End
   Begin Label Label4 
      Alignment       =   2  'Center
      BackColor       =   &H00C0C0C0&
      Caption         =   "Burst Count"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   1335
   End
End
Option Explicit
'
'

Sub Command1_Click ()
    fgrid.Row = 1
    fgrid.Col = 1
    iZeroRefInf = fgrid.Text
    fgrid.Row = 2
    iActualRefInf = fgrid.Text
    fgrid.Row = 4
    iTempRef = fgrid.Text
    fgrid.Row = 3
    iInternalRef = fgrid.Text
    ' deltafActualRef = Val(iActualRefInf) - Val(iActualRef0)
    deltaiActualRefInf = Val(iZeroRefInf) - Val(iActualRefInf)
    deltaiTempRef = Val(iZeroRefInf) - Val(iTempRef)
End Sub

Sub Command2_Click ()
    fgrid.Row = 2
    fgrid.Col = 1
    iActualRef0 = fgrid.Text
    deltaiActualRef0 = Val(iZeroRefInf) - Val(iActualRef0)
    deltafActualRef = Val(iActualRefInf) - Val(iActualRef0)
End Sub

Sub Command3_Click ()
    SaveConstants
End Sub

Sub Command4_Click ()
    RetrieveConstants
End Sub

Sub exit_Click ()
    Unload Me
End Sub

Function FerrousCalc (ureturn As header, atInf As Integer) As Single
    'Dim iZeroRefInf, iActualRefInf, iActualRef0 As Single
    'Dim iTempRef, iInternalRef, fCofRef, fMnRef as single

    'Dim deltafActualRef As Single
    'Dim deltaiActualRef0, deltaiActualRefInf, deltaiTempRef As Single
    Dim nInfMax As Single
    ' dim iZeroi, iActuali, iTempi, iInternali As Single

    Dim sign As Integer
' Static deltafTemp As Single
    Dim fTemp As Single
    ' dim fInternalAlpha, fZeroAlpha, fActualAlpha As Single
    'Dim fX, fXBeta, fCofBeta, fXKappa , fxLambda  As Single
    Dim fxGamma As Single

    iZeroi = ureturn.udata(1)
    iActuali = ureturn.udata(2)
    iTempi = ureturn.udata(4)
    iInternali = ureturn.udata(3)


    'iZeroRefInf = 59343
    'iActualRefInf = 59688
    'iActualRef0 = 28743
    'iTempRef = 47776
    'iInternalRef = 39154
    'fCofRef = 3.9
    'fMnRef = 2.2

'    deltafActualRef = iActualRefInf - iActualRef0
'    deltaiActualRef0 = iZeroRefInf - iActualRef0
'    deltaiActualRefInf = iZeroRefInf - iActualRefInf
'    deltaiTempRef = iZeroRefInf - iTempRef
    nInfMax = .07

    If iInternalRef = 0 Then
        iInternalRef = 1
    End If

    If atInf = True Then
        fTemp = (iZeroi - iTempi) * iInternali / iInternalRef
        deltafTemp = fTemp - Val(deltaiTempRef)
        FerrousCalc = 0
        Exit Function
    End If

    fInternalAlpha = iInternali / iInternalRef
    fZeroAlpha = iZeroi * fInternalAlpha
    fActualAlpha = iActuali * fInternalAlpha
    fX = fZeroAlpha - fActualAlpha

    fXBeta = (deltaiActualRef0 - fX) / deltafActualRef

    fCofBeta = fCofRef - fMnRef * Abs(fXBeta)
    fXKappa = (deltafTemp / (deltaiTempRef * fCofBeta) + 1) * fX
    fxLambda = (deltaiActualRef0 - fXKappa) / deltafActualRef
    If fxLambda < 0 Then
        sign = -1
        fxLambda = Abs(fxLambda)
    Else
        sign = 1
    End If
    fxGamma = (consa + consc * fxLambda + conse * (fxLambda ^ 2)) / (1 + consb * fxLambda + consd * (fxLambda ^ 2) + consf * (fxLambda ^ 3))

    FerrousCalc = fxGamma * sign

End Function

Sub nInfMax_Change ()
    nfInfMax = Val(nInfMax) * Val(deltaiActualRefInf)
End Sub

Sub nInfRead_Change ()
    nfInfread = Val(nInfRead) * Val(deltaiActualRef0)
End Sub

Sub Read_FProbe_Click ()
    Dim ureturn As header
    Dim X, Y As Integer
    read_fprobe.Enabled = False
    fgrid.Row = 1
    fgrid.Col = 0
    fgrid.Text = "Zero"
    fgrid.Row = 2
    fgrid.Text = "Actual"
    fgrid.Row = 3
    fgrid.Text = "Internal"
    fgrid.Row = 4
    fgrid.Text = "Temp"
    fgrid.Row = 0
    fgrid.Col = 2
    fgrid.Text = "Maximum"
    fgrid.Col = 1
    fgrid.Text = "Minimum"
    fgrid.Col = 3
    fgrid.Text = "Difference"

    For X = 1 To 3
        fgrid.ColWidth(X) = fgrid.Width / 4
    Next
    For Y = 0 To 4
        fgrid.RowHeight(Y) = fgrid.Height / 6
    Next
    If ReadFProbe(Val(BurstCount), ureturn) Then
        For Y = 1 To 4
            For X = 1 To 1
                fgrid.Col = X
                fgrid.Row = Y
                fgrid.Text = ureturn.udata((X - 1) * 4 + Y)
            Next
            fgrid.Col = 2
            fgrid.Row = 2
            fgrid.Text = ureturn.udata(5)
            fgrid.Col = 3
            fgrid.Text = ureturn.udata(5) - ureturn.udata(2)
            ' fgrid.Col = 3
            ' fgrid.Text = ureturn.udata(4 + Y) - ureturn.udata(Y)
        Next
        Result = FerrousCalc(ureturn, True)
        checkProbe = ureturn.udata(9)
        UpdatedeltaF
        fgrid.Visible = True
    Else
        fgrid.SelStartRow = 1
        fgrid.SelStartCol = 1
        fgrid.SelEndRow = fgrid.Rows - 1
        fgrid.SelEndCol = fgrid.Cols - 1
        fgrid.FillStyle = 1
        fgrid.Text = ""
        fgrid.FillStyle = 0
        fgrid.SelEndRow = 1
        fgrid.SelEndCol = 1
        fgrid.Visible = False
    End If
    read_fprobe.Enabled = True


End Sub

Function ReadFProbe (Burst As Integer, ureturn As header) As Integer
    Dim udata As header
    
    
    udata.function = PROLP_READF
    udata.flag = 0
    udata.udata(1) = Burst
    If sendrequest(udata, ureturn, 1) Then
        ReadFProbe = True
    Else
        ReadFProbe = False
    End If
End Function

Sub ReadFprobe0_Click ()
    Dim ureturn As header
    Dim X, Y As Integer
    read_fprobe.Enabled = False
    fgrid.Row = 1
    fgrid.Col = 0
    fgrid.Text = "Zero"
    fgrid.Row = 2
    fgrid.Text = "Actual"
    fgrid.Row = 3
    fgrid.Text = "Internal"
    fgrid.Row = 4
    fgrid.Text = "Temp"
    fgrid.Row = 0
    fgrid.Col = 2
    fgrid.Text = "Maximum"
    fgrid.Col = 1
    fgrid.Text = "Minimum"
    fgrid.Col = 3
    fgrid.Text = "Difference"

    For X = 1 To 3
        fgrid.ColWidth(X) = fgrid.Width / 4
    Next
    For Y = 0 To 4
        fgrid.RowHeight(Y) = fgrid.Height / 6
    Next
    If ReadFProbe(Val(BurstCount), ureturn) Then
        For Y = 1 To 4
            For X = 1 To 1
                fgrid.Col = X
                fgrid.Row = Y
                fgrid.Text = ureturn.udata((X - 1) * 4 + Y)
            Next
            fgrid.Col = 2
            fgrid.Row = 2
            fgrid.Text = ureturn.udata(5)
            fgrid.Col = 3
            fgrid.Text = ureturn.udata(5) - ureturn.udata(2)
            'fgrid.Col = 3
            'fgrid.Text = ureturn.udata(4 + Y) - ureturn.udata(Y)
        Next
        Result = FerrousCalc(ureturn, False)
        checkProbe = ureturn.udata(9)
        fgrid.Visible = True
    Else
        fgrid.SelStartRow = 1
        fgrid.SelStartCol = 1
        fgrid.SelEndRow = fgrid.Rows - 1
        fgrid.SelEndCol = fgrid.Cols - 1
        fgrid.FillStyle = 1
        fgrid.Text = ""
        fgrid.FillStyle = 0
        fgrid.SelEndRow = 1
        fgrid.SelEndCol = 1
        fgrid.Visible = False
    End If
    read_fprobe.Enabled = True


End Sub

Sub RetrieveConstants ()
    Dim udata As header
    Dim ureturn As header
    Dim i As Integer
    Dim s As Single
    If Not SetupMemoryOffset() Then
        MsgBox "Could not Get memory offset", MB_OK
        Exit Sub
    End If

    udata.function = PROLP_READMEMORY
    udata.udata(1) = sysConstants + scFConstants
    udata.udata(2) = 13 * 2 + 2
    If Not sendrequest(udata, ureturn, 2) Then
        MsgBox "Could Not Load Constants", MB_OK + MB_ICONEXCLAMATION, "Protek II"
        Exit Sub
    End If
    i = 1
    dummy = ProDllSingleDecode(ureturn.udata(i), s):   consa = s: i = i + 2
    dummy = ProDllSingleDecode(ureturn.udata(i), s):   consb = s: i = i + 2
    dummy = ProDllSingleDecode(ureturn.udata(i), s):   consc = s: i = i + 2
    dummy = ProDllSingleDecode(ureturn.udata(i), s):   consd = s: i = i + 2
    dummy = ProDllSingleDecode(ureturn.udata(i), s):   conse = s: i = i + 2
    dummy = ProDllSingleDecode(ureturn.udata(i), s):   consf = s: i = i + 2
    dummy = ProDllSingleDecode(ureturn.udata(i), s):   iTempRef = s: i = i + 2
    dummy = ProDllSingleDecode(ureturn.udata(i), s):   iInternalRef = s: i = i + 2
    dummy = ProDllSingleDecode(ureturn.udata(i), s):   deltafActualRef = s: i = i + 2
    dummy = ProDllSingleDecode(ureturn.udata(i), s):   deltaiActualRef0 = s: i = i + 2
    dummy = ProDllSingleDecode(ureturn.udata(i), s):   deltaiTempRef = s: i = i + 2
    dummy = ProDllSingleDecode(ureturn.udata(i), s):   fCofRef = s: i = i + 2
    dummy = ProDllSingleDecode(ureturn.udata(i), s):   fMnRef = s: i = i + 2
    ZeroOffset = ureturn.udata(i)
    fStableReading = ureturn.udata(i + 1)
        
        udata.function = PROLP_READMEMORY
        udata.udata(1) = sysConstants + scFnInfMax
        udata.udata(2) = 2

        If sendrequest(udata, ureturn, 2) Then
            nfInfMax = ureturn.udata(1)
            nfInfread = ureturn.udata(2)
        End If
    MsgBox "Constants Retrieved", MB_OK + MB_ICONINFORMATION, "Protek II"
End Sub

Sub SaveConstants ()
    Dim udata As header
    Dim ureturn As header
    Dim i As Integer
    Dim s As Single
    Dim l As Long
    If Not SetupMemoryOffset() Then
        MsgBox "Could not Get memory offset", MB_OK
        Exit Sub
    End If

    i = 3
    udata.function = PROLP_WRITEMEMORY
    udata.udata(1) = sysConstants + scFConstants
    udata.udata(2) = 13 * 2 + 2
    i = i + ProDllSingleEncode(Val(consa), udata.udata(i))
    i = i + ProDllSingleEncode(Val(consb), udata.udata(i))
    i = i + ProDllSingleEncode(Val(consc), udata.udata(i))
    i = i + ProDllSingleEncode(Val(consd), udata.udata(i))
    i = i + ProDllSingleEncode(Val(conse), udata.udata(i))
    i = i + ProDllSingleEncode(Val(consf), udata.udata(i))
    i = i + ProDllSingleEncode(Val(iTempRef), udata.udata(i))
    i = i + ProDllSingleEncode(Val(iInternalRef), udata.udata(i))
    i = i + ProDllSingleEncode(Val(deltafActualRef), udata.udata(i))
    i = i + ProDllSingleEncode(Val(deltaiActualRef0), udata.udata(i))
    i = i + ProDllSingleEncode(Val(deltaiTempRef), udata.udata(i))
    i = i + ProDllSingleEncode(Val(fCofRef), udata.udata(i))
    i = i + ProDllSingleEncode(Val(fMnRef), udata.udata(i))
    udata.udata(i) = Val(ZeroOffset)
    i = i + 1
    udata.udata(i) = Val(fStableReading)
    udata.len = i
    If sendrequest(udata, ureturn, i) Then
        
        udata.function = PROLP_WRITEMEMORY
        udata.udata(1) = sysConstants + scFnInfMax
        udata.udata(2) = 2
        udata.udata(3) = Val(nfInfMax)
        udata.udata(4) = Val(nfInfread)
        If sendrequest(udata, ureturn, 4) Then
            MsgBox "Constants Stored", MB_OK + MB_ICONINFORMATION, "Protek II"
            Exit Sub
        End If
    End If
    MsgBox "Constants Not Store", MB_OK + MB_ICONEXCLAMATION, "Protek II"
End Sub

Sub tile_Click ()
    mdiBase.Arrange TILE_HORIZONTAL
End Sub

Sub UpdatedeltaF ()
    Dim udata As header
    Dim ureturn As header
    Dim s As Single

        udata.function = PROLP_READMEMORY
        udata.udata(1) = userConstants + 4
        udata.udata(2) = 2
        If sendrequest(udata, ureturn, 2) Then
            dummy = ProDllSingleDecode(ureturn.udata(1), s)
            probedeltafTemp = s
        Else
            probedeltafTemp = "unknown"
        End If
End Sub


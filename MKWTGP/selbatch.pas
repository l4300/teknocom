unit selbatch;

interface

uses
  Qreport, Session, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, checklst, Buttons, ExtCtrls;

type
  TSelectBatchesForm = class(TForm)
    OkButton: TBitBtn;
    CancelButton: TBitBtn;
    CheckListBox: TCheckListBox;
    SelectAllButton: TButton;
    ClearAllButton: TButton;
    Prompt: TLabel;
    PrintOptionsPanel: TPanel;
    PrintStatsCheckBox: TCheckBox;
    PrintDataCheckBox: TCheckBox;
    PrintGChartCheckBox: TCheckBox;
    PrintDChartCheckBox: TCheckBox;
    Label1: TLabel;
    PrintQChartCheckBox: TCheckBox;
    PrintBGChartCheckBox: TCheckBox;
    procedure SelectAllButtonClick(Sender: TObject);
    procedure ClearAllButtonClick(Sender: TObject);
    procedure PrintStatsCheckBoxClick(Sender: TObject);
    procedure PrintDataCheckBoxClick(Sender: TObject);
    procedure PrintGChartCheckBoxClick(Sender: TObject);
    procedure PrintDChartCheckBoxClick(Sender: TObject);
    procedure PrintQChartCheckBoxClick(Sender: TObject);
    procedure PrintBGChartCheckBoxClick(Sender: TObject);
  private
    { Private declarations }
    procedure UpdatePrintOptions;
    procedure GetPrintOptions;
  public
    { Public declarations }
    function GetSelections(todo:string;Session:TGSession) : integer; // returns count of selected items or 0 on cancel/none
  end;

var
  SelectBatchesForm: TSelectBatchesForm;

implementation

{$R *.DFM}

function TSelectBatchesForm.GetSelections(todo:string;Session:TGSession) : integer; // returns count of selected items or 0 on cancel/none
var
   i : integer;
begin
     if todo = 'Print' then
        PrintOptionsPanel.visible := true
     else
        PrintOptionsPanel.visible := false;

     Prompt.Caption := 'Select the batches that you want to '+todo;
     GetPrintOptions;
     OkButton.Caption := '&' + todo;
     CheckListBox.Items.Clear;
     for i := 0 to Session.BatchCount-1 do
     begin
          Session.Checked[i] := false;
          CheckListBox.Items.Add(Session.BatchDataArray[i].Description);
          if i = Session.Batch then
             CheckListBox.Checked[i] := true
          else
             CheckListBox.Checked[i] := false;
     end;

     CheckListBox.TopIndex := Session.Batch;
     if mrCancel = ShowModal then
        result := 0
     else begin
          result := 0;
          for i := 0 to Session.BatchCount-1 do
          begin
               if CheckListBox.Checked[i] then begin
                  Session.Checked[i] := true;
                  inc(result);
               end;
          end;
          if result > 0 then
             UpdatePrintOptions;
     end;
end;

procedure TSelectBatchesForm.SelectAllButtonClick(Sender: TObject);
var
   i : integer;
begin
     for i := 0 to CheckListBox.Items.Count-1 do
     begin
          CheckListBox.Checked[i] := true;
     end;

end;

procedure TSelectBatchesForm.ClearAllButtonClick(Sender: TObject);
var
   i : integer;
begin
     for i := 0 to CheckListBox.Items.Count-1 do
     begin
          CheckListBox.Checked[i] := false;
     end;
end;

procedure TSelectBatchesForm.PrintStatsCheckBoxClick(Sender: TObject);
begin
     QReportForm.BatchStatsBand.Enabled := PrintStatsCheckBox.Checked;
end;

procedure TSelectBatchesForm.GetPrintOptions;
begin
     PrintStatsCheckBox.Checked := QReportForm.BatchStatsBand.Enabled;
     PrintDataCheckBox.Checked := QReportForm.BatchReadingsBand.Enabled;
     PrintGChartCheckBox.Checked := QReportForm.DataPlotBand.Enabled;
     PrintDChartCheckBox.Checked := QReportForm.DensityPlotBand.Enabled;
     PrintQChartCheckBox.Checked := QReportForm.QuantilePlotBand.Enabled;
     PrintBGChartCheckBox.Checked := QReportForm.BoxPlotBand.Enabled;
end;

procedure TSelectBatchesForm.UpdatePrintOptions;
begin
     QReportForm.BatchStatsBand.Enabled := PrintStatsCheckBox.Checked;
     QReportForm.BatchReadingsBand.Enabled := PrintDataCheckBox.Checked;
     QReportForm.DataPlotBand.Enabled := PrintGChartCheckBox.Checked;
     QReportForm.DensityPlotBand.Enabled := PrintDChartCheckBox.Checked;
     QReportForm.QuantilePlotBand.Enabled := PrintQChartCheckBox.Checked;
     QReportForm.BoxPlotBand.Enabled := PrintBGChartCheckBox.Checked;

end;

procedure TSelectBatchesForm.PrintDataCheckBoxClick(Sender: TObject);
begin
     QReportForm.BatchReadingsBand.Enabled := PrintDataCheckBox.Checked;
end;

procedure TSelectBatchesForm.PrintGChartCheckBoxClick(Sender: TObject);
begin
     QReportForm.DataPlotBand.Enabled :=  PrintGChartCheckBox.Checked;
end;

procedure TSelectBatchesForm.PrintDChartCheckBoxClick(Sender: TObject);
begin
     QReportForm.DensityPlotBand.Enabled := PrintDChartCheckBox.Checked;
end;

procedure TSelectBatchesForm.PrintQChartCheckBoxClick(Sender: TObject);
begin
     QReportForm.QuantilePlotBand.Enabled := PrintQChartCheckBox.Checked;
end;

procedure TSelectBatchesForm.PrintBGChartCheckBoxClick(Sender: TObject);
begin
     QReportForm.BoxPlotBand.Enabled := PrintBGChartCheckBox.Checked;

end;

end.

unit masterf;

interface

uses
  terminal, Excel, selbatch, qreport, inifiles, globals, registry, Gauge, Packet,session, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Menus, Buttons, OleCtrls, Grids,
  StdCtrls, series, Chart, TeeFunci, csgrid, TeEngine, TeeProcs, ExtCtrls,
  ToolWin, printers, OoMisc, AdPort, OHLChart, CandleCh, DdeMan, OleCtnrs;

type
  TMaster = class(TForm)
    ToolBar1: TToolBar;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    MainMenu: TMainMenu;
    FileMenu: TMenuItem;
    FileOpenItem: TMenuItem;
    FileSaveItem: TMenuItem;
    FileSaveAsItem: TMenuItem;
    N1: TMenuItem;
    FilePrintItem: TMenuItem;
    FilePrintSetupItem: TMenuItem;
    N4: TMenuItem;
    FileExitItem: TMenuItem;
    EditMenu: TMenuItem;
    EditCopyItem: TMenuItem;
    HelpMenu: TMenuItem;
    HelpContentsItem: TMenuItem;
    HelpSearchItem: TMenuItem;
    HelpHowToUseItem: TMenuItem;
    N3: TMenuItem;
    HelpAboutItem: TMenuItem;
    OpenDialog: TOpenDialog;
    PrintDialog: TPrintDialog;  { &Contents }
    SaveDialog: TSaveDialog;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    SpeedButton9: TSpeedButton;
    ToolButton1: TToolButton;
    StatusBar1: TStatusBar;
    LegendBevel: TBevel;
    BatchTabControl: TTabControl;
    PageControl: TPageControl;
    TabData: TTabSheet;
    CaptureButton: TBitBtn;
    UnitsButton: TSpeedButton;
    ToolButton2: TToolButton;
    N2: TMenuItem;
    ComPort1: TMenuItem;
    Stats: TMenuItem;
    LegendPanel: TPanel;
    MBatchName: TLabel;
    Label1: TLabel;
    MReadings: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    MRows: TLabel;
    MCols: TLabel;
    MCamCount: TLabel;
    MLowLimit: TLabel;
    MHighLimit: TLabel;
    MStartDate: TLabel;
    MStartTime: TLabel;
    MEndDate: TLabel;
    MEndTime: TLabel;
    BackGroundImage: TImage;
    IconPanel: TPanel;
    IconImage: TImage;
    LogoImage: TImage;
    DensityGraphSheet: TTabSheet;
    DChart: TChart;
    Series1: TLineSeries;
    GraphSheet: TTabSheet;
    GChart: TChart;
    DataGrid: TCStringGrid;
    N5: TMenuItem;
    CaptureMenuChoice: TMenuItem;
    Series2: TPointSeries;
    PrintSetupDialog: TPrinterSetupDialog;
    GraphMenuItem: TMenuItem;
    AvgMenuItem: TMenuItem;
    StdDeviationMenuItem: TMenuItem;
    View3DMenuItem: TMenuItem;
    GridMenuItem: TMenuItem;
    PointsOnDensityMenuItem: TMenuItem;
    SpeedButton11: TSpeedButton;
    PrintPreviewItem: TMenuItem;
    QuantileGraphSheet: TTabSheet;
    QChart: TChart;
    Series3: TLineSeries;
    EditMilsItem: TMenuItem;
    EditMicronsItem: TMenuItem;
    QuantileBarsMenuItem: TMenuItem;
    ShowLimitsMenuItem: TMenuItem;
    FApdComPort: TApdComPort;
    BoxGraphSheet: TTabSheet;
    BGChart: TChart;
    Series5: TCandleSeries;
    Probe: TDdeServerConv;
    Result: TDdeServerItem;
    ExporttoExcel1: TMenuItem;
    QATab: TTabSheet;
    QAGrid: TCStringGrid;
    procedure ComPort1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CaptureButtonClick(Sender: TObject);
    procedure StatsClick(Sender: TObject);
    procedure BatchTabControlChange(Sender: TObject);
    procedure UnitsButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure DataGridDrawCell(Sender: TObject; Col, Row: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure DataGridPreDrawCell(Sender: TObject; Col, Row: Integer);
    procedure DataGridPostDrawCell(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CaptureMenuChoiceClick(Sender: TObject);
    procedure FilePrint(Sender: TObject);
    procedure FilePrintPreview(Sender: TObject);
    procedure FilePrintSetup(Sender: TObject);
    procedure FileOpen(Sender: TObject);
    procedure FileSave(Sender: TObject);
    procedure FileSaveAs(Sender: TObject);
    procedure AvgMenuItemClick(Sender: TObject);
    procedure StdDeviationMenuItemClick(Sender: TObject);
    procedure View3DMenuItemClick(Sender: TObject);
    procedure GridMenuItemClick(Sender: TObject);
    procedure PointsOnDensityMenuItemClick(Sender: TObject);
    procedure EditMilsItemClick(Sender: TObject);
    procedure EditMicronsItemClick(Sender: TObject);
    procedure QuantileBarsMenuItemClick(Sender: TObject);
    procedure ShowLimitsMenuItemClick(Sender: TObject);
    procedure FileExitItemClick(Sender: TObject);
    procedure EditCopyItemClick(Sender: TObject);
    procedure HelpAboutItemClick(Sender: TObject);
    procedure ProbeExecuteMacro(Sender: TObject; Msg: TStrings);
    procedure ExporttoExcel1Click(Sender: TObject);
  private
    { Private declarations }
    FGraphItemCount : integer;
    FExcelSpreadsheet: ExcelController;
    procedure GraphBatchClick(Sender:TObject);
    procedure SavePrefs;
    procedure SavePrefsString(section:String;value:string);
    function ReadPrefsString(section:String;default:string) : string;
    function GetPath(filename:string) : string;
  protected

     procedure CreateBatchTabs;
     procedure ClearBatchTabs;
     procedure ClearDataGrid;
     procedure ClearChart;
     procedure CreateDataGrid;
     procedure CreateGChart;
     procedure CreateQChart;
     procedure CreateBoxChart;

     procedure SetLegendData;
     procedure ClearLegendData;
     procedure Refresh;    // rebuild grid and chart
     procedure ClearScreen;   // disables everything
     procedure EnableScreen;
     procedure CreateDensityChart;

     procedure LoadImages;
     procedure EnableEditCopyMenu(value:boolean);
  public
    { Public declarations }
    Session : TGSession;
    procedure DrawGChart(Chart:TChart;BW:Boolean;Printing:Boolean);
    procedure DrawDChart(Chart:TChart;BW:Boolean;Printing:Boolean);
    procedure DrawQChart(Chart:TChart;BW:Boolean;Printing:Boolean);
    procedure DrawBoxChart(Chart:TChart;BW:Boolean;Printing:Boolean);
    property ApdComPort : TApdComPort read FApdComPort;
    
  end;

var
  Master: TMaster;
  MyApplication:TApplication;
  TextFile : Text;



implementation

uses Readp, clipbrd, boxseri, aboutfrm;

{$R *.DFM}

const
     CFIXED_DATA_ROWS:integer = 5;
     CFIXED_NAMES:integer = 0;
     CFIXED_MIN:integer = 1;
     CFIXED_MAX:integer = 2;
     CFIXED_AVG:integer = 3;
     CFIXED_STDV:integer = 4;
     CMAX_GRAPH_COLUMNS = 16;
     CMAX_GRAPH_ROWS = 1000;

procedure TMaster.ComPort1Click(Sender: TObject);
var
   t:string;
begin
     Session.SetupComPortName(t);
end;

procedure TMaster.EnableEditCopyMenu(value:boolean);
begin
     EditCopyItem.Enabled := value;
end;

procedure TMaster.LoadImages;
var
   basePath : array [0..255] of char;
   c : PChar;
   reg : TRegIniFile;
begin

//     basePath := MyApplication.
    StrPCopy(basePath,MyApplication.ExeName);
    c := StrRscan(basePath,'\');
    if c = nil then Exit;
    inc(c);
    c^ := #0;

    try
       IconImage.Picture.LoadFromFile(basePath + 'iconimag.bmp');
       IconImage.Transparent := true;
    except

    end;

    try
       LogoImage.Picture.LoadFromFile(basePath + 'logoimag.bmp');
       LogoImage.Transparent := true;
    except

    end;

    try
       BackGroundImage.Picture.LoadFromFile(basePath + 'backimag.bmp');
    except

    end;

    try
       reg := TRegIniFile.Create(CBASEREGKEY);
       MyApplication.Icon.LoadFromFile(basePath+reg.ReadString('Custom','Icon','deskicon.ico'));
       reg.free;
    except

    end;


end;

function TMaster.GetPath(filename:string) : string;
var
   s : array [0..255] of char;
   c : Pchar;
begin
     result := filename;
     strPCopy(s,filename);

     c := strRScan(s,'/');
     if c = nil then
        c := strRScan(s,'\');

     if c <> nil then begin
        c^ := #0;
        result := s;
     end;


end;

procedure TMaster.SavePrefsString(section:String;value:string);
var
   reg : TRegIniFile;
begin

    reg := TRegIniFile.Create(CBASEREGKEY);
    reg.WriteString(CPREFS,section,value);
    reg.free;
end;

function TMaster.ReadPrefsString(section:String;default:string) : string;
var
   reg : TRegIniFile;
begin

    reg := TRegIniFile.Create(CBASEREGKEY);
    result := reg.readString(CPREFS,section,default);
    reg.free;
end;

procedure TMaster.FormCreate(Sender: TObject);
var
   reg : TRegIniFile;
begin

    reg := TRegIniFile.Create(CBASEREGKEY);
    if -1 <> reg.ReadInteger('Display','Left',-1) then begin
        Left := reg.ReadInteger('Display','Left',0);
        Top :=  reg.ReadInteger('Display','Top',0);
        Width := reg.ReadInteger('Display','Width',0);
        Height := reg.ReadInteger('Display','Height',0);
    end;

    try
       LoadImages;
    except

    end;

    Caption := reg.ReadString('Custom','Title','Coating Thickness Gauge');
    AvgMenuItem.Checked := reg.ReadBool(CPREFS,'ShowAvg',true);
    StdDeviationMenuItem.Checked := reg.ReadBool(CPREFS,'ShowStdDev',true);
    View3DMenuItem.Checked := reg.ReadBool(CPREFS,'View3D',true);
    GridMenuItem.Checked := reg.ReadBool(CPREFS,'ShowGrid',true);
    ShowLimitsMenuItem.Checked := reg.ReadBool(CPREFS,'ShowLimits',false);
    PointsOnDensityMenuItem.Checked := reg.ReadBool(CPREFS,'ShowPointsOnDensity',false);
    QuantileBarsMenuItem.Checked := reg.ReadBool(CPREFS,'QuantileBarsOn',false);

    Reg.Free;
    FGraphItemCount := GraphMenuItem.Count;

    Session := TGSession.Create(Self);
    EnableEditCopyMenu(false);
end;

procedure TMaster.CaptureButtonClick(Sender: TObject);
begin
     CaptureButton.enabled := false;
     CaptureMenuChoice.enabled := false;

     try
         ClearBatchTabs;
         ClearDataGrid;
         ClearChart;
         ClearScreen;
         EnableEditCopyMenu(false);

         if not Session.CaptureAllData then begin
            CaptureMenuChoice.enabled := true;
            CaptureButton.enabled := true;
            Exit;
         end;

         if Session.BatchCount <> 0 then
         begin
              CreateBatchTabs;
              BatchTabControl.TabIndex := 0;

              EnableScreen;
              Refresh;
              EnableEditCopyMenu(true);
         end;
     except

     end;
     CaptureMenuChoice.enabled := true;
     CaptureButton.enabled := true;
end;

procedure TMaster.StatsClick(Sender: TObject);
begin
     messagedlg(Packet.PktStats,mtInformation,mbOKCancel,0);
end;

procedure TMaster.CreateBatchTabs;
var
   i : integer;
   BD : TBatchData;
begin
     BatchTabControl.Enabled := true;

     for i := 0 to Session.BatchCount -1  do
     begin
          BD := Session.BatchDataArray[i];
          if not assigned(BD) then Exit;

          BatchTabControl.Tabs.Add(BD.Description);
     end;

     BatchTabControlChange(Self);     

end;

procedure TMaster.ClearBatchTabs;
begin
     BatchTabControl.Tabs.Clear;
     BatchTabControl.Enabled := false;
end;

procedure TMaster.ClearScreen;   // disables everything
begin
     ClearBatchTabs;
     ClearLegendData;
     PageControl.Visible := false;
     LegendPanel.Visible := false;
     IconPanel.Visible := true;
     BatchTabControl.Visible := false;
     BackgroundImage.Visible := true;
     BackGroundImage.BringToFront;
     IconPanel.BringToFront;
end;

procedure TMaster.EnableScreen;   // enables everything
begin
     BackgroundImage.Visible := false;
     PageControl.Visible := true;
     BatchTabControl.Visible := true;
     LegendPanel.Visible := true;
     IconPanel.Visible := false;
     IconPanel.SendToBack;
     BackGroundImage.SendToBack;
end;

procedure TMaster.ClearDataGrid;
begin
     DataGrid.ColCount := 0;
     DataGrid.RowCount := 0;
     QAGrid.FixedCols := 0;
     QAGrid.FixedRows := 0;
     QAGrid.ColCount := 0;
     QAGrid.RowCount := 0;
end;


procedure TMaster.CreateDataGrid;
var
   BD : TBatchData;
   col : integer;
   row : integer;
   index : integer;
begin
     ClearDataGrid;
     BD := Session.BatchData;

     if not assigned(BD) then Exit;

     DataGrid.ColCount := 1 + BD.Columns;
     DataGrid.RowCount := CFIXED_DATA_ROWS + BD.Rows;

     DataGrid.FixedCols := 1;
     DataGrid.FixedRows := CFIXED_DATA_ROWS;

     for index := 0 to BD.Count -1 do begin
         col := index div BD.Rows;
         row := index mod BD.Rows;

         DataGrid.Cells[col+1,row+CFIXED_DATA_ROWS] := Session.Text[index];
     end;

     for index := 0 to BD.Rows do
         DataGrid.Cells[0,index+CFIXED_DATA_ROWS] := BD.RowName[index];

     for index := 1 to BD.Columns do begin
         DataGrid.Cells[index,CFIXED_NAMES] := BD.ColumnName[index-1];
         DataGrid.Cells[0,CFIXED_NAMES] := '';

         try
             DataGrid.Cells[index,CFIXED_MIN] := Session.MinText[index-1];
             DataGrid.Cells[index,CFIXED_MAX] := Session.MaxText[index-1];
             DataGrid.Cells[index,CFIXED_AVG] := Session.AvgText[index-1];
             DataGrid.Cells[index,CFIXED_STDV] := Session.StandardDeviationText[index-1];
         except

         end;
     end;

     DataGrid.Cells[0,CFIXED_MIN] := 'Min';
     DataGrid.Cells[0,CFIXED_MAX] := 'Max';
     DataGrid.Cells[0,CFIXED_AVG] := 'Avg';
     DataGrid.Cells[0,CFIXED_STDV] := 'Stdv';
     DataGrid.Repaint;

     if BD.Questions.Count < 1 then begin
        QAGrid.Visible := false;
        Exit;
     end;

     QAGrid.Visible := true;

     QAGrid.ColCount := 2;
     QAGrid.RowCount := BD.Questions.Count;

     QAGrid.FixedCols := 1;
     QAGrid.FixedRows := 0;

     for index := 0 to BD.Questions.Count -1 do begin
         with TQA(BD.Questions.Items[index]) do begin
              QAGrid.Cells[0,index] := Question;
              QAGrid.Cells[1,index] := Answer;
         end;
     end;

     QAGrid.Repaint;


//     DataGrid.Col := 1;
//     DataGrid.Row := 1;
end;


procedure TMaster.ClearChart;
begin
//     Chart.Visible := false;
end;


procedure TMaster.Refresh;
begin
     Session.Batch := BatchTabControl.TabIndex;
     SetLegendData;
     GraphMenuItem.Visible := false;
     PointsOnDensityMenuItem.Visible := false;
     QuantileBarsMenuItem.Visible := false;

     if (PageControl.ActivePage.Name = 'TabData') or
        (PageControl.ActivePage.Name = 'QATab') then
        CreateDataGrid
     else if PageControl.ActivePage.Name = 'DensityGraphSheet' then begin
          GraphMenuItem.Visible := true;
          PointsOnDensityMenuItem.Visible := true;
          CreateDensityChart
     end else if PageControl.ActivePage.Name = 'GraphSheet' then begin
         GraphMenuItem.Visible := true;
         CreateGChart;
     end else if PageControl.ActivePage.Name = 'BoxGraphSheet' then begin
         GraphMenuItem.Visible := true;
         CreateBoxChart;
     end else if PageControl.ActivePage.Name = 'QuantileGraphSheet' then begin
         QuantileBarsMenuItem.Visible := true;
         GraphMenuItem.Visible := true;
         CreateQChart;
     end;
end;

procedure TMaster.GraphBatchClick(Sender:TObject);
var
   m : TMenuItem;
begin
     m := Sender as TMenuItem;

     m.Checked := not m.Checked;
     Session.BatchData.ColumnEnabled[m.GroupIndex-100] := m.Checked;
     Refresh;
end;


procedure TMaster.BatchTabControlChange(Sender: TObject);
Var
   m : TMenuItem;
   i : integer;
begin
     Session.Batch := BatchTabControl.TabIndex;

     // first remove all of the non-default items
     while GraphMenuItem.Count > FGraphItemCount do
           GraphMenuItem.Delete(GraphMenuItem.Count-1);

     m := TMenuItem.Create(Self);
     m.Caption := '-';

     GraphMenuItem.Add(m);
     for i := 0 to Session.BatchData.Columns-1 do
     begin
          m := TMenuItem.Create(Self);
          m.Caption := Session.BatchData.ColumnName[i];
          m.GroupIndex := 100+i;
          m.Checked := Session.BatchData.ColumnEnabled[i];
          m.Enabled := true;
          m.Default := false;
          m.RadioItem := false;
          m.OnClick := GraphBatchClick;
          GraphMenuItem.Add(m);
     end;
     Refresh;
end;

procedure TMaster.UnitsButtonClick(Sender: TObject);
begin
     if UnitsButton.Down then begin
        Session.Units := Microns;
        UnitsButton.Caption := 'Microns';
        EditMilsItem.Checked := false;
        EditMicronsItem.Checked := true;
     end else begin
         Session.Units := Mils;
         UnitsButton.Caption := 'Mils';
         EditMilsItem.Checked := true;
         EditMicronsItem.Checked := false;
     end;

     Refresh;
end;

procedure TMaster.CreateDensityChart;
begin
     DrawDChart(DChart,false,false);
end;

function GetLineStyle(i:integer) : TPenStyle;
begin
  result := psSolid;
  case (i mod 5) of
    0: result := psSolid;
    1: result := psDash;
    2: result := psDot;
    3: result := psDashDot;
    4: result := psDashDotDot;
  end;
end;

function GetMarkerStyle(i:integer) : TSeriesPointerStyle;
const
   a : array [0..8] of TSeriesPointerStyle =(
     psRectangle, psCircle, psTriangle, psDownTriangle, psCross, psDiagCross, psStar, psDiamond, psSmallDot);
begin

  result := a[i mod 9];
end;

procedure TMaster.DrawDChart(Chart:TChart;BW:Boolean;Printing:Boolean);
var
   BD : TBatchData;
   S : array [0..CMAX_GRAPH_COLUMNS] of TLineSeries;
   x : single;
   xstepvalue : single;
   h2 : single;
   i,j : integer;
   y : single;
   sum : array [0..CMAX_GRAPH_COLUMNS] of single;
   diff : single;
   col : integer;
   rcnt : array [0..CMAX_GRAPH_COLUMNS] of integer;
   maxcols : integer;
   maxsum : array [0..CMAX_GRAPH_COLUMNS] of single;
   bigsum : single;
   MeanLine : TLineSeries;
   PointSeries : TPointSeries;
begin

     BD := Session.BatchData;
     if not assigned(BD) then Exit;

     Chart.FreeAllSeries;
     Chart.Title.Text.Clear;
     Chart.Title.Text.Add(BD.Description + ' Density Chart');
     if not BW then
        Chart.UndoZoom;
     Chart.View3D := View3DMenuItem.Checked;

     if BW or Printing then begin
        Chart.BackColor := clWhite;
        Chart.LeftAxis.Grid.Width := 0;
        Chart.BottomAxis.Grid.Width := 0;
     end else begin
         if View3DMenuItem.Checked then
            Chart.BackColor := clWhite
         else
             Chart.BackColor := clSilver;
     end;

     Chart.BottomAxis.Grid.Visible := GridMenuItem.Checked;
     Chart.LeftAxis.Grid.Visible := GridMenuItem.Checked;

     if Session.Units = Mils then
        Chart.BottomAxis.Title.Caption := 'Thickness (Mils)'
     else
        Chart.BottomAxis.Title.Caption := 'Thickness (Microns)';

     Chart.BottomAxis.AxisValuesFormat := Session.UnitFormatString(Session.Avg[-1]);

     Chart.LeftAxis.Title.Caption := 'Density Factor';

     maxcols := BD.Columns;
     if maxcols > CMAX_GRAPH_COLUMNS then
        maxcols := CMAX_GRAPH_COLUMNS;

     for i := 0 to maxcols-1 do begin
         if BD.ColumnEnabled[i] and assigned(S[i]) then begin
             s[i] := TLineSeries.Create(Self);
             S[i].SeriesColor := clTeeColor;
             if BW then begin
                S[i].LinePen.Style := GetLineStyle(i);
                S[i].LinePen.Color := clBlack;
                S[i].SeriesColor := clBlack;
//                S[i].Pointer.Style := GetMarkerStyle(i);
//                S[i].Pointer.Visible := true;
             end;
             if Printing then
                 S[i].LinePen.Width := 0;

             maxsum[i] := 0;
             rcnt[i] := 0;
         end else
             S[i] := nil;
     end;

     h2 := (Session.Max[-1] - Session.Min[-1]) / 2;
     xstepvalue := (Session.Max[-1] - Session.Min[-1]) / 100;          // stepcount;
     bigsum := 0;


    for i := 0 to BD.Count-1 do
    begin
        col := i div BD.Rows;
        if col >= maxcols then continue;

        y := Session.Data[i];
        if y = CINVALID_READING then continue;
        inc(rcnt[col]);
    end;

     // calculate maximum value to normalize

{     x := BD.Min[-1];
     repeat
           for i := 0 to maxcols do
               sum[i] := 0;

           for i := 0 to BD.Count-1 do
           begin
                col := i div BD.Rows;
                if col >= maxcols then continue;

                y := Session.Data[i];
                if y = CINVALID_READING then continue;
                diff := abs(y - x)/h2;
                if diff > 0.5 then continue;
                sum[col] := sum[col] + 1 + cos(2*pi*diff);
           end;
           for i := 0 to maxcols -1 do begin
               if rcnt[i] = 0 then continue;
               sum[i] := sum[i]/(rcnt[i] * h2);
                if sum[i] > maxsum[i] then begin
                   maxsum[i] := sum[i];
                end;
           end;

           x := x + xstepvalue;
     until x >= Session.Max[-1];
 }
     // calculate actual values
     x := Session.Min[-1];
     repeat
           for i := 0 to maxcols-1 do
               sum[i] := 0;

           for i := 0 to BD.Count-1 do
           begin
                col := i div BD.Rows;
                if col >= maxcols then continue;

                y := Session.Data[i];
                if y = CINVALID_READING then continue;

                diff := abs(y - x)/h2;
                if diff > 0.5 then continue;

                sum[col] := sum[col] + 1 + cos(2*pi*diff);
           end;

           for i := 0 to maxcols -1 do begin
               if rcnt[i] = 0 then continue;
               sum[i] := sum[i]/(rcnt[i] * h2);

               if BD.ColumnEnabled[i] and assigned(S[i]) then begin
                  S[i].AddXY(x,sum[i]{(sum[i] * 100)/maxsum[i]},'',S[i].SeriesColor);
                  if sum[i] > bigsum then bigsum := sum[i];
               end;
           end;
           x := x + xstepvalue;
     until x >= Session.Max[-1];

     for i := 0 to maxcols-1 do
     begin
          if not BD.ColumnEnabled[i] or (rcnt[i] = 0) then begin
             if assigned(S[i]) then S[i].Free;
             continue;
          end;

//          S[i].Name := 'Dens_'+BD.ColumnName[i];
          S[i].Title := BD.ColumnName[i];

          Chart.AddSeries(S[i]);


          if AvgMenuItem.Checked then begin
              MeanLine := TLineSeries.Create(Self);
              MeanLine.SeriesColor := S[i].SeriesColor;
              MeanLine.ShowInLegend := false;
              if printing then
                 MeanLine.LinePen.Width := 0
              else
                 MeanLine.LinePen.Width := 1;

              MeanLine.AddXY(Session.Avg[i],0,'',MeanLine.SeriesColor);
              MeanLine.AddXY(Session.Avg[i],bigsum*0.20,'',MeanLine.SeriesColor);
              Chart.AddSeries(MeanLine);
          end;

          if StdDeviationMenuItem.Checked then begin
              MeanLine := TLineSeries.Create(Self);
              MeanLine.SeriesColor := S[i].SeriesColor;
              MeanLine.ShowInLegend := false;
              if printing then
                 MeanLine.LinePen.Width := 0
              else
                 MeanLine.LinePen.Width := 1;

              MeanLine.LinePen.Style := psDash;
              MeanLine.AddXY(Session.Avg[i] - Session.StandardDeviation[i],0,'',MeanLine.SeriesColor);
              MeanLine.AddXY(Session.Avg[i]- Session.StandardDeviation[i],bigsum*0.15,'',MeanLine.SeriesColor);
              Chart.AddSeries(MeanLine);

              MeanLine := TLineSeries.Create(Self);
              MeanLine.SeriesColor := S[i].SeriesColor;
              MeanLine.ShowInLegend := false;
              if printing then
                 MeanLine.LinePen.Width := 0
              else
                 MeanLine.LinePen.Width := 1;

              MeanLine.LinePen.Style := psDash;
              MeanLine.AddXY(Session.Avg[i] + Session.StandardDeviation[i],0,'',MeanLine.SeriesColor);
              MeanLine.AddXY(Session.Avg[i]+ Session.StandardDeviation[i],bigsum*0.15,'',MeanLine.SeriesColor);
              Chart.AddSeries(MeanLine);
          end;

          if ShowLimitsMenuItem.Checked and (TooHigh = BD.LimitCheck[-1]) then begin
              if 0 <> BD.LowLimit then begin
                  MeanLine := TLineSeries.Create(Self);
                  MeanLine.SeriesColor := S[i].SeriesColor;
                  MeanLine.ShowInLegend := false;
                  if printing then
                     MeanLine.LinePen.Width := 0
                  else
                     MeanLine.LinePen.Width := 1;
                  MeanLine.LinePen.Style := psDot;
                  MeanLine.AddXY(Session.ConvertUnits(BD.LowLimit),0,'',MeanLine.SeriesColor);
                  MeanLine.AddXY(Session.ConvertUnits(BD.LowLimit),bigsum*0.15,'',MeanLine.SeriesColor);
                  Chart.AddSeries(MeanLine);
              end;


              if 0 <> BD.HighLimit then begin
                  MeanLine := TLineSeries.Create(Self);
                  MeanLine.SeriesColor := S[i].SeriesColor;
                  MeanLine.ShowInLegend := false;
                  if printing then
                     MeanLine.LinePen.Width := 0
                  else
                     MeanLine.LinePen.Width := 1;

                  MeanLine.LinePen.Style := psDot;
                  MeanLine.AddXY(Session.ConvertUnits(BD.HighLimit),0,'',MeanLine.SeriesColor);
                  MeanLine.AddXY(Session.ConvertUnits(BD.HighLimit),bigsum*0.15,'',MeanLine.SeriesColor);
                  Chart.AddSeries(MeanLine);
              end;
          end;



          if PointsOnDensityMenuItem.Checked then begin
              PointSeries := TPointSeries.Create(Self);
              PointSeries.SeriesColor := S[i].SeriesColor;
              PointSeries.ShowInLegend := false;
              PointSeries.Pointer.Style := TSeriesPointerStyle(i mod 4); // PStyle[p];

              try
                  for j := i*BD.Rows to (i+1)*BD.Rows-1 do
                  begin
                       PointSeries.AddXY(Session.Data[j],bigsum*0.95*(j - i*BD.Rows)/BD.Rows,'',PointSeries.SeriesColor);
                  end;
              except

              end;
              Chart.AddSeries(PointSeries);
          end;
     end;

end;

procedure TMaster.DrawQChart(Chart:TChart;BW:Boolean;Printing:Boolean);
var
   BD : TBatchData;
   S :  TLineSeries;
   x : single;
   i : integer;
   y : single;
   minVal : single;
   maxVal : single;
   color : array [0..CMAX_GRAPH_COLUMNS] of TColor;
   MeanLine : TLineSeries;
begin

     BD := Session.BatchData;
     if not assigned(BD) then Exit;

     Chart.FreeAllSeries;
     if not BW then
        Chart.UndoZoom;
     Chart.View3D := View3DMenuItem.Checked;

     if BW or Printing then begin
        Chart.BackColor := clWhite;
        Chart.LeftAxis.Grid.Width := 0;
        Chart.BottomAxis.Grid.Width := 0;
     end else begin
         if View3DMenuItem.Checked then
            Chart.BackColor := clWhite
         else
             Chart.BackColor := clSilver;
     end;

     Chart.BottomAxis.Grid.Visible := GridMenuItem.Checked;
     Chart.LeftAxis.Grid.Visible := GridMenuItem.Checked;

     if Session.Units = Mils then
        Chart.LeftAxis.Title.Caption := 'Thickness (Mils)'
     else
        Chart.LeftAxis.Title.Caption := 'Thickness (Microns)';

     Chart.LeftAxis.AxisValuesFormat := Session.UnitFormatString(Session.Avg[-1]);

     Chart.BottomAxis.Title.Caption := 'Fraction of Data';

     minVal := 999;
     maxVal := 0;

     for i := 0 to BD.Columns-1 do begin
         if not BD.ColumnEnabled[i] then continue;
         S := TLineSeries.Create(Self);
         S.SeriesColor := clTeeColor;
         if BW then begin
            S.LinePen.Style := GetLineStyle(i);
            S.LinePen.Color := clBlack;
            S.SeriesColor := clBlack;
//            S.Pointer.Style := GetMarkerStyle(i);
//            S.Pointer.Visible := true;

         end;

         if printing then
            S.LinePen.Width := 0;

         x := 0.05;
         while x < 0.95 do begin
               y := Session.ComputeQuantile(i,x);
               x := x + 0.01;
               if y = CINVALID_READING then continue;
               if y < minVal then
                  minVal := y;
               if y > maxVal then
                  maxVal := y;
               S.AddXY(x,y,'',S.SeriesColor);
         end;
        S.Title := BD.ColumnName[i];

        Chart.AddSeries(S);

        color[i] := S.SeriesColor;

          if AvgMenuItem.Checked then begin
              MeanLine := TLineSeries.Create(Self);
              MeanLine.SeriesColor := S.SeriesColor;
              MeanLine.ShowInLegend := false;
              if printing then
                 MeanLine.LinePen.Width := 0
              else
                 MeanLine.LinePen.Width := 1;
              MeanLine.AddXY(0,Session.Avg[i],'',MeanLine.SeriesColor);
              MeanLine.AddXY(1.0,Session.Avg[i],'',MeanLine.SeriesColor);
              Chart.AddSeries(MeanLine);
          end;

          if StdDeviationMenuItem.Checked then begin
              MeanLine := TLineSeries.Create(Self);
              MeanLine.SeriesColor := S.SeriesColor;
              MeanLine.ShowInLegend := false;
              if printing then
                 MeanLine.LinePen.Width := 0
              else
                 MeanLine.LinePen.Width := 1;

              MeanLine.LinePen.Style := psDash;
              MeanLine.AddXY(0,Session.Avg[i] - Session.StandardDeviation[i],'',MeanLine.SeriesColor);
              MeanLine.AddXY(1.0,Session.Avg[i]- Session.StandardDeviation[i],'',MeanLine.SeriesColor);
              Chart.AddSeries(MeanLine);

              MeanLine := TLineSeries.Create(Self);
              MeanLine.SeriesColor := S.SeriesColor;
              MeanLine.ShowInLegend := false;
              if printing then
                 MeanLine.LinePen.Width := 0
              else
                 MeanLine.LinePen.Width := 1;
              MeanLine.LinePen.Style := psDash;
              MeanLine.AddXY(0,Session.Avg[i] + Session.StandardDeviation[i],'',MeanLine.SeriesColor);
              MeanLine.AddXY(1.0,Session.Avg[i]+ Session.StandardDeviation[i],'',MeanLine.SeriesColor);
              Chart.AddSeries(MeanLine);
          end;

          if ShowLimitsMenuItem.Checked and (TooHigh = BD.LimitCheck[-1]) then begin
             if BD.LowLimit <> 0 then begin
                  MeanLine := TLineSeries.Create(Self);
                  MeanLine.SeriesColor := S.SeriesColor;
                  MeanLine.ShowInLegend := false;
                  if printing then
                     MeanLine.LinePen.Width := 0
                  else
                     MeanLine.LinePen.Width := 1;
                  MeanLine.LinePen.Style := psDot;
                  MeanLine.AddXY(0,Session.ConvertUnits(BD.LowLimit),'',MeanLine.SeriesColor);
                  MeanLine.AddXY(1.0,Session.ConvertUnits(BD.LowLimit),'',MeanLine.SeriesColor);
                  Chart.AddSeries(MeanLine);

                  if Session.ConvertUnits(BD.LowLimit) < minVal then
                     minVal := Session.ConvertUnits(BD.LowLimit);
             end;

             if BD.HighLimit <> 0 then begin
                  MeanLine := TLineSeries.Create(Self);
                  MeanLine.SeriesColor := S.SeriesColor;
                  MeanLine.ShowInLegend := false;
                  if printing then
                     MeanLine.LinePen.Width := 0
                  else
                     MeanLine.LinePen.Width := 1;
                  MeanLine.LinePen.Style := psDot;
                  MeanLine.AddXY(0,Session.ConvertUnits(BD.HighLimit),'',MeanLine.SeriesColor);
                  MeanLine.AddXY(1.0,Session.ConvertUnits(BD.HighLimit),'',MeanLine.SeriesColor);
                  Chart.AddSeries(MeanLine);

                  if Session.ConvertUnits(BD.HighLimit) > maxVal then
                     maxVal := Session.ConvertUnits(BD.HighLimit);

             end;
          end;
     end;

   Chart.LeftAxis.SetMinMax(minVal - (maxVal - minVal) * 0.05,maxVal + (maxVal - minVal) * 0.05);

    if QuantileBarsMenuItem.Checked then begin
        for i := 0 to BD.Columns -1 do
        begin
              if not BD.ColumnEnabled[i] then continue;

              if CINVALID_READING <> Session.ComputeQuantile(i,0.25) then begin
                  MeanLine := TLineSeries.Create(Self);
                  MeanLine.SeriesColor := color[i];
                  MeanLine.ShowInLegend := false;
                  if printing then
                     MeanLine.LinePen.Width := 0
                  else
                     MeanLine.LinePen.Width := 1;

                  MeanLine.LinePen.Style := psDot;
                  MeanLine.AddXY(0,Session.ComputeQuantile(i,0.25),'',MeanLine.SeriesColor);
                  MeanLine.AddXY(0.25,Session.ComputeQuantile(i,0.25),'',MeanLine.SeriesColor);
                  MeanLine.AddXY(0.25,minVal,'',MeanLine.SeriesColor);
                  MeanLine.AddXY(0.25,Session.ComputeQuantile(i,0.25),'',MeanLine.SeriesColor);
                  Chart.AddSeries(MeanLine);
              end;

              if CINVALID_READING <> Session.ComputeQuantile(i,0.75) then begin
                  MeanLine := TLineSeries.Create(Self);
                  MeanLine.SeriesColor := color[i];
                  MeanLine.ShowInLegend := false;
                  if printing then
                     MeanLine.LinePen.Width := 0
                  else
                     MeanLine.LinePen.Width := 1;

                  MeanLine.LinePen.Style := psDot;
                  MeanLine.AddXY(0,Session.ComputeQuantile(i,0.75),'',MeanLine.SeriesColor);
                  MeanLine.AddXY(0.75,Session.ComputeQuantile(i,0.75),'',MeanLine.SeriesColor);
                  MeanLine.AddXY(0.75,minVal,'',MeanLine.SeriesColor);
                  MeanLine.AddXY(0.75,Session.ComputeQuantile(i,0.75),'',MeanLine.SeriesColor);
                  Chart.AddSeries(MeanLine);
              end;
        end;
    end;    // end if quantile turned on
end;

procedure TMaster.DrawBoxChart(Chart:TChart;BW:Boolean;Printing:Boolean);
var
   BD : TBatchData;
   S :  TBoxSeries;
   i,j : integer;
   y : single;
   minVal : single;
   maxVal : single;
   iqr,upperQ,lowerQ,Q75,Q25 : single;
   MeanLine : TLineSeries;
   PointSeries : TPointSeries;

begin

     BD := Session.BatchData;
     if not assigned(BD) then Exit;

     Chart.FreeAllSeries;
     if not BW then
        Chart.UndoZoom;
     Chart.View3D := View3DMenuItem.Checked;

     if BW or Printing then begin
        Chart.BackColor := clWhite;
        Chart.LeftAxis.Grid.Width := 0;
        Chart.BottomAxis.Grid.Width := 0;
     end else begin
         if View3DMenuItem.Checked then
            Chart.BackColor := clWhite
         else
             Chart.BackColor := clSilver;
     end;

     Chart.BottomAxis.Grid.Visible := GridMenuItem.Checked;
     Chart.LeftAxis.Grid.Visible := GridMenuItem.Checked;

     if Session.Units = Mils then
        Chart.LeftAxis.Title.Caption := 'Thickness (Mils)'
     else
        Chart.LeftAxis.Title.Caption := 'Thickness (Microns)';

     Chart.BottomAxis.Title.Caption := 'Column';
     Chart.LeftAxis.AxisValuesFormat := Session.UnitFormatString(Session.Avg[-1]);

     minVal := 999;
     maxVal := 0;

     S := nil;

     for i := 0 to BD.Columns-1 do begin
         if not BD.ColumnEnabled[i] then continue;
         if S = nil then begin
             S := TBoxSeries.Create(Self);
             S.SeriesColor := clTeeColor;
             S.UpCloseColor := clGreen;
             S.DownCloseColor := clRed;
             if BW then begin
                S.LinePen.Style := GetLineStyle(i);
                S.LinePen.Color := clBlack;
                S.SeriesColor := clBlack;
                S.UpCloseColor := clWhite;
                S.DownCloseColor := clWhite;
    //            S.Pointer.Style := GetMarkerStyle(i);
    //            S.Pointer.Visible := true;

             end;
         end;
         Q25 := Session.ComputeQuantile(i,0.25);
         if CINVALID_READING = Q25 then
            Q25 := Session.Min[i];

         Q75 := Session.ComputeQuantile(i,0.75);
         if CINVALID_READING = Q75 then
            Q75 := Session.Max[i];

         iqr := Q75 - Q25;

         upperQ := iqr * 1.5 + Q75;
         lowerQ := Q25 - iqr * 1.5;

         for j := BD.RowCount[i]-1 downto 0 do
         begin
              if Session.ConvertUnits(BD.SortData[i*BD.Rows+j]) <= upperQ then begin
                 upperQ := Session.ConvertUnits(BD.SortData[i*BD.Rows+j]);
                 break;
              end;
         end;

         for j := 0 to BD.RowCount[i]-1 do
         begin
              if Session.ConvertUnits(BD.SortData[i*BD.Rows+j]) >= lowerQ then begin
                 lowerQ := Session.ConvertUnits(BD.SortData[i*BD.Rows+j]);
                 break;
              end;
         end;

         if upperQ > maxVal then
            maxVal := upperQ;

         if lowerQ < minVal then
            minVal := lowerQ;

         S.AddBox(BD.ColumnName[i],Q25,upperQ,lowerQ,Q75,
             Session.ComputeQuantile(i,0.50));

         if (Session.Max[i] > upperQ) or
            (Session.Min[i] < lowerQ) then begin
                // add outliers
              PointSeries := TPointSeries.Create(Self);
              PointSeries.SeriesColor := clBlack;
              PointSeries.ShowInLegend := false;
              PointSeries.Pointer.Style := psTriangle;

              try
                  for j := 0 to BD.RowCount[i]-1 do
                  begin
                       y := Session.Data[j+i*BD.Rows];
                       if CINVALID_READING = y then
                          continue;
                       if (y < lowerQ) or (y > upperQ) then begin
                          if y < minVal then
                             minVal := y;
                          if y > maxVal then
                             maxVal := y;

                          PointSeries.AddXY(i,y,'',PointSeries.SeriesColor);
                       end;
                  end;
              except

              end;

              Chart.AddSeries(PointSeries);
          end;    // end if adding outliers


          if AvgMenuItem.Checked then begin
              MeanLine := TLineSeries.Create(Self);
              MeanLine.SeriesColor := clBlack;
              MeanLine.ShowInLegend := false;
              if printing then
                 MeanLine.LinePen.Width := 0
              else
                 MeanLine.LinePen.Width := 1;
              MeanLine.AddXY(i-0.4,Session.Avg[i],'',MeanLine.SeriesColor);
              MeanLine.AddXY(i+0.4,Session.Avg[i],'',MeanLine.SeriesColor);
              Chart.AddSeries(MeanLine);
          end;

          if StdDeviationMenuItem.Checked then begin
              MeanLine := TLineSeries.Create(Self);
              MeanLine.SeriesColor := clBlack;
              MeanLine.ShowInLegend := false;
              if printing then
                 MeanLine.LinePen.Width := 0
              else
                 MeanLine.LinePen.Width := 1;
              MeanLine.LinePen.Style := psDash;
              MeanLine.AddXY(i-0.3,Session.Avg[i] - Session.StandardDeviation[i],'',MeanLine.SeriesColor);
              MeanLine.AddXY(i+0.3,Session.Avg[i]- Session.StandardDeviation[i],'',MeanLine.SeriesColor);
              Chart.AddSeries(MeanLine);

              MeanLine := TLineSeries.Create(Self);
              MeanLine.SeriesColor := clBlack;
              MeanLine.ShowInLegend := false;
              if printing then
                 MeanLine.LinePen.Width := 0
              else
                 MeanLine.LinePen.Width := 1;
              MeanLine.LinePen.Style := psDash;
              MeanLine.AddXY(i-0.3,Session.Avg[i] + Session.StandardDeviation[i],'',MeanLine.SeriesColor);
              MeanLine.AddXY(i+0.3,Session.Avg[i]+ Session.StandardDeviation[i],'',MeanLine.SeriesColor);
              Chart.AddSeries(MeanLine);
          end;

     end;        // end for each column

     Chart.AddSeries(S);


      if ShowLimitsMenuItem.Checked and (TooHigh = BD.LimitCheck[-1]) then begin
         if BD.LowLimit <> 0 then begin
              MeanLine := TLineSeries.Create(Self);
              MeanLine.SeriesColor := clBlack;
              MeanLine.ShowInLegend := false;
              if printing then
                 MeanLine.LinePen.Width := 0
              else
                 MeanLine.LinePen.Width := 1;
              MeanLine.LinePen.Style := psDot;
              MeanLine.AddXY(-0.5,Session.ConvertUnits(BD.LowLimit),'',MeanLine.SeriesColor);
              MeanLine.AddXY(BD.EnabledColumnCount-0.5,Session.ConvertUnits(BD.LowLimit),'',MeanLine.SeriesColor);
              Chart.AddSeries(MeanLine);

              if Session.ConvertUnits(BD.LowLimit) < minVal then
                 minVal := Session.ConvertUnits(BD.LowLimit);
         end;

         if BD.HighLimit <> 0 then begin
              MeanLine := TLineSeries.Create(Self);
              MeanLine.SeriesColor := clBlack;
              MeanLine.ShowInLegend := false;
              if printing then
                 MeanLine.LinePen.Width := 0
              else
                 MeanLine.LinePen.Width := 1;
              MeanLine.LinePen.Style := psDot;
              MeanLine.AddXY(-0.5,Session.ConvertUnits(BD.HighLimit),'',MeanLine.SeriesColor);
              MeanLine.AddXY(BD.EnabledColumnCount-0.5,Session.ConvertUnits(BD.HighLimit),'',MeanLine.SeriesColor);
              Chart.AddSeries(MeanLine);

              if Session.ConvertUnits(BD.HighLimit) > maxVal then
                 maxVal := Session.ConvertUnits(BD.HighLimit);

         end;
      end;   // end if show limits

     iqr := maxVal - minVal;
     Chart.LeftAxis.SetMinMax(minVal-iqr/20,maxVal+iqr/20);
     Chart.LeftAxis.SetMinMax(minVal - (maxVal - minVal) * 0.05,maxVal + (maxVal - minVal) * 0.05);

     Chart.BottomAxis.SetMinMax(-0.5,BD.EnabledColumnCount-0.5);

end;

procedure TMaster.CreateBoxChart;
begin
    DrawBoxChart(BGChart,false,false);
end;


procedure TMaster.CreateQChart;
begin
    DrawQChart(QChart,false,false);
end;

procedure TMaster.CreateGChart;
begin
    DrawGChart(GChart,false,false);
end;

procedure TMaster.DrawGChart(Chart:TChart;BW:Boolean;Printing:Boolean);
var
   BD : TBatchData;
   S : array [0..CMAX_GRAPH_COLUMNS] of TPointSeries;
   i : integer;
   p : integer;
   y : single;
   col : integer;
   row : integer;
   maxcols : integer;
   MeanLine : TLineSeries;
begin
     BD := Session.BatchData;
     if not assigned(BD) then Exit;

     Chart.UndoZoom;
     Chart.FreeAllSeries;
     Chart.Title.Text.Clear;
     Chart.Title.Text.Add(BD.Description + ' Data Chart');
     Chart.View3D := View3DMenuItem.Checked;

     if BW or Printing then begin
        Chart.BackColor := clWhite;
        Chart.LeftAxis.Grid.Width := 0;
        Chart.BottomAxis.Grid.Width := 0;
     end else begin
         if View3DMenuItem.Checked then
            Chart.BackColor := clWhite
         else
             Chart.BackColor := clSilver;
     end;

     Chart.BottomAxis.Grid.Visible := GridMenuItem.Checked;
     Chart.LeftAxis.Grid.Visible := GridMenuItem.Checked;

     if Session.Units = Mils then
        Chart.LeftAxis.Title.Caption := 'Thickness (Mils)'
     else
        Chart.LeftAxis.Title.Caption := 'Thickness (Microns)';

     Chart.LeftAxis.AxisValuesFormat := Session.UnitFormatString(Session.Avg[-1]);

     maxcols := BD.Columns;
     if maxcols > CMAX_GRAPH_COLUMNS then
        maxcols := CMAX_GRAPH_COLUMNS;

     for i := 0 to maxcols-1 do begin
         if BD.ColumnEnabled[i] then begin
             S[i] := TPointSeries.Create(Self);
             S[i].SeriesColor := clTeeColor;
             if BW then begin
                S[i].LinePen.Style := GetLineStyle(i);
                S[i].LinePen.Color := clBlack;
                S[i].SeriesColor := clBlack;
             end;
         end else
             S[i] := nil;
     end;

     for i := 0 to BD.Count-1 do
     begin
          col := i div BD.Rows;
          if col >= maxcols then continue;

          if BD.ColumnEnabled[col] then begin
             y := Session.Data[i];
             if CINVALID_READING = y then
                continue;
             S[col].AddY(y,'',S[col].SeriesColor);
          end;
     end;
     for i := 0 to maxcols-1 do
     begin
          if not BD.ColumnEnabled[i] then continue;

          p := i mod 4;
          S[i].Pointer.Style := TSeriesPointerStyle(p); // PStyle[p];
          S[i].Title := BD.ColumnName[i];

          Chart.AddSeries(S[i]);

          if AvgMenuItem.Checked then begin
              MeanLine := TLineSeries.Create(Self);
              MeanLine.SeriesColor := S[i].SeriesColor;
              MeanLine.ShowInLegend := false;
              if Printing then
                 MeanLine.LinePen.Width := 0
              else
                  MeanLine.LinePen.Width := 1;

              MeanLine.AddXY(0,Session.Avg[i],'',MeanLine.SeriesColor);
              MeanLine.AddXY(BD.Rows-1,Session.Avg[i],'',MeanLine.SeriesColor);
              Chart.AddSeries(MeanLine);
          end;

          if StdDeviationMenuItem.Checked then begin
              MeanLine := TLineSeries.Create(Self);
              MeanLine.SeriesColor := S[i].SeriesColor;
              MeanLine.ShowInLegend := false;
              if Printing then
                 MeanLine.LinePen.Width := 0
              else
                  MeanLine.LinePen.Width := 1;
              MeanLine.LinePen.Style := psDash;
              MeanLine.AddXY(0,Session.Avg[i] - Session.StandardDeviation[i],'',MeanLine.SeriesColor);
              MeanLine.AddXY(BD.Rows-1,Session.Avg[i]- Session.StandardDeviation[i],'',MeanLine.SeriesColor);
              Chart.AddSeries(MeanLine);

              MeanLine := TLineSeries.Create(Self);
              MeanLine.SeriesColor := S[i].SeriesColor;
              MeanLine.ShowInLegend := false;
              if Printing then
                 MeanLine.LinePen.Width := 0
              else
                  MeanLine.LinePen.Width := 1;

              MeanLine.LinePen.Style := psDash;
              MeanLine.AddXY(0,Session.Avg[i] + Session.StandardDeviation[i],'',MeanLine.SeriesColor);
              MeanLine.AddXY(BD.Rows-1,Session.Avg[i]+ Session.StandardDeviation[i],'',MeanLine.SeriesColor);
              Chart.AddSeries(MeanLine);
          end;

          if ShowLimitsMenuItem.Checked and (TooHigh = BD.LimitCheck[-1]) then begin
             if BD.LowLimit <> 0 then begin
                  MeanLine := TLineSeries.Create(Self);
                  MeanLine.SeriesColor := S[i].SeriesColor;
                  MeanLine.ShowInLegend := false;
                  if Printing then
                     MeanLine.LinePen.Width := 0
                  else
                      MeanLine.LinePen.Width := 1;
                  MeanLine.LinePen.Style := psDot;
                  MeanLine.AddXY(0,Session.ConvertUnits(BD.LowLimit),'',MeanLine.SeriesColor);
                  MeanLine.AddXY(BD.Rows-1,Session.ConvertUnits(BD.LowLimit),'',MeanLine.SeriesColor);
                  Chart.AddSeries(MeanLine);
             end;

             if BD.HighLimit <> 0 then begin
                  MeanLine := TLineSeries.Create(Self);
                  MeanLine.SeriesColor := S[i].SeriesColor;
                  MeanLine.ShowInLegend := false;
                  if Printing then
                     MeanLine.LinePen.Width := 0
                  else
                      MeanLine.LinePen.Width := 1;
                  MeanLine.LinePen.Style := psDot;
                  MeanLine.AddXY(0,Session.ConvertUnits(BD.HighLimit),'',MeanLine.SeriesColor);
                  MeanLine.AddXY(BD.Rows-1,Session.ConvertUnits(BD.HighLimit),'',MeanLine.SeriesColor);
                  Chart.AddSeries(MeanLine);
             end;
          end;

     end;

end;


procedure TMaster.SetLegendData;
var
   BD : TBatchData;
begin
     ClearLegendData;
     BD := Session.BatchData;

     if not assigned(BD) then Exit;

    MBatchName.Caption := BD.Description;
    MReadings.Caption := IntToStr(BD.Count);
    MRows.Caption := IntToStr(BD.Rows);
    MCols.Caption := IntToStr(BD.Columns);

    if BD.CamMode <> 0 then
       MCamCount.Caption := IntToStr(BD.CamMode);

    if BD.LowLimit <> 0 then
       MLowLimit.Caption := Session.ConvertValueToString(Session.ConvertUnits(BD.LowLimit));

    if BD.HighLimit <> 0 then
       MHighLimit.Caption := Session.ConvertValueToString(Session.ConvertUnits(BD.HighLimit));

    if BD.StartTime <> 0 then
    begin
         MStartDate.Caption := DateToStr(BD.StartTime);
         MStartTime.Caption := TimeToStr(BD.StartTime);
    end;

    if BD.EndTime <> 0 then
    begin
         MEndDate.Caption := DateToStr(BD.EndTime);
         MEndTime.Caption := TimeToStr(BD.EndTime);
    end;

end;

procedure TMaster.ClearLegendData;
begin
    MBatchName.Caption :='';
    MReadings.Caption := '';
    MRows.Caption :='';
    MCols.Caption :='';
    MCamCount.Caption :='';
    MLowLimit.Caption :='';
    MHighLimit.Caption :='';
    MStartDate.Caption :='';
    MStartTime.Caption :='';
    MEndDate.Caption :='';
    MEndTime.Caption :='';

end;

procedure TMaster.FormShow(Sender: TObject);
var
   reg : TRegIniFile;
begin
     reg := TRegIniFile.Create(CBASEREGKEY);
     if -1 <> reg.ReadInteger('Display','Left',-1) then begin
        Left := reg.ReadInteger('Display','Left',0);
        Top :=  reg.ReadInteger('Display','Top',0);
        Width := reg.ReadInteger('Display','Width',0);
        Height := reg.ReadInteger('Display','Height',0);
     end;
     Reg.Free;

     ClearScreen;
end;

procedure TMaster.PageControlChange(Sender: TObject);
begin
     Refresh;
end;

procedure TMaster.DataGridDrawCell(Sender: TObject; Col, Row: Integer;
  Rect: TRect; State: TGridDrawState);
var
   TG : TStringGrid;
begin
     TG := Sender as TStringGrid;
     TG.Color := clRed;


end;

procedure TMaster.DataGridPreDrawCell(Sender: TObject; Col, Row: Integer);
var
   index : integer;
begin
     if (Col < 1) or (row < CFIXED_DATA_ROWS) then Exit;
     index := (row - CFIXED_DATA_ROWS) + (Col -1 ) * Session.BatchData.Rows;

     if index >= Session.BatchData.Count then begin
        Exit;
     end;
     case Session.BatchData.LimitCheck[index] of
      TooLow : DataGrid.Canvas.Font.Color := clRed;
      TooHigh  : DataGrid.Canvas.Font.Color := clBlue;
     else DataGrid.Canvas.Font.Color := clBlack;
     end;
end;

procedure TMaster.DataGridPostDrawCell(Sender: TObject);
begin
     DataGrid.Canvas.Font.Color := clBlack;
end;


procedure TMaster.FormClose(Sender: TObject; var Action: TCloseAction);
var
   Reg : TRegIniFile;
begin

    reg := TRegIniFile.Create(CBASEREGKEY);

    reg.WriteInteger('Display','Left',Left);
    reg.WriteInteger('Display','Top',Top);
    reg.WriteInteger('Display','Width',Width);
    reg.WriteInteger('Display','Height',Height);

    Reg.Free;

end;

procedure TMaster.CaptureMenuChoiceClick(Sender: TObject);
begin
     CaptureButton.Click;
end;

procedure TMaster.FilePrint(Sender: TObject);
begin
  if SelectBatchesForm.GetSelections('Print',Session) = 0 then
     Exit;

  QReportForm.Report.Print;
  Exit;
end;


procedure TMaster.FilePrintPreview(Sender: TObject);
begin
  if SelectBatchesForm.GetSelections('Print',Session) = 0 then
     Exit;

  QReportForm.Report.Preview;
end;

procedure TMaster.FilePrintSetup(Sender: TObject);
begin
     QReportForm.Report.PrinterSetup;
end;

procedure TMaster.FileOpen(Sender: TObject);
var
   iniFile : TIniFile;
begin
  OpenDialog.InitialDir := ReadPrefsString('LoadDir','');
  if OpenDialog.Execute then
  begin
    { Add code to open OpenDialog.FileName }
     SavePrefsString('LoadDir',GetPath(OpenDialog.FileName));
     ClearBatchTabs;
     ClearDataGrid;
     ClearChart;
     ClearScreen;
     EnableEditCopyMenu(false);

    iniFile := TiniFile.Create(OpenDialog.FileName);

    try
       Session.LoadFromIniFile(iniFile);
    finally
        iniFile.Free;
    end;
    if Session.BatchCount <> 0 then
    begin
        CreateBatchTabs;
        BatchTabControl.TabIndex := 0;

        EnableScreen;
        Refresh;
        EnableEditCopyMenu(true);
    end;
  end;
end;

procedure TMaster.FileSave(Sender: TObject);
begin
   { Add code to save current file under current name }
   FileSaveAs(Sender)
end;

procedure TMaster.FileSaveAs(Sender: TObject);
var
   iniFile : TIniFile;
begin
  SaveDialog.InitialDir := ReadPrefsString('SaveDir','');
  if SaveDialog.Execute then
     SavePrefsString('SaveDir',GetPath(SaveDialog.FileName));
      case SaveDialog.FilterIndex of
         1 : begin
               iniFile := TIniFile.Create(SaveDialog.FileName);

                { Add code to save current file under SaveDialog.FileName }
                try
                   Session.SaveToIniFile(iniFile);
                finally
                    iniFile.Free;
                end;
              end;
         2, 3 : begin
               AssignFile(TextFile,SaveDialog.FileName);
               ReWrite(TextFile);
               try
                  case SaveDialog.FilterIndex of
                    2: Session.SaveToTextFile(TextFile);
                    3: Session.SaveToCommaFile(TextFile);
                  end;
               finally;

               end;
               CloseFile(TextFile);
           end;
      end;  // end case

end;



procedure TMaster.AvgMenuItemClick(Sender: TObject);
begin
     AvgMenuItem.Checked := not AvgMenuItem.Checked;
     SavePrefs;
     Refresh;
end;

procedure TMaster.StdDeviationMenuItemClick(Sender: TObject);
begin

     StdDeviationMenuItem.Checked := not StdDeviationMenuItem.Checked;
     SavePrefs;
     Refresh;
end;

procedure TMaster.SavePrefs;

var
   reg : TRegIniFile;

begin
    reg := TRegIniFile.Create(CBASEREGKEY);

    reg.WriteBool(CPREFS,'ShowStdDev',StdDeviationMenuItem.Checked);
    reg.WriteBool(CPREFS,'ShowAvg',AvgMenuItem.Checked);
    reg.WriteBool(CPREFS,'View3D',View3DMenuItem.Checked);
    reg.WriteBool(CPREFS,'ShowGrid',GridMenuItem.Checked);
    reg.WriteBool(CPREFS,'ShowPointsOnDensity',PointsOnDensityMenuItem.Checked);
    reg.WriteBool(CPREFS,'QuantileBarsOn',QuantileBarsMenuItem.Checked);
    reg.WriteBool(CPREFS,'ShowLimits',ShowLimitsMenuItem.Checked);

    reg.Free;

end;

procedure TMaster.View3DMenuItemClick(Sender: TObject);
begin
     View3DMenuItem.Checked := not View3DMenuItem.Checked;
     SavePrefs;
     Refresh;
end;

procedure TMaster.GridMenuItemClick(Sender: TObject);
begin
     GridMenuItem.Checked := not GridMenuItem.Checked;
     SavePrefs;
     Refresh;
end;

procedure TMaster.PointsOnDensityMenuItemClick(Sender: TObject);
begin
     PointsOnDensityMenuItem.Checked := not PointsOnDensityMenuItem.Checked;
     SavePrefs;
     Refresh;
end;

procedure TMaster.EditMilsItemClick(Sender: TObject);
begin
     EditMilsItem.Checked := not EditMilsItem.Checked;
     EditMicronsItem.Checked := not EditMilsItem.Checked;
     UnitsButton.Down := not EditMilsItem.Checked;
     UnitsButton.Click;
end;

procedure TMaster.EditMicronsItemClick(Sender: TObject);
begin
     EditMicronsItem.Checked := not EditMicronsItem.Checked;
     EditMilsItem.Checked := not EditMicronsItem.Checked;
     UnitsButton.Down := EditMicronsItem.Checked;
     UnitsButton.Click;
end;

procedure TMaster.QuantileBarsMenuItemClick(Sender: TObject);
begin
     QuantileBarsMenuItem.Checked := not QuantileBarsMenuItem.Checked;
     SavePrefs;
     Refresh;
end;

procedure TMaster.ShowLimitsMenuItemClick(Sender: TObject);
begin
     ShowLimitsMenuItem.Checked := not ShowLimitsMenuItem.Checked;
     SavePrefs;
     Refresh;

end;

procedure TMaster.FileExitItemClick(Sender: TObject);
begin
     Application.Terminate;
end;


procedure TMaster.EditCopyItemClick(Sender: TObject);

    procedure SpaceToTabs(X:TMemo);
    var
       l : integer;
       i : integer;
       s,a : string;
       lastWasSpace : boolean;
       nextIsSpace : boolean;
    begin
         for l := 0 to X.Lines.Count do
         begin
              s := X.Lines.Strings[l];
              a := '';
              lastWasSpace := false;
              for i := 1 to Length(s) do
              begin
                   nextIsSpace := false;
                   if (i < Length(s)) and (s[i+1] = ' ') then
                         nextIsSpace := true;
                   if (S[i] = ' ') and nextIsSpace then begin
                      if lastWasSpace then continue;
                      a := a + #9;
                      lastWasSpace := true;
                   end else begin
                       lastWasSpace := false;
                       a := a + s[i];
                   end;
              end;
              X.Lines.Strings[l] := a;
         end;
    end;

var
   TM : TMemo;
begin

     if PageControl.ActivePage.Name = 'TabData' then begin
         QReportForm.Session := MySession;
         QReportForm.CreateBatchStatistics;
         TM := TMemo.Create(Self);
         try
             TM.Parent := Self;
             TM.Visible := false;
             TM.WordWrap := false;
             TM.Lines.Assign(QReportForm.StatsMemo.Lines);
             if Session.BatchData.Columns > 1 then
                QReportForm.CreateBatchVerticalReadings(false)
             else
                QReportForm.CreateBatchHorizontalReadings(false);

             TM.Lines.Add('');      // add a blank line
             TM.Lines.AddStrings(QReportForm.DataMemo.Lines);
             SpaceToTabs(TM);
             TM.SelStart := 0;
             TM.SelLength := 999999;
             TM.CopytoClipboard;
         finally
             TM.Free;
         end;
     end else if PageControl.ActivePage.Name = 'DensityGraphSheet' then begin
          CreateDensityChart;
          DChart.BackColor := clWhite;
          DChart.CopyToClipboardMetafile(true);
     end else if PageControl.ActivePage.Name = 'GraphSheet' then begin
         CreateGChart;
         GChart.BackColor := clWhite;
         GChart.CopyToClipboardMetafile(true);
     end else if PageControl.ActivePage.Name = 'BoxGraphSheet' then begin
         CreateBoxChart;
         BGChart.BackColor := clWhite;
         BGChart.CopyToClipboardMetafile(true);
     end else if PageControl.ActivePage.Name = 'QuantileGraphSheet' then begin
         CreateQChart;
         QChart.BackColor := clWhite;
         QChart.CopyToClipboardMetafile(true);
     end;

end;

procedure TMaster.HelpAboutItemClick(Sender: TObject);
begin
     AboutBox.ShowModal;
end;

procedure TMaster.ProbeExecuteMacro(Sender: TObject; Msg: TStrings);
var
   udata : PPacket;
   rc : integer;
begin
     Session.Gauge.PacketIO.Call(PROLP_NULL,[0],udata);
     
     if not Session.Gauge.GetSystemOffset then begin
        result.Text := 'Error:Probe not responding';
        Exit;
     end;

     if Msg.Strings[0] = 'RawFerrous' then begin
        rc := Session.Gauge.PacketIO.Call(PROLP_READF, [0],udata);
        if rc = 6 then
           result.Text := format('%u,%u,%u,%u,%u,%u',[udata.pdata[0],
                       udata.pdata[1],udata.pdata[2],udata.pdata[3],
                       udata.pdata[4],udata.pdata[5]])
        else
            result.Text := 'Error:General Read Error';

     end else if Msg.Strings[0] = 'RawNonFerrous' then begin
        rc := Session.Gauge.PacketIO.Call(PROLP_READNF, [5],udata);
        if rc = 2 then
           result.Text := format('%u,%u',[udata.pdata[0],
                       udata.pdata[1]])
        else
            result.Text := 'Error:General Read Error';
     end else begin
         result.Text := 'Error:Unknown Command';
     end;
end;


procedure TMaster.ExporttoExcel1Click(Sender: TObject);
begin
  if SelectBatchesForm.GetSelections('Export',Session) = 0 then
     Exit;

  FExcelSpreadsheet.Free;
  FExcelSpreadsheet := ExcelController.Create(Session);
  Exit;
end;

end.


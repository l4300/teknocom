unit Excel;

interface

uses session,  Graphics, Excel_TLB;

type
   ExcelController = class(TObject)
   private
      ExcelApplication : _Application;
      FSession:TGSession;
   protected
      procedure Populate;
      procedure PopulateBatch(Batch:Integer;wb:Variant);
      function PopulateHeader(sheet:Variant):integer;
      procedure PopulateRows(sheet:Variant;StartDataRow:integer);
   public
      constructor Create(session:TGSession);
      destructor Destroy; override;
   end;

implementation

uses Gauge, SysUtils, comObj;

const
   startcolumn:integer = 3;
   startrow:integer = 5;

constructor ExcelController.Create(session:TGSession);
begin

     ExcelApplication := CoApplication.Create();
     FSession := session;
     Populate;
     ExcelApplication.Visible[0] := true;
     FSession := nil;
end;

destructor ExcelController.Destroy;
begin
     ExcelApplication.Quit;
end;

procedure ExcelController.Populate;
var
   v: Variant;
   Batch : integer;
begin
     v := ExcelApplication.Workbooks.Add(xlWBatWorkSheet,0);
     for Batch := FSession.BatchCount-1  downto 0  do begin
         if FSession.Checked[Batch] then
            PopulateBatch(Batch,v);
     end;
end;

procedure ExcelController.PopulateBatch(Batch:Integer; wb:Variant);
var
  sheets: Variant;
  sheet: variant;
  lastrow:integer;
begin
     FSession.Batch := Batch;
     sheets := wb.Sheets;
     with FSession.BatchData do begin
          sheet := sheets.Add();
          if Description = '' then
             sheet.Name := 'Batch '+chr(65+Slot)
          else
             sheet.Name := Description;
          lastrow := PopulateHeader(sheet);
          PopulateRows(sheet,lastrow+2);
     end;
end;

function ExcelController.PopulateHeader(sheet:Variant):integer;
var
   row : integer;
   column :integer;
   i,j : integer;
   str,xstr : string;
begin
     row := startrow;  // start row
     column := startcolumn;
     with FSession.BatchData do begin
          sheet.Cells[row,column] := 'Count';
          sheet.Cells[row,column+1] := Count; inc(row);
          sheet.Cells[row,column] := 'Cam Mode';
          if CamMode <> 0 then
             sheet.Cells[row,column+1] := CamMode
          else
             sheet.Cells[row,column+1] := 'Off';
          inc(row);
          if TooHigh = LimitCheck[-1] then begin
             sheet.Cells[row,column] := 'Low Limit';
             if LowLimit <> 0 then
                sheet.Cells[row,column+1] := LowLimit
             else
                sheet.Cells[row,column+1] := 'None';
             sheet.Cells[row,column+1].Font.Color := clRed;
             inc(row);
             sheet.Cells[row,column] := 'High Limit';
             if HighLimit <> 0 then
                sheet.Cells[row,column+1] := HighLimit
             else
                sheet.Cells[row,column+1] := 'None';
             sheet.Cells[row,column+1].Font.Color := clBlue;
             inc(row);
          end;
          sheet.Cells[row,column] := 'Start';
          sheet.Cells[row,column+1] := DateTimeToStr(StartTime); inc(row);
          sheet.Cells[row,column] := 'End';
          sheet.Cells[row,column+1] := DateTimeToStr(EndTime); inc(row);
          sheet.Cells[row,column] := 'Units';
          if FSession.Units = Mils then
             sheet.Cells[row,column+1] := 'Mils'
          else
             sheet.Cells[row,column+1] := 'Microns';
          inc(row);
          for i := 0 to Questions.Count -1 do begin
             with TQA(Questions.Items[i]) do begin
                  sheet.Cells[row,column] := Question;
                  sheet.Cells[row,column+1] := Answer;
                  inc(row);
             end;
          end;

          try
             inc(row);
             for i:= 0 to Columns -1 do
                 sheet.Cells[row,column+1+i] := ColumnName[i];
             inc(row);
             for i := 0 to 5 do
             begin
                  if (4 = i) and (TooHigh <> LimitCheck[-1]) then
                     Break;
                  case i of
                    0: str := 'Min     ';
                    1: str := 'Max     ';
                    2: str := 'Average ';
                    3: str := 'Std Dev ';
                    4: str := 'Too Low ';
                    5: str := 'Too High';

                  end;

                  sheet.Cells[row,column] := str;
                  try
                     for j := 0 to Columns-1 do
                     begin
                          xstr := '';
                          case i of
                            0: xstr := format('%-8.8s',[FSession.MinText[j]]);
                            1: xstr := format('%-8.8s',[FSession.MaxText[j]]);
                            2: xstr := format('%-8.8s',[FSession.AvgText[j]]);
                            3: xstr := format('%-8.8s',[FSession.StandardDeviationText[j]]);
                            4: begin
                               xstr := format('%-8.8s',['  '+IntToStr(TooLowCount[j])]);
                               if TooLowCount[j] > 0 then
                                  sheet.Cells[row,column+1+j].Font.Color := clRed;
                               end;
                            5: begin
                               xstr := format('%-8.8s',['  '+IntToStr(TooHighCount[j])]);
                               if TooHighCount[j] > 0 then
                                  sheet.Cells[row,column+1+j].Font.Color := clBlue;
                               end;
                          end;
                          sheet.Cells[row,column+j+1] := xstr;
                     end;
                  except

                  end;
                  inc(row);
             end;
          except
          end;


     end;
     result := row;
end;

procedure ExcelController.PopulateRows(sheet:Variant;StartDataRow:integer);
var
   row : integer;
   col : integer;
   i,x : integer;
   colCount: integer;
   Cell : variant;
   s: String;
begin
     row := StartDataRow;  // start row
     col := startcolumn;
     with FSession.BatchData do begin
          colCount := Columns;
          if colCount < 1 then colCount := 1;

          for i := 0 to colCount -1 do begin
              sheet.Cells[row,startcolumn+i+1] := ColumnName[i];
          end;
          inc(row);
          for i := 0 to Rows -1 do begin
              sheet.Cells[row,startcolumn] := RowName[i];

              for x := 0 to colCount -1 do begin
                  try
                      col := startcolumn+1+x;
                      Cell := sheet.Cells[row,col];
                      s := FSession.Text[i+x*rows];
                      if s <> '' then begin
                         Cell.Value := StrToFloat(s);
                         case LimitCheck[i+x*Rows] of
                              TooHigh: Cell.Font.Color := clBlue;
                              TooLow: Cell.Font.Color := clRed;
                              InRange: ;
                         end;
                      end;
                  except
                  end;
              end;
              inc(row);
          end;
     end;
end;


end.

�
 TSELECTBATCHESFORM 0B  TPF0TSelectBatchesFormSelectBatchesFormLeftTop� BorderIcons
biMinimize
biMaximize BorderStylebsDialogCaptionSelect BatchesClientHeightkClientWidthFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style PositionpoScreenCenterPixelsPerInch`
TextHeight TLabelPromptLeftTopWidth	Height1	AlignmenttaCenterAutoSize  TPanelPrintOptionsPanelLeft Top� WidthHeight� AlignalBottomTabOrder TLabelLabel1LeftTopWidthHeightAlignalTop	AlignmenttaCenterCaptionPrint OptionsFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  	TCheckBoxPrintStatsCheckBoxLeftTopWidthYHeightCaptionPrint &StatisticsTabOrder OnClickPrintStatsCheckBoxClick  	TCheckBoxPrintDataCheckBoxLeftTop0WidthaHeightCaptionPrint &ReadingsTabOrderOnClickPrintDataCheckBoxClick  	TCheckBoxPrintGChartCheckBoxLeftTopHWidthaHeightCaptionPrint &Data PlotTabOrderOnClickPrintGChartCheckBoxClick  	TCheckBoxPrintDChartCheckBoxLeft� TopWidthyHeightCaptionPrint D&ensity PlotTabOrderOnClickPrintDChartCheckBoxClick  	TCheckBoxPrintQChartCheckBoxLeft� Top0WidthyHeightCaptionPrint &Quantile PlotTabOrderOnClickPrintQChartCheckBoxClick  	TCheckBoxPrintBGChartCheckBoxLeft� TopHWidthaHeightCaptionPrint Box PlotTabOrderOnClickPrintBGChartCheckBoxClick   TBitBtnOkButtonLeftTop0WidthKHeightCaption&PrintTabOrder KindbkOK  TBitBtnCancelButtonLeft� Top0WidthKHeightTabOrderKindbkCancel  TCheckListBoxCheckListBoxLeftTop@Width� HeightqHintSelect Desired Batches
ItemHeightItems.StringsBatch ABatch BBatch C ParentShowHintShowHint	TabOrder  TButtonSelectAllButtonLeft� TopHWidthKHeightCaptionSelect &AllTabOrderOnClickSelectAllButtonClick  TButtonClearAllButtonLeft� Top� WidthKHeightCaption
C&lear AllTabOrderOnClickClearAllButtonClick   
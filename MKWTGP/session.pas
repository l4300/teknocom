unit session;

interface

uses inifiles, globals, registry, scompf, dlform, Gauge, TPackio, dialogs, wintypes, SysUtils,
     forms, Classes,controls, Packet;

type
    TUnits = (Mils, Microns);

    TGSession = class(TComponent)
    private
        FGauge : TGauge;
        FPacketIO : TPacketIO;

        FSetComPortNameForm : TSetComPortNameForm;

        FBatchDataArray : array [0..CMAXBATCHES] of TBatchData;
        FPackedBatchDataArray : array [0..CMAXBATCHES] of TBatchData;
        FBatchCount : integer;  // number of batches
        FBatch : integer;       // which batch is current
        FBatchData : TBatchData;
        FUnits : TUnits;

        FSerialNumber : string;
        FModel : string;
        FVersion : string;
        FOemName : string;

    protected
        procedure Empty;
        function GetBatchData(Index:integer) : TBatchData;
        procedure SetBatch(batchnum:integer);
        function GetData(Index:integer) : single;
        function GetText(Index:integer) : string;


        function GetMin(Index:Integer) : single;
        function GetMax(Index:Integer) : single;
        function GetAvg(Index:Integer) : single;
        function GetStandardDeviation(Index:Integer) : single;

        function GetMinText(Index:Integer) : string;
        function GetMaxText(Index:Integer) : string;
        function GetAvgText(Index:Integer) : string;
        function GetStandardDeviationText(Index:Integer) : string;
        function ReadPrefsString(section:String;default:string) : string;
    public
      Checked : array [0..CMAXBATCHES] of Boolean;

      constructor Create(AOwner:TComponent); override;
      destructor Destroy; override;

      function InitializePort : boolean;     // check to see if port is ok
                                             // present dialog if not

      procedure ShutdownPort;
      function SetupComPortName(var xname:string) : boolean;   // true if set ok else cancelled
      function RetrieveComPortName(var name:string): boolean;  // gets it from reg
      procedure SaveComPortName(name:string);
      function CaptureAllData : boolean;

      function ConvertValueToString(value:single) : string;    // respects units
      function ConvertValueToPrintableString(value:single) : string;    // respects units

      function ConvertUnits(value:single) : single;   // convert from mils to microns if needed
      function ComputeQuantile(col:integer;Point:Single) : single;
      function SaveToIniFile(TI:TIniFile): boolean;
      function SaveToTextFile(var TextFile :Text): boolean;
      function SaveToCommaFile(var TextFile :Text): boolean;

      function LoadFromIniFile(TI:TIniFile) : boolean;

      function UnitFormatString(value:single) : string;
      // Properties here

      property Batch : integer read FBatch write SetBatch;
      property BatchCount : integer read FBatchCount;
      property BatchDataArray[Index:integer] : TBatchData read GetBatchData;
      property BatchData : TBatchData read FBatchData;

      property Gauge : TGauge read FGauge;
      property Units : TUnits read FUnits write FUnits;
      // all of these functions return values that correspond to the current
      // units setting
      property Data[Index:integer] : Single read GetData;
      property Text[Index:integer] : String read GetText;
      property Min[index:integer] : single read GetMin;
      property Max[index:integer] : single read GetMax;
      property Avg[index:integer] : single read GetAvg;
      property StandardDeviation[index:integer] : single read GetStandardDeviation;

      property MinText[Index:integer] : String read GetMinText;
      property MaxText[Index:integer] : String read GetMaxText;
      property AvgText[Index:integer] : String read GetAvgText;
      property StandardDeviationText[Index:integer] : string read GetStandardDeviationText;

      property SerialNumber : string read FSerialNumber;
      property Version : string read FVersion;
      property OemName : string read FOemName;
      property Model : string read FModel;

    published

    end;


procedure Register;

var
   MyApplication : TApplication;
   MySession : TGSession;

implementation

uses Masterf, AdPort;

const
     CI_HEADER:string = 'Header';

constructor TGSession.Create(AOwner:TComponent);
begin
     inherited Create(AOwner);

     FGauge := TGauge.Create(Self);
     FPacketIO := TPacketIO.Create(Self);
     FGauge.PacketIO := FPacketIO;
     MySession := Self;
end;

destructor TGSession.Destroy;
begin
     FGauge.Free;
     FPacketIO.Free;
     FSetComPortNameForm.Free;
end;

function TGSession.ReadPrefsString(section:String;default:string) : string;
var
   reg : TRegIniFile;
begin

    reg := TRegIniFile.Create(CBASEREGKEY);
    result := reg.readString(CPREFS,section,default);
    reg.free;
end;

function TGSession.ConvertValueToString(value:single) : string;
var
  dotpos : integer;
begin
     if value = CINVALID_READING then begin
         result := '';
         Exit;
     end;
     result := ConvertValueToPrintableString(value);
     dotpos := Pos('.',result);
     if 0 = dotpos then
        dotpos := Length(result);

     if dotpos < 4 then
        result := StringOfChar(' ',4 - dotpos) + result;
end;

function TGSession.ConvertValueToPrintableString(value:single) : string;
begin
     if value = CINVALID_READING then begin
         result := '';
         Exit;
     end;
     if Mils = Units then begin
        if value < 7 then
           result := format('%3.2f',[value])
        else
            result := format('%3.1f',[value]);
     end else begin
         if value < 100 then
            result := format('%3.1f',[value])
         else
             result := format('%4.0f',[value]);
     end;
//     result := format('%3.2f',[value]);
end;

function TGSession.UnitFormatString(value:single) : string;
begin
     if Mils = Units then begin
        if value < 7 then
           result := '#,##0.00'
        else
            result := '#,##0.0';
     end else begin
         if value < 100 then
            result := '#,##0.0'
         else
             result := '#,###';
     end;
end;

function TGSession.ComputeQuantile(col:integer;Point:Single) : single;
begin
     result := ConvertUnits(BatchData.ComputeQuantile(col,Point));
end;

function TGSession.ConvertUnits(value:single) : single;
begin
     if (FUnits = Microns) and (value <> CINVALID_READING) then
         result := value * 25.4
     else
         result := value;
end;

procedure TGSession.SetBatch(batchnum:integer);
begin
     if batchnum < FBatchCount then begin
        FBatch := batchnum;
        FBatchData := FPackedBatchDataArray[batchnum];
     end else
         raise ERangeError.Create('Invalid Batch Number');
end;

function TGSession.GetData(Index:integer) : single;
begin
     result := ConvertUnits(FBatchData.Data[Index]);
end;

function TGSession.GetText(Index:integer) : string;
begin
     result := ConvertValueToString(Data[Index]);
end;

function TGSession.GetMin(Index:Integer) : single;
begin
     result := ConvertUnits(FBatchData.Min[Index]);
end;

function TGSession.GetMax(Index:Integer) : single;
begin
     result := ConvertUnits(FBatchData.Max[Index]);
end;

function TGSession.GetAvg(Index:Integer) : single;
begin
     result := ConvertUnits(FBatchData.Avg[Index]);
end;

function TGSession.GetStandardDeviation(Index:Integer) : single;
begin
     result := ConvertUnits(FBatchData.StandardDeviation[Index]);
end;

function TGSession.GetMinText(Index:Integer) : string;
begin
     result := ConvertValueToString(Min[Index]);
end;

function TGSession.GetMaxText(Index:Integer) : string;
begin
     result := ConvertValueToString(Max[Index]);
end;

function TGSession.GetAvgText(Index:Integer) : string;
begin
     result := ConvertValueToString(Avg[Index]);
end;

function TGSession.GetStandardDeviationText(Index:Integer) : string;
begin
     result := ConvertValueToString(StandardDeviation[Index]);
end;

procedure TGSession.ShutdownPort;
begin
     FPacketIO.Close;
end;

function TGSession.GetBatchData(Index:integer) : TBatchData;
begin
     result := nil;

     if (index < 0) or (index > FBatchCount -1) then Exit;

     result := FPackedBatchDataArray[index];
end;

function TGSession.InitializePort : boolean;     // check to see if port is ok
var
   name : string;
begin

     result := true;

     if not RetrieveComPortName(name) then begin // no name set in registry
        if not SetupComPortName(name) then Exit;
     end;

     FPacketIO.ComPortName := name;
     if FPacketIO.Open then Exit;

     result := false;

     FGauge.ReportErrorMessage('Com Port "'+FPacketIO.ComPortName+'" Invalid.');
     if not SetupComPortName(name) then Exit;

     FPacketIO.ComPortName := name;
     if FPacketIO.Open then result := true;
end;

function TGSession.RetrieveComPortName(var name:string): boolean;  // gets it from reg
var
   reg : TRegIniFile;
begin
     reg := TRegIniFile.Create(CBASEREGKEY);
     name := reg.ReadString('Prefs','ComPortName','none');

     if name = 'none' then begin
        result := false;
        name := 'COM2';
     end else
        result := true;

     reg.Free;
end;

procedure TGSession.SaveComPortName(name:string);
var
   reg : TRegIniFile;
begin
     reg := TRegIniFile.Create(CBASEREGKEY);

     reg.WriteString('Prefs','ComPortName',name);
     reg.Free;
end;


function TGSession.SetupComPortName(var xname:string): boolean;
var
   rc : integer;
   name : string;
begin
     result := false;
     FPacketIO.Close;
     FSetComPortNameForm := TSetComPortNameForm.Create(nil);

     RetrieveComPortName(name);
     FSetComPortNameForm.ComPortName := name;

     while true do begin
         rc := FSetComPortNameForm.ShowModal;
         if mrOk <> rc then break;

         FPacketIO.ComPortName := FSetComPortNameForm.ComPortName;
         if not FPacketIO.Open then begin
            FGauge.ReportErrorMessage('Com Port "'+FPacketIO.ComPortName+'" Invalid.');

         end else begin
             FPacketIO.Close;
             SaveComPortName(FPacketIO.ComPortName);
             result := true;
             xname := FPacketIO.ComPortName;
             break;
         end;
     end;   // end repeat

     FSetComPortNameForm.Free;
     FSetComPortNameForm := nil;
end;

procedure TGSession.Empty;
var
   i : integer;

begin
     FGauge.EmptyCache;

     FBatchCount := 0;

     for i := 0 to  CMAXBATCHES -1 do
     begin
          if assigned(FBatchDataArray[i]) then begin
             FBatchDataArray[i].Free;
             FBatchDataArray[i] := nil;
          end;
     end;


end;

function TGSession.CaptureAllData : boolean;
label
   cleanup;

var
   DLForm : TDownloadForm;
   i : integer;
   BD : TBatchData;

begin
{$IFDEF DPacket}
     Master.ApdComPort.Tracing := tlOn;
     Master.ApdComPort.Logging := tlOn;

{$ENDIF}

     result := false;
     if not InitializePort then Exit;

     DLForm := TDownloadForm.Create(nil);

     DLForm.PacketIO := FPacketIO;
     DLForm.Gauge := FGauge;

     DLForm.SetDownloadMode(Searching);
     DLForm.Show;

     Empty;
     try
         while true do begin
               if 0 = FPacketIO.DetectVersion then begin
                  if CPERROR_CANCELLED = FPacketIO.LastError then
                     goto cleanup
                  else if CPERROR_TIMEOUT <> FPacketIO.LastError then begin
                       FGauge.ReportErrorMessage('Capture Error');
                       goto cleanup;
                  end;
               end else
                   break;
               MyApplication.ProcessMessages;
               if DLForm.Cancelled then goto cleanup;
         end;


         if DLForm.Cancelled then goto cleanup;
         if not FGauge.LoadMemoryInfo then begin
            DLForm.Hide;
            FGauge.ReportErrorMessage('Could not retrieve Batch Data');
            goto cleanup;
         end;
         // get here, the gauge has been located
         FSerialNumber := Gauge.SerialNumber;
         FModel := ReadPrefsString('Model'+Gauge.Model,Gauge.Model);
         FVersion := Gauge.Version;
         FOemName := Gauge.OemName;
     
         DLForm.SetDownloadMode(Reading);

         FPacketIO.ResetStats;

         for i := 0 to CMAXBATCHES-1 do
         begin
              MyApplication.ProcessMessages;
              if DLForm.Cancelled then goto cleanup;

              DLForm.SetCaption('Reading Batch '+Chr(i+65));
              BD := TBatchData.Load(FGauge,i);
              if nil = BD then begin
                 if CPERROR_CANCELLED = FPacketIO.LastError then goto cleanup;
                 DLForm.Hide;
                 FGauge.ReportErrorMessage('Batch Download Incomplete');
                 goto cleanup;
              end;
              if 0 <> BD.Count then begin
                  FBatchDataArray[i] := BD;
                  FPackedBatchDataArray[FBatchCount] := BD;
                  inc(FBatchCount);
              end else
                  BD.Free;

         end;
cleanup:
     finally


         if FBatchCount > 0 then
            Batch := 0;


         if FBatchCount <> 0 then result := true;

         DLForm.Free;
         ShutdownPort;

     end;   // end finally

{$IFDEF DPacket}
     Master.ApdComPort.Logging := tlDump;
     Master.ApdComPort.Tracing := tlDump;
{$ENDIF}

end;

function TGSession.LoadFromIniFile(TI:TIniFile): boolean;
var
   batches: string;
   x : integer;
   version : integer;
   count : integer;
   BD : TBatchData;
begin
     result := false;

     Empty;

     version := TI.ReadInteger(CI_HEADER,'Version',-1);
     if version = -1 then
        Exit;

     count := TI.ReadInteger(CI_HEADER,'BatchCount',0);
     if count < 1 then Exit;

     if TI.ReadString(CI_HEADER,'Units','Mils') = 'Mils' then
         FUnits := Mils
     else
         FUnits := Microns;

     FSerialNumber := TI.ReadString(CI_HEADER,'SerialNumber','');
     FModel := TI.ReadString(CI_HEADER,'Model','');
     FVersion := TI.ReadString(CI_HEADER,'GVersion','');
     FOemName := TI.ReadString(CI_HEADER,'OemName','');

     batches := TI.ReadString(CI_HEADER,'Batches','');
     FBatchCount := 0;

     for x := 0 to CMAXBATCHES-1 do
     begin
          if AnsiPos(Chr(x+65),batches) = 0 then continue;

          BD := TBatchData.LoadFromIni(TI,x);
          if nil = BD then continue;

          if 0 <> BD.Count then begin
              FBatchDataArray[x] := BD;
              FPackedBatchDataArray[FBatchCount] := BD;
              inc(FBatchCount);
          end else
              BD.Free;
     end;

    if FBatchCount > 0 then
        Batch := 0;

    if FBatchCount <> 0 then result := true;
end;

function TGSession.SaveToIniFile(TI:TIniFile): boolean;
var
   batches : string;
   x : integer;
begin
     TI.WriteInteger(CI_HEADER,'Version',1);
     TI.WriteInteger(CI_HEADER,'BatchCount',BatchCount);
     if Units = Mils then
        TI.WriteString(CI_HEADER,'Units','Mils')
     else
        TI.WriteString(CI_HEADER,'Units','Microns');

     TI.WriteString(CI_HEADER,'SerialNumber',FSerialNumber);
     TI.WriteString(CI_HEADER,'Model',FModel);
     TI.WriteString(CI_HEADER,'GVersion',FVersion);
     TI.WriteString(CI_HEADER,'OemName',FOemName);

     batches := '';
     for x := 0 to CMAXBATCHES-1 do
     begin
          if nil <> FBatchDataArray[x] then
             batches := batches + chr(x+65);
     end;

     TI.WriteString(CI_HEADER,'Batches',batches);

     for x := 0 to BatchCount -1 do
     begin
          try
             FPackedBatchDataArray[x].SaveToIniFile(TI);

          except


          end;
     end;
     result := true;
end;

function TGSession.SaveToTextFile(var TextFile : Text): boolean;
begin
     try
        BatchData.SaveToTextFile(TextFile);
     except

     end;
     result := true;

end;

function TGSession.SaveToCommaFile(var TextFile : Text): boolean;
begin
     try
        BatchData.SaveToCommaFile(TextFile);
     except

     end;
     result := true;
end;


procedure Register;
begin
  RegisterComponents('Gauge', [TGSession]);
end;

end.

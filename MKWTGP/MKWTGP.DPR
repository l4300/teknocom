program mkwtgp;

uses
  Forms,
  Main in 'MAIN.PAS' {MainForm},
  Packet in 'Packet.pas',
  Tpackio in 'Tpackio.pas',
  Gauge in 'Gauge.pas',
  masterf in 'masterf.pas' {Master},
  dlform in 'dlform.pas' {DownloadForm},
  session in 'session.pas',
  globals in 'globals.pas',
  scompf in 'scompf.pas' {SetComPortNameForm},
  csgrid in 'csgrid.pas',
  qreport in 'qreport.pas' {QReportForm},
  selbatch in 'selbatch.pas' {SelectBatchesForm},
  boxseri in 'boxseri.pas',
  aboutfrm in 'aboutfrm.pas' {AboutBox},
  Excel in 'Excel.pas';

{$R *.RES}

{$R *.TLB}

begin
  Packet.MyApplication := Application;
  Session.MyApplication := Application;
  Masterf.MyApplication := Application;
  Application.Title := 'Coating Thickness Gauge Program';

  Application.CreateForm(TMaster, Master);
  Application.CreateForm(TSelectBatchesForm, SelectBatchesForm);
  Application.CreateForm(TQReportForm, QReportForm);
  Application.CreateForm(TAboutBox, AboutBox);
  QReportForm.SetupBatchData(Master.Session);

  Packet.Acom := Master.ApdComPort;

  Application.Run;
end.

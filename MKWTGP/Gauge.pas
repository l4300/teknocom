unit gauge;

interface

uses inifiles, Math,TPackio, dialogs, wintypes, SysUtils,Classes,  Packet;


type

    TGFloat = record
            fval : array [0..3] of byte;
    end;

    TGFConstants = record
        fcVals : array [0..5] of TGFloat;
    end;

    PTGSysConstants = ^TGSysConstants;

    TGSysConstants = record
        scCsum : word;
        scStartMemBlocks : word;
        scEndMemBlocks : word;
        scSerialNumber : word;
        scFlags : word;
        scNFZero : word;
        scNFInfinity : word;
        scNFMaxInfinity : word;
        scNFMAxCalVariation : word;
        scNFStableReading : word;
        scNFInfMax : word;
        scNFInfRead : word;
        scNFConstants : TGFConstants;
        scFConstants : TGFConstants;
        scFiTempRef : TGFloat;
        scFiInternalRef : TGFloat;
        scFdeltaiActualRef : TGFloat;
        scFdeltaiActualRef0 : TGFloat;
        scFdeltaiTempRef : TGFloat;
        scFfCofRef : TGFloat;
        scFfMnRef : TGFloat;
        scFZeroOffset : word;
        scFStableReading : word;
    end;


    PTGDataBlock = ^TGDataBlock;
    TGDataBlock = record
        dbNext : word;
        pdata : array [0..14] of word;
    end;

    PTDBCache = ^TDBCache;
    TDBCache = record
        cNext : PTDBCache;
        cAddr : word;            // the base address of this record
        cDirty : boolean;           // true to write this db back
        Data : TGDataBlock;
    end;

    TGTimeInfo = record
        tiLow : word;
        tiHigh : word;
    end;

    TGSubBatchInfo = record
        sbEntryCount : word;
        sbFlags : byte;
        sbTemplate : byte;
        sbRows : byte;
        sbColumns : byte;
        sbCamMode : byte;
        sbSpare : byte;
        sbLimitLow : word;
        sbLimitHigh : word;
        sbDescription : array [0..7] of char;
        sbTimeStart : TGTimeInfo;
        sbTimeEnd : TGTimeInfo;
    end;

    PTGTemplateQuestion = ^TGTemplateQuestion;
    TGTemplateQuestion = record
        tqQuestion : array [0..7] of char;
        tqAnswerLink : word;  { points to datablock that contains list of possible answers }
    end;

    PTGTemplateQuestions = ^TGTemplateQuestions;
    TGTemplateQuestions = record
        tqNext: word;  { pointer to next data block }
        tqQuestions: array [0..2] of TGTemplateQuestion;
    end;

    PTGTemplateAnswer = ^TGTemplateAnswer;
    TGTemplateAnswer = record
        tqAnswer: array [0..9] of char;
    end;

    PTGTemplateAnswers = ^TGTemplateAnswers;
    TGTemplateAnswers = record
        tqNext: word;  { pointer to next dat block }
        tqAnswers : array [0..3] of TGTemplateAnswer;
    end;

    PTGBatchInfo = ^TGBatchInfo;
    TGBatchInfo = record
        biInfo : TGSubBatchInfo;
        biEntryPointer : word;
        biFlags : word;
        biDataBlockAddress : word;
        biBatchDescriptions : word;
    end;

    TGauge = class;
    TTemplateData = class;

    PTStatsData = ^TStatsData;
    TStatsData = record    // contains stats info
        ValueAvg : Single;
        ValueMin : Single;
        ValueMax : Single;
        ValueStandardDeviation : Single;
        ValueCount : Single;
    end;

    TLimitRange = (InRange,TooLow,TooHigh);

    TQA = class(TObject)
       Question: string;
       Answer : string;
    end;

    PSingle = ^Single;
    PTBatchData = ^TBatchData;
    PInteger = ^integer;
    TBatchData = class(TObject) // this is a batch already formatted for win32 use
        private
            FData : PSingle;
            FSortData : PSingle;
            FRowCount : PInteger;
            FTooLowCount : PInteger;
            FTooHighCount : PInteger;
            FSize : integer;
            FColumnEnabled : ^Boolean;
            FColumnStats : PTStatsData;
            TemplateData : TTemplateData;
            FRowNames : TStringList;
            FColNames : TStringList;

            procedure SetupColumnEnabled;
            procedure LoadQAFromGauge(answers:word;Gauge:TGauge);
            procedure SetData(Index:Integer; Value:single);
            function GetData(Index:Integer) : Single;
            function GetSortData(Index:Integer) : Single;
            function GetRawData(index:integer;Base:PSingle) : single;

            function GetMax(index:integer) : single;
            function GetMin(index:integer) : single;
            function GetAvg(index:integer) : single;
            function GetStandardDeviation(index:integer) : single;

            procedure SetMax(index:integer;value:single);
            procedure SetMin(index:integer;value:single);
            procedure SetAvg(index:integer;value:single);
            procedure SetStandardDeviation(index:integer;value:single);

            function GetStatsData(index:integer) : PTStatsData;
            procedure GenerateColumnStats(index:integer;startIndex:integer;count:integer);

            property Stats[Index:integer] : PTStatsData read GetStatsData;
        protected
            function GetRowName(Index:integer) : string;
            function GetColumnName(Index:integer) : string;
            function GetColumnEnabled(Index:integer) : Boolean;
            procedure SetColumnEnabled(Index:integer;value:boolean);
            function GetLimitCheck(Index:integer) : TLimitRange;
            function GetEnabledColumnCount : integer;
            function GetRowCount(Index:integer) : integer;
            procedure SetRowCount(Index:integer;value:integer);
            function GetIPtr(Index:integer;Base:PInteger) : PInteger;
            function GetTooLowCount(Index:integer) : integer;
            function GetTooHighCount(Index:integer) : integer;
            procedure SetTooLowCount(Index:integer;value:integer);
            procedure SetTooHighCount(Index:integer;value:integer);

        public
            Slot : integer;
            Count : integer;
            Description : string;
            Flags : integer;
            Rows : integer;
            Columns : integer;
            CamMode : integer;
            LowLimit : single;
            HighLimit : single;
            StartTime : TDateTime;
            EndTime : TDateTime;
            Enabled : Boolean;
            Questions : TList; { contains a list of TQA }

            constructor Create(size:integer);
            destructor Destroy; override;

            procedure GenerateStats;
            function SaveToIniFile(TI:TIniFile) : boolean;
            function SaveToTextFile(var TextFile : Text) : boolean;
            function SaveToCommaFile(var TextFile : Text) : boolean;

            function ComputeQuantile(Col:Integer;Point:Single) : single;

            class function Load(Gauge:TGauge;batchnum:integer) : TBatchData;
            class function LoadFromIni(TI:TIniFile;batchnum:integer) : TBatchData;

            property Data[Index:Integer]:Single read GetData write SetData;
            property SortData[Index:integer]:Single read GetSortData;
            property Max[Index:Integer] : single read GetMax write SetMax;
            property Min[Index:Integer] : single read GetMin write SetMin;
            property Avg[Index:Integer] : single read GetAvg write SetAvg;
            property LimitCheck[Index:Integer] : TLimitRange read GetLimitCheck;
            property StandardDeviation[Index:integer] : single read GetStandardDeviation write SetStandardDeviation;

            property ColumnName[Index:integer] : string read GetColumnName;
            property ColumnEnabled[Index:integer] : boolean read GetColumnEnabled write SetColumnEnabled;
            property RowCount[Index:integer] : integer read GetRowCount write SetRowCount;
            property RowName[Index:integer] : string read GetRowName;
            property EnabledColumnCount : integer read GetEnabledColumnCount;
            property TooLowCount[Index:integer] : integer read GetTooLowCount write SetTooLowCount;
            property TooHighCount[Index:integer] : integer read GetTooHighCount write SetTooHighCount;

    end;

    PTGTemplateInfo = ^TGTemplateInfo;
    TGTemplateInfo = record
        tiRows : byte;
        tiColumns : byte;
        tiCamMode : byte;
        tiSpare : byte;
        tiLimitLow : word;
        tiLimitHigh : word;
        tiName : array [0..15] of char;
        tiRowBlockAddress : word;
        tiColumnBlockAddress : word;
        tiBlockDescriptions : word;
    end;

    TTemplateData = class(TObject)
    private
        FRowAddress : word;
        FColumnAddress : word;
        FBlockDescriptionAddress : word;
        FGauge : TGauge;

    protected

        function GetRowName(Index:integer) : string;
        function GetColumnName(Index:integer) : string;
        function GetQuestion(Index:integer) : string;

    public
        Rows : integer;
        Columns : integer;
        CamMode : integer;
        LimitLow : single;
        LimitHigh : single;
        Description : string;

        property ColumnName[Index:integer] : string read GetColumnName;
        property RowName[Index:integer] : string read GetRowName;
        property Question[Index:integer] : string read GetQuestion;
        class function Load(Gauge:TGauge;index:integer) : TTemplateData;

{        property RowAddress : word write FRowAddress;
        property ColumnAddress : word write FColumnAddress;
        property BlockDescriptionAddress : word write FBlockDescriptionAddress;
}
    end;


    TGauge = class(TComponent)
        private
            FDataCache : PTDBCache;
            FSerialNumber : word;      // cached serial number
            FPacketIO : TPacketIO;

            FSystemOffset : word;
            SystemOffsetValid : boolean;

            FWordsUsed : integer;        // number of words used on the system
            FWordsDownloaded : integer;  // number of words we've loaded into cache
            FOEMName : string;
            FVersion : word;
            FModel : word;
            FFlags : word;

            function CreateDBEntry(Addr:word) : PTDBCache;    // creates a data entry, puts it in the cache
        protected
        public
            constructor Create(Owner:TComponent); override;
            destructor Destroy; override;

            function GetSystemConstants(var syscons:TGSysConstants) : boolean;

            function GetDataBlock(Addr:word) : PTGDataBlock;     // returns nil if error
                                                                 // if not in cache, loads from gauge

            function GetWordInChain(ChainBase:word; element:integer; var offset:integer) : PTGDataBlock;
                     // walk chain to find word element, return block and offset

            function LoadDataBlockChain(Addr:word) : PTGDataBlock;  // loads a chain into memory
            function GetBlockWithAddr(Addr:word; var offset: integer) : PTGDataBlock; // return datablock base with offset 
            function GetStringFromBase(base:word;element:integer) : string;

            procedure ReportErrorMessage(mesg:string);
            function ConvertTime(gtime:TGTimeInfo) : TDateTime;
            procedure EmptyCache;               // remove all data blocks from cache
            function LocateDataCache(Addr:word) : PTDBCache;
            function LoadMemoryInfo : boolean;  // load into cache info about used blocks

            function GetLoadedPercentage : integer;     // 0 if not known else 1 to 100
            function GetSystemOffset : boolean;

            function GetSerialNumber : string;
            function GetVersion : string;
            function GetOemName : string;
            function GetModel : string;

            property WordsUsed : integer read FWordsUsed;
            property WordsDownloaded : integer read FWordsDownloaded;

            property SystemOffset : word read FSystemOffset;


            //            function WriteBackCache : boolean;
        published
        
            property PacketIO : TPacketIO read FPacketIO write FPacketIO;
            property SerialNumber : string read GetSerialNumber;
            property Version : string read GetVersion;
            property OemName : string read GetOemName;
            property Model : string read GetModel;
            property Flags : word read FFlags;
    end;         // end TGauge

const
     CFASTAT_MIN:byte = 0;
     CFASTAT_MAX:byte = 1;
     CFASTAT_AVG:byte = 2;
     CFASTAT_DEV:byte = 3;
     CFASTAT_CNT:byte = 4;

     CSYS_CONSTANTS:word = $200;   // offset in memory of sys constants
     CUSER_CONSTANTS:word = $300;
     CBATCH_ADDRESS:word = $800;
     CTEMPLATE_ADDRESS:word = $1200;
     CDATABLOCK_ADDRESS:word = $1d00;

     CINVALID_READING:single = -10000;


     CNO_MORE_BLOCKS = $ffff;
     CINFINITY = $8000;
     CMAXBATCHES = 26;
     CMAXTEMPLATES  = 16;
     CMAXQUESTIONS = 16;
     CDATABLOCKSIZE = sizeof(TGDataBlock);  // number of bytes in a datablock

     CMAXVALUES_PER_DATABLOCK = 15;          // maximum number of words per block

     CMAXDBLOCKS_PER_REQUEST:integer = 7;     // the maximum number of blocks per request

     CFLAGS_NOFERROUS:word = $0001;
     CFLAGS_NONONFERROUS:word = $0002;
     CFLAGS_SHOWNEGATIVE:word = $0004;
     CFLAGS_EXTENDEDRANGE:word = $0008;

procedure Register;

implementation
uses Session, qreport;

function TTemplateData.GetColumnName(Index:integer) : string;
begin
     if FColumnAddress <> 0 then begin
        result := FGauge.GetStringFromBase(FColumnAddress,index);
        if result <> '' then Exit;
     end;

     result := format('C%d',[index+1]);
end;

function TTemplateData.GetRowName(Index:integer) : string;
begin
     if FRowAddress <> 0 then begin
        result := FGauge.GetStringFromBase(FRowAddress,index);
        if result <> '' then Exit;
     end;

     result := format('R%d',[index+1]);
end;

function TTemplateData.GetQuestion(Index:integer) : string;
begin
     result := '';
     if FBlockDescriptionAddress <> 0 then begin
        result := FGauge.GetStringFromBase(FBlockDescriptionAddress,index);
     end;
end;


class function TTemplateData.Load(Gauge:TGauge;index:integer) : TTemplateData;
var
   udata : PPacket;
   rc : integer;
   PTemplate : PTGTemplateInfo;
   addr : word;
begin
     result := nil;
     if not Gauge.GetSystemOffset then Exit;

     addr := Gauge.SystemOffset + CTEMPLATE_ADDRESS + sizeof(TGTemplateInfo)*index;
     rc := Gauge.PacketIO.Call(PROLP_READMEMORY, [addr,  sizeof(TGTemplateInfo) div 2], udata);
     if rc < sizeof(TGTemplateInfo) div 2  then Exit;

     PTemplate := @udata.pdata;

     result := Self.Create;
     result.FRowAddress := PTemplate.tiRowBlockAddress;
     result.FColumnAddress := PTemplate.tiColumnBlockAddress;
     result.FBlockDescriptionAddress := PTemplate.tiBlockDescriptions;
     result.FGauge := Gauge;
     result.Rows := PTemplate.tiRows;
     result.Columns := PTemplate.tiColumns;
     result.CamMode := PTemplate.tiCamMode;
     result.LimitLow := PTemplate.tiLimitLow;
     result.LimitHigh := PTemplate.tiLimitHigh;
     result.Description := PTemplate.tiName;

     if result.FRowAddress <> 0 then
        Gauge.LoadDataBlockChain(result.FRowAddress);

     if result.FColumnAddress <> 0 then
        Gauge.LoadDataBlockChain(result.FColumnAddress);

     if result.FBlockDescriptionAddress <> 0 then
        Gauge.LoadDataBlockChain(result.FBlockDescriptionAddress);
end;

constructor TBatchData.Create(size:integer);
begin
     GetMem(FData, sizeof(single)*size);
     GetMem(FSortData, sizeof(single)*size);

     FSize := size;
     Enabled := true;
     Questions := TList.Create;
     FRowNames := TStringList.Create;
     FColNames := TStringList.Create;

end;

destructor TBatchData.Destroy;
var
  i : integer;
  xptqa : TQA;
begin
     FreeMem(FData);
     FreeMem(FSortData);
     FreeMem(FColumnStats);
     FreeMem(FColumnEnabled);
     FreeMem(FRowCount);
     FreeMem(FTooLowCount);
     FreeMem(FTooHighCount);
     TemplateData.Free;
     for i:= 0 to Questions.Count -1 do begin
        xptqa := Questions.Items[i];
        xptqa.Free;
     end;
     Questions.Free;
     FRowNames.Free;
     FColNames.Free;
end;

procedure TBatchData.SetupColumnEnabled;
var
   i : integer;
begin
     GetMem(FColumnEnabled,Columns*sizeof(Boolean));
     GetMem(FRowCount,Columns*sizeof(integer));
     GetMem(FTooLowCount,Columns*sizeof(integer));
     GetMem(FTooHighCount,Columns*sizeof(integer));

     for i := 0 to Columns-1 do
     begin
          ColumnEnabled[i] := true;
          RowCount[i] := 0;
          TooLowCount[i] := 0;
          TooHighCount[i] := 0;
     end;


end;

function TBatchData.GetRowName(Index:integer) : string;
begin
     if assigned(FRowNames) and (FRowNames.Count > Index) then begin
         result := FRowNames.Strings[Index];
         Exit;
     end;

     if assigned(TemplateData) then
        result := TemplateData.RowName[Index]
     else
         result := format('R%d',[Index+1]);

end;

function TBatchData.GetColumnName(Index:integer) : string;
begin
     if assigned(FColNames) and (FColNames.Count > Index) then begin
         result := FColNames.Strings[Index];
         Exit;
     end;

     if assigned(TemplateData) then
        result := TemplateData.ColumnName[Index]
     else if Columns = 1 then
         result := format('Batch %s',[Chr(Slot+65)])
     else
         result := format('C%d',[Index+1]);
end;


procedure TBatchData.SetData(Index:Integer; Value:single);
var
   Fptr : ^Single;
begin
     if (Index >= FSize) or (Index >= Count) then raise ERangeError.Create('SetData Invalid Index');
     if nil = FData then raise EInvalidContainer.Create('SetData nil FData');
     Fptr := Pointer(Pchar(FData) + (Index*sizeof(single)));
     Fptr^ := Value;

end;

function TBatchData.GetIPtr(Index:integer;Base:PInteger) : PInteger;
begin
     if (Index >= Columns) then raise ERangeError.Create('GetIPtr  Invalid Index');
     if nil = Base then raise EInvalidContainer.Create('GetIptr nil Base');
     result := Pointer(Pchar(Base) + (Index*sizeof(integer)));
end;

function TBatchData.GetRowCount(Index:Integer) : integer;
begin
     result := GetIPtr(index,FRowCount)^;
end;

procedure TBatchData.SetRowCount(Index:Integer;value:integer);
begin
     GetIPtr(index,FRowCount)^ := value;
end;

function TBatchData.GetTooLowCount(Index:Integer) : integer;
begin
     result := GetIPtr(index,FTooLowCount)^;
end;

procedure TBatchData.SetTooLowCount(Index:Integer;value:integer);
begin
     GetIPtr(index,FTooLowCount)^ := value;
end;

function TBatchData.GetTooHighCount(Index:Integer) : integer;
begin
     result := GetIPtr(index,FTooHighCount)^;
end;

procedure TBatchData.SetTooHighCount(Index:Integer;value:integer);
begin
     GetIPtr(index,FTooHighCount)^ := value;
end;


function TBatchData.GetLimitCheck(Index:integer) : TLimitRange;
var
  y : single;
begin
     result := InRange;

     if Index = -1 then begin
        if (LowLimit = 0) and (HighLimit = 0) then
           Exit
        else begin
            result := TooHigh;
            Exit;
        end;
     end;

     try
         if (LowLimit = 0) and (HighLimit = 0) then Exit;
         y := Data[index];
         if CINVALID_READING = y then Exit;
         if y < LowLimit then
            result := TooLow
         else if y > HighLimit then
            result := TooHigh;
     except
     end;
end;

procedure TBatchData.GenerateColumnStats(index:integer;startIndex:integer;count:integer);
var
   sum : single;
   xmax : single;
   xmin : single;
   i : integer;
   cnt : integer;
   v : single;
begin
    sum := 0;

    xmax := -999;
    xmin := 999;

    cnt := 0;
    try    // GetRawData may throw an error on last column
        for i := startIndex to startIndex+count-1 do
        begin
             v := GetData(i);
             if v = CINVALID_READING then
                continue;
             sum := sum + v;
             if v < xmin then xmin := v;
             if v > xmax then xmax := v;
             inc(cnt);
        end;
    except

    end;

    if cnt > 0 then begin
        Max[index] := xmax;
        Min[index] := xmin;
        Avg[index] := sum / cnt;
        StandardDeviation[index] := 0;
    end;

    if cnt > 1 then begin
        sum := 0;
        cnt := 0;

        try    // GetRawData may throw an error on last column
            for i := startIndex to startIndex+count-1 do
            begin
                 v := GetData(i);
                 if v = CINVALID_READING then
                    continue;
                 sum := sum + Sqr(Avg[index] - v);
                 inc(cnt);
            end;

        except

        end;
        try
            if cnt > 1 then
               StandardDeviation[index] := Sqrt(sum/(cnt -1));
        except

        end;
    end;    // end if cnt > 1
end;

function TBatchData.ComputeQuantile(Col:Integer;Point:Single) : single;
var
   i : integer;
   f : single;
   Pi : Single;
   base : integer;
   rcnt : integer;
   { taken from page 12-14 of Graphical Data Analysis }
begin

     result := CINVALID_READING;
     if col >= Columns then
        Exit;

     rcnt := RowCount[col];

     if (Point < 0.5/rcnt) or
        (Point > 1 - 0.5/rcnt) then
        Exit;

     base := col * Rows;
     if Point = 0.5 then begin          // this is a special value
        if (rcnt and 1) <> 0 then      // its odd

           result := SortData[base + ((rcnt+1) div 2) -1]
        else            // its even
           result := (SortData[base + (rcnt div 2) -1] +
                  SortData[base + (rcnt div 2)])/2;
        Exit;
     end;

     { calculate largest i that is just less than p }
     i := round(rcnt * Point - 0.5);
     Pi := i/rcnt;
     f := Point - Pi;
     assert(f >= 0,'f less than 0 in ComputeQuantile');
     if (i + 1 >= rcnt) then
        Exit;
     result := (1 - f) * SortData[base + i] +
            f * SortData[base + i + 1];
end;


function SingleCompare(a:Pointer;b:Pointer) : integer;
var
   sa : ^Single;
   sb : ^Single;
begin

   sa := a;
   sb := b;
   if sa^ > sb^ then
      if CINVALID_READING = sb^ then
         result := -1
      else
          result := 1
   else if sa^ < sb^ then
      if CINVALID_READING = sa^ then
         result := 1
      else
          result := -1
   else
       result := 0;
end;

procedure TBatchData.GenerateStats;

var
   i,k : integer;
   row : integer;
   fPtr,yPtr : ^Single;
   col : integer;
   List : TList;
begin

    List := nil;

    if assigned(FRowCount) then begin
       try
           List := TList.Create;
           List.Capacity := Rows;

           for i := 0 to Columns-1 do
           begin
                List.Clear;
                try
                    for row := 0 to Rows-1 do
                    begin
                         k := i * Rows + row;
                         if k >= Count then break;
                         fPtr := Pointer(PChar(FData) + k*sizeof(Single));
                         List.Add(fPtr);
                    end;

                    List.Sort(SingleCompare);
                    for row := 0 to List.Count-1 do
                    begin
                         k := i * Rows + row;
                         if k >= Count then break;
                         fPtr := Pointer(PChar(FSortData) + k*sizeof(Single));
                         yPtr := List.Items[row];
                         Move(yPtr^,fPtr^,sizeof(Single));
                    end;

                except

                end;
           end;



           for i := 0 to Count-1 do
           begin
                col := i div Rows;
                if CINVALID_READING <> Data[i] then begin
                   RowCount[col] := RowCount[col] + 1;
                   if TooLow = LimitCheck[i] then
                      TooLowCount[col] := TooLowCount[col]+1
                   else if TooHigh = LimitCheck[i] then
                      TooHighCount[col] := TooHighCount[col]+1;
                end;
           end;
       except

       end;
       List.Free;
    end;

    if FColumnStats = nil then begin
       GetMem(FColumnStats,sizeof(TStatsData)*(Columns+1));
    end;

    GenerateColumnStats(-1,0,Count);
    for i := 0 to Columns -1 do begin
        GenerateColumnStats(i,i*Rows,Rows);
    end;
end;

function TBatchData.GetStandardDeviation(Index:integer) : single;
begin
     result := Stats[Index].ValueStandardDeviation;
end;

function TBatchData.GetMax(Index:integer) : single;
begin
     result := Stats[Index].ValueMax;
end;


function TBatchData.GetMin(Index:Integer) : single;
begin
     result := Stats[Index].ValueMin;
end;

function TBatchData.GetAvg(Index:Integer) : single;
begin
     result := Stats[Index].ValueAvg;
end;

procedure TBatchData.SetStandardDeviation(Index:integer;value:Single);
begin
     Stats[Index].ValueStandardDeviation := value;
end;

procedure TBatchData.SetMax(Index:integer;value:Single);
begin
     Stats[Index].ValueMax := value;
end;


procedure TBatchData.SetMin(Index:Integer;value:Single);
begin
     Stats[Index].ValueMin := value;
end;

procedure TBatchData.SetAvg(Index:Integer;value:Single);
begin
     Stats[Index].ValueAvg := value;
end;



function TBatchData.GetStatsData(Index:integer) : PTStatsData;
begin
     if (Index >= Columns) then raise ERangeError.Create('GetStatsData Invalid Index');
     if nil = FColumnStats then raise EInvalidContainer.Create('GetStatsData nil FColumnStats');
     result := Pointer(Pchar(FColumnStats) + ((Index+1)*sizeof(TStatsData)));
end;

function TBatchData.GetRawData(Index:Integer;Base:PSingle) : Single;
var
   Fptr : ^single;
begin
     if (Index >= FSize) or (Index >= Count) then raise ERangeError.Create('GetRawData Invalid Index');
     if nil = Base then raise EInvalidContainer.Create('GetRawData nil base');
     Fptr := Pointer(Pchar(Base) + (Index*sizeof(single)));
     result := Fptr^;
end;


function TBatchData.GetData(Index:Integer) : Single;
begin
     result := GetRawData(index,FData);
end;

function TBatchData.GetSortData(Index:Integer) : Single;
begin
     result := GetRawData(index,FSortData);
end;

function TBatchData.GetEnabledColumnCount : integer;
var
   i : integer;
begin
     result := 0;
     for i := 0 to Columns-1 do
     begin
           if ColumnEnabled[i] then inc(result);
     end;
end;

function TBatchData.GetColumnEnabled(Index:integer) : Boolean;
var
   v : ^Boolean;
begin
     if (index >= Columns) or (nil = FColumnEnabled) then raise ERangeError.Create('GetColEnabled out of range');
     v := Pointer(PChar(FColumnEnabled)+(index * sizeof(boolean)));
     result := v^;

end;

procedure TBatchData.SetColumnEnabled(Index:integer;value:boolean);
var
   v : ^Boolean;
begin
     if (index >= Columns) or (nil = FColumnEnabled) then raise ERangeError.Create('SetColEnabled out of range');
     v := Pointer(PChar(FColumnEnabled)+(index * sizeof(boolean)));
     v^ := value;
end;

procedure TBatchData.LoadQAFromGauge(answers:word;Gauge:TGauge);
var
  xtqa : TQA;
  i : integer;
  offset : integer;
  DB : PTGDataBlock;
  question : string;
  p : pchar;
  w : word;
begin
     DB := Gauge.LoadDataBlockChain(answers);
     if not assigned(DB) then Exit;
     if not assigned(TemplateData) then Exit;

     for i := 0 to CMAXQUESTIONS -1 do begin
         question := TemplateData.Question[i];
         if question = '' then Exit;
         DB := Gauge.GetWordInChain(answers,i,offset);
         if assigned(DB) then begin
            w := DB.pdata[offset]; { w points to the gauge memory address of the answer }
            if (w <> 0) and (w <> CINFINITY) then  begin
                DB := Gauge.GetBlockWithAddr(w,offset);
                if assigned(DB) then begin
                   if offset > 0 then offset := offset -1; { this should always be true }
                   p := @DB.pdata[offset];  { here is the string }
                   if p <> '' then begin
                       xtqa := TQA.Create;
                       xtqa.Question := question;
                       xtqa.Answer := p;
                       Questions.Add(xtqa);
                   end;
                end;
            end;
         end;

     end;
end;

class function TBatchData.LoadFromIni(TI:TIniFile;batchnum:integer) : TBatchData;
var
   BH : string;
   x,i : integer;
   BD : TBatchData;
   xtqa : TQA;
   s : string;
begin
     result := nil;

     BH := 'Batch '+chr(batchnum+65);
     x := TI.ReadInteger(BH,'Count',0);

     if x = 0 then Exit;

     BD := TBatchData.Create(x);

    BD.Count := x;
    BD.Slot := batchnum;

    BD.Flags := TI.ReadInteger(BH,'Flags',0);
    BD.Rows := TI.ReadInteger(BH,'Rows',0);
    BD.Columns := TI.ReadInteger(BH,'Cols',0);
    BD.SetupColumnEnabled;
    BD.CamMode := TI.ReadInteger(BH,'CamMode',0);
    BD.Description := TI.ReadString(BH,'Description','Batch '+chr(batchnum+65));

    BD.LowLimit := TI.ReadInteger(BH,'LowLimit',0)/100;
    BD.HighLimit := TI.ReadInteger(BH,'HighLimit',0)/100;
    try
        BD.StartTime := StrToDateTime(TI.ReadString(BH,'StartTime',''));
        BD.EndTime := StrToDateTime(TI.ReadString(BH,'EndTime',''));
    except
    end;

    for x := 0 to BD.Count-1 do
    begin
      BD.Data[x] := TI.ReadInteger(BH,format('R%d',[x]),0)/100;
    end;


    for x:= 0 to BD.Rows -1 do
        BD.FRowNames.Add(TI.ReadString(BH,format('RowName%d',[x]),format('R%d',[x+1])));

    for x:= 0 to BD.Columns -1 do
        BD.FColNames.Add(TI.ReadString(BH,format('ColName%d',[x]),format('C%d',[x+1])));


    for x:= 0 to CMAXQUESTIONS -1 do begin
        s := TI.ReadString(BH,format('QA%d',[x]),'');
        if s = '' then break;
        i := pos(':',s);
        if i > 0 then begin
            xtqa := TQA.Create;
            xtqa.Question := copy(s,1,i-1);
            xtqa.Answer := copy(s,i+1,99);
            BD.Questions.Add(xtqa);
        end;
    end;

    BD.GenerateStats;
    result := BD;

end;

class function TBatchData.Load(Gauge:TGauge;batchnum:integer) : TBatchData;
var
   BD : TBatchData;
   p : PTGDataBlock;

   udata : PPacket;
   rc  : integer;
   index : integer;
   k : integer;
   i : integer;
   batchAddress : word;
   value : word;
   PBI : ^TGBatchInfo;
begin

    result := nil;

    with Gauge do begin

        if not GetSystemOffset then Exit;

       rc := PacketIO.Call(PROLP_READMEMORY,
           [SystemOffset + CBATCH_ADDRESS + batchnum * sizeof(TGBatchInfo),
           sizeof(TGBatchInfo) div 2],udata);
        if rc < (sizeof(TGBatchInfo) div 2) then Exit;

        PBI := Pointer(@udata.pdata[0]);

        BD := TBatchData.Create(PBI.biInfo.sbEntryCount);

        BD.Count := PBI.biInfo.sbEntryCount;
        BD.Description := 'Batch '+chr(batchnum+65);
        BD.Slot := batchnum;

        if 0 = BD.Count then begin
           result := BD;
           Exit;
        end;

        BD.Flags := PBI.biInfo.sbFlags;
        BD.Rows := PBI.biInfo.sbRows;
        BD.Columns := PBI.biInfo.sbColumns;
        BD.StartTime := ConvertTime(PBI.biInfo.sbTimeStart);
        BD.EndTime := ConvertTime(PBI.biInfo.sbTimeEnd);

        if BD.Rows < 2 then
        begin
             BD.Rows := BD.Count;
             BD.Columns := 1;
        end else begin
            BD.Columns := BD.Count div BD.Rows;
            if BD.Columns = 0 then
               BD.Columns := 1;
            if BD.Count - (BD.Columns * BD.Rows) > 0 then
               inc(BD.Columns);
        end;
        BD.SetupColumnEnabled;
        BD.CamMode := PBI.biInfo.sbCamMode;
        BD.LowLimit := PBI.biInfo.sbLimitLow / 100;
        BD.HighLimit := PBI.biInfo.sbLimitHigh / 100;
        if ord(PBI.biInfo.sbDescription[0]) <> 0 then
           BD.Description := PBI.biInfo.sbDescription+' ('+chr(batchnum+65)+')';


        batchAddress := PBI.biDataBlockAddress;
        if PBI.biInfo.sbTemplate > 0 then begin
           BD.TemplateData := TTemplateData.Load(Gauge,PBI.biInfo.sbTemplate-1);
           if PBI.biBatchDescriptions > 0 then
              BD.LoadQAFromGauge(PBI.biBatchDescriptions,Gauge);
        end;

        // preload cache data
        p  := LoadDataBlockChain(batchaddress);
        if p = nil then begin
           BD.Free;
           Exit;
        end;

        i := 0;
        while i < BD.Count-1 do
        begin
             p := GetDataBlock(batchAddress);

             if p = nil then begin
                BD.Free;
                Exit;           // should never happen
             end;

             for k :=  0 to CMAXVALUES_PER_DATABLOCK-1 do
             begin
                  index := k + i;

                  value := p.pdata[k];

                  if value = 32768 then
                     BD.Data[index] := CINVALID_READING
                  else if value > 32767 then
                     BD.Data[index] := (value and $7fff)/-100.0
                  else
                     BD.Data[index] := value / 100.0;

                  if index = BD.Count-1 then break;
             end; // end for each element in the data block

             i := i + k;

             batchAddress := p.dbNext;
             if (batchAddress = 0) or (batchAddress = $ffff) then break;
        end; // end for each junk we're reading

        BD.Count := i+1;     // reset count in case we broke out early
        BD.GenerateStats;
    end;                 // end with do
    result := BD;
end;

function TBatchData.SaveToTextFile(var TextFile : Text) : boolean;
begin

     QReportForm.Session := MySession;
     QReportForm.CreateBatchHeader;
     WriteLn(TextFile,QReportForm.SummaryMemo.Lines.Text);
     QReportForm.CreateBatchStatistics;
     WriteLn(TextFile,QReportForm.StatsMemo.Lines.Text);
     if Columns > 1 then
        QReportForm.CreateBatchVerticalReadings(false)
     else
        QReportForm.CreateBatchHorizontalReadings(false);

     WriteLn(TextFile,QReportForm.DataMemo.Lines.Text);
     result := true;

end;

function TBatchData.SaveToCommaFile(var TextFile : Text) : boolean;
var
   i,j : integer;
begin
     result := false;

     Write(TextFile,'"Batch ',Chr(Slot+65),'",');
     for i := 0 to Columns-1 do
     begin
          Write(TextFile,'"',ColumnName[i],'"');
          if (i <> (Columns -1)) then
             Write(TextFile,',');
     end;
     WriteLn(TextFile);

     try
         for i := 0 to Rows-1 do
         begin
              Write(TextFile,'"',RowName[i],'",');
              try
                 for j := 0 to Columns-1 do
                 begin
                      Write(TextFile,MySession.ConvertValueToPrintableString(MySession.Data[i+j*Rows]));
                      if (j <> (Columns -1)) then
                         Write(TextFile,',');

                 end;
              except

              end;
              WriteLn(TextFile);
         end;
     except

     end;

     WriteLn(TextFile);
     if MySession.Units = Mils then
        WriteLn(TextFile,'"Units","Mils"')
     else
        WriteLn(TextFile,'"Units","Microns"')
end;


function TBatchData.SaveToIniFile(TI:TIniFile) : boolean;
var
   BH : string;
   x : integer;
begin
     result := false;

     BH := 'Batch '+chr(Slot+65);
     TI.WriteString(BH,'Description',Description);
     TI.WriteInteger(BH,'Count',Count);
     TI.WriteInteger(BH,'Flags',Flags);
     TI.WriteInteger(BH,'Rows',Rows);
     TI.WriteInteger(BH,'Cols',Columns);
     TI.WriteInteger(BH,'CamMode',CamMode);
     TI.WriteInteger(BH,'LowLimit',trunc(LowLimit*100));
     TI.WriteInteger(BH,'HighLimit',trunc(HighLimit*100));
     TI.WriteString(BH,'StartTime',DateTimeToStr(StartTime));
     TI.WriteString(BH,'EndTime',DateTimeToStr(EndTime));

     for x := 0 to Count-1 do
     begin
          TI.WriteInteger(BH,format('R%d',[x]),trunc(Data[x]*100));
     end;

     for x:= 0 to Rows -1 do
          TI.WriteString(BH,format('RowName%d',[x]),RowName[x]);

     for x:= 0 to Columns -1 do
          TI.WriteString(BH,format('ColName%d',[x]),ColumnName[x]);

     for x:= 0 to Questions.Count -1 do
          TI.WriteString(BH,format('QA%d',[x]),format('%s:%s',[TQA(Questions.Items[x]).Question,TQA(Questions.Items[x]).Answer]));
          

end;

constructor TGauge.Create(Owner:TComponent);
begin
    inherited Create(Owner);

    assert(sizeof(TGSysConstants) = 104, 'Size of Syscontants off'+format('=%d',[sizeof(TGSysConstants)]));
    assert(sizeof(TGBatchInfo) = 36, 'size of batchinf off');
    assert(sizeof(TGDataBlock) = 32, 'size of datablock off');
    assert(sizeof(TGTemplateInfo) = 30, 'size of templateinfo off'+format('=%d',[sizeof(TGTemplateInfo)]));

    SystemOffsetValid := false;
    FOEMName := 'Unknown';

end;

destructor TGauge.Destroy;
begin
     EmptyCache;
end;


function TGauge.LocateDataCache(Addr:word) : PTDBCache;
begin

     result := FDataCache;
     while result <> nil do
     begin
          if result.cAddr = Addr then
             exit;

          if result.cAddr > Addr then
             break;
          result := result.cNext;
     end;
     result := nil;
end;

function TGauge.CreateDBEntry(Addr:word) : PTDBCache;    // creates a data entry, puts it in the cache
var
   p : PTDBCache;
begin
     result := LocateDataCache(Addr);
     if result <> nil then Exit;

     New(result);

     result.cAddr := Addr;
     result.cNext := nil;
     FWordsDownloaded :=  FWordsDownloaded + CMAXVALUES_PER_DATABLOCK;


     if FDataCache = nil then
         FDataCache := result
     else if Addr < FDataCache.cAddr then begin
         result.cNext := FDataCache;
         FDataCache := result;
     end else begin
         p := FDataCache;
         while (p <> nil) do
         begin
              if (p.cNext <> nil) and (p.cNext.cAddr > Addr) then begin
                 result.cNext := p.cNext;
                 p.cNext := result;
                 Exit;
              end else if (nil = p.cNext) then begin
                  p.cNext := result;
                  Exit;
              end;
              p := p.cNext;
         end;  // end do
     end;
end;

procedure TGauge.EmptyCache;               // remove all data blocks from cache
var
   p,n : PTDBCache;
begin
     p := FDataCache;
     SystemOffsetValid := false;
     FWordsDownloaded := 0;

     while nil <> p do
     begin
          n := p.cNext;
          Dispose(p);
          p := n;
     end;

     FDataCache := nil;
end;

function TGauge.GetLoadedPercentage : integer;     // 0 if not known else 1 to 100
begin
     if (0 = FWordsDownloaded) or (0 = FWordsUsed) then
        result := 0
     else if FWordsDownloaded < FWordsUsed then
         result := (100 * FWordsDownloaded) div FWordsUsed
     else
         result := 100;

end;

function TGauge.GetDataBlock(Addr:word) : PTGDataBlock;     // returns nil if error
                                                            // if not in cache, loads from gauge
var
   p : PTDBCache;
   udata : PPacket;
   rc : integer;
   blockCount : integer;

begin
     result := nil;

     if Addr = CNO_MORE_BLOCKS then Exit;

     p := LocateDataCache(Addr);
     if p <> nil then begin
        result := @p.data;
        Exit;
     end;

     if not GetSystemOffset then Exit;

     // given that addr isn't in memory, lets see if addr+multiple blocks are
{     for blockCount := 1 to CMAXDBLOCKS_PER_REQUEST div 2 do
          if nil = LocateDataCache(Addr  + (blockCount) * sizeof(TGDataBlock)) then Break;

     if blockCount > (CMAXDBLOCKS_PER_REQUEST) then
        blockCount := blockCount-1;
}
     blockCount := 1;
     rc := FPacketIO.Call(PROLP_READMEMORY, [Addr,  16*blockCount], udata);
     if rc < (16*blockCount)  then Exit;
     for rc :=  0 to  blockCount-1 do
     begin
          p := CreateDBEntry(Addr);
          if rc = 0 then result := @p.Data;
          Move(udata.pdata[rc * (sizeof(TGDataBlock) div 2)],p.Data,sizeof(TGDataBlock));
          Addr := Addr + sizeof(TGDataBlock);
     end;
end;

function TGauge.GetBlockWithAddr(Addr:word; var offset:integer) : PTGDataBlock;
         // return a datablock that contains the addr, set offset into block of the address
var
  baseAddr : word;
  diff : word;
begin
     result := nil;
     if 0 <> (Addr and $01) then Exit; // odd addresses not allowed
     if not GetSystemOffset then Exit;

     diff := (Addr - (SystemOffset + CDATABLOCK_ADDRESS));
     offset := (diff mod CDATABLOCKSIZE) div 2;
     baseAddr := (diff div CDATABLOCKSIZE)*CDATABLOCKSIZE + SystemOffset + CDATABLOCK_ADDRESS;
     result := GetDataBlock(baseAddr);
end;

function TGauge.LoadDataBlockChain(Addr:word) : PTGDataBlock;  // loads a chain into memory
var
   p : PTGDataBlock;
   Adr : word;
begin
     result := nil;

     Adr := Addr;
     repeat
           p := GetDataBlock(Adr);
           if p = nil then Exit;

           Adr := p.dbNext;
     until (Adr = 0) or (Adr = $ffff);

     result := GetDataBlock(Addr);
end;

function TGauge.GetWordInChain(ChainBase:word; element:integer; var offset:integer) : PTGDataBlock;
                     // walk chain to find word element, return block and offset
var
   p : PTGDataBlock;
   addr : word;
begin

     result := nil;
     addr := ChainBase;
     repeat
           p := GetDataBlock(addr);
           if assigned(p) then begin
               if element < CMAXVALUES_PER_DATABLOCK then begin
                  offset := element;
                  result := p;
                  Exit;
               end;
               dec(element,CMAXVALUES_PER_DATABLOCK);
               addr := p.dbNext;
           end;
     until not assigned(p);
end;

function TGauge.GetStringFromBase(base:word;element:integer) : string;
var
   DB : PTGDataBlock;
   offset : integer;
   p : PChar;
begin
     result := '';

     DB := GetWordInChain(base,element*5,offset);
     if assigned(DB) and (DB.pdata[offset] <> CINFINITY) then begin
        p := @DB.pdata[offset];
        result := p;
     end;
end;

function TGauge.ConvertTime(gtime:TGTimeInfo) : TDateTime;
var
   month, day, hour, minute, year : integer;
begin

     result := 0;
     if (gtime.tiLow = 0) then Exit;
     
     try
         with gtime do begin
              month := ((tiLow and $10) div 16) * 10 + tiLow and $f;
              year := ((tiLow div 32) and 7) + 1997;
              { guage has a problem with compressing the date. It takes the lower year nibble, then subtracts 5 from it, and
                masks it to 3 bits.

                When the year is 2000, (tlow div 32 and 7) == 3
                When the year is 2001, (tlow div 32 and 7) == 4

                To correct, we use the following cheap algorithm.

                year = 2000 + (tlow div 32 and 7) - 3.

              }
              day := (tiLow shr 8) and $7f;
              day := (day div 16) * 10 + (day and $f);

              hour := tiHigh and $3f;
              hour := (hour div 16) * 10 + (hour and $f);

              minute := (tiHigh shr 8) and $7f;
              minute := (minute div 16) * 10 + (minute and $f);

              result := EncodeDate(year,month,day) +
                     EncodeTime(hour,minute,0,0);
         end;
     except

     end;

end;

procedure TGauge.ReportErrorMessage(mesg:string);
begin
     messagedlg(format('%s'#10#10'%s'#10'Error Code %d',[mesg,PacketIO.LastErrorString,PacketIO.LastError]),mtError,[mbOk],0);
end;

function TGauge.GetSystemOffset : boolean;
var
   udata : PPacket;
   rc : integer;
   i : word;
begin
     if SystemOffsetValid then begin
        result := true;
        Exit;
     end;
     result := false;

     rc := FPacketIO.Call(PROLP_GETMEMORYOFFSET,[0],udata);
     if rc < 1 then Exit;

     FSystemOffset := udata.pdata[0];
     SystemOffsetValid := true;
     result := true;

     FFlags := 0;
     // read the flags field
     rc := FPacketIO.Call(PROLP_READMEMORY,[FSystemOffset+8+CSYS_CONSTANTS,1],udata);
     if rc > 0 then begin
        FFlags := udata.pdata[0];
     end;

     rc := FPacketIO.Call(PROLP_NULL,[0],udata);
     if rc < 1 then Exit;

     FVersion := udata.pdata[0];
     if rc >= 3 then
        FModel := udata.pdata[2]
     else
        FModel := 0;

     if rc >= 4 then
        FSerialNumber := udata.pdata[3]
     else
        FSerialNumber := 0;

     if rc >= 5 then begin             // get oem name
        i := udata.pdata[4];
        rc := FPacketIO.Call(PROLP_READMEMORY,[i,8],udata);
        if rc > 0 then begin
           FOEMName := PChar(@udata.pdata[0]);
           rc := Pos('.',FOEMName);
           while rc <> 0 do begin
                 Delete(FOEMName,rc,1);
                 rc := Pos('.',FOemName);
           end;
        end;
     end;
end;


function TGauge.GetSerialNumber : string;
begin
     result := format('%d',[FSerialNumber]);
end;

function TGauge.GetVersion : string;
begin
     result := format('%x.%x',[FVersion div 16, FVersion mod 16]);
end;

function TGauge.GetOemName : string;
begin
     result := FOEMName;
end;

function TGauge.GetModel : string;
begin
     if (FModel mod 256) < 16 then
        result := format('%x.0%x',[FModel div 256, FModel mod 256])
     else
         result := format('%x.%x',[FModel div 256, FModel mod 256]);

     if (FFlags and CFLAGS_NOFERROUS) <> 0 then
        result := result + 'N'
     else if (FFlags and CFLAGS_NONONFERROUS) <> 0 then
        result := result + 'F';  
end;


function TGauge.GetSystemConstants(var syscons:TGSysConstants) : boolean;
var
   udata : PPacket;
   rc : integer;
begin
     result := false;

     if not GetSystemOffset then Exit;

     rc := FPacketIO.Call(PROLP_READMEMORY, [FSystemOffset + CSYS_CONSTANTS,sizeof(TGSysConstants) div 2],udata);

     if rc = sizeof(TGSysConstants) div 2 then begin
        move(udata.pdata,syscons,sizeof(TGSysConstants));
        result := true;
     end
end;

function TGauge.LoadMemoryInfo : boolean;  // load into cache info about used blocks
var
   udata : PPacket;
   rc : integer;
begin
     result := false;
     FWordsUsed := 0;

     if not GetSystemOffset then Exit;

     rc := FPacketIO.Call(PROLP_GETMEMUSEDFREE, [0],udata);

     if rc < 1 then Exit;
     if 0 <> (udata.pflag and $80) then begin        // function not supported
        result := true;
        FWordsUsed := 0;
        Exit;
     end;

     FWordsUsed := udata.pdata[0];

     result := true;
end;


procedure Register;
begin
  RegisterComponents('Gauge', [TGauge]);
end;

end.

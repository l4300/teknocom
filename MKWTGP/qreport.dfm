�
 TQREPORTFORM 0v)  TPF0TQReportFormQReportFormLeftYTopfWidth�Height�CaptionQuick ReportFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ScaledPixelsPerInch`
TextHeight 	TQuickRepQuickRepLeftTop Width0Height Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRight
AfterPrintQuickRepAfterPrintAfterPreviewQuickRepAfterPreviewBeforePrintQuickRepBeforePrintFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
OnNeedDataQuickRepNeedDataOnStartPageQuickRepStartPageOptionsFirstPageHeader Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeLetterPage.Values       �@      ��
@       �@      ��
@       �@       �@           PrintIfEmpty	PrinterSettings.CopiesPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage PrinterSettings.OutputBinFirstReportTitleThis is the Report Title
SnapToGrid	UnitsInchesZoomd TQRBandBatchHeaderBandLeft0Top`Width�Height)Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPage	Size.Values `UUUU��@       �	@ BandTyperbDetail TQRMemoSummaryMemoLeft TopWidthYHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values ������@           XUUUUU�@ �����z�@ 	AlignmenttaLeftJustifyAlignToBand	AutoSize	AutoStretch	ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
   TQRBandPageHeaderBandLeft0Top0Width�Height0Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values       �@       �	@ BandTyperbPageHeader 
TQRSysData
QRSysData2Left TopWidthsHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values ������@           XUUUUU�@ �����"�@ 	AlignmenttaLeftJustifyAlignToBand	AutoSize	ColorclWhiteDataqrsDateTimeTransparentFontSize
  
TQRSysData
QRSysData3Left�TopWidth*HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values ������@      <�	@ XUUUUU�@      @�@ 	AlignmenttaRightJustifyAlignToBand	AutoSize	ColorclWhiteDataqrsPageNumberTextPage:TransparentFontSize
  TQRLabel
BatchTitleLeft8Top Width_HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �����j�@     `�@           �����Z�@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretch	Caption
BatchTitleColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold OnPrintBatchTitlePrint
ParentFontTransparentWordWrap	FontSize   TQRChildBandBatchStatsBandLeft0Top� Width�Height(Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values �������@       �	@ 
ParentBandBatchHeaderBand TQRMemo	StatsMemoLeft TopWidthIHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values ������@           XUUUUU�@ XUUUU%�@ 	AlignmenttaLeftJustifyAlignToBand	AutoSize	AutoStretch	ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize
   TQRChildBandBatchReadingsBandLeft0Top� Width�Height(Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values �������@       �	@ 
ParentBandBatchStatsBand TQRMemoDataMemoLeft TopWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values ������@           XUUUUU�@ �����R�@ 	AlignmenttaLeftJustifyAlignToBand	AutoSize	AutoStretch	ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style Lines.Strings
HI there T THis is formatted output 
ParentFontTransparentWordWrap	FontSize
   TQRChildBandDataPlotBandLeft0Top� Width�Height�Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumn	ForceNewPageSize.Values XUUUU��	@       �	@ 
ParentBandBatchReadingsBand TQRLabelDataPlotTitleLeftETopWidthFHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �������@ XUUUU��@ XUUUUU�@ XUUUU5�@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretch	Caption	Data PlotColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRChartDataPlotLeftTop(Width�HeightyFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �����^�@ XUUUUU�@ �������@      *�	@  
TQRDBChart
QRDBChart1Left�Top�WidthHeightBackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearTitle.Text.Strings  Chart3DPercent TLineSeriesSeries1Marks.ArrowLengthMarks.VisibleSeriesColorclRedPointer.InflateMargins	Pointer.StylepsRectanglePointer.VisibleXValues.DateTimeXValues.NameXXValues.Multiplier       ��?XValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.Multiplier       ��?YValues.OrderloNone     TQRChildBandDensityPlotBandLeft0Top�Width�Height�Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values �����:�	@       �	@ 
ParentBandDataPlotBand TQRLabelQRLabel2Left9TopWidth]HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �������@ PUUUU	�@ XUUUUU�@ ������@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretch	CaptionDensity PlotColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRChartDensityPlotLeftTop(Width�HeightyFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �����^�@ XUUUUU�@ �������@      *�	@  
TQRDBChart
QRDBChart2Left�Top�WidthHeightBackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearTitle.Text.Strings  Title.VisibleChart3DPercent TLineSeriesLineSeries1Marks.ArrowLengthMarks.VisibleSeriesColorclRedPointer.InflateMargins	Pointer.StylepsRectanglePointer.VisibleXValues.DateTimeXValues.NameXXValues.Multiplier       ��?XValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.Multiplier       ��?YValues.OrderloNone     TQRChildBandQuantilePlotBandLeft0Top0Width�Height�Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values      ��	@       �	@ 
ParentBandDensityPlotBand TQRChartQuantilePlotLeftTop(Width�HeightyFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �����^�@ XUUUUU�@ �������@      *�	@  
TQRDBChart
QRDBChart3Left�Top�WidthHeightBackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearTitle.Text.Strings  BottomAxis.AutomaticBottomAxis.AutomaticMaximumBottomAxis.AutomaticMinimumBottomAxis.ExactDateTimeBottomAxis.Increment ��������?BottomAxis.Maximum       ��?Chart3DPercentLeftAxis.AxisValuesFormat#,##0.##Legend.LegendStylelsSeriesView3D TLineSeriesLineSeries2Marks.ArrowLengthMarks.VisibleSeriesColorclRedPointer.Draw3DPointer.InflateMargins	Pointer.StylepsRectanglePointer.VisibleXValues.DateTimeXValues.NameXXValues.Multiplier       ��?XValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.Multiplier       ��?YValues.OrderloNone    TQRLabelQRLabel1Left7TopWidthbHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �������@ �������@ XUUUUU�@ XUUUU��@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretch	CaptionQuantile PlotColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize   TQRChildBandBoxPlotBandLeft0Top�Width�Height�Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values XUUUU��	@       �	@ 
ParentBandQuantilePlotBand TQRChartBoxPlotLeftTop0Width�HeightyFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �����^�@ XUUUUU�@       �@      *�	@  
TQRDBChart
QRDBChart4Left�Top�WidthHeightBackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearTitle.Text.Strings  BottomAxis.AutomaticBottomAxis.AutomaticMaximumBottomAxis.AutomaticMinimumBottomAxis.ExactDateTimeBottomAxis.Increment ��������?BottomAxis.Maximum       ��?Chart3DPercentLeftAxis.AxisValuesFormat#,##0.##Legend.LegendStylelsSeriesLegend.VisibleView3D TLineSeriesLineSeries3Marks.ArrowLengthMarks.VisibleSeriesColorclRedPointer.Draw3DPointer.InflateMargins	Pointer.StylepsRectanglePointer.VisibleXValues.DateTimeXValues.NameXXValues.Multiplier       ��?XValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.Multiplier       ��?YValues.OrderloNone    TQRLabelQRLabel3LeftGTopWidthBHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �������@      L�@ XUUUUU�@      ��@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretch	CaptionBox PlotColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize     
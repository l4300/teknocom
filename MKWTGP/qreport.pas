unit qreport;

interface

uses
  Session, Gauge, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  quickrpt, Qrctrls, ExtCtrls, TeeProcs, TeEngine, Chart, DBChart, QrTee,
  Series;

type
  TQReportForm = class(TForm)
    QuickRep: TQuickRep;
    BatchHeaderBand: TQRBand;
    PageHeaderBand: TQRBand;
    BatchStatsBand: TQRChildBand;
    QRSysData2: TQRSysData;
    QRSysData3: TQRSysData;
    BatchReadingsBand: TQRChildBand;
    BatchTitle: TQRLabel;
    DataPlotBand: TQRChildBand;
    DensityPlotBand: TQRChildBand;
    QRLabel2: TQRLabel;
    DensityPlot: TQRChart;
    QRDBChart2: TQRDBChart;
    LineSeries1: TLineSeries;
    DataPlotTitle: TQRLabel;
    DataPlot: TQRChart;
    QRDBChart1: TQRDBChart;
    Series1: TLineSeries;
    DataMemo: TQRMemo;
    SummaryMemo: TQRMemo;
    StatsMemo: TQRMemo;
    QuantilePlotBand: TQRChildBand;
    QuantilePlot: TQRChart;
    QRDBChart3: TQRDBChart;
    LineSeries2: TLineSeries;
    QRLabel1: TQRLabel;
    BoxPlotBand: TQRChildBand;
    BoxPlot: TQRChart;
    QRDBChart4: TQRDBChart;
    LineSeries3: TLineSeries;
    QRLabel3: TQRLabel;
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure QuickRepBeforePrint(Sender: TQuickRep;
      var PrintReport: Boolean);
    procedure BatchTitlePrint(sender: TObject; var Value: String);
    procedure QuickRepAfterPrint(Sender: TObject);
    procedure QuickRepAfterPreview(Sender: TObject);
    procedure QuickRepStartPage(Sender: TQuickRep);
  private
    { Private declarations }
    FBatch : integer;
    FMonoChrome : boolean;
    FGotColorInfo : boolean;
    FLastBatch : integer;
    FMaxWidth : integer;
    FSession : TGSession;
    FIsPrinting : boolean;

    procedure CreateBatchDataPlot;
    procedure CreateBatchDensityPlot;
    procedure CreateBatchQuantilePlot;
    procedure CreateBatchBoxPlot;
    function PrinterIsColor : boolean;
  public
    { Public declarations }
    procedure CreateBatchHeader;
    procedure CreateBatchStatistics;
    procedure CreateBatchVerticalReadings(doLimits:boolean);
    procedure CreateBatchHorizontalReadings(doLimits:boolean);

    procedure SetupBatchData(XSession:TGSession);         // setup for the current batch
    procedure PrintPreview;

    property Report : TQuickRep read QuickRep;
    property Session : TGSession read FSession write FSession;
  end;

var
  QReportForm: TQReportForm;

implementation


{$R *.DFM}

uses masterf, printers;

procedure TQReportForm.SetupBatchData(XSession:TGSession);         // setup for the current batch
begin
     FSession := XSession;
end;

procedure TQReportForm.PrintPreview;
begin
     QuickRep.Preview;
end;


function TQReportForm.PrinterIsColor : boolean;
var
   i : integer;
   DevMode : PDevMode;
   X : THandle;
   Device, Driver, Port : array [0..255] of char;
begin
   result := false;

   if FGotColorInfo then
      result := not FMonochrome;
      
   if Printer.PrinterIndex <> QuickRep.QRPrinter.PrinterIndex then begin
      port[0] := #0;
      driver[0] := #0;
      StrPCopy(device,Printer.Printers[QuickRep.QRPrinter.PrinterIndex]);
      X := 0;
      Printer.SetPrinter(device,driver,port,X);
      FGotColorInfo := true;
   end;
   Printer.GetPrinter(Device,Driver,Port,X);

   if X <> 0 then begin
       DevMode := GlobalLock(X);
       i := DevMode.dmFields;
       if ((i and DM_COLOR) <> 0) and
          (DevMode.dmColor = DMCOLOR_COLOR) then
          result := true;

       GlobalUnlock(X);
   end;
end;

procedure TQReportForm.CreateBatchHeader;
var
   str : string;
   i : integer;
begin
   SummaryMemo.Lines.Clear;

   with SummaryMemo.Lines do begin
       Add('Gauge    : Model '+Session.Model+'   S/N : '+Session.SerialNumber+'  Version: '+Session.Version+'  '+Session.OemName);
       with FSession.BatchData do begin
            Add('Batch    : ' + chr(65+Slot) + '   ' + Description);
            str := 'Count    : ' + IntToStr(Count);
            if Columns > 1 then
               str := str + '   Rows: '+IntToStr(Rows)+'   Columns: '+IntToStr(Columns);
            Add(str);
            str :=   'Cam Mode : ';
            if CamMode <> 0 then
               str := str + IntToStr(CamMode)
            else
                str := str + 'Off';

            Add(str);
            str := 'Limit    : ';
            if TooHigh = LimitCheck[-1] then begin
                str := str + 'Low Limit: ';
                if LowLimit <> 0 then
                   str := str + FSession.ConvertValueToString(LowLimit)
                else
                    str := str + 'None';

                str := str + '  High Limit: ';
                if HighLimit <> 0 then
                   str := str + FSession.ConvertValueToString(HighLimit)
                else
                    str := str + 'None';
            end else
                str := str + 'Off';

            Add(str);

            if StartTime <> 0 then
               Add('Start    : '+DateTimeToStr(StartTime));


            if EndTime <> 0 then
               Add('End      : '+DateTimeToStr(EndTime));

            if FSession.Units = Mils then
               Add('Units    : Mils')
            else
               Add('Unit     : Microns');

            for i := 0 to Questions.Count -1 do begin
               with TQA(Questions.Items[i]) do begin
                    Add(format('%-8.8s : %s',[Question,Answer]));
               end;
            end;
       end;
   end;    // end with summaryMemo
end;

procedure TQReportForm.CreateBatchStatistics;
var
   maxColumns : integer;
   BD : TBatchData;
   str : string;
   j,i : integer;
begin

     BD := FSession.BatchData;

     StatsMemo.Lines.Clear;
     maxColumns := FMaxWidth div 8;
     if maxColumns > BD.Columns then
        maxColumns := BD.Columns;

     str := '          ';
     if not FIsPrinting then
        maxColumns := BD.Columns;

     for i := 0 to maxColumns-1 do
     begin
          str := str + format('%-8.8s',[BD.ColumnName[i]]);
     end;

     StatsMemo.Lines.Add(str);
     try
         with BD do
             for i := 0 to 5 do
             begin
                  if (4 = i) and (TooHigh <> LimitCheck[-1]) then
                     Break;
                  case i of
                    0: str := 'Min     ';
                    1: str := 'Max     ';
                    2: str := 'Average ';
                    3: str := 'Std Dev ';
                    4: str := 'Too Low ';
                    5: str := 'Too High';

                  end;

                  try
                     for j := 0 to maxColumns-1 do
                     begin
                          case i of
                            0: str := str + format('%-8.8s',[FSession.MinText[j]]);
                            1: str := str + format('%-8.8s',[FSession.MaxText[j]]);
                            2: str := str + format('%-8.8s',[FSession.AvgText[j]]);
                            3: str := str + format('%-8.8s',[FSession.StandardDeviationText[j]]);
                            4: str := str + format('%-8.8s',['  '+IntToStr(TooLowCount[j])]);
                            5: str := str + format('%-8.8s',['  '+IntToStr(TooHighCount[j])]);
                          end;
                     end;
                  except

                  end;
                  StatsMemo.Lines.Add(str);
             end;
     except

     end;
end;

procedure TQReportForm.CreateBatchVerticalReadings(doLimits:boolean);
var
   maxColumns : integer;
   BD : TBatchData;
   str : string;
   i,j : integer;
begin

     BD := FSession.BatchData;

     DataMemo.Lines.Clear;
     maxColumns := (FMaxWidth div 8);
     if maxColumns > BD.Columns then
        maxColumns := BD.Columns;

     if not FIsPrinting then
        maxColumns := BD.Columns;

     str := '          ';
     for i := 0 to maxColumns-1 do
     begin
          str := str + format('%-8.8s',[BD.ColumnName[i]]);
     end;

     DataMemo.Lines.Add(str);
     try
         with BD do
             for i := 0 to Rows-1 do
             begin
                  str := format('%-8.8s',[RowName[i]]);

                  try
                     for j := 0 to maxColumns-1 do
                     begin
                         if doLimits then
                             case BD.LimitCheck[i+j*Rows] of
                               TooHigh :str := str + format('%-8.8s',[FSession.Text[i+j*rows]+'H']);
                               TooLow : str := str + format('%-8.8s',[FSession.Text[i+j*rows]+'L']);
                               InRange : str := str + format('%-8.8s',[FSession.Text[i+j*rows]]);
                             end
                         else
                             str := str + format('%-8.8s',[FSession.Text[i+j*rows]]);
                     end;
                  except

                  end;
                  DataMemo.Lines.Add(str);
             end;
     except

     end;

end;

procedure TQReportForm.CreateBatchHorizontalReadings(doLimits:boolean);
var
   maxColumns : integer;
   BD : TBatchData;
   str : string;
   i : integer;
   LRowCount : integer;
begin

     BD := FSession.BatchData;

     DataMemo.Lines.Clear;
     maxColumns := ((FMaxWidth-8) div 8) - 1;
     if maxColumns > BD.Count then
        maxColumns := BD.Count;

     if not FIsPrinting then
        maxColumns := 8;

     str := '          ';
     for i := 0 to maxColumns-1 do
     begin
          str := str + format('%-8.8s',[format('R%d',[i+1])]);
     end;

     DataMemo.Lines.Add(str);
     LRowCount := 0;
     str := 'x1      ';
     try
         with BD do
             for i := 0 to Count-1 do
             begin
                  if (i div maxColumns) <> LRowCount then begin
                     DataMemo.Lines.Add(str);
                     LRowCount := i div maxColumns;
                     str := format('%-8.8s',[format('x%d',[LRowCount+1])]);
                  end;
                  try
                     if doLimits then
                         case BD.LimitCheck[i] of
                           TooHigh :str := str + format('%-8.8s',[FSession.Text[i]+'H']);
                           TooLow : str := str + format('%-8.8s',[FSession.Text[i]+'L']);
                           InRange : str := str + format('%-8.8s',[FSession.Text[i]]);
                         end
                     else
                         str := str + format('%-8.8s',[FSession.Text[i]]);
                  except

                  end;
             end;
             if Length(str) > 8 then
                DataMemo.Lines.Add(str);
     except

     end;

end;

procedure TQReportForm.CreateBatchDataPlot;
begin
     try
         DataPlot.Chart.Assign(Master.GChart);

         DataPlot.Chart.Monochrome := FMonoChrome;
         DataPlot.Chart.BackColor := clWhite;
         Master.DrawGChart(TChart(DataPlot.Chart),FMonoChrome,true);
     except

     end;
end;

procedure TQReportForm.CreateBatchDensityPlot;
begin
     try
         DensityPlot.Chart.Assign(Master.DChart);

         DensityPlot.Chart.Monochrome := FMonoChrome;
         DensityPlot.Chart.BackColor := clWhite;

         DensityPlot.Chart.Legend.ColorWidth := 50;
         Master.DrawDChart(TChart(DensityPlot.Chart),FMonoChrome,true);
     except

     end;
end;

procedure TQReportForm.CreateBatchQuantilePlot;
begin
     try
         QuantilePlot.Chart.Assign(Master.QChart);

         QuantilePlot.Chart.Monochrome := FMonoChrome;
         QuantilePlot.Chart.BackColor := clWhite;

         QuantilePlot.Chart.Legend.ColorWidth := 50;
         Master.DrawQChart(TChart(QuantilePlot.Chart),FMonoChrome,true);
     except

     end;
end;

procedure TQReportForm.CreateBatchBoxPlot;
begin
     try
         BoxPlot.Chart.Assign(Master.BGChart);

         BoxPlot.Chart.Monochrome := FMonoChrome;
         BoxPlot.Chart.BackColor := clWhite;

         Master.DrawBoxChart(TChart(BoxPlot.Chart),FMonoChrome,true);
     except

     end;
end;

procedure TQReportForm.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
var
   SaveBatch : integer;
begin
     if FBatch >= FSession.BatchCount then begin
       MoreData := false;
       Exit;
     end;

     if QuickRep.Printer.Orientation = poPortrait then
        FMaxWidth := 80
     else
        FMaxWidth := 132;

{     if not FMonoChrome then begin
        DensityPlot.TeePrintMethod := qtmMetafile;
        QuantilePlot.TeePrintMethod := qtmMetafile;
        BoxPlot.TeePrintMethod := qtmMetafile;
     end else begin
        QuantilePlot.TeePrintMethod := qtmBitmap;
        DensityPlot.TeePrintMethod := qtmBitmap;
        BoxPlot.TeePrintMethod := qtmBitmap;
     end;
} 
     FLastBatch := FBatch;
     MoreData := true;

     SaveBatch := FSession.Batch;
     FSession.Batch := FBatch;

     try
         CreateBatchHeader;
         CreateBatchStatistics;
         if FSession.BatchData.Columns > 1 then
            CreateBatchVerticalReadings(true)
         else
            CreateBatchHorizontalReadings(true);
         CreateBatchDataPlot;
         CreateBatchDensityPlot;
         CreateBatchQuantilePlot;
         CreateBatchBoxPlot;
     except

     end;

     FSession.Batch := SaveBatch;

     inc(FBatch);
     while not FSession.Checked[FBatch] do begin
        inc(FBatch);
        if FBatch >= FSession.BatchCount then begin
           Exit;
        end;
     end;
end;

procedure TQReportForm.QuickRepBeforePrint(Sender: TQuickRep;
  var PrintReport: Boolean);
begin
     FIsPrinting := true;
     FGotColorInfo := false;
     FBatch := 0;
     if QuickRep.Printer.Orientation = poPortrait then
        FMaxWidth := 80
     else
        FMaxWidth := 132;

     while not FSession.Checked[FBatch] do begin
        inc(FBatch);
        if FBatch >= FSession.BatchCount then begin
           Exit;
        end;
     end;
     FLastBatch := FBatch;
end;

procedure TQReportForm.BatchTitlePrint(sender: TObject; var Value: String);
begin
     try
          Value := FSession.BatchDataArray[FLastBatch].Description;
     except
           Value := '';
     end;
end;

procedure TQReportForm.QuickRepAfterPrint(Sender: TObject);
begin
     FIsPrinting := false;
end;

procedure TQReportForm.QuickRepAfterPreview(Sender: TObject);
begin
     FIsPrinting := false;
end;

procedure TQReportForm.QuickRepStartPage(Sender: TQuickRep);
begin
     FMonoChrome := not PrinterIsColor;
end;


end.

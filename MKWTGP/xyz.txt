Gauge    : Model CTI_MFN   S/N : 0  Version: 2.3  SABERN
Batch    : A   DESK 1  (A)
Count    : 10   Rows: 5   Columns: 2
Cam Mode : Off
Limit    : Off
Units    : Mils

          C1      C2      
Min       0.74    5.23  
Max       1.70    6.44  
Average   1.18    5.59  
Std Dev   0.40    0.49  

          C1      C2      
R1        1.70    6.44  
R2        1.11    5.53  
R3        1.46    5.32  
R4        0.87    5.42  
R5        0.74    5.23  


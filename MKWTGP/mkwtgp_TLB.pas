unit mkwtgp_TLB;

{ This file contains pascal declarations imported from a type library.
  This file will be written during each import or refresh of the type
  library editor.  Changes to this file will be discarded during the
  refresh process. }

{ mkwtgp Library }
{ Version 1.0 }

interface

uses Windows, ActiveX, Classes, Graphics, OleCtrls, StdVCL;

const
  LIBID_mkwtgp: TGUID = '{90FC84E1-934A-11D1-A134-0000F4A9BA7E}';

const

{ Component class GUIDs }
  Class_testole: TGUID = '{90FC84E3-934A-11D1-A134-0000F4A9BA7E}';

type

{ Forward declarations: Interfaces }
  Itestole = interface;
  ItestoleDisp = dispinterface;

{ Forward declarations: CoClasses }
  testole = Itestole;

{ Dispatch interface for testole Object }

  Itestole = interface(IDispatch)
    ['{90FC84E2-934A-11D1-A134-0000F4A9BA7E}']
  end;

{ DispInterface declaration for Dual Interface Itestole }

  ItestoleDisp = dispinterface
    ['{90FC84E2-934A-11D1-A134-0000F4A9BA7E}']
  end;

{ testoleObject }

  Cotestole = class
    class function Create: Itestole;
    class function CreateRemote(const MachineName: string): Itestole;
  end;



implementation

uses ComObj;

class function Cotestole.Create: Itestole;
begin
  Result := CreateComObject(Class_testole) as Itestole;
end;

class function Cotestole.CreateRemote(const MachineName: string): Itestole;
begin
  Result := CreateRemoteComObject(MachineName, Class_testole) as Itestole;
end;


end.

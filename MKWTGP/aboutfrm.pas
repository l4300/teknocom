unit aboutfrm;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls;

type
  TAboutBox = class(TForm)
    Panel1: TPanel;
    ProgramIcon: TImage;
    ProductName: TLabel;
    Version: TLabel;
    Copyright: TLabel;
    Comments: TLabel;
    OKButton: TButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AboutBox: TAboutBox;

implementation

uses Masterf, globals, registry, inifiles;

{$R *.DFM}

procedure TAboutBox.FormCreate(Sender: TObject);
var
   reg : TRegIniFile;
   i : integer;
begin
     ProgramIcon.Picture.assign(Master.IconImage.Picture);
     ProgramIcon.Transparent := true;

    reg := TRegIniFile.Create(CBASEREGKEY);
    ProductName.Caption := reg.readString('CUSTOM','Title','NoName');
    Version.Caption := 'Version ' + reg.readString('CUSTOM','Version','2.0');
    Copyright.Caption := '(C) 1998-2001 MurkWorks, Inc. All Rights Reserved';
    Comments.Caption := reg.readString('CUSTOM','Comments','Contact Distributor for Assistance');

    i := Pos('$',Comments.Caption);
    while i <> 0 do begin
          Comments.Caption := Copy(Comments.Caption,1,i-1) + #10 + Copy(comments.Caption,i+1,Length(Comments.Caption));
          i := Pos('$',Comments.Caption);
    end;

end;

end.
 

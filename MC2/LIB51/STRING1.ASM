* Concatinate to a string: strcat(dest, source)
strcat	MOV	R0,#-7		Dest string
	LCALL	?auto0		Point to it
	MOV	DPL,[R0]	Get LOW argument
	INC	R0		Advance to next
	MOV	DPH,[R0]	Get HIGH argument
?1	MOVX	A,[DPTR]	Get char
	JZ	?2		End of string
	INC	DPTR		Advance to next
	SJMP	?1		And proceed
* Copy a string: strcpy(dest, source)
strcpy	MOV	R0,#-7		Dest string
	LCALL	?auto0		Point to it
	MOV	DPL,[R0]	Get LOW dest
	INC	R0		Advance to next
	MOV	DPH,[R0]	Get HIGH dest
?2	INC	R0		Advance
	MOV	?R1,[R0]	Get LOW source
	INC	R0		Advance
	MOV	?R2,[R0]	Get HIGH source
?3	XCH	A,R1		Get LOW
	XCH	A,DPL		Swap low
	XCH	A,R1		Resave
	XCH	A,R2		Get HIGH
	XCH	A,DPH		Swap high
	XCH	A,R2		Resave
	MOVX	A,[DPTR]	Get char
	INC	DPTR		Advance
	XCH	A,R1		Get LOW
	XCH	A,DPL		Swap low
	XCH	A,R1		Resave
	XCH	A,R2		Get HIGH
	XCH	A,DPH		Swap high
	XCH	A,R2		Resave
	MOVX	[DPTR],A	Write char
	JZ	?4		End of string, exit
	INC	DPTR		Advance
	SJMP	?3		Proceed
?4	MOV	A,DPL		Get LOW
	MOV	B,DPH		Get HIGH
	RET
* S/N: 6407

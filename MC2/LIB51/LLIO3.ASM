* Write a number to the console in decimal: putnum(unsigned int n)
putnum	MOV	R0,#-5		Address of parameter
	LCALL	?auto0		Set up addressability
	MOV	R2,#0		Reset count
* Step through each digit
?1	MOV	A,[R0]		Get LOW byte
	INC	R0		Advance to next
	MOV	B,[R0]		Get HIGH byte
	MOV	R3,#10		/ 10
	MOV	R4,#0		/ 0 (16 bit)
	LCALL	?div		Perform division
	MOV	[R0],B		Resave HIGH result
	DEC	R0		Backup
	MOV	[R0],A		Resave LOW result
	PUSH	?R5		Save result
	INC	R2		Advance count
	ORL	A,B		Any number left?
	JNZ	?1		Yes, do them all
* We have the number stacked, now output it
?2	POP	A		Get value
	ADD	A,#'0'		Convert to ASCII
	LCALL	?putch		Output via monitor
	DJNZ	R2,?2		Do them all
	RET
*
* Write a number to the console in hex: puthex(unsigned int n)
*
puthex	MOV	R0,#-4		Address of parameter
	LCALL	?auto0		Set up addressability
	MOV	A,[R0]		Get HIGH byte
	DEC	R0		Advance to next
	LCALL	?3		Print it
	MOV	A,[R0]		Get LOW byte
?3	PUSH	A		Save value
	SWAP	A		Get high nibble
	LCALL	?4		Output it
	POP	A		Get low nibble
?4	ANL	A,#%00001111	Use low digit only
	ADD	A,#'0'		Convert to ASCII
	CJNE	A,#'0'+10,*+3	Non-destructive test
	JC	?5		A = 0-9
	ADD	A,#7		Convert HEX digits
?5	LJMP	?putch		And write the character
$EX:putch
* S/N: 6407

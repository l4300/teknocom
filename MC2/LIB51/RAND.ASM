* Generate a pseudo-random number: int rand(limit)
rand	MOV	R0,#-5		Address of parm
	LCALL	?auto0		Point to it
	MOV	A,RANDSEED	Get LOW value
	MOV	B,RANDSEED+1	Get HIGH value
	MOV	R3,#$8D		LOW  half of 13709
	MOV	R4,#$35		HIGH half of 13709
	LCALL	?mul		Perform multiply
	ADD	A,#$19		LOW  half of 13849
	XCH	A,B		Swap
	ADDC	A,#$36		HIGH half of 13849
	XCH	A,B		Swap
	MOV	RANDSEED,A	Resave result (LOW)
	MOV	RANDSEED+1,B	Resave result (HIGH)
	MOV	?R3,[R0]	Get LOW limit
	INC	R0		Advance
	MOV	?R4,[R0]	Get HIGH limit
	LJMP	?mod		Return remainder
$SE:2
RANDSEED DS	2
* S/N: 6407

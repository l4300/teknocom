* Compare strings: strcmp(string1, string2)
strcmp	MOV	R0,#-7		Dest string
	LCALL	?auto0		Point to it
	MOV	DPL,[R0]	Get LOW dest
	INC	R0		Advance to next
	MOV	DPH,[R0]	Get HIGH dest
	INC	R0		Advance
	MOV	?R1,[R0]	Get LOW source
	INC	R0		Advance
	MOV	?R2,[R0]	Get HIGH source
?1	XCH	A,R1		Get LOW
	XCH	A,DPL		Swap low
	XCH	A,R1		Resave
	XCH	A,R2		Get HIGH
	XCH	A,DPH		Swap high
	XCH	A,R2		Resave
	MOVX	A,[DPTR]	Get char
	MOV	B,A		Save for later
	INC	DPTR		Advance
	XCH	A,R1		Get LOW
	XCH	A,DPL		Swap low
	XCH	A,R1		Resave
	XCH	A,R2		Get HIGH
	XCH	A,DPH		Swap high
	XCH	A,R2		Resave
	MOVX	A,[DPTR]	Get value
	CJNE	A,B,?2		Test for same
	JZ	?3		End of string... Equal
	INC	DPTR		Advance
	SJMP	?1		Proceed
?2	MOV	A,#1		Assume string1 > string2
	JNC	?3		Yes, it is
	MOV	A,#-1		string1 < string2
?3	LJMP	?sign		Sign extend
* S/N: 6407

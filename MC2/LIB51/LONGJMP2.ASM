* Set a long jump in EXTERNAL memory: setjmp(char jaddr[3])
setjmp	MOV	R0,#-5		Address parameter
	LCALL	?auto0		Point to it
	MOV	DPL,[R0]	Get LOW pointer
	INC	R0		Advance
	MOV	DPH,[R0]	Get HIGH pointer
	MOV	A,SP		Get stack pointer
	MOVX	[DPTR],A	Save in structure
	MOV	R0,A		Set up pointer
	INC	DPTR		Advance
	MOV	A,[R0]		Get HIGH ret address
	MOVX	[DPTR],A	Save it
	INC	DPTR		Advance
	DEC	R0		Back to low
	MOV	A,[R0]		Get LOW ret address
	MOVX	[DPTR],A	Save it
	CLR	A		Zero LOW
	MOV	B,A		Copy
	RET
* Peform a long jump via EXTERNAL memory: longjmp(char jaddr[3], int rc)
longjmp MOV	R0,#-7		Address parameter
	LCALL	?auto0		Point to it
	MOV	DPL,[R0]	Get LOW pointer
	INC	R0		Advance
	MOV	DPH,[R0]	Get HIGH pointer
	INC	R0		Advance
	MOV	?R7,[R0]	Get LOW return
	INC	R0		Advance
	MOV	B,[R0]		Get HIGH return
	MOVX	A,[DPTR]	Get saved SP
	MOV	SP,A		Reset stack
	MOV	R0,A		Set up pointer
	INC	DPTR		Advance
	MOVX	A,[DPTR]	Get HIGH return address
	MOV	[R0],A		Save it
	INC	DPTR		Advance
	DEC	R0		Back to low
	MOVX	A,[DPTR]	Get LOW ret address
	MOV	[R0],A		Save it
	MOV	A,R7		Get LOW ret valuge
	RET
* S/N: 6407

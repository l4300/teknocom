* Set a long jump in EXTERNAL memory: setjmp(char jaddr[5])
setjmp	MOV	R0,#-5		Address parameter
	LCALL	?auto0		Point to it
	MOV	DPL,[R0]	Get LOW pointer
	INC	R0		Advance
	MOV	DPH,[R0]	Get HIGH pointer
	MOV	A,SP		Get internal stack pointer
	MOVX	[DPTR],A	Save in structure
	MOV	R0,A		Set up pointer
	INC	DPTR		Advance
	MOV	A,?stack	Get external SP low
	MOVX	[DPTR],A	Save in structure
	INC	DPTR		Advance
	MOV	A,?stack+1	Get external SP high
	MOVX	[DPTR],A	Save in structire
	INC	DPTR		Advance
	MOV	A,[R0]		Get HIGH ret address
	MOVX	[DPTR],A	Save it
	INC	DPTR		Advance
	DEC	R0		Back to low
	MOV	A,[R0]		Get LOW ret address
	MOVX	[DPTR],A	Save it
	CLR	A		Zero LOW
	MOV	B,A		Copy
	RET
* Peform a long jump via EXTERNAL memory: longjmp(char jaddr[5], int rc)
longjmp MOV	R0,#-7		Address parameter
	LCALL	?auto0		Point to it
	MOV	DPL,[R0]	Get LOW pointer
	INC	R0		Advance
	MOV	DPH,[R0]	Get HIGH pointer
	INC	R0		Advance
	MOV	?R7,[R0]	Get LOW return
	INC	R0		Advance
	MOV	B,[R0]		Get HIGH return
	MOVX	A,[DPTR]	Get saved internal SP
	MOV	SP,A		Reset stack
	MOV	R0,A		Set up pointer
	INC	DPTR		Advance
	MOVX	A,[DPTR]	Get external SP low
	MOV	?stack,A	Reset ESP low
	INC	DPTR		Advance
	MOVX	A,[DPTR]	Get external SP high
	MOV	?stack+1,A	Reset ESP high
	INC	DPTR		Advance
	MOVX	A,[DPTR]	Get HIGH return address
	MOV	[R0],A		Save it
	INC	DPTR		Advance
	DEC	R0		Back to low
	MOVX	A,[DPTR]	Get LOW ret address
	MOV	[R0],A		Save it
	MOV	A,R7		Get LOW ret valuge
	RET
* S/N: 6407

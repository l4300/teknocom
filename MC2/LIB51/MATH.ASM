* Return ABSOLUTE VALUE of argument: int abs(int n)
abs	MOV	R0,#-5		Argument address
	LCALL	?auto0		Point to it
	MOV	A,[R0]		Get LOW value
	INC	R0		Advance
	MOV	B,[R0]		Get HIGH value
	JNB	B.7,?3		Positive, leave alone
	LJMP	?neg		Return negative
* Return MAXIMUM of two arguments: int max(int n1, int n2)
max	MOV	R0,#-7		Address of first argument
	LCALL	?auto0		Point to it
	MOV	?R3,[R0]	Get LOW value
	INC	R0		Advance to high
	MOV	?R4,[R0]	Get HIGH value
	INC	R0		Advance to second arg
	MOV	?R5,[R0]	Get LOW value
	INC	R0		Advance to high
	MOV	?R6,[R0]	Get HIGH value
	MOV	A,R5		Get LOW
	MOV	B,R6		Get HIGH
	LCALL	?scomp		Perform the compare
	JC	?2		R56 < R34
?1	MOV	A,R5		Get LOW result
	MOV	B,R6		Get HIGH result
	RET
* Return MINIMUM of two arguments: int min(int n1, int n2)
min	MOV	R0,#-7		Address of first argument
	LCALL	?auto0		Point to it
	MOV	?R3,[R0]	Get LOW value
	INC	R0		Advance to high
	MOV	?R4,[R0]	Get HIGH value
	INC	R0		Advance to second arg
	MOV	?R5,[R0]	Get LOW value
	INC	R0		Advance to high
	MOV	?R6,[R0]	Get HIGH value
	MOV	A,R5		Get LOW
	MOV	B,R6		Get HIGH
	LCALL	?scomp		Perform the compare
	JC	?1		R56 < R34
?2	MOV	A,R3		Get LOW result
	MOV	B,R4		Get HIGH result
?3	RET
* Return SQUARE ROOT of argument: int sqrt(int arg)
sqrt	MOV	R0,#-5		Address of parameter
	LCALL	?auto0		Point to it
	MOV	?R3,[R0]	Get LOW value
	INC	R0		Advance
	MOV	?R4,[R0]	Get HIGH value
	MOV	B,$FE		HIGH half of (255*255)
	MOV	A,$01		LOW  half of (255*255)
	MOV	R7,#0		Begin with zero
	LCALL	?ult		Perform a compare
	JZ	?4		Its OK
	MOV	B,#1		HIGH half of 256
	CLR	A		LOW is zero
	RET
?4	MOV	A,R7		Get value
	INC	R7		Advance
	MOV	B,A		Double
	MUL			Perform multiply
	LCALL	?ult		Compare it
	JNZ	?4		Not yet
	MOV	A,R7		Get value
	DEC	A		Backup to right valuge
	MOV	B,#0		Clear high
	RET
* S/N: 6407

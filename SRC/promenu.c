/* #define	DEBUGT	1 */	/* debug template selection */

/* menu stuff */

/* the format of the menu struct is a bit of a drag to understand, however it
   makes it easy to generate different menus

   mnuptr-> word
	    string of text

   word = 0 means last menu choice
   word < 256 means a selection in the switch for an action
   word > 255 means a pointer to the next menu choice
*/

unsigned int
NextItem(ci)
	unsigned int *ci;
{
/*	gsZ = ci; */
	gsZ = ci + strlen(((char *) ci) + 2) + 3;
	
/*	ci = ((char *) ci) + strlen(((char *) ci) + 2) + 3; */
	return(gsZ);
}

unsigned int
PrevItem(ci, mi)
	unsigned int *ci;
	unsigned int *mi;
{
	unsigned int *c;

	if(ci == mi) {	/* get last item in list */
		while(*ci) {
			c = ci;
			ci = NextItem(ci);
		}
		return(c);
	}
	while(1) {
		c = mi;
		c = NextItem(c);
		if(c == ci)
			return(mi);
		mi = c;
	}
}


int
GetChoice(str)
	char *str;
{
/*	int	Key; */

	while(1) {
		LCD_Print(str);
		gsZ = ticks+30;
		while(!(Key = GetKey(tstbit(CbitKeyContinuous) ? 2 : 5))) {
			if(LpProcessPacket()) {
				longShutDown();
				setBBit(BITS_BLOCK1);
				ShowBits(bottomBits,0);
				gsZ = ticks+30;
			}
			checkShutDown();
			IDLE();
			if(ticks >= gsZ)
				break;
		}
		if (Key != KEY_NONE) {
			clrBBit(BITS_BLOCK1);
			resetShutDown();
			return(Key);
		}
	}
}


int
StringSelection(str,prompt)
	char	*str;		/* choose comma list, -1 on cancel, or selection */
	char	*prompt;
{
/*	char	*cptr, *pptr; */
	int	index;
first:;
	index = 0;
#ifdef	TINYMENU
	if(str < 100) {
		index = str;
		str = NULL;
	}
#endif
/*	cptr = str; */
	gsY = str;

	while(1) {
		if(prompt && str)
			strcpy(gString,prompt);
		else if(str != gString)
			gString[0] = 0;
		if(str) {
			for(gsA = strlen(gString), gsZ=gsY; *CharP(gsZ) && *CharP(gsZ) != ','; gsA++, gsZ++)
				gString[gsA] = *CharP(gsZ);
			gString[gsA] = 0;
		} else {
		 	gsZ = (*(char **) prompt)(&index);	/* what strangeness, call func via variable */
			if(!gsZ)
				return(-2);

			if(gsZ != gString)
				strcpy(gString,gsZ);
		}

		Key = GetChoice(gString);
		switch(Key) {
			case	KEY_PLUS :
				index++;

				if(str) {
					gsZ = gsY;
					while(*CharP(gsZ) && *CharP(gsZ) != ',')
						gsZ++;
					if(!*CharP(gsZ))
						goto first;
					gsY = gsZ+1;
				} else {
				 	if(!(*(char **) prompt)(&index))
						goto first;
				}
				break;
			case	KEY_MINUS :
				if(!str) {
					index--;
					if(!(*(char **)prompt)(&index))
						index++;
					break;
				}
				if(gsY == str) {	/* first in list */
					gsZ = gsY;
					while(*CharP(gsY)) {
						if(*CharP(gsY) == ',') {
							index++;
							gsZ = gsY+1;
						}
						gsY++;
					}
					gsY = gsZ;
				} else {
					gsY -= 2;
					while(gsY != str && *CharP(gsY) != ',')
						gsY--;
					index--;
					if(gsY != str)
						gsY++;
				}
				break;
			case	KEY_SELECT :
            	/* wait for them to let up on key */
                while(GetKey(1)) {
                	checkShutDown();
                    clockDelay(#1)
                }
				return(index);
			case	KEY_META2 :
				return(-1);
			case	KEY_META :
			default:
				Sound(SOUND_ERROR|SOUND_NOLED);
		}	/* end switch */
	}
}

#ifdef	DOMEMORY
int
SetCursor(col, str,tack)
	unsigned int col;
	unsigned char *str;
	unsigned char *tack;
{
	for(gsA=0; gsA < 8 &&  *tack; gsA++)
		*(str+gsA) = (*tack++) & 0x7f;
	*(str+gsA++) = (0x80|0x10) | (col & 0xf);
	memcpy(str+gsA,"\361\000\010\000\000\000",6);
#ifdef	old
	*(str+gsA++) = '\361';
	*(str+gsA++) = '\000';
	*(str+gsA++) = '\010';
	*(str+gsA++) = '\000';
	*(str+gsA++) = 0;
	*(str+gsA++) = 0;
#endif
}

unsigned char
FindChar(c,dir, numeric)
	unsigned char c;
	char	dir;
	unsigned char	numeric;
{
	char	*x;

/* printf("findchar num %d %d (%c) dir %d\n",numeric,c,c,dir); */

	if(numeric & 1) {
		gsD = *CharP(FindChar_NumberLen);
		gsZ = FindChar_numbers;
	} else  {
		gsD = *CharP(FindChar_ValLen);
		gsZ = FindChar_vals;
	}
	gsA = c & 0x7f;
	gsX = strchr(gsZ,gsA);

/* printf("found char @ %x from %x lim %x\n",gsX,gsZ,gsD); */

	if(!gsX) 
		return(*CharP(gsZ));

	if(dir < 0) 
		gsX -= -dir;
	else
		gsX += dir;

/* printf("gsX incr now %x\n",gsX); */

	if(gsX < gsZ)
		gsX = gsZ;
	else if(gsX >= gsZ + gsD)
		gsX = gsZ + gsD -1;

/* printf("Returning gsX %x %d (%c)\n",gsX,*CharP(gsX),*CharP(gsX)); */

	return(*CharP(gsX));
}

int
StringEdit(str, cursor,numeric)		/* edit string up to maxlen */
	char	*str;			/* the string to edit */
	int	cursor;			/* cursor location */
	int	numeric;
{
	int	Key;
	char	buff[20];	/* maximum string to edit */
	int	shift;
	int	len;
	unsigned int keydownTime;
	/* str is always edited, we just copy the appropriate values
	   to buff as needed and shift over
	*/
	shift = 0;
	len = strlen(str);
	keydownTime = ticks;
	gsB = cursor;

    if(*str != ' ')
    	numeric |= 0x80;	/* this bit set if not a blank edit */

    numeric |= 0x40;	/* this bit cleared if they changed prev char */
	while(1) {
		cursor = gsB;
		/* set up cursor, col, buff and str */
		if(cursor > 6) {
			if(len - cursor > 7)
				shift = cursor - 6;
			else
				shift = len - 8;
		} else
			shift = 0;
		SetCursor(cursor - shift,buff, str+shift);
		LCD_Print(buff);

		while(!(Key = GetKey(tstbit(CbitKeyContinuous) ? 2 : 5))) {
			checkShutDown();
			LpProcessPacket();
			IDLE();
			keydownTime = ticks;
		}

		gsY = str;
		gsB = cursor;

#define	delta	gsC

		delta = (ticks - keydownTime)/18 + 1;
		if(delta > 5) {
			delta = 5;
			keydownTime += 90;
		}

		resetShutDown();
		gsA = *CharP((gsY+gsB)); /* *(str + cursor);*/
		switch(Key) {
			case	KEY_PLUS :
				gsA = FindChar(gsA,delta,numeric);
                numeric &= ~0x40;
				break;
			case	KEY_MINUS :
				gsA = FindChar(gsA,-delta,numeric);
                numeric &=~ 0x40;
				break;
			case	KEY_SELECT :
				gsB++; /* cursor++; */
				if(*CharP((gsY + gsB)) & 0x80)
					gsB++;
				if(gsB == len) {
					return(KEY_SELECT);
				}
                if((numeric & 0xc1) || *CharP(gsY+gsB) != ' ')			/* this will not copy prev char to next char */
					continue;
            	else {
                	numeric |= 0x40;		/* this will copy prev char to next char */
                	break;
                }
			case	KEY_META2 :
				return(KEY_META2);
			case	KEY_META :
				if(gsB > 0)
					gsB--;
				else	{
					Sound(SOUND_ERROR|SOUND_NOLED);
				}
				if(*CharP((gsY+gsB)) & 0x80) {
					if(gsB > 0)
						gsB--;
					else
						gsB++;
				}
				continue;
			default:
				Sound(SOUND_ERROR|SOUND_NOLED);
				continue;
		} /* end switch */
		*CharP((gsY+gsB)) = gsA;
	} 	/* end while */
#undef	delta
}
#endif	/* domemory */


#ifdef	DOCAM
char *
mnuCamFunction(index)
	int *index;
{
	char	*z;

	gsX = *index;


	if(gsX > MAX_CAM)
		gsX = 0;

	if(*index < 0)
		gsX = MAX_CAM;

	switch(gsX) {
		case 0 :
			z = mnuCamFunctionOff;
			break;
		case MAX_CAM :
			z = mnuCamFunctionExit;
			break;
		default :
			sprintf(gString+16,"%d",gsX+1);
			z = gString + 16;
	}
	*index = gsX;

#ifdef	DOEMR
	if(sel == mnuEmrMode)
		sprintf(gString,mnuEmrFunctionEMR,z);
	else
#endif	
#ifdef	MODEL1ES
		/* goofy KIWO mesh instead of cam */
      		sprintf(gString,StrMenuMESH/*"MESH %s"*/,z);
#else
#ifdef	MODEL1EG	/* this model is special, its mesh too */
		sprintf(gString,mnuCamFunctionCAM,z);
#else
		sprintf(gString,mnuCamFunctionCAM,z);
#endif
#endif
	return(gString);
}

SetCamMode(cnt)
	unsigned char cnt;
{
	if((UserConstants.ucCamLimit = cnt) > 1) {
		UserConstants.srfFlags |= SRF_CAMMODE;
		UserConstants.ucCamCount = 0;
		movfpac(cons0,UserConstants.ucCamSum);
	} else {
	 	UserConstants.ucCamLimit = 0;
		UserConstants.srfFlags &= ~SRF_CAMMODE;
	}
}

#endif	/* DOCAM */

#ifdef	DOMEMORY
char *
mnuChooseBatch(index)
	int *index;
{
	char	*z;

	gsX = *index;

	if((!(mnuMemFlag & (PMF_ALL|PMF_OFF)) && !*index) || *index < 0)
		gsX = MAXSLOTS+1;
	else if(*index > MAXSLOTS+1) {
		if((mnuMemFlag & (PMF_ALL|PMF_OFF)))
			gsX = 0;
		else
			gsX = 1;
	} 
 
	*index = gsX;

	switch(gsX) {
		case 0 :
			if(mnuMemFlag & PMF_ALL)
				z = chooseBatchAll;
			else if(mnuMemFlag & PMF_OFF)
				z = mnuCamFunctionOff;
			else
				z = chooseBatchNone;

			goto docopy;
		case MAXSLOTS+1 :
			z = mnuCamFunctionExit;
docopy:;
			strcpy(gString,z);
			break;
		default :
			gsX--;
			sprintf(gString,chooseBatchFmt,gsX + 'A',Batches[gsX].biInfo.sbEntryCount ? '*' : ' ');
			if(mnuMemFlag & PMF_TAG) {
				strcat(gString,Batches[gsX].biInfo.sbDescription);
				gString[8] = 0;
			}
	}

	return(gString);
}

#endif	/* do memory */



int
DoMenu()
{

	if(Key == KEY_MINUS) {
		currentMenu = currentItem = MENUBASE;
		currentItem = PrevItem(currentItem, currentMenu);
		goto getMore;
	} else if(Key == KEY_META2) {
		currentMenu = currentItem = SYSMENU;
		goto getMore;
	}
menuTop:;
	currentMenu = currentItem = MENUBASE;
getMore:;
	sel = 0;
	while(!sel) {
		/* first step, pull up the current item and display it */
        gsX = (char *) currentItem+2;
        if('e' == *CharP(gsX))
        	sprintf(gString,"%s%s",mnuCamFunctionExit,CharP((gsX+1)));
        else
        	strcpy(gString,CharP(gsX));

		Key = GetChoice(gString);
		switch(Key) {
			case	KEY_SELECT :
				/* its either a menu, or a selection */
				if(*currentItem == 0xffff)
					return(0);	/* quit please */

				if(*currentItem < 256) {
					sel = *currentItem;
					break;
				}
				currentItem = currentMenu = *currentItem;
				continue;
			case	KEY_PLUS :
nextItem:;
				currentItem = NextItem(currentItem);
				if(!*currentItem)
					currentItem = currentMenu;
				break;
			case	KEY_MINUS :
				currentItem = PrevItem(currentItem, currentMenu);
				break;
			case	KEY_META2 :	/* sel minus */
				if(currentMenu == MENUBASE) {
menuExit:;
					return(0);
				} else
					currentMenu = currentItem = MENUBASE;
				break;
			case	KEY_META :
				ShutDown();
			default :;
				Sound(SOUND_ERROR|SOUND_NOLED);
				continue;
		}	/* end switch key */
	}

	switch(sel) {
#ifndef	TINYMENU
#ifndef	MODEL5
		case mnuCalOne :
		case mnuCalTwo :
			sel = (sel == mnuCalOne ? 1 : 2);
#ifdef	DUALCALMODE
			sprintf(gString+16,StrMenuDCALPrompt/*"SHIM,MESH"*/);
			gsA = StringSelection(gString+16,NULL);
			if(gsA == 0xff)
				goto menuTop;
		     else if(gsA)
				sel |= 0x80;
#endif
			if(CalProbe(sel)) {
            	LCD_Print(mnuCalComplete);
                Sound(SOUND_START);
                clockDelay(#30);
				goto menuExit;
			}
			break;
		case  menuCalFactory :
#ifdef	PRONF
#ifdef	PROF
			if(SysConstants.scFlags & SCFLG_NOFERROUS)
				gsZ = StrFactCalNonFerrous;
			else if(SysConstants.scFlags & SCFLG_NONONFERROUS)
				gsZ = StrFactCalFerrous;
			else
				gsZ = StrFactCalAll;

			gsX = StringSelection(gsZ,NULL);
			switch(gsX) {
				case 0:
					if(SysConstants.scFlags & SCFLG_NOFERROUS) 
						goto clnf;

					UserConstants.srfFlags &= ~SRF_CALMODE2;
					movfpac(cons0, UserConstants.ucFCalRatio);
					UserConstants.ucFCalibration = 0;
					break;

				case 1:
					if(SysConstants.scFlags & (SCFLG_NONONFERROUS|SCFLG_NOFERROUS))
						goto menuTop;
clnf:;
					UserConstants.srfFlags &= ~SRF_CALMODE;
					movfpac(cons0, UserConstants.ucNFCalRatio);
					UserConstants.ucCalibration = 0;
					break;
				case 2:
					UserConstants.srfFlags &= ~SRF_CALMODE2;
					movfpac(cons0, UserConstants.ucFCalRatio);
					UserConstants.ucFCalibration = 0;
					UserConstants.srfFlags &= ~SRF_CALMODE;
					movfpac(cons0, UserConstants.ucNFCalRatio);
					UserConstants.ucCalibration = 0;
					break;
						break;

#else
/* just non ferrous */
			gsX = StringSelection(StrFactCalNonFerrous,NULL);
			switch(gsX) {
				case 0:
					UserConstants.srfFlags &= ~SRF_CALMODE;
					movfpac(cons0, UserConstants.ucNFCalRatio);
					UserConstants.ucCalibration = 0;
					break;
#endif
#else
/* just ferrous */
			gsX = StringSelection(StrFactCalFerrous,NULL);
			switch(gsX) {
				case 0:
#ifdef	DUALCALMODE
					sprintf(gString+16,StrMenuDCALPrompt);
					gsA = StringSelection(gString+16,NULL);
                                       sel = 0;
				       if(gsA == 0xff)
					       goto menuTop;
                                       else if(gsA)
                                               sel |= 0x80;
#endif
#ifdef	DUALCALMODE
													 if(!sel) {
#endif
														 UserConstants.srfFlags &= ~SRF_CALMODE2;
														 movfpac(cons0, UserConstants.ucFCalRatio);
														 UserConstants.ucFCalibration = 0;
#ifdef	DUALCALMODE	/* also clear non-ferrous registers */
					} else {
														 UserConstants.srfFlags &= ~SRF_CALMODE;
														 movfpac(cons0, UserConstants.ucNFCalRatio);
														 UserConstants.ucCalibration = 0;
													 }
#endif	/* end dualcalmode */
#endif	/* endif just ferrous mode */
				default:
					goto menuTop;
			}	/* end switch */

			goto menuExit;
#endif	/* model5 */

#ifdef	DOMEMORY
		case  mnuMemoryOff:	/* choose a memory port */
			if(UserConstants.srfFlags & SRF_MEMORYMODE)
				mnuMemFlag = (PMF_TAG|PMF_OFF);
			else
				mnuMemFlag = PMF_TAG;

			gsA = StringSelection(NULL,&mnuChooseBatch);
			if(gsA == 0xff || gsA == MAXSLOTS+1)	/* exit */
				break;
			if(UserConstants.srfFlags & SRF_MEMORYMODE) {
				if(gsA == 0) {
cancelmemory:;
					UserConstants.srfFlags &= ~SRF_MEMORYMODE;
					goto menuExit;
				}
			} 
			gsA--;
			UserConstants.ucActiveMemorySlot = gsA;
			UserConstants.srfFlags |= SRF_MEMORYMODE;
			if(!Batches[gsA].biInfo.sbEntryCount && !(Batches[gsA].biFlags & 1)) {	/* select rows */
				sel = gsA;
				FreeMemSlot(gsA);
#ifdef	DOTEMPLATES
				gsX = ChooseTemplate();	/* wipes gsA */
				gsA = sel;
				if(gsX == 102)
					goto cancelmemory;
#ifdef	DEBUGT
	printf("choose %d gsa %d\n",gsX,gsA);
#endif
				ClockPack(&Batches[gsA].biInfo.sbTimeStart[0]);
				if(gsX != 100) {	/* set up template */
					Batches[gsA].biInfo.sbTemplate = gsX+1;
					memcpy(&Batches[gsA].biInfo.sbRows,&Templates[gsX].tiRows,8+7);
					gsC = 0;
					if(UserConstants.ucLimitLow = Templates[gsX].tiLimitLow)
						gsC++;
					if(UserConstants.ucLimitHigh = Templates[gsX].tiLimitHigh)
						gsC++;
					if(gsC)
						UserConstants.srfFlags |= SRF_LIMITMODE;
					else
						UserConstants.srfFlags &= ~SRF_LIMITMODE;

					SetCamMode(Templates[gsX].tiCamMode);
					TemplateQuestions(gsA);
				} else {
#endif	/* DO TEMPLATES */
					Batches[gsA].biInfo.sbRows = Batches[gsA].biInfo.sbColumns = 0;
#ifndef	NOMEMROWS
					sel = gsA;
					EditValue(1,1,99,0,0,mnuRows,gsX);
					gsA = sel;
					if(gsX != INFINITY && gsX != 1) {
						Batches[gsA].biInfo.sbRows = gsX;
						EditValue(0,0,99,0,0,mnuCols,gsX);
						gsA = sel;
						if(gsX != INFINITY && gsX > 0)
							Batches[gsA].biInfo.sbColumns = gsX;
					}
#endif	/* endif not model2 */
					if(UserConstants.srfFlags & SRF_LIMITMODE)
						memcpy(&Batches[gsA].biInfo.sbLimitLow,&UserConstants.ucLimitLow,4);
					Batches[gsA].biInfo.sbCamMode = UserConstants.ucCamLimit;
#ifdef	DOTEMPLATES
				}
#endif
			}
			goto menuExit;
		case  mnuMemoryClear:
		case  mnuMemoryStatus :
#ifdef	DOPRINT
		case  mnuMemoryPrint :
#endif
			mnuMemFlag = (PMF_ALL|PMF_TAG);
			gsA = StringSelection(NULL,&mnuChooseBatch);
			if(gsA == 0xff || gsA == MAXSLOTS+1)
				break;
			switch(sel) {
				case mnuMemoryClear :
					if(!gsA) {
						LCD_Print(DoMenu_FactoryConfig);
						for(sel = 0; sel < MAXSLOTS; sel++)
							FreeMemSlot(sel);
					} else {
						FreeMemSlot(gsA-1);
					}
					break;
				case mnuMemoryStatus :
					if(!gsA) {
						gsX = GetUsedMem();
						sprintf(gString,mnuMemoryUsed,gsX,MAXBLOCKS*DBMAXVALUES);
						GetChoice(gString);
					} else {
						gsA--;
#ifdef	DOPRINT
						MemoryStat(gsA);
#else
						sprintf(gString,mnuBatchInfo,
							gsA+'A',Batches[gsA].biInfo.sbDescription,Batches[gsA].biInfo.sbEntryCount,
							Batches[gsA].biInfo.sbRows,Batches[gsA].biInfo.sbColumns);
						GetChoice(gString);
#endif
					}
					break;
#ifdef	DOPRINT
				case mnuMemoryPrint :
					MemoryPrint(gsA);
					break;
#endif					
			}
			goto menuExit;
#ifdef	DOPRINT
		case mnuConfigPrint :
        	MemoryConfigPrinter();
            break;
#endif                        
		case  mnuMemoryTag :		/* edit memory tag */
#ifdef	DOTOUCH
		case  mnuMemoryTouch :	/* save to touch memory */
#endif
			mnuMemFlag = PMF_TAG;
			gsA = StringSelection(NULL,&mnuChooseBatch);
			if(gsA == 0xff || gsA == MAXSLOTS+1)
				break;
			gsA--;
#ifdef	DOTOUCH
			if(sel == mnuMemoryTouch)
				MemoryTouch(gsA);
			else {
#else
				{
#endif                            
				strcpy(gString,Batches[gsA].biInfo.sbDescription);
				c = gString + strlen(gString);
				while(c < gString+7)
					*c++ = ' ';
				*c = 0;

				sel = gsA;
				Key = StringEdit(gString,0,0);
				if(Key == KEY_SELECT) {
					strcpy(Batches[sel].biInfo.sbDescription,gString);
#ifdef	DOTEMPLATES

					TemplateQuestions(sel);
#endif
				} else	{
					Sound(SOUND_CANCEL|SOUND_NOLED);
				}
			}
			goto menuTop;
#endif	/* DOMEMORY */
#ifdef	DOCAM
		case  mnuCamMode :
#ifdef	DOEMR
		case  mnuEmrMode :
			UserConstants.srfFlags2 &= ~(SRF2_EMRON|SRF2_EMRE);	/* turn off emr */
#endif

			camMax = 0xffff;
			camMin = 0x7fff;

			gsA = StringSelection(NULL,&mnuCamFunction);
			if(gsA == 0x255 || gsA == MAX_CAM)
				goto menuTop;	/* cancel or exit */
			SetCamMode(gsA+1);	/* was without +1 */
#ifdef	DOEMR

			if(sel == mnuEmrMode && gsA > 0)
				UserConstants.srfFlags2 |= (SRF2_EMRON); /* turn on emr */
#endif
			goto menuExit;
#endif	/* docam */
#ifdef	DOLIMITS
		case  mnuLimitsOff:
			UserConstants.srfFlags &= ~SRF_LIMITMODE;
// 2/23/98 comment out			UserConstants.ucLimitLow = UserConstants.ucLimitHigh = 0;
			goto menuTop;
		case  mnuLimitsOn:
limon:;
			if(UserConstants.ucLimitLow != 0 || UserConstants.ucLimitHigh != 0) {
				UserConstants.srfFlags |= SRF_LIMITMODE;
				goto menuExit;
			} else	{
				Sound(SOUND_ERROR|SOUND_NOLED);
			}
			break;
		case  mnuLimitsLow:
			gsZ = UserConstants.ucLimitHigh;
			if(!gsZ)
				gsZ = MAX_LIMIT;
			else if(gsZ < UserConstants.ucLimitLow)
				gsZ = UserConstants.ucLimitLow;

			gsY = UserConstants.ucLimitLow;
			if(gsY > gsZ)
				gsY = gsZ;

			EditValue(gsY,0,gsZ,UserConstants.srfFlags,1,mnuLowLimit,gsX);
			if(gsX != INFINITY) {
				UserConstants.ucLimitLow = gsX;
				if(!UserConstants.ucLimitHigh) {
					UserConstants.ucLimitHigh = MAX_LIMIT;
					sel = 0;
					goto nextItem;
				} else if(!(UserConstants.srfFlags & SRF_LIMITMODE)) {
					sel = 0;
					goto nextItem;
				} else if(UserConstants.ucLimitHigh < gsX)
					UserConstants.ucLimitHigh = gsX;
				goto limon;
			} else	{
				Sound(SOUND_CANCEL|SOUND_NOLED);
			}
			break;
		case  mnuLimitsHigh:

			EditValue(UserConstants.ucLimitHigh,UserConstants.ucLimitLow,MAX_LIMIT,UserConstants.srfFlags,1,mnuHighLimit,gsX);
			if(gsX != INFINITY) {
				UserConstants.ucLimitHigh = gsX;
				if(UserConstants.ucLimitLow > gsX)
					UserConstants.ucLimitLow = gsX;
				goto limon;
			} else
				Sound(SOUND_CANCEL|SOUND_NOLED);
			break;
#endif	/* DOLIMITS */
#ifndef	MODEL5
		case  mnuUnitsMils:
			UserConstants.srfFlags &= ~SRF_UNITS;
			UserConstants.srfFlags |= SRF_MILS;
			goto menuExit;
		case  mnuUnitsMicrons:
			UserConstants.srfFlags &= ~SRF_UNITS;
			UserConstants.srfFlags |= SRF_MICRONS;
			goto menuExit;
#ifdef	mnuUnitsOZ
		case  mnuUnitsOZ:
			UserConstants.srfFlags &= ~SRF_UNITS;
			UserConstants.srfFlags |= SRF_OZUNITS;
			goto menuExit;
#endif
#endif	/* MODEL5 */

#ifdef	DOCLOCK
		case  mnuDate :
		case  mnuTime :
			ClockSReadWrite(sel == mnuDate ? 1 : 0,2);
			if(StringEdit(gString,0,1) == KEY_SELECT)
				ClockSReadWrite(sel == mnuDate ? 1 : 0,1);
			break;
#endif	/* DOCLOCK */
#ifdef	mnuBaudRate
		case  mnuBaudRate:
			StringSelection("300,1200,2400,4800,9600,EXIT",NULL);
			goto menuTop;
#endif
#endif	/* ENDIF NOT TINYMENU */
		case  mnuFactoryConfig:
			LCD_Print(DoMenu_FactoryConfig);
			memset(UserConstants,0, sizeof(UserConstants));
#ifdef	UDEFAULT_MICRONS
			UserConstants.srfFlags |= SRF_MICRONS;
#else
			UserConstants.srfFlags |= SRF_MILS;
#endif
#ifdef	DOPRINT
			UserConstants.ucPrinterOption = POPT_DEFAULT;	/* the saved printer configuration option */
#endif
			EraseAllMemory(0);
			Sound(SOUND_START|SOUND_NOLED);
			goto menuTop;
			break;
#ifdef	mnuTemplate
		case  mnuTemplate:
			break;
#endif
		case  mnuSerialNumber:
			sprintf(gString,"%u",SysConstants.scSerialNumber);
			GetChoice(gString);
			break;

		case mnuVersion :
#ifdef	MODEL1T
			sprintf(gString,"1T %x",VERSION);
#endif
#ifdef	MODEL1TF
			sprintf(gString,"1TF %x",VERSION);
#endif
#ifdef	MODEL1TN
			sprintf(gString,"1TN %x",VERSION);
#endif

#ifdef	MODEL1ES
			sprintf(gString,"1ES %x",VERSION);
#endif
#ifdef	MODEL1EG
			sprintf(gString,"1EG %x",VERSION);
#endif

#ifdef	MODEL1E
			sprintf(gString,"1E %x",VERSION);
#endif
#ifdef	MODEL1EN
			sprintf(gString,"1EN %x",VERSION);
#endif
#ifdef	MODEL1
			sprintf(gString,"1 %x",VERSION);
#endif
#ifdef	MODEL1F
			sprintf(gString,"1F %x",VERSION);
#endif
#ifdef	MODEL1N
			sprintf(gString,"1N %x",VERSION);
#endif
#ifdef	MODEL2
			sprintf(gString,"2 %x",VERSION);
#endif
#ifdef	MODEL2C
			sprintf(gString,"2C %x",VERSION);
#endif
#ifdef	MODEL3
			sprintf(gString,"3 %x",VERSION);
#endif
#ifdef	MODEL4
			sprintf(gString,"4 %x",VERSION);
#endif
#ifdef	MODEL5
			sprintf(gString,"5 %x",VERSION);
#endif
			GetChoice(gString);
			break;

		case mnuSysFlags :	/* set flags */
			EditValue(SysConstants.scFlags,0,0xff,0xffff,0,StrEditFlags,gsX)
			if(gsX != INFINITY)
				SysConstants.scFlags = gsX;
			break;
		case mnuSysSerial : 	/* set serial number */
			EditValue(SysConstants.scSerialNumber,0,0xffff,0xffff,0,StrEditSerialNumber,gsX)
			if(gsX != INFINITY) {
	/* hack to make batch with entries 
		nfCount = gsX;
		for(nfDelta = 0; nfDelta < nfCount; nfDelta++)
			StoreMemory(nfDelta);
	*/			
				SysConstants.scSerialNumber = gsX;
			}
			break;
#ifdef	PROF
		case mnuSysFerrous :	/* debug ferrous */
			while(!GetKey(0)) {
				fcheckProbe(1);
				sprintf(gString,"F %u",fArray[FAACTUAL]);
				LCD_Print(gString);
				IDLE();
			}
			break;
#endif
#ifdef	PRONF
		case mnuSysNonFerrous :	/* debug non ferrous */
			while(!GetKey(0)) {
				nfReading(NFRANGE,NFBURST);
				sprintf(gString,"NF %u",nfCount);
				LCD_Print(gString);
				IDLE();
			}
			break;
#endif
#ifdef	DOBENCH
		case mnuBenchTest :
newbench:;
			gsX = StringSelection(StrBenchChoice,NULL);
			if(gsX == 0xffff || gsX == 5)
				break;
	 		sel = gsX;
#ifdef	DOBENCH2
			gsX = StringSelection("NORM,CONT",NULL);
			if(gsX == 0xffff)
				break;
			else if(gsX == 1) {
				LCD_Print(DoMenu_FactoryConfig);
				MODE_SEL2PORT = MODE_SEL2PORT & ~MODE_SEL2; /* change P3 */
				switch(sel-1) {
					case 0 :		/* mode zero */
						gsZ = (MODE_ZERO|CNTR_ENABLEBIT) | (WAVEFORM_ZERO << 8);
						break;
					case 1 :		/* actual */
						gsZ = (MODE_ACTUAL|CNTR_ENABLEBIT) | (WAVEFORM_ZERO << 8);
						break;
					case 2 :		/* ref1 or reference */
						gsZ = (MODE_REF|CNTR_ENABLEBIT) | (WAVEFORM_REF << 8);
						break;
					case 3 :		/* ref2 or temperature */
						gsZ = (MODE_REF2|CNTR_ENABLEBIT) | (WAVEFORM_REF2 << 8);
						MODE_SEL2PORT = MODE_SEL2PORT | MODE_SEL2;
						break;
				}	/* end switch sel */
				CNTR_PORT = CNTR_PORT | MODE_ZERO;	/* shut down zero line */
				FANALOG_ON;			/* turn on analog power */
				ferrousP2 = gsZ  & 0xff;
				ferrousWaveform = (gsZ >> 8);
				READY_COUNTER;
				ClockFast();			/* switch to fast clock, no delay, 1 msec */
				clockDelay(#10);	   	/* 10 msec delay */
				setbit(bitClockFerrous);	/* start making waves */
				msec_reload = 49830/FFREQ;	/* set up idle refresh time 
						FFREQ is adjusted in prof.h for
						15Mhz clock
						*/
				while(!GetKey(1))
					IDLE();

				ANALOG_OFF;			/* turn of analog power */
				DISABLE_COUNTER;		/* what the heck */
				clrbit(bitClockFerrous);
				ClockSlow();      			/* reset system clock */
				break;
			}
#endif
			LCD_Print(DoMenu_FactoryConfig);
			while(!GetKey(1)) {
				KEY_PORT = (KEY_PORT & ~RKEY_MINUS);
				asm " nop";
				asm " nop";
				KEY_PORT = KEY_PORT | RKEY_MINUS;

			 	if(sel == 0) {	/* non-ferrous */
#ifdef	PRONF
					nfReading(NFRANGE,NFBURST);
					gsZ = nfCount;
#endif
				} else {
					gsA = sel-1;
					fReading(FBURST,(1 << gsA));	/* take the reading */
					gsA = sel-1;
					gsZ = fArray[gsA];
				}
			}
			sprintf(gString,"Y %u",gsZ);
			LCD_Print(gString);
			while(GetKey(1))
				IDLE();
			GetKey(0);
			sel = mnuBenchTest;
			goto newbench;
			break;
#endif
		default :;
			goto menuTop;
	}
	goto getMore;
}


 * --------- Start of Menu RootMenu -----------*
 * --------- Start of Menu CalibrateMenu -----------*
 org $524
 DRW $500
 STRZ 'e Cal'
 org $52c
 DRW $a
 STRZ '2 PTS'
 org $534
 DRW $b
 STRZ '1 PT'
 org $53b
 DRW $c
 STRZ 'Factory'
 org $545
 DW 0
 * ------  End of Menu --------- *
 org $500
 DRW $524
 STRZ 'Cal'
 org $506
 DRW $d
 STRZ 'CAM'
 * --------- Start of Menu UnitsMenu -----------*
 org $547
 DRW $500
 STRZ 'e UNITS'
 org $551
 DRW $e
 STRZ 'Mils'
 org $558
 DRW $f
 STRZ 'Microns'
 org $562
 DW 0
 * ------  End of Menu --------- *
 org $50c
 DRW $547
 STRZ 'UNITS'
 * --------- Start of Menu ConfigMenu1 -----------*
 org $564
 DRW $500
 STRZ 'e CNF'
 org $56c
 DRW $10
 STRZ 'Factory'
 org $576
 DRW $11
 STRZ 'Serial Number'
 org $586
 DRW $12
 STRZ 'Version'
 org $590
 DW 0
 * ------  End of Menu --------- *
 org $514
 DRW $564
 STRZ 'CONFIG'
 org $51d
 DRW $ffff
 STRZ 'e '
 org $522
 DW 0
 * ------  End of Menu --------- *
 * --------- Start of Menu SysMenu -----------*
 org $592
 DRW $13
 STRZ 'Flags'
 org $59a
 DRW $14
 STRZ 'SNumber'
 org $5a4
 DRW $15
 STRZ 'Ferrous'
 org $5ae
 DRW $16
 STRZ 'NonFerrous'
 org $5bb
 DRW $17
 STRZ 'Bench'
 org $5c3
 DRW $500
 STRZ 'Exit'
 org $5ca
 DW 0
 * ------  End of Menu --------- *
 org $5cc
 STRZ ' ()*+-/0123456789<=>@ABCDEFGHIJKLMNOPQRSTUVWXYZ'
 org $5fc
 DB $2f
 DB $0
 org $5fe
 STRZ '0123456789'
 org $609
 DB $a
 DB $0
 org $60b
 STRZ 'WAIT'
 org $611
 STRZ 'OFF'
 org $615
 STRZ 'EXIT'
 org $61a
 STRZ 'CAM %s'
 org $621
 STRZ 'EOM %s'
 org $628
 STRZ 'ALL'
 org $62c
 STRZ 'OFF'
 org $630
 STRZ 'NONE'
 org $635
 STRZ '%c%c'
 org $63a
 STRZ 'COMPLETE'
 org $643
 STRZ 'ROWS'
 org $648
 STRZ 'COLS'
 org $64d
 STRZ 'Used %u of %u'
 org $65b
 STRZ '%c %s %d Readings %d Rows %d Cols'
 org $67d
 STRZ 'LOWL'
 org $682
 STRZ 'HI L'
 org $687
 STRZ '00 PLATE'
 org $691
 STRZ 'MORE/SEL'
 org $69a
 STRZ 'W/ SHIM'
 org $6a3
 STRZ 'ADJC'
 org $6a8
 STRZ 'BATCH FULL'
 org $6b3
 STRZ '%s-'
 org $6b7
 STRZ ' %c%d  '
 org $6bf
 STRZ '%d   '
 org $6c5
 STRZ '�%d   '
 org $6cc
 STRZ '�R%d   '
 org $6d4
 STRZ 'MEM FULL'
 org $6dd
 STRZ 'TEMPLATE'
 org $6e6
 STRZ 'NONE/EXIT'
 org $6f0
 STRZ 'Touch'
 org $6f7
 STRZ 'Too Small'
 org $701
 STRZ 'Lift'
 org $707
 STRZ 'DONE'
 org $70d
 STRZ 'Lift Probe'
 org $719
 STRZ 'EOM '
 org $71e
 STRZ 'M DONE'
 org $725
 STRZ 'MESH %s'
 org $72d
 STRZ 'SHIM,MESH'
 org $737
 STRZ 'CAL '
 org $73c
 STRZ 'MIN '
 org $741
 STRZ 'MAX '
 org $746
 DB $40
 DB $80
 DB $0
 DB $0
 org $74a
 DB $43
 DB $c8
 DB $0
 DB $0
 org $74e
 DB $41
 DB $80
 DB $0
 DB $0
 org $752
 DB $42
 DB $a0
 DB $0
 DB $0
 org $756
 DB $41
 DB $22
 DB $8f
 DB $5e
 org $75a
 DB $40
 DB $18
 DB $2c
 DB $b8
 org $75e
 STRZ 'FERROUS,NONFERROUS,BOTH,EXIT'
 org $77b
 STRZ 'FERROUS,EXIT'
 org $788
 STRZ 'NONFERROUS,EXIT'
 org $798
 DB $a0
 DB $8a
 DB $80
 DB $80
 DB $80
 DB $45
 DB $a0
 DB $80
 DB $80
 DB $80
 DB $85
 DB $20
 DB $0
 org $7a5
 DB $a0
 DB $8a
 DB $80
 DB $80
 DB $80
 DB $4d
 DB $a0
 DB $80
 DB $80
 DB $80
 DB $85
 DB $20
 DB $0
 org $7b2
 DB $a0
 DB $8a
 DB $80
 DB $80
 DB $80
 DB $58
 DB $a0
 DB $80
 DB $80
 DB $80
 DB $85
 DB $20
 DB $0
 org $7bf
 STRZ 'NF,Z,A,I,T,EXIT'
 org $7cf
 STRZ 'FLG'
 org $7d3
 STRZ 'SNUM'
 org $bd0
 DB $96
 DB $78
 DB $96
 DB $78
 DB $1
 DB $64
 DB $b4
 DB $0
 DB $0
 DB $0
 DB $32
 DB $14
 DB $32
 DB $14
 DB $0
 DB $fa
 DB $1e
 DB $fa
 DB $1e
 DB $1
 DB $64
 DB $46
 DB $64
 DB $46
 DB $0
 DB $fa
 DB $46
 DB $fa
 DB $46
 DB $0
 DB $fa
 DB $b4
 DB $fa
 DB $b4
 DB $0
 DB $fa
 DB $14
 DB $fa
 DB $28
 DB $1
 DB $b
 DB $b4
 DB $0
 DB $0
 DB $0

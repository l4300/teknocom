/* proclck.c - Clock Routines */

xdelay(count)
	unsigned int count;
{
#ifdef	MONICA
	return;
#else
	while(count--) {
		IDLE();
	}
#endif
}



ClockFast()
{
	disable();
	msec_reload 	= 598;			/* gives 1msec clock was 614*/
	setbit(bitClockFast);
	enable();
}

ClockSlow()
{
	disable();
	msec_reload 	= 59800;		/* gives 100 msec clock was 61400*/
	clrbit(bitClockFast);
	enable();
}

ClockOn()
{
	unsigned char tmod;

	clrbit(TR0)             /* counters off */
	clrbit(ET0)		/* was et1 */

	TH0 = -( msec_reload >> 8);
	TL0 = -(msec_reload & 256);

	tmod = TMOD;
	tmod &= 0xf0;
	tmod |= 1;
	TMOD = tmod;            /* TR0 = timer */
	enable();
	setbit(TR0)
	setbit(ET0)
}

ClockInit()
{
	ClockOn();
	ClockSlow();
}



asm {
$SE:0
_/**/_TF0_	/* This label will be the address of the "prefix" stub */
		org		INTBASE+_TF0_			/* Position to interrupt vector */
		PUSH	PSW			/* Save Processor Status Word */
		PUSH	A			/* Save accumulator (2 bytes) */
		LJMP	_/**/_TF0_	/* Don't overwrite next vector (+3 = 7) */
		ORG		_/**/_TF0_	/* Position back to original PC */
		MOV	A,msec_reload+1
		CPL	A
		MOV	TH0,A
		MOV	A,msec_reload
		CPL	A
		MOV	TL0,A
		JB	bitClockFast,?DONE_two
		push	B
		mov	A,ticks
		mov	B,ticks+1
		add	A,#1
		xch	A,B
		addc	A,#0
		mov	ticks,B
		mov	ticks+1,A
		pop	B
		SJMP	?DONE_three
?DONE_two	JNB	bitClockFerrous,?DONE_three
		MOV	A,ferrousWaveform
		RL	A
		RL	A
		MOV	ferrousWaveform,A
		anl	A,#192
		orl	A,ferrousP2
		mov	P2,A
?DONE_three
		POP		A			/* Restore A accumulator */
		POP		PSW			/* Restore Processor Status Word */
		RETI				/* End interrupt */
}






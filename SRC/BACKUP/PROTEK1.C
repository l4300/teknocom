/* protek1.c - Copyright(c) 1995 MurkWorks, Inc. All Rights Reserved */

#include <8051io.h>		/* 8051 I/O definitions */
#include <8051reg.h>	/* 8051 SFR definitions */
#include <8051bit.h>
#include <8051int.h>

#include	"protek.h"
#include	"promem.h"
#include	"prodisp.h"
#include	"prokey.h"
#include	"prolcd.h"
#include	"procalc.h"
#include	"prosnd.h"
#include	"promisc.h"
#include	"prolp.h"
#include	"promnu.h"

#ifdef		NONFERROUS
#include	"pronf.h"
#endif

#ifdef		FERROUS
#include	"prof.h"
#endif

#include	"proproc.h"

/* ***************begin global memory definitions *******************/
asm {
$SE:2
_skipbak	equ *
	org	$0008
flagreg DS 1
fpacc1ex DS 1
fpacc1mn DS 3
mantsgn1 DS 1
fpacc2ex DS 1
fpacc2mn DS 3
mantsgn2 DS 1
fpaccB DS 4
error DS 1
SRwp DS 1
SRrp DS 1
STwp DS 1
STrp DS 1
msec_reload DS 2
sound_shift DS 1
nfCountDown DS 1
	org	_skipbak
}

extern register unsigned char SRwp, SRrp;	/* RX buffer & pointers */
extern register unsigned char STwp, STrp;	/* TX buffer & pointers */
extern register unsigned int  msec_reload;	/* reload value for T0 */

register unsigned int  gsX, gsY, gsZ; /* global scratch registers */
register unsigned char gsA, gsB;	/* global scratch registers */

register unsigned int  ticks;	/* 10ths of a second runtime counter */
register unsigned int  shutdownTime;	/* when we should shut down */


register	char	Key;		/* the last key pressed */

unsigned int	*ArrayData;		/* pointer to base of array data */

extern register unsigned char	sound_shift;

#ifdef	PRONF
unsigned int	nfCount;		/* raw count from nfReading */
unsigned int	nfInfinity;		/* the current infinity value */
unsigned int	nfDelta;		/* difference between last inf and factory */

#endif

unsigned int  	lastNFInfinityTime;	/* last time we got an infinity calibration */

#ifdef	PROF
register unsigned int	fArray[8];		/* the raw readings */
register unsigned char	ferrousP2;	/* normal ferrousP2 value */
register unsigned char	ferrousWaveform;	/* normal ferrous waveform */
unsigned int	lastFInfinityTime;
struct _float 	fInternalAlpha;
#endif

register unsigned char	procState;	/* the current process state */

unsigned char	proLpIBuffer[PROLP_BUFFSIZE];	/* link protocol input buffer */
struct	proLP	proLpPacket;			/* used for output and stuff */
unsigned char	proLpIcnt;			/* count of input bytes to input buffer */
unsigned char	proLpIcsum;			/* input checksum */
unsigned char	proLpLastSequence;		/* last sequence received */

unsigned char	gString[128];			/* general purpose string */

/* BOOLEAN BIT FLAGS */
#define	bitMicronMode		0	/* true if in microns */
#define	bitLimitMode		1	/* true if limits are on */
#define	bitProLPEscape	2	/* true if we've seen an escape */
#define 	bitProLPOverrun	3	/* true if we've had an input overrun */
#define	bitWatchDogReset	4	/* set if last boot was wtr */
#define	bitInfinityCalibration	5	/* set if we have an inf calibration */
#define	bitStableReading	6	/* saw a stable reading */
#define	bitLcdNoClear		7	/* don't clear the display */
#define 	bitKeyContinuous	8	/* key has been held down */

#define	bitShowStable		9	/* already showed a stable reading */
#define	bitClockFerrous	14	/* shift out the ferrous stuff */
#define	bitClockFast		15	/* clock is in fast mode */


/********************* begin initialized constants ********************/
/* floating point constants */
register unsigned char nf1[4]={0x40,0x80,0,0};
register unsigned char nf100[4]={0x43,0xc8,0,0};

unsigned char	sqrtA[4]={0x40,0x6e,0x14,0x7e};		/* 0.93 */
unsigned char	sqrtB[4]={0x3e,0x29,0xa,0xc1};		/* 0.04127 */
unsigned char	sqrtC[4]={0x3f,0xa2,0x95,0xee};		/* 0.31755 */
unsigned char	sqrtD[4]={0x39,0x40,0xf0,0x26};		/* 0.000046 */
unsigned char	sqrtE[4]={0x3c,0x16,0xed,0xf3};		/* 0.002303 */

unsigned char	arctA[4]={0x3d,0x13,0x74,0xbf};		/* 0.009 */
unsigned char	arctB[4]={0x40,0xb3,0x33,0x34};		/* 1.4 */
unsigned char	arctC[4]={0x40,0x34,0xb,0x7b};		/* 0.7033 */
unsigned char	arctD[4]={0x40,0xc1,0x37,0x50};		/* 1.5095 */
unsigned char	arctE[4]={0x41,0x17,0xbc,0xd7};		/* 2.3709 */

unsigned char	nf4[4]={0x41,0x80,0,0};			/* 4.0 */
unsigned char	nf20[4]={0x42,0xa0,0,0};		/* 20 */
unsigned char	nfmicrons[4]={0x41,0x22,0x8f,0x5e};	/* 2.54 */
unsigned char	nfozunits[4]={0x40,0x18,0x2c,0xb8};	/* 0.594432 */

/* serial port control */
int	baudrate = 9600;


/* LCD special characters */
unsigned char lessthan[]={
	0xa0,
	0x8a,
	0x80,
	0x80,
	0x80,
	0
};

unsigned char greaterthan[]={
	0xa0,
	0x80,
	0x80,
	0x80,
	0x85,
	0
};


unsigned char percent[]={
	0xa0,
	0x8e,
	0x82,
	0x84,
	0x87,
	0
};


unsigned char	mils[]={
	0xa0,
	0x82,
	0x8e,
	0x82,
	0x81,
	0
};
unsigned char microns[]={
	0xa0,
	0x85,
	0x86,
	0x80,
	0x82,
	0
};

/******************************** BEGIN INCLUDED CODE *********************** */

#include	"promem.c"
#include	"prokey.c"
#include	"proclck.c"
#include	"prosnd.c"
#include	"prolcd.c"

#ifndef	MONICA
#include	"proser.c" 
#endif

#include	"procalc.c"
#include	"promisc.c"
#include	"prodisp.c"

#ifdef	PRONF
#include	"pronf.c"
#endif

#ifdef	PROF
#include	"prof.c"
#endif

#include	"proprobe.c"
#include	"promenu.c"
#include	"proproc.c"
#include	"prolp.c"

#ifndef	PROSER
InitSerial()		/* dummy function */
{
}
#endif

Initialize()
{
	register unsigned char *r;
	IE = 0x80;		/* disable all sub ints */

	ArrayData = 0x8000;	/* what the heck */

	for(r=bitclrStart; r < bitclrEnd; r++)
		*r = 0;

	ANALOG_OFF;

	clrbit(bitInfinityCalibration);
	SerialInit();  
	ClockInit();
	sound_shift	= 0xb4;			/* 8 cycle sin approx */
	P2 = 0xff;
	P3 = (P3 & ~(SOUND_MASK|8));
	LCD(LCD_INIT, NULL,0);
	proLpIcnt = 0;
	lastNFInfinityTime = lastFInfinityTime = 0;
	LED_GREEN;
}


main()
{
	Initialize();
	if(tstbit(bitWatchDogReset))
		LCD_Print("Watchdog");
	else
		LCD_Print("TeknoCom");

	resetShutDown();

	Sound(SOUND_START);
#ifdef	teststart
	while(!GetKey(1)) {
		if(LpProcessPacket())
			resetShutDown();
		checkShutDown();
		IDLE();
	}
#endif	
	Process();
}

dunc() asm {
 INCLUDE "promnu.asm"
 org	dunc
}




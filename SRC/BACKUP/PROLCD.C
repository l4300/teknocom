/* prolcd.c - Protek LCD Control */


int
LCD_Wait() asm		/* wait for not busy */
{
	mov	R0,#255			wait up to 255 cycles
	mov	A,bitLcdPort
	orl	A,#LCD_NOTBUSY
	mov	bitLcdPort,A
lcdWaitagain
	nop
	jb	bitLcdNotBusy,lcdWaitdone
	djnz	R0,lcdWaitagain	
lcdWaitdone
}


LCD_SendChar() asm			/* set character passed on stack */
{
	mov   	R0,#-5
	lcall ?auto0
	mov   ?R6,[R0]		Get char
	mov	A,bitLcdPort
	anl	A,#~LCD_SCLK
	mov	bitLcdPort,A		clock is now low
     	mov	R3,#8			number of shifts
lcdSendMore
	mov	A,?R6
	rlc	A
	mov	?R6,A			Save it
	mov	A,bitLcdPort
	jc	lcdDataHigh
	anl	A,#~LCD_SI
	sjmp	lcdDataClock
lcdDataHigh
	orl	A,#LCD_SI
lcdDataClock
	mov	bitLcdPort,A
	nop
	orl	A,#LCD_SCLK
	mov	bitLcdPort,A		Clock high
	nop
	nop
	nop
	nop
	anl	A,#~LCD_SCLK
	mov	bitLcdPort,A		clock low 
	nop
	djnz	R3,lcdSendMore	
	lcall	LCD_Wait
}

LCD_Command() asm {
	mov   	R0,#-5
	lcall ?auto0
	mov   ?R6,[R0]		Get char
	mov	A,bitLcdPort
	anl	A,#~LCD_CS
	mov	bitLcdPort,A
	mov	B,A
	lcall	LCD_Wait
	mov	A,B
	orl	A,#LCD_CD
	mov	bitLcdPort,A
	mov	A,?R6
	push	A
	push	B
	lcall	LCD_SendChar
	pop	B
	pop	A
	mov	A,B
	orl	A,#LCD_CS
	mov	bitLcdPort,A	
}


int
LCD_Print(base)
	unsigned char *base;
{
	register unsigned char x;
	register unsigned char	i;
	register unsigned char 	c;
	unsigned char		y;
	unsigned char 	*str;
	register unsigned char	first;
    	register unsigned char	dobold;
    	register unsigned char	startKey;

	first = 1;
	dobold = 0;

	startKey = GetKey(1);

	if(tstbit(bitLcdNoClear))
		clrbit(bitLcdNoClear)
	else
		LCD_Command(0x20);	/* clear LCD */

	LCD_Command(0x18);	/* blinking off */
	LCD_Command(0);		/* clear blinking memory */
again:;

	str = base;
	if(*str == 1) {
		str++;
		dobold = 1;
	}
	LCD_Command(0x00);	/* clear blinking memory */
	LCD_Command(0x15);	/* with segment decoder */
	LCD_PORT &= ~LCD_CS;
	LCD_Wait();
	LCD_PORT &= ~LCD_CD;

	for(x=0; x < 9 && *str; x++, str++) {	/* goto 9 in case trailing . */
		y = x << 2;
nextchar:;
		c = *str;

		if(c & 0x80) {			/* command  data */
			LCD_PORT |= LCD_CD;
			LCD_SendChar(0xe0|y);
			 if(c & 0x20) {	/* write a 4 block of chars */
				for(i=0; i < 4; i++,y++) {
					str++;
/*
					if(c & 0x10) {
						dobold = 2;
						LCD_SendChar(0xcf);
						LCD_SendChar(0xe0|y);
					}
*/					
					if(c & 0x40) {
						if(c & 0x10) {
							dobold = 2;
							LCD_SendChar(0xc0|*str);	/* or */
							LCD_SendChar(0xe0|y);
						}
						LCD_SendChar(0xb0 | (*str & 0xf));
					} else
						LCD_SendChar(0xd0 | (*str & 0x0f)); 	/* write */
				}
				if(c & 0x40) {
					y -= 4;
				}
			} else if(c & 0x40) {	/* lower nybble has number of chars to blink */
				i = c & 0x0f;
				i <<= 2;	/* multiply by 4 */
				while(i--)
					LCD_SendChar(0xcf);
				x--;		/* doesn't count towards display */
				dobold = 2;
			} else if(c & 0x10) {	/* skip to lower nybble offset in chars*/
				y = (c & 0xf) * 4;
				LCD_SendChar(0xe0|y);
				str++;
				if(*str)
					goto nextchar;
				else
					x--;	
			}
			LCD_SendChar(0xe0|y);
			LCD_PORT &= ~LCD_CD;
			continue;
		}
		else if(c == '.') {
			/* at address x - 1, or the DP value */
			if(!x)
				y = 0;
			else
				y = (x-1) * 4;
			LCD_PORT |= LCD_CD;
			LCD_SendChar(0xe0|(y+2));
			LCD_SendChar(0xb8);
			y = (x)*4;
			LCD_SendChar(0xe0|y);
			LCD_PORT &= ~LCD_CD;
			x--;
			continue;
		}
		if(c >= 'a' && c <= 'z')
			c &= ~32;
		if(x == 8)	/* if not trailing ., drop out */
			break;
		c &= 0x7f;
		c |= 0x80;
		LCD_SendChar(c);
	}
	LCD_PORT |= LCD_CS;
	if(*str) {		/* still more chars to show... */
		base++;
		while(*base == '.')
			base++;
		if(!*base)
			return;
		if(first)
			x = LCD_FIRST_DELAY;
		else
			x = LCD_SHIFT_DELAY;
		while(--x) {
			if(startKey != GetKey(1))
				return;
			IDLE();
		}
		if(startKey != GetKey(1))
			return;
	
		first = 0;
		goto again;
	}
	if(dobold == 1)
		LCD(LCD_BLINKON,0,0);
	else if(dobold == 2) {
		LCD_Command(0x1A);
	}
}

int
LCD(command, arg, xtra)
	int	command;
	char	*arg;
	char	xtra;
{
	int	x;

	switch(command) {
		case LCD_INIT :
			/* initialize the display */
			LCD_PORT &= ~63;
			xdelay(10);
			setbit(LCD_PORT.2);
			LCD_PORT |= LCD_NOTRESET;
			LCD_PORT &= ~LCD_CS;	/* select device */
			LCD_Wait();
			LCD_PORT |= LCD_CD;	/* select command */
			LCD_SendChar(0x42);	/* 4-time, 1/3 bias, f=2^9 */
			LCD_SendChar(0x30);	/* sync transfers was 30 */
			LCD_SendChar(0x15);	/* with segment decoder */
			LCD_SendChar(0x20);	/* clear data memory */
			LCD_SendChar(0x11);	/* display on */
			LCD_PORT |= LCD_CS;
			break;
		case LCD_ON :
			LCD_Command(0x11);
			break;
		case LCD_OFF :
			LCD_Command(0x10);
			break;
		case LCD_CLEAR :
			LCD_Command(0x20);
			break;
		case LCD_PRINT :
			LCD_Print(arg);
			break;
			break;
		case LCD_BLINKON :
			LCD_Command(0x0);   	/* clear blinking memory */
			LCD_PORT |= LCD_CD;
			LCD_PORT &= ~LCD_CS;
			LCD_Wait();

			for(x=0; x < (4  * 8); x++) 
				LCD_SendChar(0xcf); 	/* all 1's */
			LCD_PORT |= LCD_CS;
			xdelay(1);
			LCD_Command(0x1A);
			break;
		case LCD_BLINKOFF :
			LCD_Command(0x18);
			break;
	}	/* end switch */
	xdelay(1);
}



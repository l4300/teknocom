 * --------- Start of Menu RootMenu -----------*
 * --------- Start of Menu MemoryMenu -----------*
 org $536
 DRW $500
 STRZ 'e MEM'
 org $53e
 DRW $a
 STRZ 'ON/OFF'
 org $547
 DRW $b
 STRZ 'CLEAR'
 org $54f
 DRW $c
 STRZ 'Status'
 org $558
 DRW $d
 STRZ 'PRINT'
 org $560
 DRW $e
 STRZ 'Tag'
 org $566
 DW 0
 * ------  End of Menu --------- *
 org $500
 DRW $536
 STRZ 'MEMORY'
 * --------- Start of Menu CalibrateMenu -----------*
 org $568
 DRW $500
 STRZ 'e Cal'
 org $570
 DRW $f
 STRZ '2 PTS'
 org $578
 DRW $10
 STRZ '1 PT'
 org $57f
 DRW $11
 STRZ 'Factory'
 org $589
 DW 0
 * ------  End of Menu --------- *
 org $509
 DRW $568
 STRZ 'Cal'
 org $50f
 DRW $12
 STRZ 'CAM'
 * --------- Start of Menu LimitsMenu -----------*
 org $58b
 DRW $500
 STRZ 'e LIM'
 org $593
 DRW $13
 STRZ 'Off'
 org $599
 DRW $14
 STRZ 'On'
 org $59e
 DRW $15
 STRZ 'Low'
 org $5a4
 DRW $16
 STRZ 'High'
 org $5ab
 DW 0
 * ------  End of Menu --------- *
 org $515
 DRW $58b
 STRZ 'LIMITS'
 * --------- Start of Menu UnitsMenu -----------*
 org $5ad
 DRW $500
 STRZ 'e UNITS'
 org $5b7
 DRW $17
 STRZ 'Mils'
 org $5be
 DRW $18
 STRZ 'Microns'
 org $5c8
 DW 0
 * ------  End of Menu --------- *
 org $51e
 DRW $5ad
 STRZ 'UNITS'
 * --------- Start of Menu ConfigMenu -----------*
 org $5ca
 DRW $500
 STRZ 'e CNF'
 org $5d2
 DRW $19
 STRZ 'Date'
 org $5d9
 DRW $1a
 STRZ 'Time'
 org $5e0
 DRW $1b
 STRZ 'PRINTER'
 org $5ea
 DRW $1c
 STRZ 'Factory'
 org $5f4
 DRW $1d
 STRZ 'Serial Number'
 org $604
 DRW $1e
 STRZ 'Version'
 org $60e
 DW 0
 * ------  End of Menu --------- *
 org $526
 DRW $5ca
 STRZ 'CONFIG'
 org $52f
 DRW $ffff
 STRZ 'e '
 org $534
 DW 0
 * ------  End of Menu --------- *
 * --------- Start of Menu SysMenu -----------*
 org $610
 DRW $1f
 STRZ 'Flags'
 org $618
 DRW $20
 STRZ 'SNumber'
 org $622
 DRW $21
 STRZ 'Ferrous'
 org $62c
 DRW $22
 STRZ 'NonFerrous'
 org $639
 DRW $500
 STRZ 'Exit'
 org $640
 DW 0
 * ------  End of Menu --------- *
 org $642
 STRZ ' ()*+-/0123456789<=>@ABCDEFGHIJKLMNOPQRSTUVWXYZ'
 org $672
 DB $2f
 DB $0
 org $674
 STRZ '0123456789'
 org $67f
 DB $a
 DB $0
 org $681
 STRZ 'WAIT'
 org $687
 STRZ 'OFF'
 org $68b
 STRZ 'EXIT'
 org $690
 STRZ 'CAM %s'
 org $697
 STRZ 'ALL'
 org $69b
 STRZ 'NONE'
 org $6a0
 STRZ '%c%c'
 org $6a5
 STRZ 'COMPLETE'
 org $6ae
 STRZ 'ROWS'
 org $6b3
 STRZ 'COLS'
 org $6b8
 STRZ 'Used %u of %u'
 org $6c6
 STRZ '%c %s %d Readings %d Rows %d Cols'
 org $6e8
 STRZ 'LOWL'
 org $6ed
 STRZ 'HI L'
 org $6f2
 STRZ '00 PLATE'
 org $6fc
 STRZ 'MORE/SEL'
 org $705
 STRZ 'W/ SHIM'
 org $70e
 STRZ 'ADJC'
 org $713
 STRZ 'BATCH FULL'
 org $71e
 STRZ '%s-'
 org $722
 STRZ ' %c%d  '
 org $72a
 STRZ '%d   '
 org $730
 STRZ '�%d   '
 org $737
 STRZ '�R%d   '
 org $73f
 STRZ 'MEM FULL'
 org $748
 STRZ 'TEMPLATE'
 org $751
 STRZ 'NONE/EXIT'
 org $75b
 STRZ 'Touch'
 org $762
 STRZ 'Too Small'
 org $76c
 STRZ 'Lift'
 org $772
 STRZ 'DONE'
 org $778
 STRZ 'Lift Probe'
 org $784
 STRZ 'CAL '
 org $789
 STRZ 'MIN '
 org $78e
 STRZ 'MAX '
 org $793
 DB $40
 DB $80
 DB $0
 DB $0
 org $797
 DB $41
 DB $0
 DB $0
 DB $0
 org $79b
 DB $43
 DB $c8
 DB $0
 DB $0
 org $79f
 DB $41
 DB $80
 DB $0
 DB $0
 org $7a3
 DB $42
 DB $a0
 DB $0
 DB $0
 org $7a7
 DB $41
 DB $22
 DB $8f
 DB $5e
 org $7ab
 DB $40
 DB $18
 DB $2c
 DB $b8
 org $7af
 STRZ 'FLG'
 org $7b3
 STRZ 'SNUM'
 org $7b8
 STRZ 'FERROUS,NONFERROUS,BOTH,EXIT'
 org $7d5
 STRZ 'FERROUS,EXIT'
 org $7e2
 STRZ 'NONFERROUS,EXIT'
 org $7f2
 DB $a0
 DB $8a
 DB $80
 DB $80
 DB $80
 DB $58
 DB $a0
 DB $80
 DB $80
 DB $80
 DB $85
 DB $20
 DB $0
 org $bd0
 DB $96
 DB $78
 DB $96
 DB $78
 DB $1
 DB $64
 DB $b4
 DB $0
 DB $0
 DB $0
 DB $32
 DB $14
 DB $32
 DB $14
 DB $0
 DB $fa
 DB $1e
 DB $fa
 DB $1e
 DB $1
 DB $64
 DB $46
 DB $64
 DB $46
 DB $0
 DB $fa
 DB $46
 DB $fa
 DB $46
 DB $0
 DB $fa
 DB $b4
 DB $fa
 DB $b4
 DB $0
 DB $fa
 DB $14
 DB $fa
 DB $28
 DB $1
 DB $b
 DB $b4
 DB $0
 DB $0
 DB $0

* defines for model1 unit
MEMOFFSET	EQU	$7000
CPU		EQU	DS2250
* DS2252	EQU	TRUE
* DS15MHZ	EQU	TRUE

SysConstants	EQU	$200+MEMOFFSET
UserConstants	EQU	$300+MEMOFFSET
Menu		EQU	$500+MEMOFFSET
Sounds		EQU	$7d0+MEMOFFSET
Batches		EQU	$800+MEMOFFSET

?RAM	EQU	$800+MEMOFFSET		RAM Starts here
?RAMEND	EQU	$ff0+MEMOFFSET		RAM Ends here  was 7700


Templates	EQU	$1200+MEMOFFSET
DataBlocks	EQU	$1d00+MEMOFFSET

?PARTITION	EQU	$7000	Partition Address
?PARTCODE	EQU	%11100000	Include Bit 4 for Alternate 32 Memory


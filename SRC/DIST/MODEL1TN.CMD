rem Model1tn.cmd file to load model1t non-ferrous code
com 2
speed 9600
unlock
stat
range 8000
key random
part 8000
fill 0 7300 7fff
rem loading data area
load m1data.hex
load chekline.hex
part 7000
stat
load model1tn.hex
lock
quit


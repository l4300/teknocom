/* protek1.c - Copyright(c) 1995 MurkWorks, Inc. All Rights Reserved */

/* #define	FLOATDEBUG	1 */

#ifndef	MODEL1
#ifdef	MODEL3
/* #define	DEBUGM	1 */
/* #define	MONICA	1 */
#endif
#endif

/*
 * Copyright (c) 1996-97, Murkworks, Inc.
 * $Log: /Teknocom/Protek Gauge/src/protek1.c $
 * 
 * 17    4/19/00 1:21p Bkc
 * Added check for SCFLG_EXTENDEDFERROUS to reduce startup loop count of
 * ferrous poll.
 * 
 * 16    3/23/98 4:15p Bkc
 * Version 2.3
 * 
 * 15    2/26/98 10:29p Bkc
 * 
 * 14    2/19/98 10:00p Bkc
 * Added new sounds, fixed limit EditValue problem in proprobe not
 * returning lastreading.
 * 
 * 13    2/10/98 3:14p Bkc
 * Added REVCAMSEL to protek.h for use in proproc.c for this model.
 * 
 * 12    7/11/97 5:57p Bkc
 * Added oem name location at offset $32 through $43 and copyOEMName()
 * function to copy to gString.
 *
 * $NoKeywords: $
 */

#include <8051io.h>		/* 8051 I/O definitions */
#include <8051reg.h>	/* 8051 SFR definitions */
#include <8051bit.h>
#include <8051int.h>

#ifdef	DEBUGMX
#define	xjunk	1
#define	FLOATDEBUG	1
#endif

void copyOEMName();

#include	"protek.h"
#include	"promem.h"
#include	"prodisp.h"
#include	"prokey.h"
#include	"prolcd.h"
#include	"procalc.h"
#include	"prosnd.h"
#include	"promisc.h"
#include	"prolp.h"

#ifdef	MODEL1T
#include	"promnu1b.h"
#endif
#ifdef	MODEL1TF
#include	"promnu1.h"
#endif

#ifdef	MODEL1TN
#include	"promnu1.h"
#endif


#ifdef	MODEL1S
#include	"promnu1b.h"
#endif
#ifdef	MODEL1SF
#include	"promnu1.h"
#endif

#ifdef	MODEL1SN
#include	"promnu1.h"
#endif



#ifdef	MODEL1E
#include	"promnu1e.h"
#endif

#ifdef	MODEL1EN
#include	"promnu1n.h"
#endif

#ifdef	MODEL1ES
#include	"promnu1s.h"
#endif

#ifdef	MODEL1EG
#include	"promnu1g.h"
#endif


#ifdef	MODEL1
#include	"promnu1b.h"
#endif

#ifdef	MODEL1N
#include	"promnu1.h"
#endif

#ifdef	MODEL1F
#include	"promnu1.h"
#endif

#ifdef	MODEL5
#include	"promnu1.h"
#endif

#ifdef	MODEL2
#include	"promnu.h"
#endif

#ifdef	MODEL2C
#include	"promnu.h"
#endif

#ifdef	MODEL3
#include	"promnu.h"
#endif

#ifdef	MODEL4
#include	"promnu.h"
#endif


#ifdef		NONFERROUS
#include	"pronf.h"
#endif

#ifdef		FERROUS
#include	"prof.h"
#endif

#include	"proproc.h"

/* ***************begin global memory definitions *******************/
asm {
$SE:2
_skipbak	equ *
	org	$0008
flagreg DS 1
fpacc1ex DS 1
fpacc1mn DS 3
mantsgn1 DS 1
fpacc2ex DS 1
fpacc2mn DS 3
mantsgn2 DS 1
fpaccB DS 4
error DS 1
SRwp DS 1
SRrp DS 1
STwp DS 1
STrp DS 1
msec_reload DS 2
sound_shift DS 1
nfCountDown DS 1
	org	_skipbak
}
extern register unsigned char	nfCountDown;

#define	touch_byte	error
#define	touch_crc	nfCountDown
#define	touch_crcL	gsD
#define	touch_crcH	gsE

extern register unsigned char SRwp, SRrp;	/* RX buffer & pointers */
extern register unsigned char STwp, STrp;	/* TX buffer & pointers */
extern register unsigned int  msec_reload;	/* reload value for T0 */

struct _snd {
	unsigned char f1;
	unsigned char l1;
	unsigned char f2;
	unsigned char l2;
	unsigned char red;
};

extern struct _snd  Sounds[9];


register unsigned int  gsX, gsY, gsZ; /* global scratch registers */
register unsigned char gsA, gsB,gsC,gsD,gsE;	/* global scratch registers */

register unsigned int  ticks;	/* 10ths of a second runtime counter */

register	char	Key;		/* the last key pressed */

extern register unsigned char	sound_shift;

#ifdef	PRONF
unsigned int	nfCount;		/* raw count from nfReading */
unsigned int	nfInfinity;		/* the current infinity value */
unsigned int	nfDelta;		/* difference between last inf and factory */

#endif
unsigned int	lastFInfinityTime;
unsigned int  	lastNFInfinityTime;	/* last time we got an infinity calibration */

#ifdef	PROF
/* struct _float 	fInternalAlpha; */
#endif

register unsigned int	fArray[5];		/* the raw readings */


register unsigned char	ferrousP2;	/* normal ferrousP2 value */
register unsigned char	ferrousWaveform;	/* normal ferrous waveform */

register unsigned char	procState;	/* the current process state */

unsigned char	proLpIBuffer[PROLP_BUFFSIZE];	/* link protocol input buffer */
struct	proLP	proLpPacket;			/* used for output and stuff */
unsigned char	proLpIcnt;			/* count of input bytes to input buffer */
unsigned char	proLpIcsum;			/* input checksum */
unsigned char	proLpLastSequence;		/* last sequence received */

#ifdef	DOREMOTEREADING
unsigned char	asyncReadCount;
#endif

unsigned int	camMax, camMin;
unsigned char	gString[64];			/* general purpose string */
unsigned char	mnuMemFlag;			/* mnuMemory flag scroller flags */

unsigned int  shutdownTime;	/* when we should shut down */

#ifdef	DEBUGPOLL
unsigned int pcount;
#endif

/* promenu.c globals for DoMenu */
unsigned int	*currentMenu, *currentItem;
unsigned char	*c;
unsigned char	sel;

/* prodisp.c showbits bottom */
unsigned int	bottomBits;

/* proprobe.c flag values */
unsigned int	flagvals;

/* promisc.c globals for editValue */
unsigned int val;
unsigned int lowv;		/* low value */
unsigned int highv;		/* high value limit */
unsigned int bits;		/* display bits */
unsigned char pcheck;		/* true to check probe */
char	*pfx;

	

/* BOOLEAN BIT FLAGS */
asm {
bitFerrousMode		BIT 0
bitLedTimeout		BIT 1
bitProLPEscape		BIT 2
bitProLPOverrun		BIT 3
bitWatchDogReset	BIT 4
bitInfinityCalibration	BIT 5
bitStableReading	BIT 6
bitLcdNoClear		BIT 7
bitKeyContinuous	BIT 8

bitShowStable		BIT 9
bitLcdCheckProbe	BIT 10
bitNoLed		BIT 11
bitNoPrint		BIT 12
bitFSaveReading		BIT 13
bitClockFerrous		BIT 14
bitClockFast		BIT 15

bitLcdPort      	EQU $90
bitLcdCD        	BIT $90
bitLcdCS        	BIT $91
bitLcdNotBusy   	BIT $92
bitLcdSI        	BIT $93
bitLcdSclk      	BIT $94
bitLcdNotReset  	BIT $95
}

#define	CbitFerrousMode		0	/* true last probe chose ferrous */
#define	CbitLedTimeout		1	/* true if led timeout is counting down */
#define	CbitProLPEscape		2	/* true if we've seen an escape */
#define	CbitProLPOverrun		3	/* true if we've had an input overrun */
#define	CbitWatchDogReset	4	/* set if last boot was wtr */
#define	CbitInfinityCalibration	5	/* set if we have an inf calibration */
#define	CbitStableReading	6	/* saw a stable reading */
#define	CbitLcdNoClear		7	/* don't clear the display */
#define	CbitKeyContinuous	8	/* key has been held down */

#define	CbitShowStable		9	/* already showed a stable reading */
#define	CbitLcdCheckProbe	10	/* if scrolling, check probe for proximity */
#define	CbitNoLed		11	/* set if no led on sound */
#define	CbitNoPrint		12	/* set in debug mode to disable print */
#define	CbitFSaveReading	13	/* true to re-use last ferrous reading */
#define	CbitClockFerrous		14	/* shift out the ferrous stuff */
#define	CbitClockFast		15	/* clock is in fast mode */



/********************* begin initialized constants ********************/
/* floating point constants */
#ifdef includeConstants
unsigned char nf1[4]={0x40,0x80,0,0};
unsigned char nf100[4]={0x43,0xc8,0,0};		/* these two were register */
unsigned char	nf4[4]={0x41,0x80,0,0};			/* 4.0 */
unsigned char	nf20[4]={0x42,0xa0,0,0};		/* 20 */
unsigned char	nfmicrons[4]={0x41,0x22,0x8f,0x5e};	/* 2.54 */
unsigned char	nfozunits[4]={0x40,0x18,0x2c,0xb8};	/* 0.594432 */
#endif

/* serial port control */
int	baudrate = 9600;

#ifdef	oldjunk
#ifdef	STARTLINE
char	*TEKNOCOM = STARTLINE;
#else
#ifdef	MODEL1ES
char	*TEKNOCOM = " . .K.I.WO  .";
#else
char	*TEKNOCOM = "T.e.k.n.o.c.o.m.";
#endif
#endif
#endif	/* oldjunk */

#ifdef	junk
/* LCD special characters */
unsigned char lessthan[]={
	0xa0,
	0x8a,
	0x80,
	0x80,
	0x80,
	0
};

unsigned char greaterthan[]={
	0xa0,
	0x80,
	0x80,
	0x80,
	0x85,
	0
};


unsigned char percent[]={
	0xa0,
	0x8e,
	0x82,
	0x84,
	0x87,
	0
};


unsigned char	mils[]={
	0xa0,
	0x82,
	0x8e,
	0x82,
	0x81,
	0
};
unsigned char microns[]={
	0xa0,
	0x85,
	0x86,
	0x80,
	0x82,
	0
};

#endif
/******************************** BEGIN INCLUDED CODE *********************** */

#include	"promem.c"
#include	"prokey.c"
#include	"proclck.c"
#include	"prosnd.c"
#include	"prolcd.c"

#include	"proser.c"

#include	"procalc.c"
#include	"promisc.c"
#include	"prodisp.c"

#ifdef	PRONF
#include	"pronf.c"
#endif

#ifdef	PROF
#include	"prof.c"
#endif

#include	"proprobe.c"
#include	"promenu.c"
#include	"proproc.c"
#include	"prolp.c"

#ifdef	DOTOUCH
#include	"protouch.c"
#endif
#ifndef	PROSER
InitSerial()		/* dummy function */
{
}
#endif

Initialize()
{
	register unsigned char *r;
	IE = 0x80;		/* disable all sub ints */

	for(r=bitclrStart; r < bitclrEnd; r++)
		*r = 0;

	ANALOG_OFF;
	bottomBits = 0;
	clrbit(bitInfinityCalibration);
	SerialInit();
	ClockInit();
	sound_shift	= 0xb4;			/* 8 cycle sin approx */
	P2 = 0xff;
/*	P3 = (P3 & ~(SOUND_MASK|8)); */
	LED_OFF;
	LCD(LCD_INIT, NULL,0);
	proLpIcnt = 0;
	lastNFInfinityTime = lastFInfinityTime = 1;
/*	LED_GREEN; */
#ifdef	FERROUS
#ifdef	DS2252
		/* this is a stupid work-around for the 2252 clock
  		   not working when the ferrous probe has not
		   yet been accessed.. Also, must set lastFInfinity
		   to non-zero as shown above 12/20/95 */
	fcheckProbe(2);
#endif
#endif
}


void copyOEMName()
asm {
	MOV	DPTR,#$32	Point to OEMName
	MOV	R1,#gString	Get LOW RAM address
	MOV	R2,#=gString	Get HIGH RAM address
	MOV	R3,#16
	MOV	R4,#0	Get HIGH size
?Oeminit1	MOV	A,R3		Get LOW
	ORL	A,R4		Zero?
	JZ	?OemDone		Yes, dont do it
	CLR	A		Zero offset
	MOVC	A,[A+DPTR]	Read from ROM
	INC	DPTR		Advance source
	XCH	A,R2		Save data, get HIGH dest
	XCH	A,DPH		Swap with HIGH source
	XCH	A,R2		Resave HIGH dest, recover data
	XCH	A,R1		Save data, get LOW dest
	XCH	A,DPL		Swap with LOW source
	XCH	A,R1		Resave LOW dest, recover data
	MOVX	[DPTR],A	Write to RAM
	INC	DPTR		Advance dest
	XCH	A,R2		Save data, get HIGH dest
	XCH	A,DPH		Swap with HIGH source
	XCH	A,R2		Resave HIGH dest, recover data
	XCH	A,R1		Save data, get LOW dest
	XCH	A,DPL		Swap with LOW source
	XCH	A,R1		Resave LOW dest, recover data
	DEC	R3		Reduce count LOW
	CJNE	R3,#-1,?Oeminit1	Not expired
	DEC	R4		Reduce count HIGH
	SJMP	?Oeminit1		And proceed
?OemDone
	ret
}

#ifdef	DEBUGM
	char	buff[32];
	unsigned int res;


#endif

main()
{
#ifdef DEBUGM
unsigned int startCycles,mainCycles,burstCount;
    char slot;

	startCycles = 8;
	mainCycles = 2;
	burstCount = 3;
    slot = 0;
#endif
	Initialize();

/*	if(tstbit(bitWatchDogReset))
		LCD_Print("Watchdog");
	else */
	{
    	copyOEMName();
		LCD_Print(gString);
		ShowBits(0xffff,1);
	}
	resetShutDown();

	Sound(SOUND_START);
#ifdef	PROF
	if(!(SysConstants.scFlags & SCFLG_NOFERROUS)) {
       if(SysConstants.scFlags & SCFLG_LONGFERROUS)
           gsC = 6;
       else
           gsC = 13;
	 	for(; gsC > 0; gsC--)
			fcheckProbe(2);
	} else
#else
	for(gsC = 0; gsC < 15; gsC++)	/* rashid wants this delay for */
		IDLE();			/* for startup banner */

#endif
#ifdef	DEBUGM
	setbit(bitNoPrint)
#endif
	IDLE();
	if(GetKey(1) != KEY_NONE) {
		while(GetKey(1) != KEY_NONE) {
			IDLE();
			if(idleTime() > 50) {	/* hold startup for 5 seconds */
				GetKey(0);
				IDLE();
				Sound(SOUND_START);
				while(GetKey(1) != KEY_NONE)
					IDLE();
				while(!GetKey(1)) {
					if(LpProcessPacket())
						resetShutDown();
					checkShutDown();
					IDLE();
				}
				Key = GetKey(0);
				if(Key == KEY_MINUS || Key == KEY_META2)
					DoMenu(Key);
			}
		}
	}
#ifdef	DEBUGM

	clrbit(bitNoPrint)
	lastFInfinityTime = 0;
	lastNFInfinityTime = 0;

	gsE = 0x55;

	while (1) {
    	printf("L - Slot %d\n",slot);
    	printf("Range Low High  Len is D1. Use this format 'R llll hhhh'\n");
/*	printf("P - %d pattern use 85, 51, 15\n",gsE); */
        printf("M F D  - Make sound Frequency Duration   'M FFFF DDDD'\n");
    	printf("(1) F1 - %d\n(2) D1 - %d\n(3) F2 - %d\n(4) D2 - %d\nS - Make Sound\nSelect: ",
        	Sounds[slot].l1,Sounds[slot].f1,Sounds[slot].l2,Sounds[slot].f2);
//		printf("f - ferrous\nn - nonferrous\nq - quit\nd - take debug reading\nw-windows  e - erase memory S - Sound\n");
//		printf("s - startCycles %d\nm - mainCycles %d\nb - burstCount %d\nSelect: ",
//			startCycles, mainCycles, burstCount);

		getstr(buff,sizeof(buff));
		switch (buff[0]) {
/*			case 'e' :
			case 'E' :
				EraseAllMemory(0);
				break; */
			case 'w' :
			case 'W' :
				Sound(SOUND_START);
				LCD_Print("WINDOWS");
				setbit(bitNoPrint)
				while(!GetKey(1)) {
					if(LpProcessPacket())
						resetShutDown();
					checkShutDown();
					IDLE();
				}
				LCD_Print("DEBUG");
				clrbit(bitNoPrint);
				break;
            case '1' :
            	Sounds[slot].l1 = atoi(&buff[2]);
                break;
            case '2' :
            	Sounds[slot].f1 = atoi(&buff[2]);
                break;
            case '3' :
            	Sounds[slot].l2 = atoi(&buff[2]);
                break;
            case '4' :
            	Sounds[slot].f2 = atoi(&buff[2]);
                break;
/*	     case 'p' :
	     case 'P' :
			gsE = atoi(&buff[2]); */
		break;	
            case 'r' :
            case 'R' :
            	res = atoi(&buff[2]);
                burstCount = atoi(&buff[7]);
                printf("Range %d - %d\n",res,burstCount);
                while(res <= burstCount) {
                    ClockFast();
                    SerialStop();
                    sound_shift = 0x30;
                    MakeSound(Sounds[0].f1,res);
                    ClockSlow();
                    SerialInit();
                    res++;
                }
                break;
			case 's' :
			case 'S' :
                Sound(slot|SOUND_LED);
				break;
			case 'm' :
			case 'M' :
            	res = atoi(&buff[2]);
                burstCount = atoi(&buff[7]);
                printf("Sound Freq %d - Duration %d\n",res,burstCount);
                ClockFast();
                SerialStop();
                sound_shift = 0x30;
                MakeSound(burstCount,res);
                ClockSlow();
                SerialInit();
				break;
            case 'l' :
            case 'L' :
            	slot = atoi(&buff[2]);
                break;
			case 'b' :
			case 'B' :
				burstCount = atoi(&buff[2]);
				break;
			case 'd' :
			case 'D' :
				DfReading(burstCount, (startCycles << 8)|mainCycles);
				for(res=0; res < 5; res++)
					printf("%d. %u\n",res+1,fArray[4-res]);
				break;

			case 'f' :
			case 'F' :

				gsX = fcheckProbe(1);
				if(!gsX) {
					printf("checkprobe 0\n");
					continue;
				}
				printf("checkprobe %u\n", gsX);

				fReading(FBURST,BMAPREADING);

				gsX = res = fConvertReading();

				goto showres;
			case 'n' :
			case 'N' :

				printf("checkprobe %u\n", nfcheckProbe(1));
				gsY = gsX = res = nfreadProbe();
showres:;

				printf("result = %c %u res %u\n",gsX & 0x8000 ? '-' : ' ',gsX & 0x7fff, res);
				ShowReading(res,GetMemoryString(UserConstants.srfFlags|SRF_DONE),UserConstants.srfFlags);

				break;
			case 'q' :
			case 'Q' :
				setbit(bitNoPrint)
                        	Process();
		}
	}

#else
	Process();
#endif
}

#ifdef	imbedmenu
dunc() asm {
 INCLUDE "promnu.asm"
 org	dunc
}
#endif





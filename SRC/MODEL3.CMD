rem Model3.cmd file to load model3 code
com 2
speed 19200
unlock
stat
range 128k
stat
time set
time on
time read
rem end of time setting, now set msl 0 to drop data
key random
write msl 0
fill 0 300 ffff
load m3data.hex
write msl 1
load model3.hex
lock
quit


 * --------- Start of Menu RootMenu -----------*
 * --------- Start of Menu CalibrateMenu -----------*
 org $7524
 DRW $7500
 STRZ 'e Cal'
 org $752c
 DRW $a
 STRZ '2 PTS'
 org $7534
 DRW $b
 STRZ '1 PT'
 org $753b
 DRW $c
 STRZ 'Factory'
 org $7545
 DW 0
 * ------  End of Menu --------- *
 org $7500
 DRW $7524
 STRZ 'Cal'
 org $7506
 DRW $d
 STRZ 'CAM'
 * --------- Start of Menu UnitsMenu -----------*
 org $7547
 DRW $7500
 STRZ 'e UNITS'
 org $7551
 DRW $e
 STRZ 'Mils'
 org $7558
 DRW $f
 STRZ 'Microns'
 org $7562
 DW 0
 * ------  End of Menu --------- *
 org $750c
 DRW $7547
 STRZ 'UNITS'
 * --------- Start of Menu ConfigMenu1 -----------*
 org $7564
 DRW $7500
 STRZ 'e CNF'
 org $756c
 DRW $10
 STRZ 'Factory'
 org $7576
 DRW $11
 STRZ 'Serial Number'
 org $7586
 DRW $12
 STRZ 'Version'
 org $7590
 DW 0
 * ------  End of Menu --------- *
 org $7514
 DRW $7564
 STRZ 'CONFIG'
 org $751d
 DRW $ffff
 STRZ 'e '
 org $7522
 DW 0
 * ------  End of Menu --------- *
 * --------- Start of Menu SysMenu -----------*
 org $7592
 DRW $13
 STRZ 'Flags'
 org $759a
 DRW $14
 STRZ 'SNumber'
 org $75a4
 DRW $15
 STRZ 'Ferrous'
 org $75ae
 DRW $16
 STRZ 'NonFerrous'
 org $75bb
 DRW $17
 STRZ 'Bench'
 org $75c3
 DRW $7500
 STRZ 'Exit'
 org $75ca
 DW 0
 * ------  End of Menu --------- *
 org $75cc
 STRZ ' ()*+-/0123456789<=>@ABCDEFGHIJKLMNOPQRSTUVWXYZ'
 org $75fc
 DB $2f
 DB $0
 org $75fe
 STRZ '0123456789'
 org $7609
 DB $a
 DB $0
 org $760b
 STRZ 'WAIT'
 org $7611
 STRZ 'OFF'
 org $7615
 STRZ 'EXIT'
 org $761a
 STRZ 'CAM %s'
 org $7621
 STRZ 'EOM %s'
 org $7628
 STRZ 'ALL'
 org $762c
 STRZ 'OFF'
 org $7630
 STRZ 'NONE'
 org $7635
 STRZ '%c%c'
 org $763a
 STRZ 'COMPLETE'
 org $7643
 STRZ 'ROWS'
 org $7648
 STRZ 'COLS'
 org $764d
 STRZ 'Used %u of %u'
 org $765b
 STRZ '%c %s %d Readings %d Rows %d Cols'
 org $767d
 STRZ 'LOWL'
 org $7682
 STRZ 'HI L'
 org $7687
 STRZ '00 PLATE'
 org $7691
 STRZ 'MORE/SEL'
 org $769a
 STRZ 'W/ SHIM'
 org $76a3
 STRZ 'ADJC'
 org $76a8
 STRZ 'BATCH FULL'
 org $76b3
 STRZ '%s-'
 org $76b7
 STRZ ' %c%d  '
 org $76bf
 STRZ '%d   '
 org $76c5
 STRZ '�%d   '
 org $76cc
 STRZ '�R%d   '
 org $76d4
 STRZ 'MEM FULL'
 org $76dd
 STRZ 'TEMPLATE'
 org $76e6
 STRZ 'NONE/EXIT'
 org $76f0
 STRZ 'Touch'
 org $76f7
 STRZ 'Too Small'
 org $7701
 STRZ 'Lift'
 org $7707
 STRZ 'DONE'
 org $770d
 STRZ 'Lift Probe'
 org $7719
 STRZ 'CAL '
 org $771e
 STRZ 'MIN '
 org $7723
 STRZ 'MAX '
 org $7728
 DB $40
 DB $80
 DB $0
 DB $0
 org $772c
 DB $43
 DB $c8
 DB $0
 DB $0
 org $7730
 DB $41
 DB $80
 DB $0
 DB $0
 org $7734
 DB $42
 DB $a0
 DB $0
 DB $0
 org $7738
 DB $41
 DB $22
 DB $8f
 DB $5e
 org $773c
 DB $40
 DB $18
 DB $2c
 DB $b8
 org $7740
 STRZ 'FLG'
 org $7744
 STRZ 'SNUM'
 org $7749
 STRZ 'FERROUS,NONFERROUS,BOTH,EXIT'
 org $7766
 STRZ 'FERROUS,EXIT'
 org $7773
 STRZ 'NONFERROUS,EXIT'
 org $7783
 STRZ 'NF,Z,A,I,T,EXIT'
 org $7793
 DB $a0
 DB $8a
 DB $80
 DB $80
 DB $80
 DB $45
 DB $a0
 DB $80
 DB $80
 DB $80
 DB $85
 DB $20
 DB $0
 org $77a0
 DB $a0
 DB $8a
 DB $80
 DB $80
 DB $80
 DB $4d
 DB $a0
 DB $80
 DB $80
 DB $80
 DB $85
 DB $20
 DB $0
 org $77ad
 DB $a0
 DB $8a
 DB $80
 DB $80
 DB $80
 DB $58
 DB $a0
 DB $80
 DB $80
 DB $80
 DB $85
 DB $20
 DB $0
 org $7bd0
 DB $96
 DB $78
 DB $96
 DB $78
 DB $1
 DB $64
 DB $b4
 DB $0
 DB $0
 DB $0
 DB $32
 DB $14
 DB $32
 DB $14
 DB $0
 DB $fa
 DB $1e
 DB $fa
 DB $1e
 DB $1
 DB $64
 DB $46
 DB $64
 DB $46
 DB $0
 DB $fa
 DB $46
 DB $fa
 DB $46
 DB $0
 DB $fa
 DB $b4
 DB $fa
 DB $b4
 DB $0
 DB $fa
 DB $14
 DB $fa
 DB $28
 DB $1
 DB $b
 DB $b4
 DB $0
 DB $0
 DB $0

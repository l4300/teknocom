/* proprobe.c - All probe related functions */
/* #define	DEBUGC	1  */   	/* debug calibration */
/* #define	DEBUGS	1     */	/* debug stability */
/* #define	DEBUGC1	1	 */	/* debug cal summation */

/*
 * Copyright (c) 1996-97, Murkworks, Inc.
 * $Log: /Teknocom/Protek Gauge/src/proprobe.c $
 * 
 * 17    10/22/98 4:52p Bkc
 * Version 2.4
 * 
 * 16    3/23/98 4:15p Bkc
 * Version 2.3
 * 
 * 15    2/26/98 10:29p Bkc
 * 
 * 14    2/19/98 10:00p Bkc
 * Added new sounds, fixed limit EditValue problem in proprobe not
 * returning lastreading.
 * 
 * 13    7/14/97 6:40p Bkc
 * Corrected mis-use of #ifdef PRONF setting of readings = 0x80 in
 * CalProbe
 * 
 * 12    7/11/97 4:44p Bkc
 * The zero offset factor was not being scaled by the 2nd point factor.
 * Added scaling to zero offset after 2nd point ratio is calculated. Moved
 * sintoflt() to proprobe.c
 * 
 * 11    7/08/97 9:13p Bkc
 * Changed CalProbe to set Mesh/Shim (USENF()) mode during calibration
 * procedure and to restore value at exit.
 *
 * $NoKeywords: $
 */

int
setupProbe()		/* set up for readings */
{
	lastFInfinityTime = 0;
	lastNFInfinityTime = 0;

	if(!checkProbe(1))
		return;

	while((gsX = checkProbe(1)) && (!lastNFInfinityTime && !lastFInfinityTime)) {
#ifdef	debug
 sprintf(gString,"%u lf %d",gsX,lastNFInfinityTime); 
LCD_Print(gString);
		if(GetKey(1) == KEY_MINUS) {
printf("zero %u act %u\n",fArray[FAZERO], fArray[FAACTUAL]);
		}

#else
		LCD_Print(StrLiftProbe); 
#endif
/*		if(GetKey(1) == KEY_PLUS)
			ShutDown(); 
		while(LpProcessPacket())
			; */
		Sound(SOUND_ERROR);
		clockDelay(#20);
#ifndef	MODEL5
		checkShutDown();
#endif		
	}
	resetShutDown();
	ShowReading(INFINITY,NULL,UserConstants.srfFlags);
	Sound(SOUND_READING); 
}

int
checkProbe(ctype)		/* see if its not infinity, also do calibration */
	char	ctype;		/* true if calibration type, otherwise normal check */
{
#ifdef	PRONF
	if(gsX = nfcheckProbe(ctype)) {
		clrbit(bitFerrousMode)
		return(gsX);
	}
#endif

#ifdef	PROF
	if(gsX = fcheckProbe(ctype)) {
		setbit(bitFerrousMode)
		return(gsX);
	}
#endif
	return(0);
}

#ifdef	DUALCALMODE
int
UseNF()
{
				/* reading, else ferrous for Normal */
	if(!(UserConstants.srfFlags2 & SRF2_EMRON) &&
	     (UserConstants.srfFlags & SRF_CAMMODE)) 
		return(1);
	else
		return(0);
}
#endif

unsigned int
CalibrateReading(val)
	unsigned int val;
{
	unsigned int savegsZ;

	savegsZ = gsZ;

	if(val == INFINITY) {
blastoff:;
		gsZ = savegsZ;
		return(val);
	}

	savegsZ = gsZ;
	gsZ = val;
#define	val	gsZ
	gsX = val & 0x7fff;
#ifdef	DUALCALMODE		/* special dual cal, non-ferrous for EOM  */
				/* reading, else ferrous for Normal */
		  if(UseNF()) {
				if(!(UserConstants.srfFlags & SRF_CALMODE))
					goto blastoff;
				gsY = UserConstants.ucCalibration;
		  } else {
				if(!(UserConstants.srfFlags & SRF_CALMODE2))
					goto blastoff;
				gsY = UserConstants.ucFCalibration;
        }
#else
	if(tstbit(CbitFerrousMode)) {
		if(!(UserConstants.srfFlags & SRF_CALMODE2))
			goto blastoff;
		gsY = UserConstants.ucFCalibration;
	} else {
		if(!(UserConstants.srfFlags & SRF_CALMODE))
			goto blastoff;
		gsY = UserConstants.ucCalibration;
	}
#endif	/* DUALCALMODE */

	gsA = 0;
	if(gsY & 0x8000)
		gsA++;
	gsY &= 0x7fff;

	if(val & 0x8000)  { 	/* reading was negative */
		if(gsA) {	/* calibration was negative */
			if(gsY >= gsX) {
				val = gsY - gsX;
			} else {
				val = (gsX - gsY) | 0x8000;
			}
		} else { 	/* more negative */
			val = (gsX + gsY) | 0x8000;
		}
	} else	{		/* reading was positive */
		if(gsA) {	/* calibration negative */
			val = (gsX + gsY);
		} else {
			if(gsY > gsX) {
				val = (gsY - gsX) | 0x8000;
			} else {
				val = gsX - gsY;
			}
		}
	}
#undef	val
	val = gsZ;
	gsZ = savegsZ;
	return(val);
}

static	unsigned int	lastReading;
static	char	*memstr, *ptr;
static	char	bleep;

unsigned int
readProbe(oneup)		/* take readings, display them, save them, etc */
	unsigned char oneup;	/* if true, return first stable reading */
{
#ifdef	DEBUGS
	int	scnt;

	scnt = 0;
#endif

	bleep = 0;
	if(oneup & PROBE_ONESHOT) {
		memstr = NULL;
		if(oneup & PROBE_INCAL)
    		memstr = StrProbeCal;
	} else
		memstr = GetMemoryString(UserConstants.srfFlags | SRF_NOTCAMLAST);
	
	clrbit(bitShowStable);
	lastReading = 0x7fff;
	resetShutDown();

	while(1) {
		clrbit(bitStableReading);
		clrBBit(BBITS_PROBETYPE);
#ifdef	PRONF
#ifdef	PROF
		if(!tstbit(CbitFerrousMode)) 
#endif
		{
			gsY = nfreadProbe();
			setBBit(BITS_NONFERROUS);
		}
#endif

#ifdef	PROF
#ifdef	PRONF
		if(tstbit(CbitFerrousMode))
#endif
		{
			gsY = freadProbe();		/* get reading into gsZ */
			setBBit(BITS_FERROUS);
		}
#endif
		gsZ = UserConstants.srfFlags | SRF_NOTCAMLAST;

		if(!(oneup & PROBE_NOCALMODE)) {
			gsY = CalibrateReading(gsY);
#ifndef	MODEL5
#ifdef	NEGATIVEASZERO
			if(!(SysConstants.scFlags & SCFLG_SHOWNEGATIVE)) {
				if(gsY != INFINITY && (gsY & 0x8000))
					gsY = 0;
			}
#endif
#endif	/* model 5 */		
		} else 
			gsZ &= ~(SRF_MEMORYMODE|SRF_LIMITMODE|SRF_CAMMODE);

		if((gsZ & SRF_LIMITMODE) && LimitCheck(gsY)) 
			gsZ |= SRF_LIMITERROR;
		else
			gsZ &= ~SRF_LIMITERROR;

		setbit(bitLcdNoClear); 
		if(gsY == INFINITY || ShowReading(gsY,memstr,gsZ)) {
			if(lastReading == 0x7fff)
				lastReading = INFINITY;
			break;	/* if infinity quit loop */
		}
#ifdef	DEBUGS
		scnt++;
#endif
#ifdef	MODEL5	/* AUTOSPEC */
		return(lastReading = minReading(gsY,lastReading));
	}	/* end while */
	return(lastReading);
#else 	/* NOT AUTOSPECT */
		if(tstbit(CbitStableReading)) {
			lastReading = minReading(gsY,lastReading);
			if(!tstbit(CbitShowStable)) {
#ifdef	DEBUGS
		printf("%d\n",scnt);
#endif
#ifdef	DOLIMITS
				if((gsZ & SRF_LIMITMODE) && (gsA = LimitCheck(lastReading)) && !(oneup & PROBE_ONESHOT)) {
					bleep = 1;
                    if (1 == gsA)
                    	Sound(SOUND_START|SOUND_LED);	/* above limit */
                    else
                    	Sound(SOUND_ERROR|SOUND_LED); 	/* below limit */
				} else
#endif	/* end if dolimits */
					Sound(SOUND_READING|SOUND_LED);
				setbit(bitShowStable);
				if(oneup & PROBE_ONESHOT)
					return(lastReading);
			}
		}
		checkShutDown();
#ifdef	emergencyout
		if(GetKey(1) == KEY_PLUS)
			return(lastReading);
#endif
	}
	resetShutDown();

	flagvals = UserConstants.srfFlags;
	memstr = GetMemoryString(flagvals);


	if(tstbit(CbitShowStable) && lastReading != INFINITY) {
#ifdef	DOCAM
		if(UserConstants.srfFlags & SRF_CAMMODE) {

			intoflt(lastReading & 0x7fff);		/* can't be negative */
            if(lastReading & 0x8000)
            	mantsgn2 = 0xff;
#ifdef	DOCAMMINMAX
			if(!UserConstants.ucCamCount) {
				camMax = 0xffff;
				camMin = 0x7fff;
			}

			if((int) lastReading > (int) camMax)
				camMax = lastReading;
			if((int) lastReading < (int) camMin)
				camMin = lastReading;
#endif

			movfpac(UserConstants.ucCamSum,fpacc1);
			asm " lcall fltadd";
			movfpac(fpacc1,UserConstants.ucCamSum);
			if(++UserConstants.ucCamCount == UserConstants.ucCamLimit) {
				intoflt(UserConstants.ucCamLimit);
				asm " lcall fltdiv";
				lastReading = fix16(fpacc1);
				movfpac(cons0,UserConstants.ucCamSum);
				UserConstants.ucCamCount = 0;
#ifdef	DOEMR
				if(UserConstants.srfFlags2 & SRF2_EMRON) {
					if(UserConstants.srfFlags2 & SRF2_EMRE) {
						UserConstants.ucEmrE = lastReading;
						UserConstants.srfFlags2 &= ~SRF2_EMRE;
#ifndef	MODEL1ES
						gsX = 0;
						if(!(UserConstants.ucEmrM & 0x7fff)) { /* divide by zero */
							lastReading = UserConstants.ucEmr = 0x8000;
						} else {
							if(UserConstants.ucEmrM > lastReading) {
								intoflt(UserConstants.ucEmrM-lastReading);
								gsX = 0x8000;
							} else
								intoflt(lastReading-UserConstants.ucEmrM);
							movfpac(fpacc2,fpacc1);
							intoflt(100);
							asm " lcall fltmul";
							intoflt(UserConstants.ucEmrM);	/* was lastReading */
							asm " lcall fltdiv";
							lastReading = UserConstants.ucEmr = fix16(fpacc1) | gsX;
						}
#endif
/*						memstr = "EOM "; */
                        memstr = StrProbeEOM;
						Sound(SOUND_START);
					} else {
						UserConstants.srfFlags2 |= SRF2_EMRE;
						UserConstants.ucEmrM = lastReading;
						clrbit(bitLcdNoClear);
                        LCD_Print(StrProbeMDone);
/*						LCD_Print("M DONE"); */
						Sound(SOUND_START);
						resetShutDown();
						for(gsD = 0; gsD < 4; gsD++) {
							IDLE();
							if(checkProbe(0) || GetKey(1))
								break;
							checkShutDown();
						}
						if(gsD == 4)
							Sound(SOUND_NEXTREADING);
						memstr = NULL;
					}
				} else
#endif
				if( gsA =LimitCheck(lastReading)) {
                    if (1 == gsA)
                        Sound(SOUND_START|SOUND_LED);
                    else
                        Sound(SOUND_ERROR|SOUND_LED);
                    clockDelay(#2)
				} else
                	Sound(SOUND_READING|SOUND_LED);

                clockDelay(#1)
                Sound(SOUND_START);
			} else
				flagvals |= SRF_NOTCAMLAST;
		}
#endif	/* DOCAM */
#ifdef	DOLIMITS
		if(LimitCheck(lastReading) && !(flagvals & SRF_NOTCAMLAST)) {	/* show limit error */
			flagvals |= SRF_LIMITERROR;
/*			Sound(SOUND_BADREADING|SOUND_LED); */
		}
#endif
#ifdef	DOMEMORY
		if(!(flagvals & SRF_NOTCAMLAST)) {
			flagvals = UserConstants.srfFlags | (flagvals & SRF_LIMITERROR);
			memstr = GetMemoryString(flagvals); 
/* printf("EP %d memstr %s\n",Batches[UserConstants.ucActiveMemorySlot].biEntryPointer,memstr); */
			StoreMemory(lastReading);
			if((UserConstants.srfFlags | (flagvals & SRF_LIMITERROR)) != flagvals)	/* if memory got turned off */
				memstr = GetMemoryString(flagvals = UserConstants.srfFlags); 

		}
#endif
	} else {
		if(UserConstants.srfFlags & SRF_CAMMODE)
			flagvals |= SRF_NOTCAMLAST;

		flagvals |= SRF_LIMITERROR;
	}

	ShowReading(lastReading,memstr,flagvals);
	if((!tstbit(CbitShowStable)) /* || (flagvals & SRF_LIMITERROR)) && !bleep */) { 	/* do we want to do this ? */
			Sound(SOUND_BADREADING|SOUND_LED);

/*		for(gsA=0; gsA < 3; gsA++) {
			Sound(SOUND_BADREADING|SOUND_LED);
			if(gsA != 2)
				clockDelay(#6);
		} */
	} /* else
		Sound(SOUND_NEXTREADING); */
	return(lastReading);
#endif	/* END NOT AUTOSPEC */
}


#ifdef	DOREMOTEREADING
unsigned int
getReading(whichProbe,howlong, mode)
	unsigned int whichProbe;
	unsigned int howlong;	/* howlong in msec, which probe is LPFF */
	unsigned char mode;
{

	howlong /= 200;
	howlong += ticks;
	clrbit(bitStableReading)
	lastReading = INFINITY;
	/* while ticks < howlong */

	do {
#ifdef	PRONF
		if(whichProbe & LPFF_NONFERROUS)
			if(nfcheckProbe(0)) {
				clrbit(bitFerrousMode)
				lastReading = readProbe(mode);
				if(tstbit(CbitStableReading) && lastReading != INFINITY)
					break;
			}
#endif
#ifdef	PROF
		if(whichProbe & LPFF_FERROUS)
			if(fcheckProbe(0)) {
				setbit(bitFerrousMode)
				lastReading = readProbe(mode);
				if(tstbit(CbitStableReading) && lastReading != INFINITY)
					break;

			}
#endif
		IDLE();
	} while (ticks < howlong);
	if(!tstbit(CbitStableReading) && !tstbit(CbitShowStable))
		lastReading = INFINITY;
	return(lastReading);
}

#endif

void
sintoflt(unsigned int v)
{
	gsX = v;

	intoflt(gsX & 0x7fff);
	if(gsX & 0x8000)
		mantsgn2 = 0xff;
	else
		mantsgn2 = 0;
}


#ifndef	MODEL5
int
CalProbe(mode)
	char	mode;		/* 1 for 1 pt, 2 for 2pt */
{
	char	readings;
	unsigned int lastkey;
	char	*disp;
/*	char	isset; */
#ifdef	DUALCALMODE
	char	emode;		/* in dual mode, EOM mode is selected if mode and 0x80 is true */
	unsigned int f2, f;

    emode = mode & 0x80;
    mode &= 0x7f;

    /* if emode is true, than UseNF should also be true */
    f2 = UserConstants.srfFlags2;
    f = UserConstants.srfFlags;

    if(emode) {
    	UserConstants.srfFlags2 &= ~SRF2_EMRON;
        UserConstants.srfFlags |= SRF_CAMMODE;
    } else {
    	UserConstants.srfFlags &= ~SRF_CAMMODE;
    }

#endif

	readings = 0;

/*	isset = 0; */
	movfpac(cons0, UserConstants.ucCalibrateSum);

getMore:;
	disp = StrcalZero;
	resetShutDown();
	while(1) {
		setbit(bitLcdCheckProbe)
		LCD_Print(disp);
		Sound(SOUND_START);
		lastkey = ticks;
		while(!(Key = GetKey(1)) && (ticks - lastkey < 10)) {
			IDLE();
			if(checkProbe(1))
				break;
			checkShutDown();
		}
		if(Key) {
			Key = GetKey(0);
			resetShutDown();
			if(Key == KEY_SELECT) {
				if(!(readings & 0x7f)) {
					Sound(SOUND_ERROR);
					goto getMore;
				} else
					break;	/* break while */
			} else if (Key == KEY_META2) {
cancel:;
				if(readings) {
#ifdef	PRONF
					if(readings & 0x80)
#endif
						UserConstants.srfFlags &= ~(SRF_CALMODE2);
#ifdef	PRONF
					else
						UserConstants.srfFlags &= ~(SRF_CALMODE);
#endif
				}

/*				UserConstants.srfFlags &= ~(SRF_CALMODE|SRF_CALMODE2); */
/*				movfpac(cons0,UserConstants.ucCalibrateSum); */
				Sound(SOUND_CANCEL);
#ifdef	DUALCALMODE
      			UserConstants.srfFlags2 = f2;
                UserConstants.srfFlags = f;
#endif
				return(0);
			}
			Sound(SOUND_ERROR);
			goto getMore;
		}
		if(ticks - lastkey >= 10)
			continue;

#ifdef	DEBUGC
	printf("r %x F %d\n",readings,tstbit(CbitFerrousMode));
#endif

		if(!readings) {
#ifdef	DUALCALMODE
			if(!emode) {
#else
			if(tstbit(CbitFerrousMode)) {
#endif
                movfpac(cons0,UserConstants.ucFCalRatio);
            }  else {
                movfpac(cons0,UserConstants.ucNFCalRatio);
            }

			if(tstbit(CbitFerrousMode)) {
				readings |= 0x80;
			}
		}
#ifdef	PRONF
		else {
		 	if(((readings & 0x80) && !tstbit(CbitFerrousMode)) ||
			   (!(readings & 0x80) && tstbit(CbitFerrousMode))) {
			 	Sound(SOUND_ERROR);
				continue;
			}
		}
#endif

		resetShutDown();
		/* get here, probe proximity */
		gsX = readProbe(PROBE_ONESHOT|PROBE_NOCALMODE|PROBE_INCAL);
#ifdef	DEBUGC
	printf("gsx %x\n",gsX);
#endif
		if(gsX == INFINITY)
			continue;
		if(gsX & 0x8000)
			gsA = 1;
		else
			gsA = 0;

		intoflt(gsX & 0x7fff);
		movfpac(UserConstants.ucCalibrateSum, fpacc1);
		if(gsA)
			asm " lcall fltsub";
		else
			asm " lcall fltadd";
#ifdef	DEBUGC
	printf("gsx %u %u gsa %d\n",gsX, gsX & 0x7fff,gsA);
#endif
		movfpac(fpacc1,UserConstants.ucCalibrateSum);
#ifdef	DEBUGC
	printf("sum 0x%x %u ",fix16(fpacc1), fix16(fpacc1));
	fltasc();
#endif
		readings++;
#ifdef	DS15MHZ
		clockDelay(#40);
#else
		clockDelay(#20);
#endif
		disp = StrCalMoreSel;
	}
	intoflt(readings & 0x7f);
	movfpac(UserConstants.ucCalibrateSum,fpacc1);
	asm " lcall fltdiv";

#ifdef	DUALCALMODE
	if(!emode) {
#else
	if(tstbit(CbitFerrousMode)) {
#endif
		UserConstants.ucFCalibration = fix16(fpacc1);
//		movfpac(cons0,UserConstants.ucFCalRatio);
		UserConstants.srfFlags |= SRF_CALMODE2;
#ifdef	DUALCALMODE
        f |= SRF_CALMODE2;
#endif
	}  else {
		UserConstants.ucCalibration = fix16(fpacc1);
//		movfpac(cons0,UserConstants.ucNFCalRatio);
		UserConstants.srfFlags |= SRF_CALMODE;
#ifdef	DUALCALMODE
        f |= SRF_CALMODE;
#endif
    }
#if	DEBUGC1
	printf("readings 0x%x offset 0x%x\n",readings,fix16(fpacc1));
#endif
	if(mode != 2) {
#ifdef	DUALCALMODE
      			UserConstants.srfFlags2 = f2;
                UserConstants.srfFlags = f;
#endif
		return(1);
	}
	disp = StrCalNonZero;
	resetShutDown();
//#ifdef	DUALCALMODE		This is wrong in/after vers 0x21
//	if(emode) {
//		movfpac(UserConstants.ucFCalRatio,UserConstants.ucNFCalRatio);		/* save ratio */
//		movfpac(cons0,UserConstants.ucFCalRatio);
//    }
//#endif

	while(1) {
		LCD_Print(disp);
		Sound(SOUND_START);
		lastkey = ticks;
		while(!(Key = GetKey(1)) && (ticks - lastkey < 10)) {
			IDLE();
			if(checkProbe(1))
				break;
			checkShutDown();
		}
		if(Key) {
			resetShutDown();
			if(Key == KEY_META2)
				goto cancel;
			Sound(SOUND_ERROR);
			continue;
		}
		if(ticks - lastkey >= 10)
			continue;
#ifdef	PRONF
#ifdef	PROF
		 if(((readings & 0x80) && !tstbit(CbitFerrousMode)) ||
		    (!(readings & 0x80) && tstbit(CbitFerrousMode))) {
		  	Sound(SOUND_ERROR);
		 	continue;
		}
#endif
#endif
		resetShutDown();
		gsX = readProbe(PROBE_ONESHOT|PROBE_INCAL);
		if(gsX == INFINITY)
			continue;

		break;
	}

	/* get here, adjust ratio */
	lastkey = gsX;
	EditValue(gsX,0,MAX_LIMIT,UserConstants.srfFlags & ~(SRF_CALMODE|SRF_MEMORYMODE|SRF_LIMITMODE),0,StrCalAdjust,gsY);
	if(gsY == INFINITY)
		goto cancel;
	intoflt(gsY);
	movfpac(fpacc2,fpacc1);
	intoflt(lastkey);
	asm " lcall fltdiv";
#ifdef	DUALCALMODE
	if(!emode) {
#else
	if(tstbit(CbitFerrousMode)) {
#endif
		movfpac(fpacc1,UserConstants.ucFCalRatio);		/* save ratio */
        sintoflt(UserConstants.ucFCalibration);
        asm " lcall fltmul";
        if(gsY = fix16(fpacc1))
        	UserConstants.ucFCalibration = gsY;
	} else {
		movfpac(fpacc1,UserConstants.ucNFCalRatio);		/* save ratio */
        sintoflt(UserConstants.ucCalibration);
        asm " lcall fltmul";
        if(gsY = fix16(fpacc1))
	        UserConstants.ucCalibration = gsY;
	}

#ifdef	DUALCALMODE
    UserConstants.srfFlags2 = f2;
    UserConstants.srfFlags = f;
#endif

	return(1);
}


#endif

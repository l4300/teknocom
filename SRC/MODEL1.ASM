* defines for model1 unit
MEMOFFSET	EQU	$7000
* DS2252	EQU	TRUE
* DS15MHZ	EQU	TRUE

SysConstants	EQU	($200+MEMOFFSET)
UserConstants	EQU	($300+MEMOFFSET)
Menu		EQU	($500+MEMOFFSET)
* Sounds		EQU	($7d0+MEMOFFSET)
Sounds		EQU	($bd0+MEMOFFSET)
Batches		EQU	($800+MEMOFFSET)

* ?RAM	EQU	($800+MEMOFFSET)		RAM Starts here
?RAM	EQU	($c00+MEMOFFSET)		RAM Starts here
?RAMEND	EQU	($ff0+MEMOFFSET)		RAM Ends here  was 7700
RAMSTART	EQU	?RAM


Templates	EQU	$1200+MEMOFFSET
DataBlocks	EQU	$1d00+MEMOFFSET

?PARTITION	EQU	$7000	Partition Address
?PARTCODE	EQU	%11101000	Include Bit 4 for Alternate 32 Memory

*
* DDS MICRO-C 8031/51 Startup Code & Runtime library for LARGE model
*
* Copyright 1991-1994 Dave Dunfield
* All rights reserved.
*
* System Memory Map
TA	EQU	$c7
MCON	EQU	$C6
TRUE	EQU	$ff

	ORG	$0000		ROM Starts here
*	ORG	$0800		ROM Starts here

* Fixed memory locations for alternate access to the CPU register bank.
* If you are NOT useing BANK 0, these equates must be adjusted.
?R0	EQU	0		Used for "POP" from stack
?R1	EQU	?R0+1		Used to load index indirectly
?R2	EQU	?R0+2		""		""		""		""
?R3	EQU	?R0+3		Used by some runtime lib functions
?R4	EQU	?R0+4
?R5	EQU	?R0+5
?R6	EQU	?R0+6
?R7	EQU	?R0+7
*
* Startup code entry point
*
* If you are NOT using interrupts, you can reclaim 50 bytes
* of code space by removing the following TWO lines.
	AJMP	*+$0044		Skip interrupt vectors
	DS	$0032-2		Reserve space for interrupt vectors
    DB	'T','.','E','.','K','.','N','.','O','C','.','O','.','M','.',0,0,0

*
*	disable watchdog timer
	MOV	A,PCON
	MOV	TA,#$AA
	MOV	TA,#$55
	ANL	PCON,#~4
	MOV	B,A

	MOV	A,MCON
	ANL	A,#$01
	ORL	A,#?PARTCODE
	MOV	TA,#$AA
	MOV	TA,#$55
	ORL	MCON,#$2	Enable PAA bit
	MOV	MCON,A		Reset Partition

	MOV	TA,#$AA
	MOV	TA,#$55
	ANL	MCON,#~$2

*	ORL	MCON,#4		Enable alternate 32K on 64K model




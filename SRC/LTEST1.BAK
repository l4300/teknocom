/*
 * ltest1.c --  Larry Test Program 1
 * Copyright (C) 1993.94 MurkWorks, All Rights Reserved
*/


#define	KDEBUG	1
#include <8051bit.h>
#include <8051io.h>
#include <8051int.h>
#include <8051reg.h>


/* common register definitions */

#define EX0     IE.0
#define ET0     IE.1
#define EX1     IE.2
#define ET1     IE.3
#define ES      IE.4
#define EA      IE.7

#define TF1     TCON.7
#define TR1     TCON.6
#define TF0     TCON.5
#define TR0     TCON.4
#define IE1     TCON.3
#define IT1     TCON.2
#define IE0     TCON.1
#define IT0     TCON.0

#define	CNTR_ENABLE	P2.5


#define ANALOG_POWER    128
#define ANALOG_PORT	   P1
#define SYSTEM_POWER    128
#define SYSTEM_PORT     P3

#define ANALOG_OFF      	(ANALOG_PORT |= ANALOG_POWER)
#define ANALOG_ON      	(ANALOG_PORT &= ~ANALOG_POWER)
#define SYSTEM_OFF		(SYSTEM_PORT &= ~SYSTEM_POWER)
#define SYSTEM_ON		(SYSTEM_PORT |= SYSTEM_POWER)


#define CNTR_VALUEPORT       P0
#define CNTR_MASK       	0x7f
#define CNTR_ENABLEBIT		32
#define CNTR_PORT		P2
#define MODEBIT_MASK		231
#define MODEBIT_ZERO		0x08
#define MODEBIT_REF		0x10

#define	MODE_ZERO		(MODEBIT_ZERO|7)
#define	MODE_REF		(MODEBIT_REF|7)
#define	MODE_ACTUAL		(7)

#define	WAVEFORM_ZERO		0xf0
#define	WAVEFORM_REF		0x78

#define MMODE_ZERO		(CNTR_PORT = (CNTR_PORT & MODEBIT_MASK) | MODEBIT_ZERO)
#define MMODE_REF		(CNTR_PORT = (CNTR_PORT & MODEBIT_MASK) | MODEBIT_REF)
#define MMODE_ACTUAL		(CNTR_PORT = (CNTR_PORT & MODEBIT_MASK))

/*
#define CLEAR_COUNTER	(CNTR_PORT |= CNTR_CLEARBIT)
#define READY_COUNTER	(CNTR_PORT &= ~CNTR_CLEARBIT)
*/

#define ENABLE_COUNTER	(CNTR_PORT &= ~CNTR_ENABLEBIT)
#define DISABLE_COUNTER	(CNTR_PORT |= (CNTR_ENABLEBIT|0xf|MODEBIT_ZERO))

#define TR1ON           setbit(TR1)
#define TR1OFF          clrbit(TR1)

#define	IDLE()		(PCON |= 1)	/* enter IDL mode */

unsigned char   *copyright = "Copyright 1993,94 (C) MurkWorks, Inc.";

/* clock master time count */
register unsigned int  ticks;

/* variables for taking readings */
register unsigned int  msec_reload;	/* reload value for T0 */

register unsigned char	cyclecount;	/* how many cycles to do */
register unsigned char	waveform;
register unsigned char	normalp2;

unsigned int	values[4];
char	*readings[3]={
	"Zero", "Actual", "Reference"
};

/* global flag	*/
unsigned char	sysflags;
#define	SYSFLAG_SERIALON	1

int     baud = 9600;

unsigned char	general_string[64];		/* general work string off stack */
asm {
$SE:0
_/**/_TF0_	/* This label will be the address of the "prefix" stub */
		org		_TF0_			/* Position to interrupt vector */
		PUSH	PSW			/* Save Processor Status Word */
		PUSH	A			/* Save accumulator (2 bytes) */
		LJMP	_/**/_TF0_	/* Don't overwrite next vector (+3 = 7) */
		ORG		_/**/_TF0_	/* Position back to original PC */
 /*		MOV    TH0,#253
		MOV	TL0,#167 */

		MOV	A,msec_reload+1
		CPL	A
		MOV	TH0,A
		MOV	A,msec_reload
		CPL	A
		MOV	TL0,A

		POP		A			/* Restore A accumulator */
		POP		PSW			/* Restore Processor Status Word */
		RETI				/* End interrupt */
}


Cycles()
{

asm {

	/* setup registers as follows

		R6 - Iteration count  (1 - 255)
		R5 - Cycle count   ( 3 - 0)
		R4 - Waveform
		R3 - P2 normal values

	*/



		MOV	R6,cyclecount
		MOV	R4,waveform
		MOV	R3,normalp2
NextWaveform

		MOV	R5,#4
		SJMP	Work

NextShift
		NOP		/* these nops required to get square waves equal */
		NOP
		NOP
		NOP
		NOP
		NOP
Work
		MOV	A,R4
		RL	A
		RL	A
		MOV	R4,A

		ANL	A,#192
		ORL	A,R3
		MOV	P2,A

		ORL	PCON,#1	/* enter idle mode */

		DJNZ	R5,NextShift

		DJNZ	R6,NextWaveform

		SETB	P2.5

}

}

int
Take_Reading(ctime, ccount)          	/* take a reading from probe or reference */
	int ctime;			/* cycles per second value */
	int ccount;			/* number of cycles per reading */
{
	unsigned int x;
	unsigned char rval, oldval;

	unsigned int oldmsec;

	if(!ccount || !ctime)
		return(-1);

	if(ctime < 10 || ctime > 6000) {
		return(-1);
	}

	x = 14950/ctime;
	x *= 10;

/*	x = -x; */

#ifdef	KDEBUG
	printf("ctime %u x %u\n", ctime, x);
	xdelay(5);
	SerialOff();
#endif
#ifdef	CDEBUG
	SerialOff();
#endif

	oldmsec = msec_reload;
	cyclecount = ccount;


	Fast();

	ANALOG_ON;			/* turn on analog power */

	TR1OFF				/* turn off counter 1 */

	xdelay(50);			/* wait 50msec for warmup */

	msec_reload = x;		/* reset clock */

	DISABLE_COUNTER;

	for(x=0; x < 3; x++) {

		switch (x) {
			case 0 :		/* mode zero */
				normalp2 = MODE_ZERO;
				waveform = WAVEFORM_ZERO; /* z */
				break;
			case 1 :		/* actual */
				normalp2 = MODE_ACTUAL;
				waveform = WAVEFORM_ZERO; /* z */
				break;
			case 2 :		/* reference */
				normalp2 = MODE_REF;
				waveform = WAVEFORM_REF;
				break;
		}

		TH1 = 0;
		TL1 = 0;

		oldval = P0 & 0x7f;
		if(P3 & 0x20)
			oldval |= 0x80;

		TR1ON
		Cycles();
		TR1OFF
		DISABLE_COUNTER;		/* also sets zero bit */
		rval = P0 & 0x7f;
		if(P3 & 0x20)
			rval |= 0x80;

		rval -= oldval;
		values[x] = (TL1 << 8) | rval;

	}


	ANALOG_OFF;			/* turn of analog power */

	Fast();				/* reset clock */

#ifdef	CDEBUG
	SerialOn();
	xdelay(2);
	printf("rval=%u mincount=%u\n",rval,MinCount);
#endif
#ifdef KDEBUG
	SerialOn();
#endif
	return(0);			/* return the min or max reading */
}


xdelay(count)
	unsigned int count;
{

	while(count--) {
		IDLE();
	}
}

ClockOn()
{
	unsigned char tmod;

	clrbit(TR0)             /* counters off */
	clrbit(ET1)

	TH0 = -( msec_reload >> 8);
	TL0 = -(msec_reload & 256);

	tmod = TMOD;
	tmod &= 0xf0;
	tmod |= 1;
	TMOD = tmod;            /* TR1 = counter, TR0 = timer */
	enable();
	setbit(TR0)
	setbit(ET0)
}

SerialOn()
{
	serinit(baud);
	ClockOn();
	sysflags |= SYSFLAG_SERIALON;
}

SerialOff()
{
	TMOD = 0x51;
	ClockOn();
	sysflags &= ~SYSFLAG_SERIALON;
}

Fast()
{
	disable();
	msec_reload 	= 598;			/* gives 1msec clock was 614*/
	enable();
}

Slow()
{
	disable();
	msec_reload 	= 59800;		/* gives 100 msec clock was 614*/
	enable();
}

Initialize()
{
	unsigned int x;

	ANALOG_OFF;

	ticks		= 0;

	Fast();
	P2 = 0xff;
	ClockOn();
	SerialOn();
}


unsigned int
longdivide(a,b)
	unsigned int a;
	unsigned int b;
{
	int	bits;

	unsigned int result;

	result = 0;
	bits = 16;

#ifdef	CDEBUG1
	printf("ldiv %u/%d = ",a,b);
#endif
	if(a & (1 << (bits - 1)) ||
	   b & (1 << (bits - 1))) {
		a >>= 1;
		b >>= 1;
	}
	while(bits--) {
		result <<= 1;
		if(a >= b) {
			result |= 1;
			a -= b;
		}
		a <<= 1;
	}
#ifdef	CDEBUG1
	printf("%u\n",result);
#endif
	return(result);
}

int
Xgetval(p)
	int p;
{
	int c;

	c = 99;
	switch(p) {
		case 0 :
			c = P0;
			break;
		case 1 :
			c = P1;
			break;
		case 2 :
			c = P2;
			break;
		case 3 :
			c = P3;
			break;
	}
	return(c);
}


Xchange(p)
	int p;
{
	unsigned int	x;
	unsigned char ovalue, xvalue;
	unsigned char y;

	xvalue = 0;
	ovalue = Xgetval(p);
	printf("P%d = %u ... ",p, y=ovalue);

	for(x=0; x < 0x4ff0; x++) {
		xvalue = Xgetval(p);
		if(xvalue != ovalue)
			break;
	}
	if(x == 0xfff0)
		printf("timeout\n");
	else
		printf("%u\n",y=xvalue);
}


Debug()
{
	unsigned char   str[64];
	char len;
	unsigned int    x;
	char    *arg,*arg2;
	unsigned char y;
	unsigned int rh, vh, zh;

	unsigned int	ccount, cps;

	ccount = 20;
	cps = 250;

	Fast();

	printf("%s\n",copyright);

	while(1) {
		printf("> ");
		len = getstr(str, sizeof(str));
		if(!len)
			goto usage;

		arg = &str[1];
		arg2 = strchr(str,'=');
		if(arg2)
			arg2++;

		while(*arg && *arg == ' ')
			arg++;

		if(!*arg && len > 1 && !arg2) {
			printf("E:arg required\n");
			continue;
		}
		switch(str[0]) {


			case '0' :
				if(!arg2)
					printf("P0=%u\n",y = P0);
				else if(*arg2 == '!')
					Xchange(0);
				else {
					P0 = atoi(arg2);
					printf("P0 set to %u, equals %u\n",atoi(arg2), y=P0);
				}
				break;
			case '1' :
				if(!arg2)
					printf("P1=%u\n",y = P1);
				else if(*arg2 == '!')
					Xchange(1);
				else {
					P1 = atoi(arg2);
					printf("P1 set to %u, equals %u\n",atoi(arg2), y=P1);
				}
				break;
			case '2' :
				if(!arg2)
					printf("P2=%u\n",y = P2);
				else if(*arg2 == '!')
					Xchange(2);
				else {
					P2 = atoi(arg2);
					printf("P2 set to %u, equals %u\n",atoi(arg2), y=P2);
				}
				break;
			case '3' :
				if(!arg2)
					printf("P3=%u\n",y = P3);
				else if(*arg2 == '!')
					Xchange(3);
				else {
					P3 = atoi(arg2);
					printf("P3 set to %u, equals %u\n",atoi(arg2), y=P3);
				}
				break;

			case 'x' :
				xdelay(5);
				vh = Take_Reading(cps,ccount);
				if(vh) {
					printf("couldn't take reading\n");
					break;
				}
				for(x=0;x < 3; x++)
					printf("%s %u\n", readings[x], values[x]);
				break;
			case 'r' :
				if(arg)
					ccount = atoi(arg);
				printf("Cyclecount = %d\n", ccount);
				break;


			case 'f' :
				if(arg)
					cps = atoi(arg);
				printf("CPS=%d\n",cps);
				if(cps < 10 || cps > 6000) {
					printf("invalid cps, choose 10 - 6000\n");
					cps = 100;
				}
				break;
			default:;
usage:;
				printf("x take reading\nr set reading time\nf freq\nq quit\n");
				printf("Read or Set ports...\n0 [=val]\n1 [=val]\n2 [=val]\n3 [=val]\n");
				break;
		}
	}
}

main()
{
	Initialize();

	xdelay(750);
	Debug();
}


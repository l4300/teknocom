# menu lists

set templateMenu  {
	flags
	prevMenu
	{ "String 1" "string 2" "String 3" }
	{ FredMenu NoWhereMenu }
}


set	RootMenu {
	0
	NONE
	{ {MEMORY MemoryMenu} {"Cal" CalibrateMenu}
	  {CAM mnuCamMode} {LIMITS LimitsMenu}
	  {UNITS UnitsMenu} {CONFIG ConfigMenu} {"e " NONE}}
}

#this one is emr
set	RootMenu1E {
	0
	NONE
	{ {"Cal" CalibrateMenu} {EMR mnuEmrMode} {CAM mnuCamMode}
	  {UNITS UnitsMenu} {CONFIG ConfigMenu1E} {"e " NONE}}
}

set	RootMenu1ES {
	0
	NONE
	{ {"Cal 1-2" CalibrateMenu} {EMR mnuEmrMode} {MESH mnuCamMode}
	  {UNITS UnitsMenu} {CONFIG ConfigMenu1E} {"e " NONE}}
}


set	RootMenu1EG {
	0
	NONE
	{ {"KALIB" CalibrateMenuG} {AUFBAU mnuEmrMode} {GEWEBE mnuCamMode}
	  {EINHEIT UnitsMenuG} {KONFIG ConfigMenu1EG} {"e " NONE}}
}


set	RootMenu1 {
	0
	NONE
	{ {"Cal" CalibrateMenu}  {CAM mnuCamMode}
	  {UNITS UnitsMenu} {CONFIG ConfigMenu1} {"e " NONE}}
}


set	CalibrateMenu {
	0
	RootMenu
	{ {"e Cal" RootMenu} {"2 PTS" mnuCalTwo} {"1 PT" mnuCalOne}
	  {"Factory" menuCalFactory} }
}

set	CalibrateMenuG {
	0
	RootMenu
	{ {"e KAL" RootMenu} {"2-P KAL" mnuCalTwo} {"1-P KAL" mnuCalOne}
	  {"GRUNDKAL" menuCalFactory} }
}

set	MemoryMenu {
	0
	RootMenu
	{ {"e MEM" RootMenu} {"ON/OFF" mnuMemoryOff} {CLEAR mnuMemoryClear} {Status mnuMemoryStatus}
	 {PRINT mnuMemoryPrint} {Tag mnuMemoryTag} }
}

# {EXPORT mnuMemoryTouch}   # was in memory menu

set	CamMenu {
	0
	RootMenu
	{{"e CAM" RootMenu} {Off mnuCamOff} {On mnuCamOn} }
}

set	EmrMenu {
	0
	RootMenu
	{{"e EMR" RootMenu} {Off mnuEmrOff} {On mnuEmrOn} }
}

set	LimitsMenu {
	0
	RootMenu
	{ {"e LIM" RootMenu} {Off mnuLimitsOff} {On mnuLimitsOn} {Low mnuLimitsLow}
	 {High mnuLimitsHigh} }
}

set	UnitsMenu {
	0
	RootMenu
	{{"e UNITS" RootMenu} {Mils mnuUnitsMils} {Microns mnuUnitsMicrons}
	 }
}

set	UnitsMenuG {
	0
	RootMenu
	{{"e EINHEIT" RootMenu} {"US MILS" mnuUnitsMils} {"MIKRON" mnuUnitsMicrons}
	 }
}

set	ConfigMenu {
	0
	RootMenu
	{ {"e CNF" RootMenu} {Date mnuDate} {Time mnuTime}
      {PRINTER mnuConfigPrint}
	  {Factory mnuFactoryConfig}
	  {"Serial Number" mnuSerialNumber}
	  {"Version" mnuVersion}
	  }
}

set	SysMenu1 {
	0
	SysMenu
	{
		{Flags mnuSysFlags} {SNumber mnuSysSerial}
		{Ferrous mnuSysFerrous} {NonFerrous mnuSysNonFerrous}
		{Bench mnuBenchTest}
		{Exit RootMenu}
	}

}

set	SysMenu {
	0
	SysMenu
	{
		{Flags mnuSysFlags} {SNumber mnuSysSerial}
		{Ferrous mnuSysFerrous} {NonFerrous mnuSysNonFerrous}
		{Exit RootMenu}
	}
}


set	ConfigMenu1 {
	0
	RootMenu
	{
	  {"e CNF" RootMenu} {Factory mnuFactoryConfig}
	  {"Serial Number" mnuSerialNumber}
	  {"Version" mnuVersion}
	  }
}

set	ConfigMenu1E {
	0
	RootMenu
	{
	  {"e CNF" RootMenu} {Factory mnuFactoryConfig}
	  {"Serial Number" mnuSerialNumber}
	  {"Version"  mnuVersion}
	  }
}

set	ConfigMenu1EG {
	0
	RootMenu
	{
	  {"e KON" RootMenu} {GRUNDEINSTELLUNG mnuFactoryConfig}
	  {"SERIEN NUMMER" mnuSerialNumber}
	  {"Version"  mnuVersion}
	  }
}

set	strings {
	{FindChar_vals " ()*+-/0123456789<=>@ABCDEFGHIJKLMNOPQRSTUVWXYZ"}
	{FindChar_ValLen 47 0}
	{FindChar_numbers "0123456789"}
	{FindChar_NumberLen 10 0}
	{DoMenu_FactoryConfig "\001WAIT"}
	{mnuCamFunctionOff "OFF"}
	{mnuCamFunctionExit "EXIT"}
	{mnuCamFunctionCAM "CAM %s"}
	{chooseBatchAll "ALL"}
	{chooseBatchNone "NONE"}
	{chooseBatchFmt "%c%c"}
	{mnuCalComplete "COMPLETE"}
	{mnuRows "ROWS"}
	{mnuCols "COLS"}
	{mnuMemoryUsed "Used %u of %u"}
	{mnuBatchInfo "%c %s %d Readings %d Rows %d Cols"}
	{mnuLowLimit "LOWL"}
	{mnuHighLimit "HI L"}
	{StrcalZero "\00100 PLATE"}
	{StrCalMoreSel "MORE/SEL"}
	{StrCalNonZero "\001W/ SHIM"}
	{StrCalAdjust "ADJC"}
	{StrBatchFull "BATCH FULL"}
	{StrMemDash "%s-"}
	{StrMemString " %c%d  "}
	{StrMemString2 "%d   "}
	{StrMemString3 "\303%d   "}
	{StrMemString4 "\301R%d   "}
	{StrMemFull  "MEM FULL"}
	{StrTemplate "TEMPLATE"}
	{StrNoneExit "NONE/EXIT"}
	{StrTouch "\001Touch"}
	{StrTooSmall "Too Small"}
	{StrLift "\001Lift"}
	{StrDone "\001DONE"}
	{StrLiftProbe "\001Lift Probe"}
    {StrProbeCal "CAL "}
    {StrProcCamMin "MIN "}
    {StrProcCamMax "MAX "}


	{nf1 0x40 0x80 0x0 0x0}
	{nf2 0x41 0x0 0x0 0x0}
	{nf100 0x43 0xc8 0 0}
	{nf4 0x41 0x80 0 0}
	{nf20 0x42 0xa0 0 0}
	{nfmicrons 0x41 0x22 0x8f 0x5e}
	{nfozunits 0x40 0x18 0x2c 0xb8}
	{StrEditFlags "FLG"}
	{StrEditSerialNumber "SNUM"}
	{StrFactCalAll "FERROUS,NONFERROUS,BOTH,EXIT"}
	{StrFactCalFerrous "FERROUS,EXIT"}
	{StrFactCalNonFerrous "NONFERROUS,EXIT"}
	{StrAvgX 0xa0 0x8a 0x80 0x80 0x80 0x58 0xa0 0x80 0x80 0x80 0x85 0x20 0}

	{@0xbd0 150 120 150 120 1
		100 180 0 0 0
		50 20 50 20 0
		250 30 250 30 1
		100 70 100 70 0
		250 70 250 70 0
		250 180 250 180 0
		250 20 250 40 1
		11 180 0 0 0
	}

}

set	strings1 {
	{FindChar_vals " ()*+-/0123456789<=>@ABCDEFGHIJKLMNOPQRSTUVWXYZ"}
	{FindChar_ValLen 47 0}
	{FindChar_numbers "0123456789"}
	{FindChar_NumberLen 10 0}
	{DoMenu_FactoryConfig "\001WAIT"}
	{mnuCamFunctionOff "OFF"}
	{mnuCamFunctionExit "EXIT"}
	{mnuCamFunctionCAM "CAM %s"}
	{mnuEmrFunctionEMR "EOM %s"}
	{chooseBatchAll "ALL"}
	{chooseBatchOff "OFF"}
	{chooseBatchNone "NONE"}
	{chooseBatchFmt "%c%c"}
	{mnuCalComplete "COMPLETE"}
	{mnuRows "ROWS"}
	{mnuCols "COLS"}
	{mnuMemoryUsed "Used %u of %u"}
	{mnuBatchInfo "%c %s %d Readings %d Rows %d Cols"}
	{mnuLowLimit "LOWL"}
	{mnuHighLimit "HI L"}
	{StrcalZero "\00100 PLATE"}
	{StrCalMoreSel "MORE/SEL"}
	{StrCalNonZero "\001W/ SHIM"}
	{StrCalAdjust "ADJC"}
	{StrBatchFull "BATCH FULL"}
	{StrMemDash "%s-"}
	{StrMemString " %c%d  "}
	{StrMemString2 "%d   "}
	{StrMemString3 "\303%d   "}
	{StrMemString4 "\301R%d   "}
	{StrMemFull  "MEM FULL"}
	{StrTemplate "TEMPLATE"}
	{StrNoneExit "NONE/EXIT"}
	{StrTouch "\001Touch"}
	{StrTooSmall "Too Small"}
	{StrLift "\001Lift"}
	{StrDone "\001DONE"}
	{StrLiftProbe "\001Lift Probe"}
    {StrProbeCal "CAL "}
    {StrProcCamMin "MIN "}
    {StrProcCamMax "MAX "}


	{nf1 0x40 0x80 0x0 0x0}
	{nf100 0x43 0xc8 0 0}
	{nf4 0x41 0x80 0 0}
	{nf20 0x42 0xa0 0 0}
	{nfmicrons 0x41 0x22 0x8f 0x5e}
	{nfozunits 0x40 0x18 0x2c 0xb8}
	{StrEditFlags "FLG"}
	{StrEditSerialNumber "SNUM"}
	{StrFactCalAll "FERROUS,NONFERROUS,BOTH,EXIT"}
	{StrFactCalFerrous "FERROUS,EXIT"}
	{StrFactCalNonFerrous "NONFERROUS,EXIT"}
	{StrBenchChoice "NF,Z,A,I,T,EXIT"}
	{StrAvgE 0xa0 0x8a 0x80 0x80 0x80 0x45 0xa0 0x80 0x80 0x80 0x85 0x20 0}
	{StrAvgM 0xa0 0x8a 0x80 0x80 0x80 0x4d 0xa0 0x80 0x80 0x80 0x85 0x20 0}
	{StrAvgX 0xa0 0x8a 0x80 0x80 0x80 0x58 0xa0 0x80 0x80 0x80 0x85 0x20 0}

	{@0x7bd0 150 120 150 120 1
		100 180 0 0 0
		50 20 50 20 0
		250 30 250 30 1
		100 70 100 70 0
		250 70 250 70 0
		250 180 250 180 0
		250 20 250 40 1
		11 180 0 0 0
	}

}


set	strings1b {
	{FindChar_vals " ()*+-/0123456789<=>@ABCDEFGHIJKLMNOPQRSTUVWXYZ"}
	{FindChar_ValLen 47 0}
	{FindChar_numbers "0123456789"}
	{FindChar_NumberLen 10 0}
	{DoMenu_FactoryConfig "\001WAIT"}
	{mnuCamFunctionOff "OFF"}
	{mnuCamFunctionExit "EXIT"}
	{mnuCamFunctionCAM "CAM %s"}
	{mnuEmrFunctionEMR "EOM %s"}
	{chooseBatchAll "ALL"}
	{chooseBatchOff "OFF"}
	{chooseBatchNone "NONE"}
	{chooseBatchFmt "%c%c"}
	{mnuCalComplete "COMPLETE"}
	{mnuRows "ROWS"}
	{mnuCols "COLS"}
	{mnuMemoryUsed "Used %u of %u"}
	{mnuBatchInfo "%c %s %d Readings %d Rows %d Cols"}
	{mnuLowLimit "LOWL"}
	{mnuHighLimit "HI L"}
	{StrcalZero "\00100 PLATE"}
	{StrCalMoreSel "MORE/SEL"}
	{StrCalNonZero "\001W/ SHIM"}
	{StrCalAdjust "ADJC"}
	{StrBatchFull "BATCH FULL"}
	{StrMemDash "%s-"}
	{StrMemString " %c%d  "}
	{StrMemString2 "%d   "}
	{StrMemString3 "\303%d   "}
	{StrMemString4 "\301R%d   "}
	{StrMemFull  "MEM FULL"}
	{StrTemplate "TEMPLATE"}
	{StrNoneExit "NONE/EXIT"}
	{StrTouch "\001Touch"}
	{StrTooSmall "Too Small"}
	{StrLift "\001Lift"}
	{StrDone "\001DONE"}
	{StrLiftProbe "\001Lift Probe"}
    {StrProbeEOM "EOM "}
    {StrProbeMDone "M DONE"}
    {StrMenuMESH "MESH %s"}
	{StrMenuDCALPrompt "SHIM,MESH"}
    {StrProbeCal "CAL "}
    {StrProcCamMin "MIN "}
    {StrProcCamMax "MAX "}

	{nf1 0x40 0x80 0x0 0x0}
	{nf100 0x43 0xc8 0 0}
	{nf4 0x41 0x80 0 0}
	{nf20 0x42 0xa0 0 0}
	{nfmicrons 0x41 0x22 0x8f 0x5e}
	{nfozunits 0x40 0x18 0x2c 0xb8}
	{StrFactCalAll "FERROUS,NONFERROUS,BOTH,EXIT"}
	{StrFactCalFerrous "FERROUS,EXIT"}
	{StrFactCalNonFerrous "NONFERROUS,EXIT"}
	{StrAvgE 0xa0 0x8a 0x80 0x80 0x80 0x45 0xa0 0x80 0x80 0x80 0x85 0x20 0}
	{StrAvgM 0xa0 0x8a 0x80 0x80 0x80 0x4d 0xa0 0x80 0x80 0x80 0x85 0x20 0}
	{StrAvgX 0xa0 0x8a 0x80 0x80 0x80 0x58 0xa0 0x80 0x80 0x80 0x85 0x20 0}

	{StrBenchChoice "NF,Z,A,I,T,EXIT"}
	{StrEditFlags "FLG"}
	{StrEditSerialNumber "SNUM"}


	{@0xbd0 150 120 150 120 1
		100 180 0 0 0
		50 20 50 20 0
		250 30 250 30 1
		100 70 100 70 0
		250 70 250 70 0
		250 180 250 180 0
		250 20 250 40 1
		11 180 0 0 0
	}

}

# Following are german model strings
set	strings1bg {
	{FindChar_vals " ()*+-/0123456789<=>@ABCDEFGHIJKLMNOPQRSTUVWXYZ"}
	{FindChar_ValLen 47 0}
	{FindChar_numbers "0123456789"}
	{FindChar_NumberLen 10 0}
	{DoMenu_FactoryConfig "\001WAIT"}
	{mnuCamFunctionOff "AUS"}
	{mnuCamFunctionExit "ENDE"}
	{mnuCamFunctionCAM "GEWE %s"}
	{mnuEmrFunctionEMR "AUFB %s"}
	{chooseBatchAll "ALL"}
	{chooseBatchOff "AUS"}
	{chooseBatchNone "NIEN"}
	{chooseBatchFmt "%c%c"}
	{mnuCalComplete "KOMPLETT"}
	{mnuRows "ROWS"}
	{mnuCols "COLS"}
	{mnuMemoryUsed "Used %u of %u"}
	{mnuBatchInfo "%c %s %d Readings %d Rows %d Cols"}
	{mnuLowLimit "LOWL"}
	{mnuHighLimit "HI L"}
	{StrcalZero "\001BASIS"}
	{StrCalMoreSel "MEHR/SEL"}
	{StrCalNonZero "\001MIT STANDARD"}
	{StrCalAdjust "JUST"}
	{StrBatchFull "BATCH FULL"}
	{StrMemDash "%s-"}
	{StrMemString " %c%d  "}
	{StrMemString2 "%d   "}
	{StrMemString3 "\303%d   "}
	{StrMemString4 "\301R%d   "}
	{StrMemFull  "MEM FULL"}
	{StrTemplate "TEMPLATE"}
	{StrNoneExit "AUS"}
	{StrTouch "\001Touch"}
	{StrTooSmall "Too Small"}
	{StrLift "\001Lift"}
	{StrDone "\001DONE"}
	{StrLiftProbe "\001Lift Probe"}
    {StrProbeEOM "AUF "}
    {StrProbeMDone "KOMPLET.G"}
    {StrMenuMESH "GEWE %s"}
	{StrMenuDCALPrompt "FOLIE,GEWEBE"}
    {StrProbeCal "KAL "}
    {StrProcCamMin "MIN "}
    {StrProcCamMax "MAX "}

	{nf1 0x40 0x80 0x0 0x0}
	{nf100 0x43 0xc8 0 0}
	{nf4 0x41 0x80 0 0}
	{nf20 0x42 0xa0 0 0}
	{nfmicrons 0x41 0x22 0x8f 0x5e}
	{nfozunits 0x40 0x18 0x2c 0xb8}
	{StrFactCalFerrous "METALL,ENDE"}
	{StrAvgE 0xa0 0x8a 0x80 0x80 0x80 0x45 0xa0 0x80 0x80 0x80 0x85 0x20 0}
	{StrAvgM 0xa0 0x8a 0x80 0x80 0x80 0x47 0xa0 0x80 0x80 0x80 0x85 0x20 0}
	{StrAvgX 0xa0 0x8a 0x80 0x80 0x80 0x58 0xa0 0x80 0x80 0x80 0x85 0x20 0}

	{StrBenchChoice "NF,Z,A,I,T,EXIT"}
	{StrEditFlags "FLG"}
	{StrEditSerialNumber "SNUM"}

	{@0xbd0 150 120 150 120 1
		100 180 0 0 0
		50 20 50 20 0
		250 30 250 30 1
		100 70 100 70 0
		250 70 250 70 0
		250 180 250 180 0
		250 20 250 40 1
		11 180 0 0 0
	}

}


set	constants {
	mnuMemoryOffSel mnuMemoryClearSel mnuCamSel
}

set	basefile	"promnu"
set	outfile		"promnu"
set 	baseAddress 0x500
set		maxAddress 0x7ff


if {$argc > 1 && $argv(1) == "1"} {
	set baseAddress 0x7500
	set outfile "promnu1"
	set RootMenu $RootMenu1
	set strings $strings1
	set SysMenu $SysMenu1
	set		maxAddress 0x77ff
}

if {$argc > 1 && $argv(1) == "b"} {
	set baseAddress 0x500
	set outfile "promnu1b"
	set RootMenu $RootMenu1
	set strings $strings1b
	set SysMenu $SysMenu1
}

if {$argc > 1 && $argv(1) == "e"} {
	set baseAddress 0x500
	set outfile "promnu1e"
	set RootMenu $RootMenu1E
	set strings $strings1b
	set SysMenu $SysMenu1
}

if {$argc > 1 && $argv(1) == "n"} {
	set baseAddress 0x7500
	set outfile "promnu1n"
	set RootMenu $RootMenu1E
	set strings $strings1
	set SysMenu $SysMenu1
	set		maxAddress 0x77ff
}

if {$argc > 1 && $argv(1) == "s"} {
	set baseAddress 0x500
	set outfile "promnu1s"
	set RootMenu $RootMenu1ES
	set strings $strings1b
	set SysMenu $SysMenu1
}

if {$argc > 1 && $argv(1) == "g"} {
	set baseAddress 0x500
	set outfile "promnu1g"
	set RootMenu $RootMenu1EG
	set strings $strings1bg
	set SysMenu $SysMenu1
}


set	constant	10
set	sizeofMenu	8
set	menuPtr		$baseAddress

set	memarray(0)	0

proc	hexAddress	{addr} {
#	return [format "&H%04x" $addr]
	return [format "\$%x" $addr]
}

proc	XhexAddress	{addr} {
	return [format "%04x" $addr]
}

proc	hexValue	{val} {
#	set low [expr "$val & 0xff"]
#	set high [expr "$val / 256"]
#	return [format "\$%02x,\$%02x" $low $high]
#	return [format "&H%x" $val]
	return [format "\$%x" $val]
}


proc	gMenu	{mnu} {
	global	$mnu
	global	constantsFile menuPtr sizeofMenu asmFile
	global memarray

	if ![info exists $mnu] {	# its gonna have to be a constant
		global constant
		set res $constant
		if {$mnu == "NONE"} {
			return "0xffff"
		}
		puts $constantsFile "#define $mnu $res"
		incr constant
		return $res
	}

	if [info exists memarray($mnu)] {
		return $memarray($mnu)
	}
	set menu [set $mnu]
					# get a copy of the menu
	set mnuptr $menuPtr

	global memarray($mnu)
	set memarray($mnu) $mnuptr
					# our offset

	set	str 	""
	set	l 	0
	foreach el [lindex $menu 2] {	# process each menu
		set s [lindex $el 0]
		incr l [string length $s]
		incr l 3
	}

	incr 	l 2

#	puts $asmFile "* $mnu Length $l [hexAddress $l]"
	incr	menuPtr	$l
					# total length of array

	puts $asmFile " * --------- Start of Menu $mnu -----------*"
#	puts $asmFile " org [hexAddress $mnuptr]"

#	puts $asmFile "* $mnu	equ	*"
	set	cnt	1
	set offset $mnuptr
	foreach el [lindex $menu 2] {	# process each menu
		set s [lindex $el 0]
		set m [lindex $el 1]
		set res [gMenu $m]

		puts $asmFile " org [hexAddress $offset]"
#		puts $asmFile "* ${mnu}_${cnt}     equ	*"
		puts $asmFile " DRW [hexValue $res]"
        set c [string index $s 0]
        scan $c "%c" c
        if {$c == 129} {
         	puts $asmFile " DB \$81"
            set s [string range $s 1 end]
        }
        puts $asmFile " STRZ '$s'"
		set  xoffset [expr "$offset + 2"]
		puts $constantsFile "#define ${m}_String ((char*)0x[XhexAddress $xoffset])"
		incr cnt
		incr offset [string length $s]
		incr offset 3
	}
	puts $asmFile " org [hexAddress $offset]"
	puts $asmFile " DW 0"
	puts $asmFile " * ------  End of Menu --------- *"
	return $mnuptr
}

proc	gStrings	{mnu} {
	global	$mnu
	global	constantsFile menuPtr sizeofMenu asmFile
	global memarray

	set menu [set $mnu]
					# get a copy of the menu
	foreach el $menu {	# process each menu
		set token [lindex $el 0]
		if {[string index $token 0] == "@"} {
			puts $asmFile " org [hexAddress [string range $token 1 end]]"
			set noskip 1
		} else  {
			puts $constantsFile "#define $token ((char*)0x[XhexAddress $menuPtr])"
			puts $asmFile " org [hexAddress $menuPtr]"
			set noskip 0
		}

		if {[llength $el] > 2} {
		 	foreach xl [lrange $el 1 end] {
			 	puts $asmFile " DB [hexValue $xl]"
				if !$noskip {
					incr menuPtr
				}
			}
		} else {
			set s [lindex $el 1]
			set l [string length $s]
			incr l 1

			puts $asmFile " STRZ '$s'"
			incr menuPtr $l
		}
	}
}


proc	Make {} {
	global	constantsFile basefile outfile asmFile
	global baseAddress constants menuPtr maxAddress

	set constantsFile   [open "$outfile.h" w]

	puts $constantsFile "#define	MENUBASE $baseAddress"

	set asmFile [open "$outfile.asm" w]

	gMenu RootMenu

	puts $constantsFile "#define	SYSMENU $menuPtr"
	gMenu SysMenu

	foreach el $constants {
		gMenu $el
	}
	gStrings strings
	close $constantsFile
	close $asmFile
    if {$menuPtr > $maxAddress} {
    	echo "WARNING !!!! Offset Written ([XhexAddress $menuPtr]) is greater than Max ($maxAddress)\r\n"
    }
    echo "Max Address Written is [XhexAddress $menuPtr]  of $maxAddress"
    set sizFile [open "$outfile.siz" w]
    puts $sizFile "$menuPtr"
    close $sizFile
}


Make
quit



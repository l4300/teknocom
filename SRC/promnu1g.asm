 * --------- Start of Menu RootMenu -----------*
 * --------- Start of Menu CalibrateMenuG -----------*
 org $534
 DRW $500
 STRZ 'e KAL'
 org $53c
 DRW $a
 STRZ '2-P KAL'
 org $546
 DRW $b
 STRZ '1-P KAL'
 org $550
 DRW $c
 STRZ 'GRUNDKAL'
 org $55b
 DW 0
 * ------  End of Menu --------- *
 org $500
 DRW $534
 STRZ 'KALIB'
 org $508
 DRW $d
 STRZ 'AUFBAU'
 org $511
 DRW $e
 STRZ 'GEWEBE'
 * --------- Start of Menu UnitsMenuG -----------*
 org $55d
 DRW $500
 STRZ 'e EINHEIT'
 org $569
 DRW $f
 STRZ 'US MILS'
 org $573
 DRW $10
 STRZ 'MIKRON'
 org $57c
 DW 0
 * ------  End of Menu --------- *
 org $51a
 DRW $55d
 STRZ 'EINHEIT'
 * --------- Start of Menu ConfigMenu1EG -----------*
 org $57e
 DRW $500
 STRZ 'e KON'
 org $586
 DRW $11
 STRZ 'GRUNDEINSTELLUNG'
 org $599
 DRW $12
 STRZ 'SERIEN NUMMER'
 org $5a9
 DRW $13
 STRZ 'Version'
 org $5b3
 DW 0
 * ------  End of Menu --------- *
 org $524
 DRW $57e
 STRZ 'KONFIG'
 org $52d
 DRW $ffff
 STRZ 'e '
 org $532
 DW 0
 * ------  End of Menu --------- *
 * --------- Start of Menu SysMenu -----------*
 org $5b5
 DRW $14
 STRZ 'Flags'
 org $5bd
 DRW $15
 STRZ 'SNumber'
 org $5c7
 DRW $16
 STRZ 'Ferrous'
 org $5d1
 DRW $17
 STRZ 'NonFerrous'
 org $5de
 DRW $18
 STRZ 'Bench'
 org $5e6
 DRW $500
 STRZ 'Exit'
 org $5ed
 DW 0
 * ------  End of Menu --------- *
 org $5ef
 STRZ ' ()*+-/0123456789<=>@ABCDEFGHIJKLMNOPQRSTUVWXYZ'
 org $61f
 DB $2f
 DB $0
 org $621
 STRZ '0123456789'
 org $62c
 DB $a
 DB $0
 org $62e
 STRZ 'WAIT'
 org $634
 STRZ 'AUS'
 org $638
 STRZ 'ENDE'
 org $63d
 STRZ 'GEWE %s'
 org $645
 STRZ 'AUFB %s'
 org $64d
 STRZ 'ALL'
 org $651
 STRZ 'AUS'
 org $655
 STRZ 'NIEN'
 org $65a
 STRZ '%c%c'
 org $65f
 STRZ 'KOMPLETT'
 org $668
 STRZ 'ROWS'
 org $66d
 STRZ 'COLS'
 org $672
 STRZ 'Used %u of %u'
 org $680
 STRZ '%c %s %d Readings %d Rows %d Cols'
 org $6a2
 STRZ 'LOWL'
 org $6a7
 STRZ 'HI L'
 org $6ac
 STRZ 'BASIS'
 org $6b3
 STRZ 'MEHR/SEL'
 org $6bc
 STRZ 'MIT STANDARD'
 org $6ca
 STRZ 'JUST'
 org $6cf
 STRZ 'BATCH FULL'
 org $6da
 STRZ '%s-'
 org $6de
 STRZ ' %c%d  '
 org $6e6
 STRZ '%d   '
 org $6ec
 STRZ '�%d   '
 org $6f3
 STRZ '�R%d   '
 org $6fb
 STRZ 'MEM FULL'
 org $704
 STRZ 'TEMPLATE'
 org $70d
 STRZ 'AUS'
 org $711
 STRZ 'Touch'
 org $718
 STRZ 'Too Small'
 org $722
 STRZ 'Lift'
 org $728
 STRZ 'DONE'
 org $72e
 STRZ 'Lift Probe'
 org $73a
 STRZ 'AUF '
 org $73f
 STRZ 'KOMPLET.G'
 org $749
 STRZ 'GEWE %s'
 org $751
 STRZ 'FOLIE,GEWEBE'
 org $75e
 STRZ 'KAL '
 org $763
 STRZ 'MIN '
 org $768
 STRZ 'MAX '
 org $76d
 DB $40
 DB $80
 DB $0
 DB $0
 org $771
 DB $43
 DB $c8
 DB $0
 DB $0
 org $775
 DB $41
 DB $80
 DB $0
 DB $0
 org $779
 DB $42
 DB $a0
 DB $0
 DB $0
 org $77d
 DB $41
 DB $22
 DB $8f
 DB $5e
 org $781
 DB $40
 DB $18
 DB $2c
 DB $b8
 org $785
 STRZ 'METALL,ENDE'
 org $791
 DB $a0
 DB $8a
 DB $80
 DB $80
 DB $80
 DB $45
 DB $a0
 DB $80
 DB $80
 DB $80
 DB $85
 DB $20
 DB $0
 org $79e
 DB $a0
 DB $8a
 DB $80
 DB $80
 DB $80
 DB $47
 DB $a0
 DB $80
 DB $80
 DB $80
 DB $85
 DB $20
 DB $0
 org $7ab
 DB $a0
 DB $8a
 DB $80
 DB $80
 DB $80
 DB $58
 DB $a0
 DB $80
 DB $80
 DB $80
 DB $85
 DB $20
 DB $0
 org $7b8
 STRZ 'NF,Z,A,I,T,EXIT'
 org $7c8
 STRZ 'FLG'
 org $7cc
 STRZ 'SNUM'
 org $bd0
 DB $96
 DB $78
 DB $96
 DB $78
 DB $1
 DB $64
 DB $b4
 DB $0
 DB $0
 DB $0
 DB $32
 DB $14
 DB $32
 DB $14
 DB $0
 DB $fa
 DB $1e
 DB $fa
 DB $1e
 DB $1
 DB $64
 DB $46
 DB $64
 DB $46
 DB $0
 DB $fa
 DB $46
 DB $fa
 DB $46
 DB $0
 DB $fa
 DB $b4
 DB $fa
 DB $b4
 DB $0
 DB $fa
 DB $14
 DB $fa
 DB $28
 DB $1
 DB $b
 DB $b4
 DB $0
 DB $0
 DB $0

/* #define	debugdensity 1 */
/* promem.c - Protek Memory Functions */
/* #define	DEBUGT	1	*/
/* #define	DEBUGS	1	*/  /* check gsX in skip memory */
/*#define	DEBUGQ	1     */	/* debug questions */

/*
 * Copyright (c) 1996-97, Murkworks, Inc.
 * $Log: /Teknocom/Protek Gauge/src/promem.c $
 * 
 * 11    10/22/98 4:52p Bkc
 * Version 2.4
 * 
 * 10    3/23/98 4:14p Bkc
 * Version 2.3
 * 
 * 9     2/26/98 10:29p Bkc
 * 
 * 8     2/19/98 10:00p Bkc
 * Added new sounds, fixed limit EditValue problem in proprobe not
 * returning lastreading.
 * 
 * 7     2/10/98 3:17p Bkc
 * Reversed DOTEMPLATE check in promem.c to make CTI_H = model 3 (with
 * templates)
 * 
 * 6     7/11/97 4:44p Bkc
 * The zero offset factor was not being scaled by the 2nd point factor.
 * Added scaling to zero offset after 2nd point ratio is calculated. Moved
 * sintoflt() to proprobe.c
 *
 * $NoKeywords: $
 */


unsigned int
CSum(data, count)			/* return checksum */
	unsigned int *data;
	int	count;
{
	unsigned int	sum;
	unsigned char	i;

	gsX = 0;
	gsY = count;
	gsA = 0;
	while(gsY > 0) {
		gsX += data[gsA];
		gsA++;
		gsY--;
	}
	return(gsX);
}

#ifdef	dosum

unsigned int
SystemCSum()
{
	return(CSum(SysConstants, sizeof(SysConstants)));
}

SystemReCSum()				/* reset the control block csum */
{
	SysConstants.scCsum = 0;
	SysConstants.scCsum = ~SystemCSum() + 1;
}

unsigned int
UserCSum()
{
	return(CSum(UserConstants, sizeof(UserConstants)));
}

UserReCSum()				/* reset the control block csum */
{
	UserConstants.ucCsum = 0;
	UserConstants.ucCsum = ~UserCSum() + 1;
}


UserReset()				/* erase all user data */
{
	memset(UserConstants,0, sizeof(UserConstants));
	UserReCSum();
}
#endif

#ifdef	DOMEMORY
FreeDataBlockChain(db)
	struct _datablock *db;
{

	gsX = db;

	while(gsX && gsX != 0xffff) {
		gsY = *((unsigned int *) gsX);
		*((unsigned int *) gsX) = 0;
		gsX = gsY;
	}
#ifdef	OLD
	struct _datablock *tmp;

	while(db && db != 0xffff) {
		tmp = db->dbNext;
		db->dbNext = 0;
		db = tmp;
	}
#endif
}

FreeMemSlot(slot)
	unsigned char slot;
{
	gsA = slot;

	if(gsA >= MAXSLOTS)
		return;

	FreeDataBlockChain(Batches[gsA].biDataBlockAddress);
	FreeDataBlockChain(Batches[gsA].biBatchDescriptions);
	memset(Batches + (sizeof(struct _batchInfo) * gsA),0, sizeof(struct _batchInfo));
}

FreeTemplateSlot(slot)
	unsigned char slot;
{
	gsA = slot;
	FreeDataBlockChain(Templates[gsA].tiRowBlockAddress);
	FreeDataBlockChain(Templates[gsA].tiColumnBlockAddress);
	gsZ = Templates[gsA].tiBlockDescriptions;
	while(gsZ && gsZ != 0xffff) {
		for(gsB = 0; gsB < 3; gsB++)
			FreeDataBlockChain(*( (unsigned int *) (CharP(gsZ) + 10*gsB + 10)));

		gsZ = *((unsigned int *) gsZ);		
	}
	memset(Templates + gsA * sizeof(struct _templateInfo), 0, sizeof(struct _templateInfo));
}

unsigned int
GetUsedMem()
{
	gsX = 0;
	for(gsY=0; gsY < MAXBLOCKS; gsY++) {
		if(DataBlocks[gsY].dbNext)
			gsX += DBMAXVALUES;
	}
	return(gsX);
}
#endif	/* endif do memory */

EraseAllMemory(mode)
	unsigned char mode;		/* non zero for just datablocks and batches, else all */
{
#ifdef	DOMEMORY
	gsX = Templates-1;
	if(mode) 		/* this is wrong, causes templates to screw up*/
		gsX = DataBlocks-1;
#ifdef	DS2252
	while(gsX < 0xfffe)
#else
	while(gsX < 0x7ffe)
#endif
		*CharP(++gsX) = 0;

	gsX = Batches-1;
	while(gsX < Sounds-1)
		*CharP(++gsX) = 0;

#endif	/* do memory */
}

#ifdef	DOMEMORY
struct _datablock *
GetFreeDataBlock()			/* returns pointer to next free memblock */
{
	unsigned char _gsA;
	unsigned int _gsZ;
	unsigned char *ptr;
	_gsA = gsA;
	_gsZ = gsZ;
	ptr = NULL;

	for(gsZ = 0; gsZ < MAXBLOCKS; gsZ++) {
		if(!DataBlocks[gsZ].dbNext) {
			DataBlocks[gsZ].dbNext = 0xffff;
			for(gsA = 0; gsA < DBMAXVALUES; gsA++)
				DataBlocks[gsZ].dbValues[gsA] = INFINITY;
			ptr = &DataBlocks[gsZ].dbNext;
			break;
		}
	}
	gsA = _gsA;
	gsZ = _gsZ;
	return(ptr);
}

unsigned int *
GetDataAddress(offset, alloc,database)			/* return the address of a mem save area */
	unsigned int 	offset;				/* which vector */
	unsigned char	alloc;				/* true if we should allocate memory */
	struct _datablock	**database;
{
	struct _datablock	*datablock;
	struct _datablock	*tmp;

	if(offset < 0)
		return(NULL);
	datablock = *database;

lookagain:;
	while(offset >= DBMAXVALUES && datablock) {		/* find the datablock */
		if(datablock->dbNext == 0xffff) {		/* end of array */
			break;
		}
		datablock = datablock->dbNext;
		offset -= DBMAXVALUES;
	}
	/* offset = 0 or < datablock size */
	if(!datablock || ((datablock->dbNext == 0xffff) && offset >= DBMAXVALUES)) {
		if(!alloc || !(tmp = GetFreeDataBlock()))
			return(NULL);
/*		tmp->dbNext = 0xffff;	     */	/* last data block */
		if(datablock)
			datablock->dbNext = tmp;
		else
			*database = tmp;
		datablock = tmp;
		if(offset >= DBMAXVALUES) {
			offset -= DBMAXVALUES;
			goto lookagain;
		}
	}
	return(&datablock->dbValues[offset]);
}

#ifdef	DOTEMPLATES
char *
GetTemplateTag(slot,entry,getcol)		/* get the row name or null */
	char	slot;
	unsigned int entry;
	char	getcol;				/* true if I want the column */
{
	char	*ptr;
	gsA = slot;

	if(!(gsB = Batches[gsA].biInfo.sbTemplate) || !Batches[gsA].biInfo.sbRows)
		return(NULL);

	gsB--;		/* adjust offset into templates */

	if(getcol) {
		gsX = entry/Batches[gsA].biInfo.sbRows;
		ptr = GetDataAddress(gsX*5,0,&Templates[gsB].tiColumnBlockAddress);
	} else {
		gsX = entry % Batches[gsA].biInfo.sbRows;
		ptr = GetDataAddress(gsX*5,0,&Templates[gsB].tiRowBlockAddress);
	}

	return(ptr);
}


static char TQSel;	/* which memory batch */
static char TAindex;	/* which answer they're working on */
static unsigned char TQindex;		/* which question they're working with */
static unsigned int 	*TQanswers;

char *
mnuTQuestion(index)		/* *index is selecting a question in the current mem template */
	int *index;
{
	gsA = TQSel;
	gsB = TQindex;
	gsC = Batches[gsA].biInfo.sbTemplate -1;
	gsZ = *index;

	if(gsB & 0x80) {	/* first time through */
		gsZ = gsB & 0x7f;
		TQindex = gsZ;
	} else if (gsZ == 0xffff) {
	 	for(gsD = 0; gsD < 16; gsD++) {
			gsX = 	GetDataAddress(gsD*5,0,&Templates[gsC].tiBlockDescriptions);
			if(!gsX || !*CharP(gsX))
				break;
		}
		gsZ = gsD-1;
	}

	/* try to select this entry, possible adjust *index */
	gsX = GetDataAddress(gsZ*5,0,&Templates[gsC].tiBlockDescriptions);
#ifdef	DEBUGQ
printf("Tq idex %d res %u (%s)\n",gsZ,gsX,gsX ? gsX : "none");
#endif
	if(!gsX || !*CharP(gsX))
		return(NULL);

	*index = gsZ;
	return(gsX);
}

char *
mnuTAnswer(index)		/* *index is selecting a answer in the current mem template */
	int *index;
{
	/* try to select this entry, possible adjust *index */
	gsZ = *index;
	if(TAindex & 0x80) {
		gsZ = TAindex & 0x7f;
		TAindex = gsZ;
	} else if (gsZ == 0xffff) {
	 	for(gsC = 0; gsC < 16; gsC++) {
			gsX = 	GetDataAddress(gsC*5,0,&TQanswers);
			if(!gsX || !*CharP(gsX))
				break;
		}
		gsZ = gsC-1;
	}
	gsX = GetDataAddress(gsZ*5,0,&TQanswers);
#ifdef	DEBUGQ
printf("TA idex %d res %u (%s)\n",gsZ,gsX,gsX ? gsX : "none");
#endif

	if(!gsX || !*CharP(gsX))
		return(NULL);

	*index = gsZ; 
	return(gsX);
}



int
TemplateQuestions(sel)
	char sel;		/* which batch */
{
	gsA = sel;
	gsB = Batches[gsA].biInfo.sbTemplate-1;

	if(!Templates[gsB].tiBlockDescriptions)
		return(-1);		/* there aren't any questions */

	TQSel = gsA;
	TQindex = 0;

	while(1) {			/* allow them to select question to edit */
		TQindex |= 0x80;

		gsX = StringSelection(NULL, &mnuTQuestion);
		if(gsX == 0xffff)
			return(-2);		/* cancelled out */		

		TQindex = gsX;		

		gsA = sel;
		gsB = Batches[gsA].biInfo.sbTemplate-1;
		gsY = GetDataAddress(gsX*5,0,&Templates[gsB].tiBlockDescriptions);
		if(!gsY)
			continue;

		gsY += 8;	/* get address of answers */
#ifdef	DEBUGQ
printf("Tans gsX %d gsY %u\n",gsX,gsY);
#endif

		if(!*((unsigned int *) gsY)) {
			Sound(SOUND_ERROR);
			continue;
		}
		TQanswers = *((unsigned int *) gsY);

		gsY = GetDataAddress(gsX,1,&Batches[gsA].biBatchDescriptions);
		if(!gsY)
			break;

		gsY = *((unsigned int *) gsY);

		for(gsC = 0; gsC < 16; gsC++) {
		 	gsX = GetDataAddress(gsC*5, 0, &TQanswers);
			if(!gsX)
				break;

/* printf("gsY %u gsX %u gsc %d\n",gsY,gsX, gsC);  */

			if(gsX == gsY)
				break;
		}

		if(gsX && gsC != 16) 
			TAindex = gsC| 0x80;
		else	
			TAindex = 0;

		gsX = StringSelection(NULL, &mnuTAnswer);
		if(gsX == 0xffff)
			continue;

		/* save answer */
		gsZ = GetDataAddress(gsX*5,0,&TQanswers);
		if(!gsZ)	  	/* not likely */
			continue;

		gsA = sel;
		gsB = Batches[gsA].biInfo.sbTemplate-1;

		gsY = GetDataAddress(TQindex,1,&Batches[gsA].biBatchDescriptions);
		if(!gsY)
			break;

		*((unsigned int *) gsY) = gsZ;

		gsX = TQindex;
		gsX++;

		gsY = GetDataAddress(gsX*5,0,&Templates[gsB].tiBlockDescriptions);
		if(!gsY || !*CharP(gsY))
			break;		/* that was the last question */

		TQindex = gsX;
	}
	return(0);
}

char *
MakeTemplateTag()		/* return current template string in gString*/
{
	char 	*ptr,*ptc;
#define	ptr	gsZ
#define	ptc	gsY

	if(!(UserConstants.srfFlags & SRF_MEMORYMODE))
		return(NULL);

	gsE = UserConstants.ucActiveMemorySlot;
	ptr = GetTemplateTag(gsE,Batches[gsE].biEntryPointer,0);
	ptc = GetTemplateTag(gsE,Batches[gsE].biEntryPointer,1);
	if((!ptr || !*CharP(ptr)) && (!ptc || !*CharP(ptc)))
		return(NULL);

	gString[0] = 0;

	if(ptc)
		sprintf(gString,StrMemDash,CharP(ptc));
	if(ptr)
		strcat(gString,CharP(ptr));
	else {
		gsB = *(CharP(ptc) + 8);		/* get row limit */
		gsC = Batches[gsE].biEntryPointer % Batches[gsE].biInfo.sbRows;
		gsC++;

#ifdef	DEBUGT
printf("ptr %s ", ptr ? ptr : "none");
printf("ptc %s ",ptc ? ptc : "none");
printf("gsb %d gsc%d\n",gsB, gsC);
#endif
		if(!gsB)
			gsB = Batches[gsE].biInfo.sbRows;
		sprintf(gString+16,"%d/%d",gsC,gsB);
		strcat(gString,gString+16);
	}
	return(gString);
#undef	ptr
#undef	ptc
}

#else	/* else not do templates */
char *
GetTemplateTag(slot,entry,getcol)		/* get the row name or null */
	char	slot;
	unsigned int entry;
	char	getcol;				/* true if I want the column */
{
	return(NULL);
}

char *
MakeTemplateTag()		
{
 	return(NULL);
}

#endif	/* endif DO TEMPLATES */

char *
_GMemoryString(str,slot,offset,bits)		/* get the current memory string */
	char	*str;
	unsigned char slot;
	unsigned int offset;
	unsigned int bits;
{
	gsE = slot;

	if((bits & SRF_CAMMODE) && (bits & SRF_NOTCAMLAST)) {
		gsE = ' ';
#ifdef	DOEMR
		if(UserConstants.srfFlags2 & SRF2_EMRON) {
			if(UserConstants.srfFlags2 & SRF2_EMRE)
				gsE = 'E';
			else {
#ifdef	GERMAN
				gsE = 'G';
#else
				gsE = 'M';
#endif                
            }
		}
#endif
		sprintf(str,StrMemString,gsE,UserConstants.ucCamLimit-UserConstants.ucCamCount);
		goto fourchar;
	} else {
		if(!(UserConstants.srfFlags & SRF_MEMORYMODE))
			return(NULL);
		str[0] = gsE + 'A';
		if(!Batches[gsE].biInfo.sbRows) {
			sprintf(str+1,StrMemString2,(offset % 1000)+1/* - (((bits & SRF_DONE) && offset) ? 1 : 0) */);
fourchar:;
			str[4] = 0;
		} else {
			gsY = offset % Batches[gsE].biInfo.sbRows; /* row # */

			if((!gsY) /*&& (bits & SRF_DONE) */) {	/* if first row show A flash*/
				sprintf(str+1,StrMemString3,offset/Batches[gsE].biInfo.sbRows+1);
			} else {
/*				if(!(bits & SRF_DONE))
					gsY++;
				else if(!gsY)
					gsY = Batches[gsE].biInfo.sbRows;
*/
				sprintf(str,StrMemString4,gsY+1);
			}
		}
	}
	str[5] = 0;
	return(str);
}


int
NormalizePointer(dir) 
	char	dir;		/* 1 means forward, 0 means backwards, otherwise just setup bitflags */
{				/* adjust current batch memory pointer for
				   row limits, if any */
	unsigned char *ptr;

	if(!(UserConstants.srfFlags & SRF_MEMORYMODE)) {
		clrBBit(BITS_BLOCK2|BBITS_BLOCK2);
		return(NULL);
	}


#define	row	gsD
#define	column	gsC

	gsA = UserConstants.ucActiveMemorySlot;
	gsY = Batches[gsA].biEntryPointer;

	ptr = GetTemplateTag(gsA,gsY,1);

	row = gsY % Batches[gsA].biInfo.sbRows;
	column = gsY / Batches[gsA].biInfo.sbRows;

#ifdef	MODEL3
	if(column+1 == Batches[gsA].biInfo.sbColumns) {
		setBBit(BITS_BLOCK2);
		if(row+2 >= Batches[gsA].biInfo.sbRows & Batches[gsA].biInfo.sbRows > 1)
			setBBit(BBITS_BLOCK2);
		else
			clrBBit(BBITS_BLOCK2);
	} else
		clrBBit(BITS_BLOCK2|BBITS_BLOCK2);
#else
		clrBBit(BITS_BLOCK2|BBITS_BLOCK2);
#endif

	if(dir > 1)
		return;

	if(!ptr)
		return;

	gsE = *(ptr + 8) & 0xff;	/* gsE has row limit */
	if(!gsE)
		return;
	else {
		gsE;

/*		row = gsY % Batches[gsA].biInfo.sbRows;
		column = gsY / Batches[gsA].biInfo.sbRows;
*/
		if(row < gsE)
			return;

		/* else we're over the limit */
		if(dir) {	/* going forward increment column */
			column++;
			row = 0;
		} else  {
			row = gsE-1;
		}
		gsY = (column * Batches[gsA].biInfo.sbRows) + row;
		Batches[gsA].biEntryPointer = gsY;
#undef	row
#undef	column
	}
}

int
StoreExtraBatchInfo()	/* assumes gsA has pointer to current batch, save
				   limits, cam mode to batch info */
{

	Batches[gsA].biInfo.sbCamMode = UserConstants.ucCamLimit;
	if(!(UserConstants.srfFlags & SRF_LIMITMODE))
		return;

	Batches[gsA].biInfo.sbLimitLow = UserConstants.ucLimitLow;
	Batches[gsA].biInfo.sbLimitHigh = UserConstants.ucLimitHigh;
}


int
StoreMemory(val)	/* return true if low memory */
	unsigned int val;
{
	unsigned int *ptr;

	if(!(UserConstants.srfFlags & SRF_MEMORYMODE))
		return(0);

	gsA = UserConstants.ucActiveMemorySlot;
	ptr = GetDataAddress(Batches[gsA].biEntryPointer,1,&Batches[gsA].biDataBlockAddress);
	if(!ptr) {
		Sound(SOUND_ERROR);
		GetChoice(StrMemFull);
memfull:;
		UserConstants.srfFlags &= ~SRF_MEMORYMODE;
		return(SRF_MEMORYLOW);
	}

	*ptr = val;
	if(Batches[gsA].biInfo.sbEntryCount == 0)
		ClockPack(&Batches[gsA].biInfo.sbTimeStart[0]);
		 
	
	Batches[gsA].biEntryPointer++;
	StoreExtraBatchInfo();

	if(Batches[gsA].biEntryPointer > Batches[gsA].biInfo.sbEntryCount)
		Batches[gsA].biInfo.sbEntryCount = Batches[gsA].biEntryPointer;

/* printf("EPPNP %d\n",Batches[UserConstants.ucActiveMemorySlot].biEntryPointer); */

	NormalizePointer(1);
/* printf("EPANP %d\n",Batches[UserConstants.ucActiveMemorySlot].biEntryPointer); */

	gsX = Batches[gsA].biInfo.sbRows * Batches[gsA].biInfo.sbColumns;
	if(gsX && Batches[gsA].biEntryPointer >= gsX) {
		Sound(SOUND_ERROR);
		GetChoice(StrBatchFull);
         	goto memfull;
	}
	return(0);
}

int
ChooseTemplate()	/* choose a template, return -1 if no choice */
{
#ifndef	DOTEMPLATES
	return(-1);
#else
	char	sel;

	for(gsA = 0; gsA < MAXTEMPLATES;gsA++) {
		if(Templates[gsA].tiName[0])
			break;
	}
	if(gsA == MAXTEMPLATES)
		return(100);

	Sound(SOUND_START);
	Key = GetChoice(StrTemplate);
	if(Key == KEY_META2)
		return(100);
first:;
	gsA = 0;

	while(1) {
		for(;gsA < MAXTEMPLATES; gsA++) {
			if(Templates[gsA].tiName[0])
				break;
		}
choose:;
		sel = gsA;
		if(gsA == MAXTEMPLATES)
			Key = GetChoice(StrNoneExit);
		else
			Key = GetChoice(Templates[gsA].tiName);

		gsA = sel;

		switch(Key) {
			case	KEY_PLUS :
				if(gsA == MAXTEMPLATES)
					goto first;
				gsA++;
				break;
			case	KEY_MINUS :
				if(gsA == 0)
					gsA = MAXTEMPLATES;
				else
					while(gsA > 0) {
						if(Templates[--gsA].tiName[0])
							goto choose;
					}
				break;
			case	KEY_SELECT :
				if(gsA == MAXTEMPLATES)
					return(100);
				else
					return(gsA);
			case	KEY_META2 :
				return(102);	/* cancel */
			case	KEY_META :
				Sound(SOUND_ERROR|SOUND_NOLED);
		}	/* end switch */
	}	/* end while */
#endif
}

#endif	/* domemory */w

char *
GetMemoryString(bits)		/* get the current memory string */
	unsigned int bits;
{
static char	str[12];

#ifdef	DOMEMORY
	return(_GMemoryString(str,UserConstants.ucActiveMemorySlot,
		Batches[UserConstants.ucActiveMemorySlot].biEntryPointer,
		bits));
#else
#ifdef	DOCAM
	if((bits & SRF_CAMMODE) && (bits & SRF_NOTCAMLAST)) {
		gsE = ' ';
#ifdef	DOEMR
		if(UserConstants.srfFlags2 & SRF2_EMRON) {
			if(UserConstants.srfFlags2 & SRF2_EMRE)
				gsE = 'E';
			else {
#ifdef	GERMAN
				gsE = 'G';
#else
				gsE = 'M';
#endif
			}    
		}
#endif
		sprintf(str,StrMemString,gsE,UserConstants.ucCamLimit-UserConstants.ucCamCount);
		str[4] = 0;
		return(str);
	}
#endif	/* end if docam*/
	return(NULL);
#endif
}

#ifdef	DOMEMORY
AisLarger(a,b)
	unsigned int a;
	unsigned int b;
{
	if(a & 0x8000) {	/* a is negative */
		if(!(b & 0x8000))
			return(0);/* b positive */
		return((a & 0x7ffff) < (b & 0x7ffff));
	} else if(b & 0x8000) 	/* b is negative */
		return(1);

	return((a & 0x7ffff) > (b & 0x7ffff));
}


calcBatchStats()
{
	/* use gsC for batch pointer */

	fArray[FASTAT_CNT] = 0;

	fArray[FASTAT_MIN] = 0x7fff;
	fArray[FASTAT_MAX] = 0x8fff;

	movfpac(cons0,fpacc1);
	for(gsZ = 0; gsZ < Batches[gsC].biInfo.sbEntryCount; gsZ++) {

		gsX = GetDataAddress(gsZ,0,&Batches[gsC].biDataBlockAddress);
		if(!gsX)
			break;

		if(*((unsigned int *)gsX) == INFINITY)
			continue;

		disable();
		if(AisLarger(*((unsigned int *)gsX) , fArray[FASTAT_MAX]))
			fArray[FASTAT_MAX] = *((unsigned int *)gsX);

		if(AisLarger(fArray[FASTAT_MIN],*((unsigned int *)gsX)))
			fArray[FASTAT_MIN] = *((unsigned int *)gsX);

		enable();
		sintoflt(*((unsigned int *)gsX));

		asm " lcall fltadd";
		fArray[FASTAT_CNT]++;
	}	/* end gsZ */

	if (fArray[FASTAT_CNT] > 1) {
		intoflt(fArray[FASTAT_CNT]);
		asm " lcall fltdiv";


		pushfpac1();
		gsY = mantsgn1 ? 0x8000 : 0;
		mantsgn1 = 0;
		fArray[FASTAT_AVG] = fix16(fpacc1) | gsY;

		pop_fpac1();

		movfpac(fpacc1,gString);	/* gString keeps xavg */
		movfpac(cons0,fpacc1);		/* fpacc1 has sum of squares */


		pushfpac1();

		disable();
		for(gsZ = 0; gsZ < Batches[gsC].biInfo.sbEntryCount; gsZ++) {

			gsX = GetDataAddress(gsZ,0,&Batches[gsC].biDataBlockAddress);
			if(!gsX)
				break;

			if(*((unsigned int *)gsX) == INFINITY)
				continue;

			sintoflt(*((unsigned int *)gsX));

			movfpac(gString,fpacc1);

			asm " lcall fltsub";
			movfpac(fpacc1,fpacc2);
			asm " lcall fltmul";
			movfpac(fpacc1,fpacc2);

			pop_fpac1();
			asm " lcall fltadd";
			pushfpac1();
		}	/* end for */

		intoflt(fArray[FASTAT_CNT] - 1);
		pop_fpac1();
		asm " lcall fltdiv";
		Sqrt();
		fArray[FASTAT_DEV] = fix16(fpacc1);
		enable();
	} else {
		fArray[FASTAT_AVG] = 0;
		fArray[FASTAT_DEV] = 0;
	}

}
#endif

#ifdef	DOPRINT
PrintRepeatChars(cnt,c)
	unsigned char cnt;
	unsigned char c;
{
	for(gsE = 0; gsE < cnt; gsE++)
		printf("%c",c);

}


PrintValue(c, v)
	char *c;
	unsigned int v;
{
	StringValue(v,gString,UserConstants.srfFlags|SRF_DONE);
	printf("%s  : %s\n",c,gString);
}


printfloat(v)
	unsigned int v;
{
	movfpac(v,fpacc1);
	fltasc();
}


PrintDensity()	/* print line graph of density plot for batch gsC
		  stats must have already been calculated */
{
	// nfCountDown has options bitmap
	// 1  - Graphics, 0 - Text
	// 2  - Course, 0 - Smooth
	// 4  - Double Wide, 0 - Standard Width
	// 8  - Print stats
	// 16 - Print Data

	unsigned char _gsD, _gsC,avgtick;
	unsigned int h2;

	_gsD = gsD;
	_gsC = gsC;

	/* 20 lines x 7 dots per line, 120 pixels wide, 25 pixel offset */
	// convert min and max to float and calculate range divided by slices
#define	GA_MAXD		0
#define	GA_MAXX		4
#define	GA_DELTA	8
#define	GA_CURX		12
#define	GA_H2		16			// H/2
#define	GA_DENS		20			// current density value
#define	GA_MINX		24
#define	GA_MINDEV	28
#define	GA_MAXDEV	32		// avg - dev
#define	GA_TEXT		36		// avg + dev
#define	GA_PTOFFSET	48		// where data points for graphics mode are kept

	movfpac(cons0,gString);		// gs[0] has max density encountered


	sintoflt(fArray[FASTAT_MAX]);

	movfpac(fpacc2,gString+GA_MAXX);
	movfpac(fpacc2,fpacc1);
	sintoflt(fArray[FASTAT_MIN]);
	movfpac(fpacc2,gString+GA_CURX);
	asm " lcall fltsub";		// got difference
	pushfpac1();


	if(nfCountDown & 1)
		intoflt(120);
	else
		intoflt(10);			// number of STEPS TO PLOT

	asm " lcall fltdiv";			// x delta

	if(nfCountDown & 4) {	// double width
		movfpac(nf2,fpacc2);
		asm " lcall fltdiv";
	}

	movfpac(fpacc1,gString+GA_DELTA);
	if(nfCountDown & 1) {
		intoflt(16);
		asm " lcall fltmul";
		movfpac(gString+GA_MAXX,fpacc2);
		asm " lcall fltadd";
		movfpac(fpacc1,gString+GA_MAXX);	// add extra line
	}

	pop_fpac1();
	if(nfCountDown & 2)
		intoflt(12);			// was 16
	else
		intoflt(6);			// calculate H/2 value was 8
		
	asm " lcall fltdiv";
					// result is H

	movfpac(fpacc1,gString+GA_H2);

	h2 = fix16(fpacc1);
	if(!h2)
		h2 = 1;


	movfpac(gString+GA_DELTA,fpacc1);
	movfpac(gString+GA_MAXX,fpacc2);
	asm " lcall fltadd";
	movfpac(fpacc1, gString+GA_MAXX);

#ifdef	debugdensity
printf("GA_MAXX ");
printfloat(gString+GA_MAXX);
printf("GA_GURX ");
printfloat(gString+GA_CURX);
printf("GA_DELTA ");
printfloat(gString+GA_DELTA);
printf("H2 ");
printfloat(gString+GA_H2);
#endif



	sintoflt(fArray[FASTAT_AVG]);
	movfpac(fpacc2,fpacc1);

#ifdef	used_to_support_dev_marks
	pushfpac1();
	intoflt(fArray[FASTAT_DEV]);
	asm " lcall fltsub";
	movfpac(fpacc1,gString+GA_MINDEV);
	pop_fpac1();
	pushfpac1();
	intoflt(fArray[FASTAT_DEV]);
	asm " lcall fltadd";
	movfpac(fpacc1,gString+GA_MAXDEV);
	pop_fpac1();
#endif

	sintoflt(fArray[FASTAT_MIN]);
    movfpac(fpacc2,gString+GA_MINX);

	asm " lcall fltsub";
	movfpac(gString+GA_DELTA,fpacc2);
	asm " lcall fltdiv";
	movfpac(nf1,fpacc2);
	asm " lcall fltadd";

	gsY = fix16(fpacc1);
    avgtick = gsY;

#ifdef oldjunkfor_avg
	if(nfCountDown & 1 && gsY > 8) {	// graphics mode
		gsY &= 248;
		gsY++;
	}

	intoflt(gsY);

	movfpac(gString+GA_DELTA,fpacc1);
	asm " lcall fltmul";
	pushfpac1();

	sintoflt(fArray[FASTAT_AVG]);
	movfpac(fpacc2,fpacc1);
	pop_fpac2();
	asm " lcall fltsub";

	movfpac(fpacc1,gString+GA_MINX);
#endif

	for(gsD = 0; gsD < 2; gsD++) {	// begin master loop
#ifdef	debugdensity
printf("starting loop %d\n",gsD);
#endif


		if(gsD) {
			intoflt(fArray[FASTAT_CNT]);
			movfpac(gString+GA_H2,fpacc1);
			asm " lcall fltmul";
			movfpac(fpacc1,fpacc2);
			movfpac(gString+GA_MAXD,fpacc1);
			asm " lcall fltdiv";
			movfpac(nf100,fpacc2);
			asm " lcall fltmul";
			movfpac(nf100,fpacc2);
			asm " lcall fltmul";		// mult by 10000

			gsY = fix16(fpacc1);

			StringValue(gsY,gString+GA_TEXT,(UserConstants.srfFlags|SRF_DONE|SRF_MILS) & ~SRF_MICRONS);

			gsY >>= 1;

			StringValue(gsY,gString+GA_TEXT+10,(UserConstants.srfFlags|SRF_DONE|SRF_MILS) & ~SRF_MICRONS);

			printf("\nValue|     %s   %s\n-----|",gString+GA_TEXT+10,
				gString+GA_TEXT);
			PrintRepeatChars(18,'-');
			printf("\n");

			if(nfCountDown & 1) {
				printf("\0331");
				Putch(0);
			}

		}
		gsE = 0;
		movfpac(gString+GA_MINX,gString+GA_CURX);

		goto cmpfloat;

again:;
		// calculate density for all values at current X
#ifdef	debugdensity
printf("A\n");
#endif
		movfpac(cons0,gString+GA_DENS);
		movfpac(gString+GA_CURX,fpacc1);
		gsY = fix16(fpacc1);
		
		if(mantsgn1)
			gsY |= 0x8000;

		for(gsZ = 0; gsZ < Batches[gsC].biInfo.sbEntryCount; gsZ++) {

			gsX = GetDataAddress(gsZ,0,&Batches[gsC].biDataBlockAddress);
			if(!gsX)
				break;

			if(*((unsigned int *)gsX) == INFINITY)
				continue;



			gsX = *((unsigned int *)gsX);
			if(abs((gsX & 0x7fff) - (gsY & 0x7fff)) > h2)
				continue;

			sintoflt(gsX);
#ifdef	debugdensity_a

	movfpac(fpacc2,fpacc1);
	pushfpac2();
	printf("X=");
	fltasc();
	pop_fpac2();
#endif

			movfpac(gString+GA_CURX,fpacc1);

			asm " lcall fltsub";
			mantsgn1 = 0;
            
			movfpac(gString+GA_H2,fpacc2);
			asm " lcall fltdiv";

			// if abs fpacc1 <= 1 then its within range
#ifdef	debugdensity_a
	pushfpac1();
	fltasc();
	pop_fpac1();
#endif
			movfpac(nf1,fpacc2);
			asm " lcall absgrtr";
			asm " mov gsA,A";

			if(1 == gsA)
				continue;	// out of range, continue

			// add to density value
			// fpacc has 0 to 1, 0 is better
			movfpac(fpacc1,fpacc2);
			mantsgn2 = 0;		// its positive
			movfpac(nf1,fpacc1);
			asm " lcall fltsub";

#ifdef	debugdensity_a
	pushfpac1();
	printf("Add val ");
	fltasc();
	pop_fpac1();
#endif
			movfpac(gString+GA_DENS,fpacc2);
			asm " lcall fltadd";
			movfpac(fpacc1,gString+GA_DENS);
		}	/* end gsZ */

#ifdef	debugdensity
	printf("E %d\n",gsE);

/*	printf("at curx ");
	printfloat(gString+GA_CURX);
	printf("dens ");
	printfloat(gString+GA_DENS); */
#endif

		if(!gsD) {
			if(fpgrtr(gString+GA_DENS,gString+GA_MAXD))
				movfpac(gString+GA_DENS,gString+GA_MAXD);
		} else { 	// print out density plot here

			movfpac(gString+GA_CURX,fpacc1);

			gsY = mantsgn1 ? 0x8000 : 0;
			mantsgn1 = 0;
			gsY = fix16(fpacc1) | gsY;



			if(!(nfCountDown & 1) ||
				!((gsE+4) % 8)) {
				StringValue(gsY,gString+GA_TEXT,UserConstants.srfFlags|SRF_DONE);

				Key = '|';
#ifdef oldjunk
				if(nfCountDown & 1) {	// can be off by 8


					sintoflt(gsY);
					pushfpac2();
					sintoflt(fArray[FASTAT_AVG]);
					pop_fpac1();
					asm " lcall fltsub";
					mantsgn1 = 0;

//					intoflt(8);
//					asm " lcall fltdiv";

					movfpac(fpacc1,gString+GA_MINDEV);

					if(fpgrtr(gString+GA_DELTA,gString+GA_MINDEV) == 1)
						Key = '+';

				} else if(gsY == fArray[FASTAT_AVG])
					Key = '+';
#else
				if(avgtick == gsE)
                	Key = '+';
#endif
				if(!(nfCountDown & 1))
					printf("%s%c",gString+GA_TEXT,Key);
			}

			movfpac(gString+GA_DENS,fpacc1);

			if(nfCountDown & 1)
				intoflt(100);	// was 90
			else
				intoflt(17);	// max width of max density

			asm " lcall fltmul";
			movfpac(gString+GA_MAXD,fpacc2);
			asm " lcall fltdiv";
			gsA = fix16(fpacc1);

			if(nfCountDown & 1) {
				gsB = gsE % 8;
				*(gString+GA_PTOFFSET+gsB) = gsA;
//				printf("\033\047\002");
//				Putch(32);
//				Putch(gsA+32);
//				Putch(13);
			} else {
				for(gsB = 0; gsB < gsA; gsB++)
					Putch(' ');

				printf("*\n");
			}
		}

		// increment current x
		gsE++;

		if(gsD && !(gsE % 8) && (nfCountDown & 1)) {	// time to print out values
			printf("%s%c\033K\205",(gsE % 16) ? gString+GA_TEXT : "     ",Key);
			Putch(0);	// REQUIRED to complete K escape sequence

			gsX = gsE;
			for(gsB = 0; gsB <= 132; gsB++) {

				if(gsB < 16 && !(gsB % 2) &&
                	avgtick <= gsX && gsX-8 < avgtick)
					gsA = (1 << (gsX-avgtick));
				else

					gsA = 0;

				for(gsE = 0; gsE < 8; gsE++) {
					if(*(gString+gsE+GA_PTOFFSET) == gsB)
						gsA |= (128 >> gsE);
				}
				Putch(gsA);
			}
			gsE = gsX;
			printf("\n");
		}
		intoflt(gsE);
		movfpac(gString+GA_DELTA,fpacc1);
		asm " lcall fltmul";
		movfpac(gString+GA_MINX,fpacc2);
		asm " lcall fltadd";
		movfpac(fpacc1,gString+GA_CURX);


cmpfloat:;
#ifdef	debugdensity
printf("C\n");

	if(!gsD)
		printf("fpgrtr %d\n",fpgrtr(gString+GA_CURX,gString+GA_MAXX));
#endif

		if(255 != gsE && (fpgrtr(gString+GA_CURX,gString+GA_MAXX) != 1))
			goto again;

#ifdef junk
	movfpac(gString+GA_CURX,fpacc1);
	movfpac(gString+GA_MAXX,fpacc2);
	asm " lcall absgrtr";
	asm " mov gsA,A";

	if(1 != gsA)
		goto again;
#endif

#ifdef	debugdensity
		if(!gsD) {
			printf("max dens ");
			printfloat(gString+GA_MAXD);
			printf("at x ");
			printfloat(gString+GA_CURX);
		}
#endif
	}	// end for gsD
	// end of for loop

done:;
	if(nfCountDown & 1) {
		printf("\0331\003");	// reset printer line spacing
	}

	printf("\n");
	gsD = _gsD;
	gsC = _gsC;
}

char *ar[]={"Min","Max","Avg","Dev"};

char *
mnuStatFunction(index)
	int *index;
{
	gsB = *index;
	if(gsB > 4)
		gsB = 0;

	if(*index < 0)
		gsB = 4;

	*index = gsB;

	if(4 == gsB) {
#ifdef	NOMEMROWS
		sprintf(gString,"CNT %d",fArray[FASTAT_CNT]);
#else
		sprintf(gString,"CNT %d Rows %d Cols %d",fArray[FASTAT_CNT],
			Batches[nfCountDown].biInfo.sbRows,Batches[nfCountDown].biInfo.sbColumns);
#endif
	} else {
		StringValue(fArray[gsB],gString+16,UserConstants.srfFlags|SRF_DONE);
		gsB = *index;
		sprintf(gString,"%s %s",ar[gsB],gString+16);
	}
	return(gString);
}

MemoryStat(sel)
	unsigned char sel;
{
	nfCountDown = gsC = sel;

	calcBatchStats();
	StringSelection(NULL,&mnuStatFunction);
	lastFInfinityTime = 0;	/* force reset of fArray */
	
}

MemoryConfigPrinter()
{

	nfCountDown = 0;

	gsA = StringSelection("PRT STAT,NO STAT",NULL);
	if(gsA == 0xff)
		return;

	if(!gsA)
		nfCountDown |= POPT_STATS;

	gsA = StringSelection("PRT DENS,NO DENS",NULL);

	if(gsA == 0xff)
		return;
	if(!gsA) {
		nfCountDown |= POPT_DENSITY;

		gsA = StringSelection("TEXT,GRAPHICS",NULL);
		if(gsA == 0xff)
			return;

		if(gsA)
			nfCountDown |= POPT_GRAPHICS;

		gsA = StringSelection("SMOOTH,COURSE",NULL);
		if(gsA == 0xff)
			return;


		if(gsA)
			nfCountDown |= POPT_COURSE;

		gsA = StringSelection("NORMAL,WIDE",NULL);
		if(gsA == 0xff)
			return;

		if(gsA)
			nfCountDown |= POPT_WIDE;

	}

	gsA = StringSelection("PRT DATA,NO DATA",NULL);
	if(gsA == 0xff)
		return;


	if(!gsA)
		nfCountDown |= POPT_DATA;

	UserConstants.ucPrinterOption = nfCountDown;
    LCD_Print(mnuCalComplete);
    Sound(SOUND_START);
    clockDelay(#30);

}

MemoryPrint(sel)			/* print this selection, 0 means all */
	unsigned char sel;
{
	char *p;

    p = "\001Printing";

	LCD_Print(p);

    for(nfCountDown = 0; nfCountDown < 20; nfCountDown++) {
    	if(KEY_PLUS == GetKey(1)) {
        	Sound(SOUND_START);
        	GetChoice("SETUP");
        	MemoryConfigPrinter();
			LCD_Print(p);
            break;
        }
        IDLE();
    }

    nfCountDown = UserConstants.ucPrinterOption; /* nfCountDown is the saved printer configuration */


	if(sel) {
		gsD = gsC = sel;
		gsC--;
	} else {
		gsC = 0;
		gsD = MAXSLOTS;
	}

	for(;gsC < gsD; gsC++) {
		if(!sel && !Batches[gsC].biInfo.sbEntryCount)
			continue;

		PrintRepeatChars(PWIDTH,PSEP);
		printf("\n Coating Thickness Data\n");
		PrintRepeatChars(PWIDTH,PSEP);
		if(SysConstants.scFlags & SCFLG_NOFERROUS)
			gsE = 1;
		else if(SysConstants.scFlags & SCFLG_NONONFERROUS)
			gsE = 2;
		else
			gsE = 0;

		printf("\n\nModel: %s",
#ifdef	DOCHECKLINE
			"DC"
#else
#ifdef	DOTEMPLATES
			"CTI_H"
#else
			"CTI_M"
#endif
#endif
		);
		if(SysConstants.scFlags & SCFLG_NOFERROUS)
			printf("N");
		else if(SysConstants.scFlags & SCFLG_NONONFERROUS)
			printf("F");
		else {
#ifdef	DOCHECKLINE
			printf("FN");
#else
			printf("T");
#endif
		}
#ifdef	DOCHECKLINE
		printf("-977M\n");
#else
		printf("\n");
#endif
		printf("Ser #: %u\n",SysConstants.scSerialNumber);

		/* print the date and time .. should be time of batch, not current time */
		gsX = Batches[gsC].biInfo.sbTimeStart[0];
		UnpackDateString();
		printf("Date : %s\n",gString);
		gsX = Batches[gsC].biInfo.sbTimeStart[1];
		UnpackTimeString();
		printf("Time : %s\nBatch: %c\nID   : %s\n",gString,gsC+'A',
			Batches[gsC].biInfo.sbDescription);

		printf("Desc : ");
		PrintRepeatChars(PWIDTH-8,'_');
		printf("\nUser : ");
		PrintRepeatChars(PWIDTH-8,'_');
		printf("\n\n");


		calcBatchStats();

		if(nfCountDown & POPT_STATS) {
			PrintRepeatChars(PWIDTH,PSEP);
			printf("\nStatistics\n");
			PrintRepeatChars(PWIDTH,PSEP);

			printf("\n#    : %d\nUnits: %s\n",Batches[gsC].biInfo.sbEntryCount,
				UserConstants.srfFlags & SRF_MICRONS ? "Microns" : "Mils");

			PrintValue(ar[1], fArray[FASTAT_MAX]);
			PrintValue(ar[0], fArray[FASTAT_MIN]);
			PrintValue(ar[2], fArray[FASTAT_AVG]);
			PrintValue(ar[3], fArray[FASTAT_DEV]);
			Putch('\n');
		}

		if(nfCountDown & POPT_DENSITY) {
			PrintRepeatChars(PWIDTH,PSEP);
			printf("\n%s Density Plot\n",
				(nfCountDown & POPT_COURSE) ? "Course" : "Smooth");
			PrintRepeatChars(PWIDTH,PSEP);
			printf("\n");

			PrintDensity();
		}


		if(!(nfCountDown & POPT_DATA))
			goto doendTime;	// skip data printout

		PrintRepeatChars(PWIDTH,PSEP);
		printf("\nData\n");
		PrintRepeatChars(PWIDTH,PSEP);
		printf("\n\n");

#ifdef oldstuff
		printf("Batch %c %s Readings:%d Rows:%d Cols:%d Units:%s\n\n",
			gsC+'A',Batches[gsC].biInfo.sbDescription,Batches[gsC].biInfo.sbEntryCount,
			Batches[gsC].biInfo.sbRows,Batches[gsC].biInfo.sbColumns,
				UserConstants.srfFlags & SRF_MICRONS ? "Microns" : "Mils");
#endif

		for(gsZ = 0; gsZ < Batches[gsC].biInfo.sbEntryCount; gsZ++) {

			gsX = GetDataAddress(gsZ,0,&Batches[gsC].biDataBlockAddress);
			if(!gsX)
				break;
			else {
				StringValue(*((unsigned int *)gsX),gString,UserConstants.srfFlags|SRF_DONE);

				if(gsE = Batches[gsC].biInfo.sbRows)
					printf("%c%2d R%d ",gsC+'A',gsZ/gsE+1,(gsZ % gsE)+1);
				else
					printf("%c%3d ",gsC+'A',(gsZ % 1000)+1);


				printf("%s\n",gString);
			}
			if(GetKey(1) != KEY_NONE) {
				resetShutDown();
				Sound(SOUND_CANCEL);
				while(GetKey(1)) {
					IDLE();
					checkShutDown();
				}
				goto done;
			}
		}	/* end readings */
doendTime:;
		gsX = Batches[gsC].biInfo.sbTimeEnd[0];
		UnpackDateString();
		printf("\nDate : %s\n",gString);
		gsX = Batches[gsC].biInfo.sbTimeEnd[1];
		UnpackTimeString();
		printf("Time : %s\n <<<<    END >>>>>>\n",gString);
		printf("\n\n");
	}	/*end batch */
done:;
	resetShutDown();
	lastFInfinityTime = 0;	/* force reset of fArray */

}
#endif

MemoryTouch(sel)
	unsigned char sel;
{
	struct	_tmemHeader	*tmh;
	struct	_tmemData	*tmd;
#ifdef	DOTOUCH
	while(1) {
	 	/* get them to touch it */
		LCD_Print(StrTouch);
		resetShutDown();
		while(!GetKey(1) && !(gsA = TouchFamily()))
			checkShutDown();
		if(!gsA) {
			Sound(SOUND_CANCEL);
			return;
		}
		LCD_Print(DoMenu_FactoryConfig);
		gsY = Batches[sel].biInfo.sbEntryCount;
		/* family codes
		   4 DS1994  16 pages
		   6 DS1993  16 pages
		   8 DS1992  4 Pages
		   A DS1995  32 Pages
		   C DS1996  256 Pages
		*/
		if(gsA == 4 || gsA == 6) {
			if(gsY > 210) {
toosmall:;
				GetChoice(StrTooSmall);
				goto Xerror;
			}
			gsC = 15;
		} else if(gsA == 8) {
			if(gsY > 42)
				goto toosmall;
			gsC = 3;
		} else if(gsA == 0x0a) {
			gsC = 31;
			if(gsY > 434)
				goto toosmall;
		} else if(gsA == 0x0c) {
			gsC = 255;
			if(gsY > 3570)
				goto toosmall;
		} else {
Xerror:;
			Sound(SOUND_ERROR);
			LCD_Print(StrLift);
			while(TouchFamily())
				checkShutDown();
			continue;
		}
		/* get here, we can save it */
		tmh = gString;
		gsB = sel;
		memcpy(&tmh->tmInfo.sbEntryCount,Batches + (sizeof(struct _batchInfo) * gsB), sizeof(tmh->tmInfo));
		tmh->tmLength = 29;
		if(TouchWritePage(0))
			goto Xerror;
		gsA = 1;
		for(gsX = 0; gsA < gsC && gsX < Batches[gsB].biInfo.sbEntryCount; gsX += 14) {
			tmd = gString;
			for(gsD = 0; gsD < 14; gsD++) {
				gsY = GetDataAddress(gsX + gsD, 0, &Batches[gsB].biDataBlockAddress);
				if(!gsY)
					break;
				else
					tmd->tdData[gsD] = *((unsigned int *)gsY);
			}
			for(gsY = 0; gsY < 10; gsY++) {
				if(!TouchWritePage(gsA))
					break;
			}
			if(gsY == 10)
				goto Xerror;
			gsA++;
		}
		Sound(SOUND_START);
		sprintf(gString,"%u/%u",gsX,(gsC*14));
		LCD_Print(gString);
		resetShutDown();
		while(TouchFamily()) {
			checkShutDown();
			IDLE();
		}
		break;
	}
#endif
}

#ifdef	DOMEMORY
unsigned int
SkipMemory(dir)
	char	dir;		/* 0 dec row, 1 inc row, 2 inc col, 3 dec col, 4 nowhere */
{

	if(!(UserConstants.srfFlags & SRF_MEMORYMODE))  {
errout:;
		NormalizePointer(5);
		Sound(SOUND_ERROR);
		return(INFINITY);
	}

	gsA = UserConstants.ucActiveMemorySlot;

	if((dir == 2 || dir == 3) && !Batches[gsA].biInfo.sbColumns)
		goto errout;	/* must specify number of columns */

	gsD = gsC = Batches[gsA].biInfo.sbRows;
	gsX = Batches[gsA].biEntryPointer;
	gsC = gsX / gsC;

	if(dir == 4)
		goto curptr;

	if(dir == 2 || dir == 3) {
		if(dir == 2)
			gsC++;
		else
			gsC--;
		if(gsC == 0xff)
			gsC = Batches[gsA].biInfo.sbColumns-1;
		else if(gsC >= Batches[gsA].biInfo.sbColumns)
			gsC = 0;

		gsX = gsC * Batches[gsA].biInfo.sbRows;

		gsY = GetDataAddress(gsX,1,&Batches[gsA].biDataBlockAddress);
		if(!gsY)
			goto errout;
	} else {
		if(dir)
			gsX++;
		else 
			gsX--;

		if(gsY = Batches[gsA].biInfo.sbColumns * Batches[gsA].biInfo.sbRows) {
#ifdef	DEBUGS
printf("gsX now %x gsY %d\n",gsX, gsY);
#endif
			if(gsX == 0xffff)
				gsX = gsY;
			else if (gsY < gsX)
				gsX = 0;
#ifdef	DEBUGS
printf("2nd gsX %x\n",gsX);
#endif
		}
		gsY = GetDataAddress(gsX,0,&Batches[gsA].biDataBlockAddress);
		if(!gsY)
			goto errout;
	}
	Batches[gsA].biEntryPointer = gsX;
	NormalizePointer(dir);
	if(dir != 2 && dir != 3) {
		if(Batches[gsA].biInfo.sbColumns && 
			(Batches[gsA].biEntryPointer/Batches[gsA].biInfo.sbRows) >= Batches[gsA].biInfo.sbColumns) 
			Batches[gsA].biEntryPointer = 0;
curptr:;
		gsY = GetDataAddress(Batches[UserConstants.ucActiveMemorySlot].biEntryPointer,0,&Batches[gsA].biDataBlockAddress);
		if(!gsY)
			goto errout;
	}
	return(*((unsigned int *) gsY));
}

#endif	/* do memory */

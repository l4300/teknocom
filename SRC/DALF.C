/*
 * Dalf.c -- Thickness meter source -- Non Ferous
 * Copyright (C) 1993 MurkWorks, All Rights Reserved
*/

#define	OLDWAY	1

#include <8051bit.h>
#include <8051io.h>
#include <8051int.h>
#include <8051reg.h>

/*
#define	DOSIN	0
*/

#define	EX0	IE.0
#define	ET0	IE.1
#define	EX1	IE.2
#define	ET1	IE.3
#define	ES	IE.4
#define	EA	IE.7

#define	TF1	TCON.7
#define	TR1	TCON.6
#define	TF0	TCON.5
#define	TR0	TCON.4
#define	IE1	TCON.3
#define	IT1	TCON.2
#define	IE0	TCON.1
#define	IT0	TCON.0

#define	ERROR		65535
#define	TIMEOUT	65534

#define	CNTR_PORT	P0
#define	POWER_PORT	P2

#define	CNTR_MASK	0x07
#define	OSC1		0x08
#define	OSC2		0x10
#define	CNTR_NOT_CLR	0x20
#define	CNTR_ENABLE	0x40
#define	PROBE_NOT_REF	0x80

#define	TR1ON		setbit(TR1)
#define	TR1OFF		clrbit(TR1)

unsigned char	*copyright = "DS2250 Thickness Meter VF1.0, Copyright (C) 1993 MurkWorks";
register unsigned int  osc_warmup_delay;
register unsigned int  powerup_delay;
register unsigned int  reading_time;
register unsigned char sequence;
register unsigned int  countdown;

register unsigned int  reading1_high;
register unsigned char reading1_low;
register unsigned int  reading2_high;
register unsigned char reading2_low;
register unsigned char reading3_high;
register unsigned char reading3_low;
register unsigned int  tr1_1;
register unsigned int  msec_reload;

int	baud = 9600;

INTERRUPT(_TF0_) timer0_interrupt() {

	/* reset the timer */

	TH0 = -( msec_reload >> 8);
	TL0 = -(msec_reload & 256);
	if(sequence) {
		if(!--countdown)
			Sequence();
	}
}

int
Sequence()			/* step through the next sequence */
{
	switch(sequence) {
		case	1 :	/* power on warmup is over, turn on osc 1 */
			P2 |= OSC1;
			countdown = osc_warmup_delay;
			break;
		case 	2 :	/* osc has warmed up, reset counters and
				   begin reading */

			P2 &= ~CNTR_ENABLE;	/* disable counter */
			P2 &= ~CNTR_NOT_CLR;	/* clear counter */
			countdown = reading_time;
			P2 |= (CNTR_ENABLE|CNTR_NOT_CLR);
			TR1ON
			break;
		case	3 :	/* end of first reading */
			P2 &= ~CNTR_ENABLE;	/* turn off counter */
			TR1OFF
			tr1_1 = (TH1 << 8) + TL1;
			reading1_high = tr1_1;
			reading1_low = (CNTR_PORT & CNTR_MASK);
			if(P3 & 0x20)
				reading1_low |= 0x08;
			countdown = osc_warmup_delay;
			P2 &= ~OSC1;
			P2 |= OSC2;
			break;
		case 	4 :	/* osc2 has warmed up, reset counters and
				   begin reading */

			P2 &= ~CNTR_ENABLE;	/* disable counter */
			P2 &= ~CNTR_NOT_CLR;	/* clear counter */
			TH1 = 0;
			TL1 = 0;
			countdown = reading_time;
			P2 |= (CNTR_ENABLE|CNTR_NOT_CLR);
			TR1ON
			break;
		case	5 :	/* end of first reading */
			P2 &= ~CNTR_ENABLE;	/* turn off counter */
			TR1OFF
			tr1_1 = (TH1 << 8) + TL1;
			reading2_high = tr1_1;
			reading2_low = (CNTR_PORT & CNTR_MASK);
			if(P3 & 0x20)
				reading2_low |= 0x08;
			countdown = osc_warmup_delay;
			P2 &= ~OSC2;
			break;
		case 	6 :	/* osc have shut down, reset counters and
				   begin reading */

			P2 &= ~CNTR_ENABLE;	/* disable counter */
			P2 &= ~CNTR_NOT_CLR;	/* clear counter */
			TH1 = 0;
			TL1 = 0;
			countdown = reading_time;
			P2 |= (CNTR_ENABLE|CNTR_NOT_CLR);
			TR1ON
			break;
		case	7 :	/* end of first reading */
			P2 &= ~CNTR_ENABLE;	/* turn off counter */
			TR1OFF
			tr1_1 = (TH1 << 8) + TL1;
			reading3_high = tr1_1;
			reading3_low = (CNTR_PORT & CNTR_MASK);
			if(P3 & 0x20)
				reading3_low |= 0x08;
			countdown = 0;
			sequence = 0;
#ifdef	POWER_PORT
			POWER_PORT |= 0x01;
#else
			P3 |= 0x80;	/* turn off power */
#endif
			return;
		default:
			sequence = 0;
			return;
	}
	sequence++;
}

Take_Reading()		/* take a reading from probe or reference */
{
	unsigned int x;

	sequence = 0;

	TR1OFF
	clrbit(TR0)		/* counters off */
	clrbit(ET1)

	TH0 = -( msec_reload >> 8);
	TL0 = -(msec_reload & 256);

	TH1 = 0;
	TL1 = 0;

	TMOD = 0x51;		/* TR1 = counter, TR0 = timer */

	P2 &= ~(OSC1|OSC2);	/* no osc */
#ifdef	POWER_PORT
	POWER_PORT &= ~1;
#else
	P3 &= ~(0x80);		/* turn on power to osc */
#endif
			/* turn on timer 0 */
	countdown = powerup_delay;
	sequence = 1;
	enable();
	setbit(TR0)
	setbit(ET0)
	for(x=0; x < 0xfff0; x++) {
		if(!sequence)
			break;
	}
#ifdef	POWER_PORT
	POWER_PORT |= 1;
#else
	P3 |= 0x80;		/* make sure power is off */
#endif
	disable();
	clrbit(ET0)
	TR1OFF
	clrbit(TR0)

	serinit(baud);
	xdelay(10);
	return(sequence);
}

PrintResult(which)
	char  *which;
{
	if(!which) {
		printf("Source\tOsc1\t\tOsc2\t\tNo Osc\n");
		return;
	}
	printf("%s\t(16*%u)+%u\t(16*%u)+%u\t(16*%u)+%u\n",
		which, reading1_high, reading1_low,
		reading2_high, reading2_low,
		reading3_high, reading3_low);

}

xdelay(count)
	unsigned int count;
{

	unsigned char a;
	unsigned int x;

	while(count--) {
		asm " nop";
	}
}

main()
{
	unsigned char	str[24];
	char len;
	unsigned int	x;
	char	*arg,*arg2;
 	unsigned char y;

	osc_warmup_delay = 10;
	powerup_delay = 10;
	reading_time = 250;	
	msec_reload = 614;

	serinit(baud);
	disable();

	printf("%s\n",copyright);

	while(1) {
		printf("> ");
		len = getstr(str, sizeof(str));
		if(!len)
			goto usage;

		arg = &str[1];
		arg2 = strchr(str,'=');
		if(arg2)
			arg2++;

		while(*arg && *arg == ' ')
			arg++;

		if(!*arg && len > 1 && !arg2) {
			printf("E:arg required\n");
			continue;
		}
		switch(str[0]) {
			case '0' :
				if(!arg2)
					printf("P0=%u\n",y = P0);
				else {
					P0 = atoi(arg2);
					printf("P0 set to %u, equals %u\n",atoi(arg2), y=P0);
				}
				break;
			case '1' :
				if(!arg2)
					printf("P1=%u\n",y = P1);
				else {
					P1 = atoi(arg2);
					printf("P1 set to %u, equals %u\n",atoi(arg2), y=P1);
				}
				break;
			case '2' :
				if(!arg2)
					printf("P2=%u\n",y = P2);
				else {
					P2 = atoi(arg2);
					printf("P2 set to %u, equals %u\n",atoi(arg2), y=P2);
				}
				break;

			case '3' :
				if(!arg2)
					printf("P3=%u\n",y = P3);
				else {
					P3 = atoi(arg2);
					printf("P3 set to %u, equals %u\n",atoi(arg2), y=P3);
				}
				break;
			case 'i' :
			case 'I' :
				printf("powerup %u\nosc warmup %u\nreading %u\n",
					powerup_delay, osc_warmup_delay, reading_time);
				break;
			case 'o' :
			case 'O' :
				x = atoi(arg);
				osc_warmup_delay = x;
				if(str[0] == 'o')
					printf("I:osc warmup set to %u\n",osc_warmup_delay);
				break;
			case 'p' :
			case 'P' :
				x = atoi(arg);
				powerup_delay = x;
				if(str[0] == 'p')
					printf("I:power delay set to %u\n",powerup_delay);
				break;
			case 'r' :
			case 'R' :
				x = atoi(arg);
				reading_time = x;
				if(str[0] == 'r')
					printf("I:reading time set to %u\n",reading_time);
				break;

			case 'x':
			case 'X' :
				P2 |= PROBE_NOT_REF;
				xdelay(10);
				x = Take_Reading();
				if(x) {
					printf("E:System timed out\n");
					break;
				}
				PrintResult(0);
				PrintResult("Probe");
				P2 &= ~PROBE_NOT_REF;
				x = Take_Reading();
				if(x) {
					printf("E:System timed out\n");
					break;
				}
				PrintResult("Ref.  ");
				break;
			default:;
usage:;
				printf("i info\no osc_warmup_time\np powerup_time\nr reading_time\nx take reading\n");
				printf("Read or Set ports...\n0 [=val]\n1 [=val]\n2 [=val]\n3 [=val]\n");
				break;
		}
	}
}

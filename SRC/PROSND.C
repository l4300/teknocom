/* prosnd.c - Sound and LED goodies */

asm {		/* timer 1 interrupt */
$SE:0
_/**/_TF1_	/* This label will be the address of the "prefix" stub */
		org		INTBASE+_TF1_			/* Position to interrupt vector */
		PUSH	PSW			/* Save Processor Status Word */
		PUSH	A			/* Save accumulator (2 bytes) */
		LJMP	_/**/_TF1_	/* Don't overwrite next vector (+3 = 7) */
		ORG		_/**/_TF1_	/* Position back to original PC */
		/* auto reload, use sound_shift */
		PUSH	B
		MOV	A,sound_shift
        SWAP A
		MOV	sound_shift,A
        MOV B,A
        MOV A,P3
        ANL A,#~(64|4)		/* initially, clear them */
        JNB B.0,?nored
		ORL A,#4
?nored
		JNB B.1,?nogreen
        ORL A,#64
?nogreen
        MOV P3,A
		POP	B
        MOV A,gsZ
        MOV TL1,A
        MOV A,gsZ+1
        MOV TH1,A
		POP		A			/* Restore A accumulator */
		POP		PSW			/* Restore Processor Status Word */
		RETI				/* End interrupt */
}



unsigned int saveZ;

int
MakeSound(len, freq)
	int	len;
	unsigned int	freq;	/* freq in 10'ths of hertz, ie 300hz = 30 */
{
//	sound_shift = gsE; /* 0x55; */	/* 0xb4; */

	disable();
	clrbit(ET1);
	clrbit(TR1);
	TMOD = (TMOD & 0x0f) | 0x10;	/* 16 bit timer no prescale */
    saveZ = gsZ;
#ifdef	DS15MHZ
	gsZ	= 65535 - (62490/freq);
#else
	gsZ  = 65535 - (30703/freq);
#endif

	TL1 = gsZ & 0xff;
    TH1 = gsZ >> 8;
	setbit(TR1);
	setbit(ET1);			/* enable T1 */
	enable();
	while(len-- > 0) {
		IDLE();
	}
	clrbit(TR1);
	clrbit(ET1)
    gsZ = saveZ;
	LED_OFF;

/*	P3 = P3 & ~SOUND_MASK; */
}


Sound(which)
	int	which;
{
	ClockFast();
	SerialStop();

	if(which & SOUND_LED) 
		clrbit(bitNoLed)
	else
		setbit(bitNoLed)

	which &= 15;
	sound_shift = 0x30;
    error = P3;
/*	if(Sounds[which].red)
		LED_RED; */

/*	if(which == SOUND_READING) {
	  	MakeSound(300,130);
	} else */
	  asm {
		mov	R0,#-5
		lcall	?auto0
		mov	A,[R0]
		mov	B,A
		rl	A
		rl	A
		add	A,B
		add	A,#Sounds
		mov	DPL,A
		mov	A,#=Sounds
		mov	DPH,A
		movx	A,[DPTR]	get f1
		mov	R7,A
		inc	DPTR
		movx	A,[DPTR]	get l1
		mov	R6,A
		inc	DPTR
		movx	A,[DPTR]	get f2
        push A				******     PUSH HERE   ******
		inc	DPTR
		movx	A,[DPTR]	get l2
		mov	fpacc2ex,A
		inc	DPTR
		movx	A,[DPTR]
        jb bitNoLed,?doneled
		jz	?greenledsnd
        mov A,sound_shift
        orl A,#2			turn off green led
        mov sound_shift,A
        mov A,error
        ANL A,#~4			turn on red led
        mov P3,A
        mov error,A
        sjmp ?doneled
?greenledsnd
        mov A,sound_shift
        orl A,#1			turn off red led
        mov sound_shift,A
        mov A,error
        ANL A,#~64			turn on green led
        mov P3,A
        mov error,A

?doneled
		mov	A,R7
		push	A
		CLR	A
		push	A
		mov	A,R6
		push	A
		clr	A
		push	A
		lcall	MakeSound

        mov A,error
        mov P3,A
		dec	SP
		dec	SP
		dec	SP
		dec	SP
		mov	A,fpacc2ex
		jz	?no2ndsnd
		mov     R0,#50
		orl     PCON,#1
		djnz    R0,*-3

        		* NOTE, F2 Still on Stack here
		clr	A
		push	A
		mov	A,fpacc2ex
		push	A
		clr	A
		push	A
		lcall	MakeSound

		dec	SP
		dec	SP
		dec	SP
?no2ndsnd
		dec	SP			** POPS A PUSHED ABOVE

        mov A,error
        mov P3,A
}
#ifdef	old
	if (*CharP((gsX+4)))
		LED_RED;

	MakeSound(*CharP(gsX),*CharP((gsX+1)));
	gsX += 2;
	if(*CharP(gsX))
		MakeSound(*CharP(gsX),*CharP((gsX+1)));

	MakeSound(Sounds[gsA].f1,Sounds[gsA].l1);
	if(Sounds[gsA].f2)
		MakeSound(Sounds[gsA].f2,Sounds[gsA].l2);
#endif
 	ClockSlow();
    setbit(bitNoLed);	/* THIS DISABLES LED after beep  2/20/98 against my better wishes. */
 	if(tstbit(CbitNoLed)) {
		LED_OFF;
		sound_shift = 0;
	} else {
/*		sound_shift = (ticks & 0xff) + 7;  */
		sound_shift = 7;
/*		sound_shift = ticks - 1; */
		setbit(bitLedTimeout)
	}
	clrbit(bitNoLed)
	SerialInit();
}

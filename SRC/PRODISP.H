#ifndef	PRODISP_H
#define	PRODISP_H

#define	SRF_MEMORYMODE	0x01	/* we're currently in memory mode */
#define	SRF_CAMMODE		0x02
#define	SRF_LIMITMODE		0x04
#define	SRF_CALMODE		0x08	/* calibrated ferrous */
#define	SRF_LOWBAT		0x10	/* low battery */
#define	SRF_OZUNITS		0x20
#define	SRF_MICRONS		0x40
#define	SRF_MILS		0x80

#define	SRF_UNITS		(SRF_OZUNITS|SRF_MICRONS|SRF_MILS)

#define	SRF_MEMORYLOW		0x0100	/* low memory */
#define	SRF_NOTCAMLAST	0x0200	/* haven't taken last cam */
#define	SRF_LIMITERROR	0x0400	/* out of range value */
#define	SRF_CALMODE2		0x0800	/* was 2 pt cal, now cal nonferrous */
#define	SRF_DONE		0x8000	/* done updating */


/* these defs are in srfFlags2 for EMR mode */
#define	SRF2_EMRON		0x0001	/* emr mode is on */
#define	SRF2_EMRE		0x0002	/* we're taking the e-mode readings */

#define	BITS_BLOCK1		0x0001	/* block 1 on */
#define	BBITS_BLOCK1		0x0100	/* blink block 1 */
#define	BITS_STAT		0x0002
#define	BBITS_STAT		0x0200
#define	BITS_NONFERROUS	0x000C	/* 4 + 8 */
#define	BBITS_NONFERROUS	0x0C00
#define	BITS_FERROUS		0x0008
#define	BBITS_FERROUS		0x0800
#define	BITS_BLOCK2		0x0080
#define	BBITS_BLOCK2		0x8000
#define	BBITS_PROBETYPE	(BITS_NONFERROUS|BBITS_NONFERROUS)

#define	setBBit(a)	(bottomBits |= a)
#define	clrBBit(a)	(bottomBits &= ~(a))

#endif

 * --------- Start of Menu RootMenu -----------*
 * --------- Start of Menu CalibrateMenu -----------*
 org $52a
 DRW $500
 STRZ 'e Cal'
 org $532
 DRW $a
 STRZ '2 PTS'
 org $53a
 DRW $b
 STRZ '1 PT'
 org $541
 DRW $c
 STRZ 'Factory'
 org $54b
 DW 0
 * ------  End of Menu --------- *
 org $500
 DRW $52a
 STRZ 'Cal'
 org $506
 DRW $d
 STRZ 'EOM'
 org $50c
 DRW $e
 STRZ 'CAM'
 * --------- Start of Menu UnitsMenu -----------*
 org $54d
 DRW $500
 STRZ 'e UNITS'
 org $557
 DRW $f
 STRZ 'Mils'
 org $55e
 DRW $10
 STRZ 'Microns'
 org $568
 DW 0
 * ------  End of Menu --------- *
 org $512
 DRW $54d
 STRZ 'UNITS'
 * --------- Start of Menu ConfigMenu1E -----------*
 org $56a
 DRW $500
 STRZ 'e CNF'
 org $572
 DRW $11
 STRZ 'Factory'
 org $57c
 DRW $12
 STRZ 'Serial Number'
 org $58c
 DRW $13
 STRZ 'Version'
 org $596
 DW 0
 * ------  End of Menu --------- *
 org $51a
 DRW $56a
 STRZ 'CONFIG'
 org $523
 DRW $ffff
 STRZ 'e '
 org $528
 DW 0
 * ------  End of Menu --------- *
 * --------- Start of Menu SysMenu -----------*
 org $598
 DRW $14
 STRZ 'Flags'
 org $5a0
 DRW $15
 STRZ 'SNumber'
 org $5aa
 DRW $16
 STRZ 'Ferrous'
 org $5b4
 DRW $17
 STRZ 'NonFerrous'
 org $5c1
 DRW $18
 STRZ 'Bench'
 org $5c9
 DRW $500
 STRZ 'Exit'
 org $5d0
 DW 0
 * ------  End of Menu --------- *
 org $5d2
 STRZ ' ()*+-/0123456789<=>@ABCDEFGHIJKLMNOPQRSTUVWXYZ'
 org $602
 DB $2f
 DB $0
 org $604
 STRZ '0123456789'
 org $60f
 DB $a
 DB $0
 org $611
 STRZ 'WAIT'
 org $617
 STRZ 'OFF'
 org $61b
 STRZ 'EXIT'
 org $620
 STRZ 'CAM %s'
 org $627
 STRZ 'EOM %s'
 org $62e
 STRZ 'ALL'
 org $632
 STRZ 'OFF'
 org $636
 STRZ 'NONE'
 org $63b
 STRZ '%c%c'
 org $640
 STRZ 'COMPLETE'
 org $649
 STRZ 'ROWS'
 org $64e
 STRZ 'COLS'
 org $653
 STRZ 'Used %u of %u'
 org $661
 STRZ '%c %s %d Readings %d Rows %d Cols'
 org $683
 STRZ 'LOWL'
 org $688
 STRZ 'HI L'
 org $68d
 STRZ '00 PLATE'
 org $697
 STRZ 'MORE/SEL'
 org $6a0
 STRZ 'W/ SHIM'
 org $6a9
 STRZ 'ADJC'
 org $6ae
 STRZ 'BATCH FULL'
 org $6b9
 STRZ '%s-'
 org $6bd
 STRZ ' %c%d  '
 org $6c5
 STRZ '%d   '
 org $6cb
 STRZ '�%d   '
 org $6d2
 STRZ '�R%d   '
 org $6da
 STRZ 'MEM FULL'
 org $6e3
 STRZ 'TEMPLATE'
 org $6ec
 STRZ 'NONE/EXIT'
 org $6f6
 STRZ 'Touch'
 org $6fd
 STRZ 'Too Small'
 org $707
 STRZ 'Lift'
 org $70d
 STRZ 'DONE'
 org $713
 STRZ 'Lift Probe'
 org $71f
 STRZ 'EOM '
 org $724
 STRZ 'M DONE'
 org $72b
 STRZ 'MESH %s'
 org $733
 STRZ 'SHIM,MESH'
 org $73d
 STRZ 'CAL '
 org $742
 STRZ 'MIN '
 org $747
 STRZ 'MAX '
 org $74c
 DB $40
 DB $80
 DB $0
 DB $0
 org $750
 DB $43
 DB $c8
 DB $0
 DB $0
 org $754
 DB $41
 DB $80
 DB $0
 DB $0
 org $758
 DB $42
 DB $a0
 DB $0
 DB $0
 org $75c
 DB $41
 DB $22
 DB $8f
 DB $5e
 org $760
 DB $40
 DB $18
 DB $2c
 DB $b8
 org $764
 STRZ 'FERROUS,NONFERROUS,BOTH,EXIT'
 org $781
 STRZ 'FERROUS,EXIT'
 org $78e
 STRZ 'NONFERROUS,EXIT'
 org $79e
 DB $a0
 DB $8a
 DB $80
 DB $80
 DB $80
 DB $45
 DB $a0
 DB $80
 DB $80
 DB $80
 DB $85
 DB $20
 DB $0
 org $7ab
 DB $a0
 DB $8a
 DB $80
 DB $80
 DB $80
 DB $4d
 DB $a0
 DB $80
 DB $80
 DB $80
 DB $85
 DB $20
 DB $0
 org $7b8
 DB $a0
 DB $8a
 DB $80
 DB $80
 DB $80
 DB $58
 DB $a0
 DB $80
 DB $80
 DB $80
 DB $85
 DB $20
 DB $0
 org $7c5
 STRZ 'NF,Z,A,I,T,EXIT'
 org $7d5
 STRZ 'FLG'
 org $7d9
 STRZ 'SNUM'
 org $bd0
 DB $96
 DB $78
 DB $96
 DB $78
 DB $1
 DB $64
 DB $b4
 DB $0
 DB $0
 DB $0
 DB $32
 DB $14
 DB $32
 DB $14
 DB $0
 DB $fa
 DB $1e
 DB $fa
 DB $1e
 DB $1
 DB $64
 DB $46
 DB $64
 DB $46
 DB $0
 DB $fa
 DB $46
 DB $fa
 DB $46
 DB $0
 DB $fa
 DB $b4
 DB $fa
 DB $b4
 DB $0
 DB $fa
 DB $14
 DB $fa
 DB $28
 DB $1
 DB $b
 DB $b4
 DB $0
 DB $0
 DB $0

 org  &H7524
 dw  &H7500
 strz  "e Cal"
 org  &H752c
 dw  &Ha
 strz  "2 PTS"
 org  &H7534
 dw  &Hb
 strz  "1 PT"
 org  &H753b
 dw  &Hc
 strz  "Factory"
 org  &H7545
 dw  0
 org  &H7500
 dw  &H7524
 strz  "Cal"
 org  &H7506
 dw  &Hd
 strz  "CAM"
 org  &H7547
 dw  &H7500
 strz  "e UNITS"
 org  &H7551
 dw  &He
 strz  "Mils"
 org  &H7558
 dw  &Hf
 strz  "Microns"
 org  &H7562
 dw  0
 org  &H750c
 dw  &H7547
 strz  "UNITS"
 org  &H7564
 dw  &H7500
 strz  "e CNF"
 org  &H756c
 dw  &H10
 strz  "Factory"
 org  &H7576
 dw  &H11
 strz  "Serial Number"
 org  &H7586
 dw  &H12
 strz  "Version"
 org  &H7590
 dw  0
 org  &H7514
 dw  &H7564
 strz  "CONFIG"
 org  &H751d
 dw  &Hffff
 strz  "e "
 org  &H7522
 dw  0
 org  &H7592
 dw  &H13
 strz  "Flags"
 org  &H759a
 dw  &H14
 strz  "SNumber"
 org  &H75a4
 dw  &H15
 strz  "Ferrous"
 org  &H75ae
 dw  &H16
 strz  "NonFerrous"
 org  &H75bb
 dw  &H17
 strz  "Bench"
 org  &H75c3
 dw  &H7500
 strz  "Exit"
 org  &H75ca
 dw  0
 org  &H75cc
 strz  " '()*+-/0123456789<=>@ABCDEFGHIJKLMNOPQRSTUVWXYZ"
 org  &H75fd
 db  &H30
 db  &H0
 org  &H75ff
 strz  "0123456789"
 org  &H760a
 db  &Ha
 db  &H0
 org  &H760c
 strz  "WAIT"
 org  &H7612
 strz  "OFF"
 org  &H7616
 strz  "EXIT"
 org  &H761b
 strz  "CAM %s"
 org  &H7622
 strz  "EOM %s"
 org  &H7629
 strz  "ALL"
 org  &H762d
 strz  "OFF"
 org  &H7631
 strz  "NONE"
 org  &H7636
 strz  "%c%c"
 org  &H763b
 strz  "COMPLETE"
 org  &H7644
 strz  "ROWS"
 org  &H7649
 strz  "COLS"
 org  &H764e
 strz  "Used %u of %u"
 org  &H765c
 strz  "%c %s %d Readings %d Rows %d Cols"
 org  &H767e
 strz  "LOWL"
 org  &H7683
 strz  "HI L"
 org  &H7688
 strz  "00 PLATE"
 org  &H7692
 strz  "MORE/SEL"
 org  &H769b
 strz  "W/ SHIM"
 org  &H76a4
 strz  "ADJC"
 org  &H76a9
 strz  "BATCH FULL"
 org  &H76b4
 strz  "%s-"
 org  &H76b8
 strz  " %c%d  "
 org  &H76c0
 strz  "%d   "
 org  &H76c6
 strz  "�%d   "
 org  &H76cd
 strz  "�R%d   "
 org  &H76d5
 strz  "MEM FULL"
 org  &H76de
 strz  "TEMPLATE"
 org  &H76e7
 strz  "NONE/EXIT"
 org  &H76f1
 strz  "Touch"
 org  &H76f8
 strz  "Too Small"
 org  &H7702
 strz  "Lift"
 org  &H7708
 strz  "DONE"
 org  &H770e
 strz  "Lift Probe"
 org  &H771a
 strz  "CAL "
 org  &H771f
 strz  "MIN "
 org  &H7724
 strz  "MAX "
 org  &H7729
 db  &H40
 db  &H80
 db  &H0
 db  &H0
 org  &H772d
 db  &H43
 db  &Hc8
 db  &H0
 db  &H0
 org  &H7731
 db  &H41
 db  &H80
 db  &H0
 db  &H0
 org  &H7735
 db  &H42
 db  &Ha0
 db  &H0
 db  &H0
 org  &H7739
 db  &H41
 db  &H22
 db  &H8f
 db  &H5e
 org  &H773d
 db  &H40
 db  &H18
 db  &H2c
 db  &Hb8
 org  &H7741
 strz  "FLG"
 org  &H7745
 strz  "SNUM"
 org  &H774a
 strz  "FERROUS NONFERROUS BOTH EXIT"
 org  &H7767
 strz  "FERROUS EXIT"
 org  &H7774
 strz  "NONFERROUS EXIT"
 org  &H7784
 strz  "NF Z A I T EXIT"
 org  &H7794
 db  &Ha0
 db  &H8a
 db  &H80
 db  &H80
 db  &H80
 db  &H45
 db  &Ha0
 db  &H80
 db  &H80
 db  &H80
 db  &H85
 db  &H20
 db  &H0
 org  &H77a1
 db  &Ha0
 db  &H8a
 db  &H80
 db  &H80
 db  &H80
 db  &H4d
 db  &Ha0
 db  &H80
 db  &H80
 db  &H80
 db  &H85
 db  &H20
 db  &H0
 org  &H77ae
 db  &Ha0
 db  &H8a
 db  &H80
 db  &H80
 db  &H80
 db  &H58
 db  &Ha0
 db  &H80
 db  &H80
 db  &H80
 db  &H85
 db  &H20
 db  &H0
 org  &H7bd0
 db  &H96
 db  &H78
 db  &H96
 db  &H78
 db  &H1
 db  &H64
 db  &Hb4
 db  &H0
 db  &H0
 db  &H0
 db  &H32
 db  &H14
 db  &H32
 db  &H14
 db  &H0
 db  &Hfa
 db  &H1e
 db  &Hfa
 db  &H1e
 db  &H1
 db  &H64
 db  &H46
 db  &H64
 db  &H46
 db  &H0
 db  &Hfa
 db  &H46
 db  &Hfa
 db  &H46
 db  &H0
 db  &Hfa
 db  &Hb4
 db  &Hfa
 db  &Hb4
 db  &H0
 db  &Hfa
 db  &H14
 db  &Hfa
 db  &H28
 db  &H1
 db  &Hb
 db  &Hb4
 db  &H0
 db  &H0
 db  &H0

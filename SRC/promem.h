#ifndef	PROMEM_H
#define	PROMEM_H	1

#define	PMF_ALL	1		/* show ALL as a selection */
#define	PMF_OFF	2		/* show OFF as a selection */
#define	PMF_TAG	4		/* show tags info */

struct	_float {
	unsigned char	fval[4];
};

struct	_floatConstants {
	struct _float	fcVals[6];			/* 6 floating point constants */
};

/* stat function stores values in fArray as follows */
#define	FASTAT_MIN	0
#define	FASTAT_MAX	1
#define	FASTAT_AVG	2
#define	FASTAT_DEV	3	/* these four values are stored in standard mils */
#define	FASTAT_CNT	4	/* actual number of readings */

#define	GSTR_SUMX	32	/* offset in gString for temp values */
#define	GSTR_SUMSQUARE	GSTR_SUMX+sizeof(_float)



/* define flag bits for scFlags below */
#define	SCFLG_NOFERROUS		0x0001		/* does not have a ferrous probe */
#define	SCFLG_NONONFERROUS		0x0002		/* does not have a non-ferrous probe */
#define	SCFLG_SHOWNEGATIVE		0x0004		/* show negative values */
#define	SCFLG_EXTENDEDRANGE		0x0008		/* go to 2000 microns or 120 mils */
#define    SCFLG_LONGFERROUS       0x0010      /* take longer ferrous reading 4* longer */

#define	SYSVERSION	0x15				/* version of system constants for autospec */

struct	_sysConstants {					/* system-wide constants */
	unsigned int	scCsum;				/* csum of the system area */
/*2*/	unsigned int	scStartMemBlocks;		/* where memblocks start */
/*4*/	unsigned int	scEndMemBlocks;			/* no mem blocks => to this address */
/*6*/	unsigned int	scSerialNumber;			/* system serial number */
/*8*/	unsigned int	scFlags;			/* flag bits */
/*10*/	unsigned int	scNFZero;			/* factory Zero Value */
/*12*/	unsigned int	scNFInfinity;			/* factory Infinity */
/*14*/	unsigned int	scNFMaxInfinity;		/* maximum infinity value */
/*16*/	unsigned int	scNFMaxCalVariation;		/* max variance in a calibration */
/*18*/	unsigned int	scNFStableReading;		/* count less than this is stable */
/*20*/	unsigned int	scFnInfMax;
/*22*/	unsigned int	scFnInfRead;
/*24*/	struct _floatConstants scNFConstants;		/* non-ferrous constants for ffunction */
/*48*/	struct _floatConstants scFConstants;		/* ferrous constants for ffunction */
/*72*/	struct _float	scFiTempRef;			/* ferrous temp ref */
/*76*/	struct _float	scFiInternalRef;
/*80*/	struct _float	scFdeltafActualRef;
/*84*/	struct _float	scFdeltaiActualRef0;
/*88*/	struct _float	scFdeltaiTempRef;
/*92*/	struct _float	scFfCofRef;
/*96*/	struct _float	scFfMnRef;
/*100*/	unsigned int	scFZeroOffset;		/* add this to the zero reading */
/*102*/	unsigned int	scFStableReading;	/* Actual Variation less than this for stable reading */
};

struct	_userConstants {				/* user constants change all the time */
	unsigned int 	ucCsum;
	unsigned int	ucShutdownTimeout;		/* time to shutdown in ticks */
	struct _float	ucFdeltafTemp;
	struct _float	ucFCalRatio;			/* calibrated ratio */
	struct _float ucNFCalRatio;			/* non ferrous calibration */
	struct _float	ucCalibrateSum;
	/* constants after here are not csum'd	*/
	unsigned int	srfFlags;			/* units, modes, etc for prodisp */
	unsigned int	srfFlags2;			/* units, modes, etc for prodisp */
#ifdef	DOEMR
	unsigned int	ucEmrE;
	unsigned int	ucEmrM;
	unsigned int	ucEmr;				/* the calculated emr value */
#endif
	unsigned int	ucCalibration;			/* calibration value */
	unsigned int	ucFCalibration;		/* ferrous calibration */
	struct _float	ucCamSum;       		/* current sum */
	unsigned int	ucCamCount;			/* # cam readings thus far */
	unsigned int	ucCamLimit;			/* # cam readings to take */
	unsigned int	ucLimitLow;			/* low reading limit */
	unsigned int	ucLimitHigh;			/* high reading limit */
	unsigned int	ucActiveMemorySlot;		/* current memory slot */
    unsigned int 	ucPrinterOption;	/* the saved printer configuration option */
};

/* ucPrinterOption is are these bits */
#define	POPT_GRAPHICS		0x01
#define	POPT_COURSE		0x02
#define	POPT_WIDE		0x04
#define	POPT_STATS		0x08
#define	POPT_DATA		0x10
#define	POPT_DENSITY		0x20	// print density plot
#define	POPT_DEFAULT	(POPT_STATS|POPT_DATA)

#define	DBMAXVALUES	15
#ifdef	DS2252
#define	MAXBLOCKS	1814
#else
#define	MAXBLOCKS	790
#endif
/* datablocks base 0x1d00 */

#define	MAXSLOTS        26
#define	MAXTEMPLATES	16

/* #define	MAXCOLDESCRIP	15
#define	MAXROWDESCRIP	7 */

struct	_datablock {
	struct _datablock *dbNext;
	unsigned int	dbValues[DBMAXVALUES];
};


struct	_subBatchInfo {
/*0*/	unsigned int  sbEntryCount;	/* total number of readings in this batch */
/*2*/	unsigned char	sbFlags;	/* the user flags */
/*3*/	unsigned char	sbTemplate;
/*4*/	unsigned char	sbRows;
/*5*/	unsigned char	sbColumns;
/*6*/	unsigned char	sbCamMode;
/*7*/	unsigned char	sbSpare;
/*8*/	unsigned int	sbLimitLow;	/* low limit */
/*10*/	unsigned int	sbLimitHigh;
/*12*/	unsigned char	sbDescription[8];
/*20*/	unsigned int	sbTimeStart[2];
/*24*/	unsigned int	sbTimeEnd[2];
/*28*/
};

struct _tmemHeader {		/* touch memory header */
	unsigned char	tmLength;	/* length in bytes of this structure */
	unsigned char tmScratch;
	struct _subBatchInfo	tmInfo;	/* 2 length 28 */
/*30*/	unsigned int	tmcrc16;
};

struct	_tmemData	{	/* touch memory datablock */
	unsigned char	tdLength;
	unsigned char	tdScratch;
	unsigned int	tdData[14];
	unsigned int	tdcrc16;
};

struct	_batchInfo {
	struct	_subBatchInfo	biInfo;			/* len 28 */
	unsigned int		biEntryPointer;         /* 28 the current item */
	unsigned int		biFlags;                /* 30 */
	struct _datablock 	*biDataBlockAddress;	/* 32 */
	struct _datablock 	*biBatchDescriptions;	/* 34 answered description block */
/* 36 */
};

struct	_templateInfo {
	unsigned char	tiRows;			/* 0 number of rows */
	unsigned char	tiColumns;		/* 1 number of columns, 0 no limit */
	unsigned char	tiCamMode;		/* 2 non-zero cam mode */
	unsigned char	tiSpare;		/* 3 */
	unsigned int	tiLimitLow;		/* 4 low limit */
	unsigned int	tiLimitHigh;		/* 6 high limit */
	unsigned char	tiName[16];		/* 8 name of template */
	struct _datablock *tiRowBlockAddress;	/* 24 datablocks with row descriptors */
	struct _datablock *tiColumnBlockAddress; /* 26 datablocks with column descriptors */
	struct _datablock *tiBlockDescriptions;	/* 28 general user blocks to fill in */
};

extern struct _sysConstants	SysConstants;
extern struct _userConstants	UserConstants;
extern struct _batchInfo        Batches[MAXSLOTS];
extern struct _datablock	DataBlocks[MAXBLOCKS];
extern struct _templateInfo	Templates[MAXTEMPLATES];
extern char 	RAMSTART;
#endif

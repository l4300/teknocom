 * --------- Start of Menu RootMenu -----------*
 * --------- Start of Menu CalibrateMenu -----------*
 org $52f
 DRW $500
 STRZ 'e Cal'
 org $537
 DRW $a
 STRZ '2 PTS'
 org $53f
 DRW $b
 STRZ '1 PT'
 org $546
 DRW $c
 STRZ 'Factory'
 org $550
 DW 0
 * ------  End of Menu --------- *
 org $500
 DRW $52f
 STRZ 'Cal 1-2'
 org $50a
 DRW $d
 STRZ 'EOM'
 org $510
 DRW $e
 STRZ 'MESH'
 * --------- Start of Menu UnitsMenu -----------*
 org $552
 DRW $500
 STRZ 'e UNITS'
 org $55c
 DRW $f
 STRZ 'Mils'
 org $563
 DRW $10
 STRZ 'Microns'
 org $56d
 DW 0
 * ------  End of Menu --------- *
 org $517
 DRW $552
 STRZ 'UNITS'
 * --------- Start of Menu ConfigMenu1E -----------*
 org $56f
 DRW $500
 STRZ 'e CNF'
 org $577
 DRW $11
 STRZ 'Factory'
 org $581
 DRW $12
 STRZ 'Serial Number'
 org $591
 DRW $13
 STRZ 'Version'
 org $59b
 DW 0
 * ------  End of Menu --------- *
 org $51f
 DRW $56f
 STRZ 'CONFIG'
 org $528
 DRW $ffff
 STRZ 'e '
 org $52d
 DW 0
 * ------  End of Menu --------- *
 * --------- Start of Menu SysMenu -----------*
 org $59d
 DRW $14
 STRZ 'Flags'
 org $5a5
 DRW $15
 STRZ 'SNumber'
 org $5af
 DRW $16
 STRZ 'Ferrous'
 org $5b9
 DRW $17
 STRZ 'NonFerrous'
 org $5c6
 DRW $18
 STRZ 'Bench'
 org $5ce
 DRW $500
 STRZ 'Exit'
 org $5d5
 DW 0
 * ------  End of Menu --------- *
 org $5d7
 STRZ ' ()*+-/0123456789<=>@ABCDEFGHIJKLMNOPQRSTUVWXYZ'
 org $607
 DB $2f
 DB $0
 org $609
 STRZ '0123456789'
 org $614
 DB $a
 DB $0
 org $616
 STRZ 'WAIT'
 org $61c
 STRZ 'OFF'
 org $620
 STRZ 'EXIT'
 org $625
 STRZ 'CAM %s'
 org $62c
 STRZ 'EOM %s'
 org $633
 STRZ 'ALL'
 org $637
 STRZ 'OFF'
 org $63b
 STRZ 'NONE'
 org $640
 STRZ '%c%c'
 org $645
 STRZ 'COMPLETE'
 org $64e
 STRZ 'ROWS'
 org $653
 STRZ 'COLS'
 org $658
 STRZ 'Used %u of %u'
 org $666
 STRZ '%c %s %d Readings %d Rows %d Cols'
 org $688
 STRZ 'LOWL'
 org $68d
 STRZ 'HI L'
 org $692
 STRZ '00 PLATE'
 org $69c
 STRZ 'MORE/SEL'
 org $6a5
 STRZ 'W/ SHIM'
 org $6ae
 STRZ 'ADJC'
 org $6b3
 STRZ 'BATCH FULL'
 org $6be
 STRZ '%s-'
 org $6c2
 STRZ ' %c%d  '
 org $6ca
 STRZ '%d   '
 org $6d0
 STRZ '�%d   '
 org $6d7
 STRZ '�R%d   '
 org $6df
 STRZ 'MEM FULL'
 org $6e8
 STRZ 'TEMPLATE'
 org $6f1
 STRZ 'NONE/EXIT'
 org $6fb
 STRZ 'Touch'
 org $702
 STRZ 'Too Small'
 org $70c
 STRZ 'Lift'
 org $712
 STRZ 'DONE'
 org $718
 STRZ 'Lift Probe'
 org $724
 STRZ 'EOM '
 org $729
 STRZ 'M DONE'
 org $730
 STRZ 'MESH %s'
 org $738
 STRZ 'SHIM,MESH'
 org $742
 STRZ 'CAL '
 org $747
 STRZ 'MIN '
 org $74c
 STRZ 'MAX '
 org $751
 DB $40
 DB $80
 DB $0
 DB $0
 org $755
 DB $43
 DB $c8
 DB $0
 DB $0
 org $759
 DB $41
 DB $80
 DB $0
 DB $0
 org $75d
 DB $42
 DB $a0
 DB $0
 DB $0
 org $761
 DB $41
 DB $22
 DB $8f
 DB $5e
 org $765
 DB $40
 DB $18
 DB $2c
 DB $b8
 org $769
 STRZ 'FERROUS,NONFERROUS,BOTH,EXIT'
 org $786
 STRZ 'FERROUS,EXIT'
 org $793
 STRZ 'NONFERROUS,EXIT'
 org $7a3
 DB $a0
 DB $8a
 DB $80
 DB $80
 DB $80
 DB $45
 DB $a0
 DB $80
 DB $80
 DB $80
 DB $85
 DB $20
 DB $0
 org $7b0
 DB $a0
 DB $8a
 DB $80
 DB $80
 DB $80
 DB $4d
 DB $a0
 DB $80
 DB $80
 DB $80
 DB $85
 DB $20
 DB $0
 org $7bd
 DB $a0
 DB $8a
 DB $80
 DB $80
 DB $80
 DB $58
 DB $a0
 DB $80
 DB $80
 DB $80
 DB $85
 DB $20
 DB $0
 org $7ca
 STRZ 'NF,Z,A,I,T,EXIT'
 org $7da
 STRZ 'FLG'
 org $7de
 STRZ 'SNUM'
 org $bd0
 DB $96
 DB $78
 DB $96
 DB $78
 DB $1
 DB $64
 DB $b4
 DB $0
 DB $0
 DB $0
 DB $32
 DB $14
 DB $32
 DB $14
 DB $0
 DB $fa
 DB $1e
 DB $fa
 DB $1e
 DB $1
 DB $64
 DB $46
 DB $64
 DB $46
 DB $0
 DB $fa
 DB $46
 DB $fa
 DB $46
 DB $0
 DB $fa
 DB $b4
 DB $fa
 DB $b4
 DB $0
 DB $fa
 DB $14
 DB $fa
 DB $28
 DB $1
 DB $b
 DB $b4
 DB $0
 DB $0
 DB $0
